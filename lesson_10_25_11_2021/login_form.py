from lesson_10_25_11_2021.common_page import CommonPage


class LoginForm(CommonPage):
    def __init__(self, url, color,close_icon):
        super().__init__(url, color)
        self.close_icon = close_icon

    def enter_email(self, email):
        print(f"Enter {email}")

    def click_enter(self):
        print("Click enter")