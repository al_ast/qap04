class BasketPage:
    def make_order(self, email, mobile):
        self.click_make_order()
        self.enter_required_information(email, mobile)
        self.click_confirm()

    def click_make_order(self):
        print("Click make order")

    def enter_email(self, email):
        print(f"enter email:{email}")

    def enter_mobile_number(self, mobile):
        print(f"Enter mobile:{mobile}")

    def enter_required_information(self, email, mobile):
        self.enter_email(email)
        self.enter_mobile_number(mobile)
        # add other required fields here

    def click_confirm(self):
        print("Click confirm order")
