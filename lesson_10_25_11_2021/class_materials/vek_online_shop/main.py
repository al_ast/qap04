from lesson_10_25_11_2021.class_materials.vek_online_shop.account_form import AccountForm
from lesson_10_25_11_2021.class_materials.vek_online_shop.basket_page import BasketPage
from lesson_10_25_11_2021.class_materials.vek_online_shop.catalog_page import CatalogPage
from lesson_10_25_11_2021.class_materials.vek_online_shop.login_page import LoginPage
from lesson_10_25_11_2021.class_materials.vek_online_shop.main_page import MainPage

main_page = MainPage()
main_page.click_account()

account_form = AccountForm()
account_form.click_enter_button()

login_page = LoginPage()
login_page.login("email", "password")

main_page.click_category("Ноутбуки")

catalog_page = CatalogPage()
catalog_page.add_to_basket("Ноутбук Lenovo IdeaPad 3 15ITL6 (82H8009URE)")
catalog_page.click_basket()

basket_page = BasketPage()
basket_page.make_order("email", "375291111111")
