from lesson_10_25_11_2021.class_materials.vek_online_shop.header import Header


class LoginPage(Header):
    def enter_email(self, email):
        print(f"Enter email: {email}")

    def enter_password(self, password):
        print(f"Enter password:{password}")

    def click_enter_button(self):
        print("Click enter button")

    def login(self, email, password):
        self.enter_email(email)
        self.enter_password(password)
        self.click_enter_button()