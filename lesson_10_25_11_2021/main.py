from lesson_10_25_11_2021.account_form import AccountForm
from lesson_10_25_11_2021.login_form import LoginForm

account_form = AccountForm("http://123", "red")
print(account_form.color)
print(account_form.url)
account_form.click_home_icon()

login_form = LoginForm("http://12345", "blue","Close icon")
print(login_form.color)
print(login_form.url)
login_form.click_home_icon()
print(login_form.close_icon)
print(account_form.close_icon)