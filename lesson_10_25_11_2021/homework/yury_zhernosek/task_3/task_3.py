'''
Создать 4 фрукта.
Апельсин, яблоко, мандарин, банан

У всех фруктов есть сорт, список витаминов, цена, имя

У апельсина, мандарина, банана есть метод очистить
У яблока метод порезать

У банана есть характеристика: кол-во калия

Создать 4 объекта фрукта и использовать все доступные методы и характеристики
При вызове метода очистить, должно писаться имя фрукта

Например:
apple = Apple("sort", [a,b,c], 120, "apple")
apple.clear()
>>"apple очищен"

'''

from apple import Apple
from orange import Orange
from mandarin import Mandarin
from banana import Banana

apple = Apple('sort', ['a', 'b', 'c'], 10, 'aplle1')
orange = Orange('sort1', ['a'], 21, 'orange1')
mandarin = Mandarin('sort2', ['b'], 28, 'mandarin1')
banana = Banana('sort3', ['b', 'c'], 22, 'banana1', 12)

apple.cut()
orange.peel()
mandarin.peel()
banana.peel()