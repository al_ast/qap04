from fruit import Fruit

class PeeledFruit(Fruit):
    def peel(self):
        print(f'Pell {self.name}')