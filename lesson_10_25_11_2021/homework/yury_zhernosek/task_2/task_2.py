'''
Вам реально покупать билет не надо.
Только в рамках кода выполнить действия для покупки билета
Купить билет на https://silverscreen.by/ на выбранный фильм
'''

from main_page import MainPage
from search_page import SearchPage
from found_films_page import FoundFilmsPage
from film_page import FilmPage
from select_free_time_page import SelectFreeTimePage
from seat_selection_page import SeatSelectionPage
from select_pay_method_page import SelectPayMethodPage
from pay_page import PayPage
from email_form import EmailForm

main_page = MainPage()
search_page = SearchPage()
found_films_page = FoundFilmsPage()
film_page = FilmPage()
select_free_time_page = SelectFreeTimePage()
seat_selection_page = SeatSelectionPage()
select_pay_method_page = SelectPayMethodPage()
pay_page = PayPage()
email_form = EmailForm()

main_page.click_search_button()
search_page.enter_name_film('Encanto')
search_page.click_seearch_button()
found_films_page.click_film('Encanto')
film_page.click_buy_ticket_button()
select_free_time_page.click_selected_free_time_button()
seat_selection_page.click_free_seat()
seat_selection_page.click_confirm_and_pay()
email_form.enter_email('email@gmail.com')
email_form.click_continue_button()
select_pay_method_page.click_selected_pay_method()
select_pay_method_page.click_pay_order()
pay_page.enter_name('Yury')
pay_page.enter_last_name('Zhernosek')
pay_page.click_confirm_button()