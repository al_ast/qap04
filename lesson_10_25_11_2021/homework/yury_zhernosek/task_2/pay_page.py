class PayPage:
    def enter_name(self, name):
        print(f'Name entered: {name}')
    def enter_last_name(self, last_name):
        print(f'Last name entered: {last_name}')
    def click_confirm_button(self):
        print('Click "Confirm"')