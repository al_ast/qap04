from class_fruit import Fruit

class Banana(Fruit):
    def __init__(self, name, sort, vitamins, price, potassium):
        super().__init__(name, sort, vitamins, price)
        self.potassium = potassium

    def peel(self):
        print(f"{self.name} очищен")