# Задание3*:
# Создать 4 фрукта.
# Апельсин, яблоко, мандарин, банан
#
# У всех фруктов есть сорт, список витаминов, цена, имя
#
# У апельсина, мандарина, банана есть метод очистить
# У яблока метод порезать
#
# У банана есть характеристика: кол-во калия
#
# Создать 4 объекта фрукта и использовать все доступные методы и характеристики
# При вызове метода очистить, должно писаться имя фрукта
#
# Например:
# apple = Apple("sort", [a,b,c], 120, "apple")
# apple.clear()
# >>"apple очищен"

from class_fruit import Fruit
from class_orange import Orange
from class_tangerine import Tangerine
from class_banana import Banana
from class_apple import Apple

orange = Orange("Orange", "Bahia", ["a, b, c"], 3.99)
tangerine = Tangerine("Tangerine", "Clementine", ["a,d,k"], 3.29)
banana = Banana("Banana", "Kavendish", ["b,c"], 2.99, "358 mg")
apple = Apple("Apple", "Gala", ["a,c,e"], 2.63)

orange.peel()
tangerine.peel()
banana.peel()
apple.cut()
