from class_fruit import Fruit

class Tangerine(Fruit):
    def peel(self):
        print(f"{self.name} очищен")