from class_fruit import Fruit

class Orange(Fruit):
    def peel(self):
        print(f"{self.name} очищен")