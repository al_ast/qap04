# Задание2:
# Купить билет на https://silverscreen.by/ на выбранный фильм

from class_homepage import HomePage
from class_afisha import Afisha
from class_now_in_cinemas import NowInCinemas
from class_film_page import FilmPage
from class_show import Show
from class_email import Email
from class_prepayment import PrePayment
from class_pay import Pay
from class_payment_info import PaymentInfo


home_page = HomePage()
home_page.open_afisha()

afisha = Afisha()
afisha.open_now_in_cinemas()

current_films = NowInCinemas()
current_films.choose_theater("SiverScreen в АренаСити")
current_films.choose_date("29.11")
current_films.choose_time("17:00 - 21:59")
current_films. choose_format("3D")
current_films.choose_other("на русском языке")
current_films.choose_film("Веном 2")

film_page = FilmPage()
film_page.choose_show("21:50")
#film_page.change_date опционально

show = Show()
show.choose_seat("ряд 3, место 3")
show.enter_promocode("PROMOCODE")
show.confirm()

email = Email()
email.enter_email("email@email.com")

pre_payment = PrePayment()
pre_payment.choose_payment()
pre_payment.press_pay()

pay = Pay()
pay.enter_personal_data("Имя", "Фамилия")

pay_info = PaymentInfo()
pay_info.enter_payment_info("0000 0000 0000 0000", '12/24', "NAME SURNAME", "000")
pay_info.pay()




