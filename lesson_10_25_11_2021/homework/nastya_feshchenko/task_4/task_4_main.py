# Задание4**
# Переписать классное задание 21 век использовав один метод.
# Чтобы открыть любую указанную секцию (холодильники, ноутбуки и.т.д) и один метод, чтобы купить любой товар.
#
# Так же вынести верхнюю часть страницы(с категориями) в общий класс, т.к она доступна на всех страницах.



from class_main_page import MainPage
from class_catalogue import Catalogue
from class_enter_account import EnterAccount
from class_login_form import LoginForm
from class_item_page import ItemPage
from class_cart import Cart


main_page = MainPage()
main_page.click_account()

enter_account = EnterAccount()
enter_account.choose_opt_login()

login_form = LoginForm()
login_form.enter_email("email")
login_form.enter_password("password")
login_form.click_login()

main_page.choose_category("smartphone")

item_page = ItemPage()
item_page.read_info()
item_page.add_to_cart("iPhone 13")

main_page.go_to_cart()

cart = Cart()
cart.pay("card, date, cvv")
cart.buy()
