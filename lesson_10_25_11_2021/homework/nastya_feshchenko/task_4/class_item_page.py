from class_main_page import MainPage

class ItemPage(MainPage):
    def read_info(self):
        print("Read more about the item")

    def add_to_cart(self, item):
        self.item = item
        print(f"Add {self.item} to your cart")
