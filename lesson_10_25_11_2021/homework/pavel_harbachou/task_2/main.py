# Вам реально покупать билет не надо.
# Только в рамках кода выполнить действия для покупки билета
# Купить билет на https://silverscreen.by/ на выбранный фильм
from lesson_12_06_12_2021.home_work.yury_zhernosek.main_page import MainPage
from dropdown_menu import DropdownMenu
from site_search_page import SiteSearchPage
from movie_page import MoviePage
from cinema_hall_page import CinemaHallPage
from pop_up_window import PopUpWindow
from payment_page import PaymentPage

main_page = MainPage()
main_page.hover_the_cursor_over_the_search_icon()

dropdown_menu = DropdownMenu()
dropdown_menu.click_field_search()
dropdown_menu.enter_name_of_movie('Обитель зла')
dropdown_menu.press_enter_button()

site_search_page = SiteSearchPage()
site_search_page.click_buy_button('Обитель зла')

movie_page = MoviePage()
movie_page.click_time_button('22.05')

cinema_hall_page = CinemaHallPage()
cinema_hall_page.click_on_a_place_number('2')
cinema_hall_page.click_on_button_confirm_and_proceed_to_payment()

pop_up_window = PopUpWindow()
pop_up_window.click_on_email_field()
pop_up_window.enter_email('aaa@mail.ru')

payment_page = PaymentPage()
payment_page.choose_a_payment_method('Оплатить любой картой')
payment_page.click_pay_for_the_order_button()

