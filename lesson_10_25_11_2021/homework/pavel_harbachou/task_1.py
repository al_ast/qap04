# Дан список ["new", "hello world", "my name  is", "Yesterday"]
# Создать новый списков со словами,
# содержащими пробел используя генератор списков
list = ["new", "hello world", "my name  is", "Yesterday"]
new_list = [i for i in list
            if ' ' in i]
print(new_list)