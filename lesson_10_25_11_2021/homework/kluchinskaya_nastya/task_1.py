# Задание1:
# Дан список ["new", "hello world", "my name  is", "Yesterday"]
# Создать новый списков со словами,
# содержащими пробел используя генератор списков

my_list = ["new", "hello world", "my name  is", "Yesterday"]
new_list = [i for i in my_list if ' ' in i]

print(new_list)
