from fruits_peel import FruitsPeel


class Banana(FruitsPeel):
    def __init__(self, grade, vitamin_list, price, name, potassium):
        super().__init__(grade, vitamin_list, price, name)
        self.banana_potassium = potassium
