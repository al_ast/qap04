from all_fruits import Fruits


class FruitsPeel(Fruits):

    def peel(self):
        return f'{self.fruit_name} is peeled'