"""
Задание3*:
Создать 4 фрукта.
Апельсин, яблоко, мандарин, банан
У всех фруктов есть сорт, список витаминов, цена, имя
У апельсина, мандарина, банана есть метод очистить
У яблока метод порезать
У банана есть характеристика: кол-во калия
Создать 4 объекта фрукта и использовать все доступные методы и характеристики
При вызове метода очистить, должно писаться имя фрукта
Например:
apple = Apple("sort", [a,b,c], 120, "apple")
apple.clear()
>>"apple очищен"
"""

from orange import Orange
from mandarin import Mandarin
from banana import Banana
from apple import Apple

orange = Orange('verna', ["А", "В1", "В2", "РР", "С"], "15", "orange")
mandarin = Mandarin("clementine", ["С", "B1", "B2"], "30", "mandarin")
banana = Banana("kavendish", ["B1", "B2", "B6", "C"], "20", "banana", "358")
apple = Apple("antonovka", ["A", "С", "В1", "В2", "РР", "Е"], "10", "apple")
print(orange.peel())
print(mandarin.peel())
print(banana.peel())
print(f'Banana potassium: {banana.banana_potassium}')
print(apple.cut())
