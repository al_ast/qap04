# Только в рамках кода выполнить действия для покупки билета
# Купить билет на https://silverscreen.by/ на выбранный фильм

from afisha_main_page import AfishaMainPage
from afisha_page import AfishaPage
from search_parameters_page import SearchParametersPage
from film_page import FilmPage
from seat_page import SeatPage
from email_page import Email
from payment_page import PaymentCard
from card_info import CardInfo


afisha_main_page = AfishaMainPage()
afisha_main_page.open_afisha()

afisha_page = AfishaPage()
afisha_page.open_now_in_cinemas()

search_parameters = SearchParametersPage()
search_parameters.choose_cinema("Арена Сити")
search_parameters.choose_date("02.12.21")
search_parameters.choose_time("12:00 - 16:59")
search_parameters.choose_parameter("2D, Dolby Digital")
search_parameters.choose_another("Русский язык")

film_page = FilmPage()
film_page.choose_film("Летчик")

seat_page = SeatPage()
seat_page.choose_seat("Row 10, place 15")
seat_page.confirm()

email_page = Email()
email_page.my_email("abc@gmail.com")

payment_page = PaymentCard()
payment_page.choose_payment()
payment_page.press_card_pay()

card_info = CardInfo()
card_info.enter_card_info("9999 8888 7777 6666", "01/22", "NAME SURNAME", "111")
card_info.pay()