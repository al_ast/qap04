from film_selection import Choice
from buying_a_ticket import Purchase
from timing import Time
from places import Places
from payment import Payment
from email import Email
from payment_method import PaymentMethod
from personal_data import Personal_data
from confirmation import Confirmation

film_selection = Choice()
buying_a_ticket = Purchase()
timing = Time()
places = Places()
payment = Payment()
email = Email()
payment_method = PaymentMethod()
personal_data = Personal_data()
confirmation = Confirmation()

film_selection.enter_the_movie_of_interest("Человек-паук: Нет пути домой")
buying_a_ticket.click_the_button_to_buy_a_ticket()
timing.timing('15 декабря', '20:45')
places.choose_places('5 ряд, 10 место')
payment.go_to_payment()
email.enter_your_email('sasha-simonenko-2000@mail.ru')
payment_method.choose_payment()
personal_data.enter_your_name_and_surname('Sasha', 'Simonenko')
confirmation.click_on_the_confirm_button()


