"""
Дан список ["new", "hello world", "my name  is", "Yesterday"]
Создать новый списков со словами,
содержащими пробел используя генератор списков
"""

my_list = ["new", "hello world", "my name  is", "Yesterday"]
my_list_new = [i for i in my_list if i.count(" ")]
print(my_list_new)