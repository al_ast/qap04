from card_pay_page import CardPayPage
from main_page import MainPage
from login_page import LoginPage
from header import Header
from afisha import Afisha
from  film_page import FilmPage
from pay_page import PayPage

main_page = MainPage()
main_page.click_on_login()

login_page = LoginPage()
login_page.enter_email("artem_mikulich@gmail.com")
login_page.enter_password("12345678")
login_page.click_enter_button()

header = Header()
header.click_category("Afisha")
header.click_now_in_cinema()

afisha = Afisha()
afisha.click_film("Eternal")

film_page = FilmPage()
film_page.click_time_button()
film_page.click_locatin_for_view()
film_page.click_confirm_and_proceed_to_payment_button()

pay_page = PayPage()
pay_page.click_pay_for_the_order_button()

card_pay_page = CardPayPage()
card_pay_page.enter_the_card_number("1234432156788765")
card_pay_page.enter_validity_period("01/24")
card_pay_page.enter_the_card_number("Artem Mikulich")
card_pay_page.enter_CVC2("111")
card_pay_page.click_pay_button()
