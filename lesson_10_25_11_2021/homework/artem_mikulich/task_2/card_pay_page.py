class CardPayPage:
    def enter_the_card_number(self, card_number):
        print(f"card number {card_number}")

    def enter_validity_period(self, validity_period):
        print(f"validity period {validity_period}")

    def enter_card_holders_name(self, name):
        print(f"card holder's name {name}")

    def enter_CVC2(self, cvc2):
        print(f"cvc2 {cvc2}")

    def click_pay_button(self):
        print("Click pay button")