"""Задание1:
Дан список ["new", "hello world", "my name  is", "Yesterday"]
Создать новый список со словами,
содержащими пробел используя генератор списков
"""

given_list = ["new", "hello world", "my name  is", "Yesterday"]
new_list = [words for words in given_list if " " in words]
print(new_list)
