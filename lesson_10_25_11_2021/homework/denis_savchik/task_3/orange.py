from fruit import Fruit
from peeled_fruit import PeeledFruit


class Orange(Fruit, PeeledFruit):
    pass