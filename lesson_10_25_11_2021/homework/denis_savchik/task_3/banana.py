from fruit import Fruit
from peeled_fruit import PeeledFruit


class Banana(Fruit, PeeledFruit):

    def __init__(self, name, grade, list_of_vitamins, price, potassium):
        self.potassium = potassium
        super().__init__(name, grade, list_of_vitamins, price)
