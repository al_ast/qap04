# Задание3*:
# Создать 4 фрукта.
# Апельсин, яблоко, мандарин, банан
# У всех фруктов есть сорт, список витаминов, цена, имя
# У апельсина, мандарина, банана есть метод очистить
# У яблока метод порезать
# У банана есть характеристика: кол-во калия
# Создать 4 объекта фрукта и использовать все доступные методы и характеристики
# При вызове метода очистить, должно писаться имя фрукта


from orange import Orange
from apple import Apple
from mandarin import Mandarin
from banana import Banana


mandarin = Mandarin("Mandarin", "Sort", ["C", "D"], 10)
apple = Apple("Apple", "White", ["A", "C", "B"], 5)
banana = Banana("Banana", "Green", ["B"], 6, 50)
orange = Orange("Orange", "Verna", ["C", "A"], 7)

orange.peel_fruit(orange)
mandarin.peel_fruit(mandarin)
banana.peel_fruit(banana)
apple.cut()




