from fruit import Fruit
from peeled_fruit import PeeledFruit


class Mandarin(Fruit, PeeledFruit):
    pass