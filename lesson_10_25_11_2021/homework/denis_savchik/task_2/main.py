# Задание2:
# Вам реально покупать билет не надо.
# Только в рамках кода выполнить действия для покупки билета
# Купить билет на https://silverscreen.by/ на выбранный фильм

from main_page import MainPage
from header_page import HeaderPage
from poster_page import PosterPage
from film_page import FilmPage
from time_film_screening import TimeFilmScreening
from payment import Payment
from enter_card_info import EnterCardInfo

main_page = MainPage()
header_page = HeaderPage()
poster_page = PosterPage()
film_page = FilmPage()
time_film_screening = TimeFilmScreening()
payment = Payment()
card_info = EnterCardInfo()


main_page.click_to_swipe_film_page()
main_page.click_buy_ticket("Spider Man")
film_page.choose_time_of_film_screening("17.30")
payment.choose_payment_method("Card")
card_info.enter_card_info("0000 0000 0000 0000")
card_info.payment_info()
card_info.send_tickets_to_mail("123@gmail.com")


