class FilmPage:
    def select_cinema(self, cinema):
        print(f"Select cinema '{cinema}'")

    def select_date(self, date):
        print(f"Select date '{date}'")

    def select_time(self, time):
        print(f"Select time '{time}'")

    def select_ticket(self):
        print("Select ticket")
