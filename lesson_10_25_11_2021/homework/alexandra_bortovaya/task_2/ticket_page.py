class TicketPage:
    def select_seat(self, row, seat):
        print(f"Select row {row}, seat {seat}")

    def enter_promocode(self, promocode):
        print(f"Enter promocode '{promocode}'")

    def apply_promocode(self):
        print("Apply promocode")

    def go_to_payment(self):
        print("Go to payment")
