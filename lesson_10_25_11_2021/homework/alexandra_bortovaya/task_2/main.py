"""Задание2:
Купить билет на https://silverscreen.by/ на выбранный фильм"""
from main_page import MainPage
from affiche_page import Affiche
from film_page import FilmPage
from ticket_page import TicketPage


open_affiche = MainPage()
open_affiche.open_main_page()
open_affiche.hover_affiche()
open_affiche.select_affiche_now()

select_film = Affiche()
select_film.select_film("Вечные")

select_film_params = FilmPage()
select_film_params.select_cinema("Arena City")
select_film_params.select_date("понедельник, 29 ноября")
select_film_params.select_time("17:00 - 21:59")
select_film_params.select_ticket()

select_ticket_params = TicketPage()
select_ticket_params.select_seat(2, 5)
select_ticket_params.select_seat(2, 6)
select_ticket_params.enter_promocode("SGH451")
select_ticket_params.apply_promocode()
select_ticket_params.go_to_payment()
