class CartPage:
    def check_products_in_cart(self):
        print("Check products in cart")

    def change_product_quantity(self, change):
        print(f"Change product quantity {change}")

    def enter_promocode(self, promocode):
        print(f"Enter promocode '{promocode}'")

    def click_add_promocode_button(self):
        print("Click 'Enter promocode' button")

    def check_price_is_changed(self, promocode):
        print(f"Check price is changed after '{promocode}' promocode")

    def click_place_order_button(self):
        print("Click 'Place order' button")
