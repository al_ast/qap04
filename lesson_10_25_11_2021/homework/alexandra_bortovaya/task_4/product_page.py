class ProductPage:
    def open_overview_tab(self):
        print("Open 'Overview' tab")

    def open_review_tab(self):
        print("Open 'Review' tab")

    def open_qa_tab(self):
        print("Open 'QA' tab")

    def check_overview_tab(self):
        print("Check 'Overview' tab")

    def check_review_tab(self):
        print("Check 'Review' tab")

    def check_qa_tab(self):
        print("Check 'QA' tab")

    def click_add_to_cart_button(self):
        print("Click 'Add to cart' button")
