"""Задание4**
Переписать классное задание 21 век использовав один метод.
Чтобы открыть любую указанную секцию(холодильники, ноутбуки и.т.д)
и один метод, чтобы купить любой товар

Так же вынести верхнюю часть страницы(с категориями) в общий класс
т.к она доступна на всех страницах"""
from main_page import MainPage
from common_header import CommonHeader
from login_form import LoginForm
from category_page import CategoryPage
from product_page import ProductPage
from cart_page import CartPage

main_page = MainPage()
main_page.open_main_page()
header = CommonHeader()
header.open_login_form()
login_form = LoginForm()
login_form.enter_email("test@email.com")
login_form.eter_password("123456")
login_form.click_login_button()
header.open_category("Refrigerators")
category_page = CategoryPage()
category_page.select_product("LG GA-B419SLGL")
product_page = ProductPage()
product_page.check_overview_tab()
product_page.open_review_tab()
product_page.check_review_tab()
product_page.open_qa_tab()
product_page.check_qa_tab()
product_page.click_add_to_cart_button()
header.open_cart()
cart_page = CartPage()
cart_page.check_products_in_cart()
cart_page.change_product_quantity("+2")
cart_page.change_product_quantity("-1")
cart_page.enter_promocode("MINUS25")
cart_page.click_add_promocode_button()
cart_page.check_price_is_changed("MINUS25")
cart_page.click_place_order_button()
