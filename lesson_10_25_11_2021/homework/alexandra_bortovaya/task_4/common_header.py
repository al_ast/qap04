class CommonHeader:
    def open_login_form(self):
        print("Open login form")

    def open_category(self, category):
        print(f"Open '{category}' category")

    def open_cart(self):
        print("Open cart")
