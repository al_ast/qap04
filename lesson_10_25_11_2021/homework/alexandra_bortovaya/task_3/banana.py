from fruits import Fruits


class Banana(Fruits):
    def __init__(self, sort, vitamins, price, name, potassium):
        super().__init__(sort, vitamins, price, name)
        self.potassium = potassium

    def slice(self):
        print(f"{self.name} sliced")
