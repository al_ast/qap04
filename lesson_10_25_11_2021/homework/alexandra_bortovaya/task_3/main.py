"""Задание3*:
Создать 4 фрукта. Апельсин, яблоко, мандарин, банан. У всех фруктов есть сорт, список витаминов, цена, имя
У апельсина, мандарина, банана есть метод очистить. У яблока метод порезать
У банана есть характеристика: кол-во калия
Создать 4 объекта фрукта и использовать все доступные методы и характеристики
При вызове метода очистить, должно писаться имя фрукта
Например:
apple = Apple("sort", [a,b,c], 120, "apple")
apple.clear()
>>"apple очищен"
"""
from apple import Apple
from banana import Banana
from orange import Orange
from tangerine import Tangerine


orange = Orange("navel", ["A", "B1", "B2", "PP", "C"], 100, "Orange")
apple = Apple("honeycrisp", ["A", "С", "В1", "В2", "РР", "Е"], 80, "Apple")
tangerine = Tangerine("encore", ["A", "B1", "B2", "PP", "C"], 120, "Tangerine")
banana = Banana("cavendish", ["B1", "B2", "B6", "PP", "C"], 130, "Banana", 50)

orange.peel()
apple.peel()
tangerine.peel()
banana.slice()
