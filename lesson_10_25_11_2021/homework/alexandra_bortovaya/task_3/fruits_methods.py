from fruits import Fruits


class FruitsMethods(Fruits):
    def peel(self):
        print(f"{self.name} peeled")
