"""Задание1:
Дан список ["new", "hello world", "my name  is", "Yesterday"]
Создать новый списков со словами, содержащими пробел используя генератор списков"""

given_list = ["new", "hello world", "my name  is", "Yesterday"]
new_list = [list_element for list_element in given_list if ' ' in list_element]
print(new_list)
