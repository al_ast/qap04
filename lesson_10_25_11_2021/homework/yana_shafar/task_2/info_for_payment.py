class InfoForPayment:
    def input_name_and_surname(self, name, surname):
        print(f'Input name and surname: {name} {surname}')

    def click_continue_button(self):
        print('Click continue button')

    def input_payment_card_info(self, number_of_card, valid_until, card_owner, cvv ):
        print(f'Input number of card and card owner {number_of_card} {valid_until} {card_owner} {cvv}')

    def click_pay_button(self):
        print('Click pay button')