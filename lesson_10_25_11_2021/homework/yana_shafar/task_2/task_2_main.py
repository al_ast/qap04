from main_page import MainPage
from afisha_page import AfishaPage
from film_page import FilmPage
from info_for_payment_window import InfoForPaymentWindow
from payment_page import PaymentPage
from info_for_payment import InfoForPayment

main_page = MainPage()
main_page.click_afisha_button()

afisha_page = AfishaPage()
afisha_page.click_buy_ticket()

film_page = FilmPage()
film_page.choose_and_click_time_button()
film_page.choose_and_click_place_to_view()
film_page.click_go_to_the_payment_button()

info_for_payment_window = InfoForPaymentWindow()
info_for_payment_window.input_email('yanashafar@gmil.com')
info_for_payment_window.click_continue_button()

payment_page = PaymentPage()
payment_page.click_pay_order_button()

info_for_payment = InfoForPayment()
info_for_payment.input_name_and_surname('yana', 'shafar')
info_for_payment.click_continue_button()
info_for_payment.input_payment_card_info('1234567899876543', '03/22', 'yana shafar', '789')
info_for_payment.click_pay_button()

