

class Fruit:
    def __init__(self, sort, vitamins, price, name):
        self.sort = sort
        self.vitamins = vitamins
        self.price = price
        self.name = name


class FruitForClear(Fruit):
    def clear(self):
        print(f'{self.name} clear')
