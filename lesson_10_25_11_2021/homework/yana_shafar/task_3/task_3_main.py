# Создать 4 фрукта.
# Апельсин, яблоко, мандарин, банан
#
# У всех фруктов есть сорт, список витаминов, цена, имя
#
# У апельсина, мандарина, банана есть метод очистить
# У яблока метод порезать
#
# У банана есть характеристика: кол-во калия
#
# Создать 4 объекта фрукта и использовать все доступные методы и характеристики
# При вызове метода очистить, должно писаться имя фрукта
#
# Например:
# apple = Apple("sort", [a,b,c], 120, "apple")
# apple.clear()
# >>"apple очищен"

from lesson_10_25_11_2021.homework.yana_shafar.task_3.apple import Apple
from lesson_10_25_11_2021.homework.yana_shafar.task_3.banana import Banana
from lesson_10_25_11_2021.homework.yana_shafar.task_3.mandarin import Mandarin
from lesson_10_25_11_2021.homework.yana_shafar.task_3.orange import Orange

orange1 = Orange('sort1', ['a', 'b', 'c'], 10, 'orange1')
orange1.clear()

banana1 = Banana(potassium=2, sort='sort2', vitamins=['a', 'b', 'd'],  price=11, name='banana1')
banana1.clear()

mandarin1 = Mandarin('sort1', ['a', 'b', 'e'], 20, 'mandarin1')
mandarin1.clear()

apple1 = Apple('sort3', ['a', 'b', 'c'], 9, 'apple1')
apple1.slice()
