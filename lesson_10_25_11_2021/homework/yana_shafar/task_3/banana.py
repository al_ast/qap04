from lesson_10_25_11_2021.homework.yana_shafar.task_3.fruit import FruitForClear


class Banana(FruitForClear):
    def __init__(self, potassium, sort, vitamins, price, name):
        self.potassium = potassium
        super().__init__(sort, vitamins, price, name)