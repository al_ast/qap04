# Задание1:
# Дан список ["new", "hello world", "my name  is", "Yesterday"]
# Создать новый списков со словами,
# содержащими пробел используя генератор списков

list1 = ["new", "hello world", "my name  is", "Yesterday"]
list2 = [i for i in list1 if  ' ' in i]
print(list2)