from orange import Orange
from apple import Apple
from tangerine import Tangerine
from banana import Banana

orange = Orange("verna", "А, В1, В2", 100, "апельсин")
orange.print_info()
orange.clear()

apple = Apple("golden", " A, С, В1, В2", 60, "яблоко")
apple.print_info()
apple.cut()

tangerine = Tangerine("Unshiu", "А, D, К", 80, "мандарин")
tangerine.print_info()
tangerine.clear()

banana = Banana("baby banana", "В6, В9, Е", 200, "банан", "358mg")
banana.print_info()
banana.clear()