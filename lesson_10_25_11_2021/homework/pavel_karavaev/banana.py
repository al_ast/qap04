from fruit import Fruit
class Banana(Fruit):
    def __init__(self, grade, vitamins, price, name, potassium):
        super().__init__(grade, vitamins, price, name)
        self.potassium = potassium
    def print_info(self):
        print(f"\n{self.name}"
               f"\nсорт: {self.grade}"
               f"\nвитамины: {self.vitamins}"
               f"\nцена:{self.price}"
               f"\nкол-во калия в банане: {self.potassium}")
    def clear(self):
        print(f"{self.name} очищен")

