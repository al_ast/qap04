class Fruit:
    def __init__(self, grade, vitamins, price, name):
        self.grade = grade
        self.vitamins = vitamins
        self.price = price
        self.name = name
    def print_info(self):
        print(f"\n{self.name}"
               f"\nсорт: {self.grade}"
               f"\nвитамины: {self.vitamins}"
               f"\nцена:{self.price}")
