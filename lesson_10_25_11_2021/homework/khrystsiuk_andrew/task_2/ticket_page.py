class TicketPage:

    def row_and_sit(self, row, sit):
        print(f'I want to sit in a {row} row, on sit number {sit}')

    def enter_promo_code(self, promo_code):
        print(f'Entering promo code {promo_code}')

    def applying_promo_code(self):
        print('Promo code applied...')

    def payment(self, price):
        print(f'Paying {price} BYN for the ticket')
