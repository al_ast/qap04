# Задание 2
# Купить билет на https://silverscreen.by/ на выбранный фильм

from main_page import MainPage
from affiche_page import AffichePage
from film_page import FilmPage
from ticket_page import TicketPage

open_main_page = MainPage()
open_main_page.open_main_page()
open_main_page.look_through_affiche()
open_main_page.now_in_cinema()

select_film = AffichePage()
select_film.affice('House Of Gucci')

film_details = FilmPage()
film_details.select_movie_session('DanaMoll', '10th of December', '17:20')
film_details.select_ticket()

ticket_info = TicketPage()
ticket_info.row_and_sit('6', '27')
ticket_info.enter_promo_code('FA23AD4')
ticket_info.applying_promo_code()
ticket_info.payment('25')



