from fruit import Fruit


class Peeled(Fruit):

    def peel(self):
        print(f'Peeling {self.name}')
