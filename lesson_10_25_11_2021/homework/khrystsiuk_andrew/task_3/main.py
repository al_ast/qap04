# Задание3*:
# Создать 4 фрукта.
# Апельсин, яблоко, мандарин, банан
# У всех фруктов есть сорт, список витаминов, цена, имя
# У апельсина, мандарина, банана есть метод очистить
# У яблока метод порезать
# У банана есть характеристика: кол-во калия
# Создать 4 объекта фрукта и использовать все доступные методы и характеристики
# При вызове метода очистить, должно писаться имя фрукта

from apple import Apple
from banana import Banana
from mandarin import Mandarin
from orange import Orange

apple = Apple('Apple', 'Golden', 5, ['C', 'D', 'E'])
banana = Banana('Banana', 'African', 7, ['Zink', 'D'], '50mg')
mandarin = Mandarin('Mandarin', 'Clementin', 3, ['C', 'A', 'D', 'K'])
orange = Orange('Orange', 'Gamlin', 6, ['C', 'D'])

print('\n')
print(apple)
print(banana, f'It contains {banana.potassium} of potassium.')
print(mandarin)
print(orange)
print('\n')

apple.cut()
banana.peel()
mandarin.peel()
orange.peel()
