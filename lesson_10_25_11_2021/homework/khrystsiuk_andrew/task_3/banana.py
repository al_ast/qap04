from peel import Peeled


class Banana(Peeled):
    def __init__(self, name, sort, price, vitamins, potassium):
        super().__init__(name, sort, price, vitamins)
        self.potassium = potassium
