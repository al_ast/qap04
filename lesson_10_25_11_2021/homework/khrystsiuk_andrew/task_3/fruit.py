class Fruit:

    def __init__(self, name, sort, price, vitamins):
        self.name = name
        self.sort = sort
        self.price = price
        self.vitamins = vitamins

    def __repr__(self):
        return f'This is {self.sort} {self.name}, it is rich with {self.vitamins} vitamins, it costs {self.price} BYN.'
