from fruit import Fruit


class Apple(Fruit):
    def cut(self):
        print(f'Cutting {self.name}')
