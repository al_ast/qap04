from account_section import AccountSection
from login_page import LoginPage
from cart_page import Cart
from category_page import CategoryPage
from homepage import Homepage

homepage = Homepage()
account_section = AccountSection()
print(homepage.click_account_section())
print(account_section.click_sign_in_button())
login_form = LoginPage()
print(login_form.login('vlad@test.by', '*****'))
print(homepage.click_category_page('TV'))
category_page = CategoryPage()
print(category_page.add_to_cart_button('Samsung UE43T5272AUXRU'))
print(homepage.click_cart_page())
cart = Cart()
print(cart.click_place_order())
