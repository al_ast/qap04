class Cart:

    def enter_promocode(self, promocode):
        return f'Provide promo code - {promocode}. Discount applied'

    def click_place_order(self):
        return f'Click "Place order" button'
