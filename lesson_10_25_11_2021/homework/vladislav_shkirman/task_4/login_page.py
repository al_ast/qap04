class LoginPage:

    def email(self, email):
        return f'Email {email} provided'

    def password(self, password):
        return f'Password ***{password}*** provided'

    def click_enter(self):
        return f'Click "Enter"'

    def login(self, email, password):
        self.email(email)
        self.password(password)
        self.click_enter()
        return 'Perform login'
