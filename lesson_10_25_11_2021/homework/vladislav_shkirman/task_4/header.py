class Header:

    def click_account_section(self):
        return f'Click "Account" section'

    def click_category_page(self, category):
        return f'Open category "{category}"'

    def click_cart_page(self):
        return f'Click "Cart" page. Cart page is opened'
