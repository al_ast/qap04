from header import Header


class CategoryPage(Header):

    def add_to_cart_button(self, product):
        return f'Add product "{product}" to cart'
