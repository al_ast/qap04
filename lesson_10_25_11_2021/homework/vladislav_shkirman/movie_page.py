class MoviePage:

    def click_cinema_hall(self):
        return 'Click on cinema hall'

    def choose_seats_with_status_available(self, status):
        return f'Choose the seat with the status "{status}"'

    def click_seat(self):
        return 'Click available seat to book'

    def click_confirm_and_checkout_button(self):
        return 'Click "Confirm and checkout" button'

    def enter_email_and_click_continue(self, email):
        return f'Enter email {email} and click "Continue"'

    def click_payment_option(self):
        return 'Click payment option'

    def click_place_order_button(self):
        return 'Click place order button'