"""
Задание3*:
Создать 4 фрукта.
Апельсин, яблоко, мандарин, банан
У всех фруктов есть сорт, список витаминов, цена, имя
У апельсина, мандарина, банана есть метод очистить
У яблока метод порезать
У банана есть характеристика: кол-во калия
Создать 4 объекта фрукта и использовать все доступные методы и характеристики
При вызове метода очистить, должно писаться имя фрукта
Например:
apple = Apple("sort", [a,b,c], 120, "apple")
apple.clear()
>>"apple очищен"
"""

from orange import Orange
from mandarin import Mandarin
from banana import Banana
from apple import Apple

orange = Orange('verna', ['a', 'c'], '40', 'orange')
mandarin = Mandarin('clementine', ['a', 'c'], '30', 'mandarin')
banana = Banana('honey', ['b', 'd'], '15', 'banana', '358')
apple = Apple('red prince', ['a', 'b', 'c'], '85', 'apple')
print(orange.peel())
print(mandarin.peel())
print(banana.peel())
print(f'Banana potassium: {banana.banana_potassium}')
print(apple.cut())
