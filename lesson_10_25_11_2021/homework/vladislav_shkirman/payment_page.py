class PaymentPage:

    def enter_name(self, name):
        return f'Enter name {name}'

    def enter_surname(self, surname):
        return f'Enter surname {surname}'

    def click_continue_button(self):
        return 'Click continue button'

    def enter_cc_data(self, card_number, valid_mm, valid_yy, card_holder_name, card_holder_surname, cvv):
        return f'Enter CC data:' \
               f'\nCard number: {card_number}\nMM/YY: {valid_mm}/{valid_yy}' \
               f'\nCard holder name: {card_holder_name} {card_holder_surname}\nCVV: {cvv}'

    def review_cc_data(self):
        return 'Review CC data'

    def click_pay_button(self):
        return 'Click "Pay" button'
