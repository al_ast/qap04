from fruits import Fruits


class Apple(Fruits):

    def cut(self):
        return f'cut {self.fruit_name}'
