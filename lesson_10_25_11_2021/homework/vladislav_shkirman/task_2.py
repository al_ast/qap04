"""
Задание2:
Вам реально покупать билет не надо.
Только в рамках кода выполнить действия для покупки билета
Купить билет на https://silverscreen.by/ на выбранный фильм
"""

from homepage import HomePage
from afisha import Afisha
from movie_page import MoviePage
from payment_page import PaymentPage

main_page = HomePage()
print(main_page.hover_on_afisha())
print(main_page.click_afisha_now())
afisha_page = Afisha()
print(afisha_page.click_drop_down_list_cinema())
print(afisha_page.select_cinema('VOKA Cinema'))
print(afisha_page.click_drop_down_list_day())
print(afisha_page.select_day('27.11.2021'))
print(afisha_page.find_movie('French Dispatch'))
print(afisha_page.click_buy_ticket_button())
movie_page = MoviePage()
print(movie_page.click_cinema_hall())
print(movie_page.choose_seats_with_status_available('Available'))
print(movie_page.click_seat())
print(movie_page.click_confirm_and_checkout_button())
print(movie_page.enter_email_and_click_continue('vlad@test.by'))
print(movie_page.click_payment_option())
print(movie_page.click_place_order_button())
payment_page = PaymentPage()
print(payment_page.enter_name('Vlad'))
print(payment_page.enter_surname('Shkirman'))
print(payment_page.click_continue_button())
print(payment_page.enter_cc_data('4988 4388 4388 4305', '03', '30', 'Vladislav', 'Shkirman', '737'))
print(payment_page.review_cc_data())
print(payment_page.click_pay_button())
