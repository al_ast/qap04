class Afisha:

    def click_drop_down_list_cinema(self):
        return 'Click cinema list'

    def select_cinema(self, cinema):
        return f'Select cinema "{cinema}"'

    def click_drop_down_list_day(self):
        return 'Click day list'

    def select_day(self, day):
        return f'Select day {day}'

    def find_movie(self, movie):
        return f'Find movie with the name "{movie}"'

    def click_buy_ticket_button(self):
        return 'Click "Buy a ticket" button'