from lesson_10_25_11_2021.homework.karina_zaremba.task_3.alex_exampels.fruit import Fruit


class Apple(Fruit):
    def cut(self):
        print(f"cut {self.name}")
