from fruits import Fruits
from apple import Apples
from banana import Bananas
from orange import Oranges
from tangerine import Tangerines
from fruits import ClearFruits

fruit1 = Fruits('Вкусняшка', "a", 88, "Киви")
print(fruit1.sort, fruit1.vitamins, fruit1.cost, fruit1.name)

orange1 = Oranges("Турецкий", "a, b, c", 90, 'Апельсин')
print(orange1.sort, orange1.vitamins, orange1.cost, orange1.name)

banana1 = Bananas("Тропик", "a, b, c", 250, "Банан", 358)
print(banana1.sort, banana1.vitamins, banana1.cost, banana1.name, banana1.kalium)

tangerine1 = Tangerines('Праздничный', "a, b, c", 200, 'Мандарин')
print(tangerine1.sort, tangerine1.vitamins, tangerine1.cost, tangerine1.name)

apple1 = Apples("Малиновка", "a, b, c", 45, "Яблоко")
print(apple1.sort, apple1.vitamins, apple1.cost, apple1.name)

clean = ClearFruits
orange1.clear()
banana1.clear()
tangerine1.clear()
apple1.cut()



