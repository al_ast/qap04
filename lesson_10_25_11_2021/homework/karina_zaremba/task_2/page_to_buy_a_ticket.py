class PageToBuyTicket:
    def choose_the_type_of_place(self, type_place):
        self.type_place = type_place
        print(f"Please, choose the type of place - {type_place}")

    def enter_the_promo_code(self, promo):
        self.promo_code = promo
        print(f" Enter your promo code - {promo}")

    def click_on_the_payment_button(self):
        print("Next - click on the 'Payment button'")

    def enter_email(self, email):
        self.email = email
        print(f"Enter your email {email}")
