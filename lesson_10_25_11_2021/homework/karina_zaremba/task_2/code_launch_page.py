from main_page import MainPage
from time_page import TimePage
from page_with_places import PageWithPlaces
from page_to_buy_a_ticket import PageToBuyTicket

main_page = MainPage()
main_page.choose_any_film_on_the_main_page("'Encanto'")
main_page.click_the_button_buy_a_ticket()

time_page = TimePage()
time_page.choose_a_convenient_time("19:40")
time_page.click_on_the_button_with_time()

places = PageWithPlaces()
places.select_a_place_on_the_scheme("6", "12")
places.click_on_a_place_on_the_scheme()

buy_a_ticket = PageToBuyTicket()
buy_a_ticket.choose_the_type_of_place("Standart")
buy_a_ticket.enter_the_promo_code("Silver")
buy_a_ticket.click_on_the_payment_button()
buy_a_ticket.enter_email("123@tut.by")