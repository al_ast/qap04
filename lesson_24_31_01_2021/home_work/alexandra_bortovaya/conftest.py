import allure
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture
def setup_teardown():
    with allure.step("test setup step1"):
        pass
    with allure.step("test setup step2"):
        pass
    with allure.step("test setup step3"):
        pass

    yield
    with allure.step("test teardown step1"):
        pass
    with allure.step("test teardown step2"):
        pass


@pytest.fixture
def chromedriver():
    return webdriver.Chrome(ChromeDriverManager().install())


@pytest.fixture
def add_attachment(chromedriver, request):
    chromedriver.get("https://www.google.by/")

    yield
    screenshot_name = f"scr_{request.node.name}.png"
    chromedriver.save_screenshot(screenshot_name)

    allure.attach("Some text", attachment_type=allure.attachment_type.TEXT)
    allure.attach.file(screenshot_name, attachment_type=allure.attachment_type.PNG)

    chromedriver.quit()

