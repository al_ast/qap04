import allure
import pytest


class TestAllure:
    @pytest.mark.newmark
    @pytest.mark.usefixtures("add_attachment")
    def test_status_passed_with_attachment(self):
        assert True

    def test_status_failed(self):
        assert False

    @allure.description("test skipped by mark")
    @pytest.mark.skip("reason for skip")
    def test_status_skipped_by_mark(self):
        assert True

    @allure.description("test skipped in test body")
    def test_skipped(self):
        pytest.skip("reason for skip")
        assert True

    def test_status_broken(self):
        raise Exception("broken test")

    @pytest.mark.xfail(reason="xfail reason")
    def test_xfail_passed(self):
        assert True

    @pytest.mark.xfail(reason="xfail reason")
    def test_xfail_failed(self):
        assert False

    @allure.title("custom test title")
    def test_with_custom_name(self):
        assert True

    @allure.link("https://docs.qameta.io/allure-report/frameworks/python/pytest", name="link")
    @allure.issue("https://docs.qameta.io/allure-report/frameworks/python/pytest", "NHX-1524")
    @allure.testcase("https://docs.qameta.io/allure-report/frameworks/python/pytest", "01-124 TestCase")
    def test_with_links(self):
        assert True

    @pytest.mark.parametrize('param', [True, False])
    def test_parametrized(self, param):
        a = param
        assert a, "Should be True"

    def test_with_steps(self):
        with allure.step("first step"):
            pass
        with allure.step("second step"):
            pass
        assert True

    @pytest.mark.usefixtures("setup_teardown")
    def test_with_fixture(self):
        assert True
