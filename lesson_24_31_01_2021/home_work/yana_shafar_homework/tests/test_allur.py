import allure
import pytest


class TestAllure:
    @allure.description("Invalid login check. User must stay on the login page")
    @pytest.mark.slow
    def test_login_with_not_valid_credentials(self, login_page):
        login_page.login("not valid", "not valid")

        assert login_page.is_opened(), "User cannot login with not valid credentials"

    @pytest.mark.slow
    @allure.link("https://www.saucedemo.com/", "Saucedemo")
    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")

        assert products_page.is_opened(), "Product page must be open after login with valid credentials"

    @allure.title("This test has a custom title")
    @pytest.mark.skip(reason="This test will be fail")
    def test_comparing_two_numbers(self):
        assert 8 > 11, "The number 8 is less than the number 11"

    @pytest.mark.xfail(reason="This test will be fail")
    def test_equality_of_two_numbers(self):
        assert 9 == 15, "The number 9 is not equal to the number 11"
