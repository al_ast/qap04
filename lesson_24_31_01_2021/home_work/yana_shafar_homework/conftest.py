import allure
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from lesson_24_31_01_2021.home_work.yana_shafar_homework.pages.login_page import LoginPage
from lesson_24_31_01_2021.home_work.yana_shafar_homework.pages.product_page import ProductsPage


@pytest.fixture(autouse=True)
def navigate_to_login_page(chromedriver, request):
    login_page = LoginPage(chromedriver)
    login_page.navigate()


@pytest.fixture()
def chromedriver():
    chrome_options = Options()
    chrome_options.add_argument('--headless')

    return webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)


@pytest.fixture()
def login_page(chromedriver):
    return LoginPage(chromedriver)


@pytest.fixture()
def products_page(chromedriver):
    return ProductsPage(chromedriver)


@pytest.fixture(autouse=True)
def tests_cleanup(request, chromedriver):
    yield
    screenshot_name = f"{request.node.name}.png"
    chromedriver.save_screenshot(screenshot_name)

    allure.attach("Test is finished", allure.attachment_type.TEXT)
    allure.attach.file(screenshot_name, attachment_type=allure.attachment_type.PNG)

    chromedriver.quit()
