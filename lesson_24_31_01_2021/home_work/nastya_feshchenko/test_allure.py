import allure
import pytest


class TestsAllure:
    @allure.description("The test is needed to check the process of login")
    def test_login_with_valid_credentials(self, login_page, products_page):
        with allure.step("Check login with valid creds first"):
            login_page.login("standard_user", "secret_sauce")
        assert products_page.is_opened(), "User should be redirected further to the Product Page"

    def test_login_with_invalid_credentials(self, login_page):
        with allure.step("Then check login with invalid creds"):
            login_page.login("invalid_user", "invalid_password")
        assert login_page.is_opened(), "User shouldn't be redirected further and should stay on the Login Page"

    @allure.description("This test is held to study the usage of allure descriptions")
    @allure.issue("This test can't pass")
    @pytest.mark.xfail(reason="This test is meant to fail")
    def test_3(self):
        assert 0 > 3, "3 is more than 0"

    @allure.description("This test is held to study the usage of allure links")
    @allure.link("https://bitbucket.org/al_ast/qap04/src/master/lesson_24_31_01_2021/class_materials/conftest.py")
    def test_4(self):
        a = 4
        b = 2
        assert a > b, "b is bigger than a"






