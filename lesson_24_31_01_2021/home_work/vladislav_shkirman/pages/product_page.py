from lesson_24_31_01_2021.home_work.vladislav_shkirman.pages.base_page import BasePage


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)
