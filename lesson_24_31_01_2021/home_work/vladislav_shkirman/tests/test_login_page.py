import pytest
import allure


@pytest.mark.usefixtures("screenshot")
class TestLoginPage:
    @allure.description("Check if a user is able to login with valid credentials")
    @allure.testcase('test_id: 01-451')
    @pytest.mark.smoke
    @allure.title('Test login user with valid credentials')
    @allure.step('Login with valid credentials')
    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")
        assert products_page.is_opened(), 'Products page should be opened after login with valid credentials'

    @allure.link("https://www.saucedemo.com/", name='Saucedemo')
    @allure.title('Test login with invalid credentials')
    @allure.step('Login with invalid credentials')
    def test_login_with_invalid_credentials(self, login_page):
        login_page.login("wrong_user", "wrong_password")
        assert login_page.is_opened(), 'User should not be loged in'

    @pytest.mark.xfail(reason="Fail reason")
    @allure.issue('QA-3254')
    def test_login_with_empty_credentials(self, login_page, products_page):
        login_page.login("", "")
        assert products_page.is_opened(), 'User should not be loged in'

    @allure.title('Test locked user recieves error message')
    @allure.step('Login with locked out user')
    def test_locked_user_error_message(self, login_page):
        login_page.login("locked_out_user", "secret_sauce")
        assert login_page.get_error_text() == 'Epic sadface: Sorry, this user has been locked out.', \
            'Locked user must receive error message'
