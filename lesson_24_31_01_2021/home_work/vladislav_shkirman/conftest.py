import pytest
import allure
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from lesson_24_31_01_2021.home_work.vladislav_shkirman.pages.login_page import LoginPage
from lesson_24_31_01_2021.home_work.vladislav_shkirman.pages.product_page import ProductsPage


@pytest.fixture()
def chromedriver():
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    return webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)


@pytest.fixture(autouse=True)
@allure.title('Open login page')
def navigate_to_login_page(chromedriver):
    login_page = LoginPage(chromedriver)
    login_page.navigate()


@pytest.fixture()
@allure.title('Create login_page fixture')
def login_page(chromedriver):
    return LoginPage(chromedriver)


@pytest.fixture()
@allure.title('Create products_page fixture')
def products_page(chromedriver):
    return ProductsPage(chromedriver)


@pytest.fixture
@allure.title('Attach a screenshot')
def screenshot(chromedriver, request):
    yield
    screenshot_name = f"{request.node.name}.png"
    chromedriver.save_screenshot(screenshot_name)

    allure.attach.file(screenshot_name, attachment_type=allure.attachment_type.PNG)
    allure.attach("Test has finished", "file", allure.attachment_type.TEXT)

    chromedriver.quit()
