import allure
import pytest


@pytest.mark.usefixtures("")
class TestLoginPage:
    @allure.description("Verify login with not valid credentials. "
                        "User should stay on the login page")
    @pytest.mark.slow
    def test_login_with_not_valid_credentials(self, login_page):
        with allure.step("Allure step from test. No method annotation"):
            login_page.login("not valid user name", "not valid password")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    @pytest.mark.xfail(reason="XFAIL reason")
    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")

        assert 3 == 5, "msg"

    @allure.link("Simple link")
    @allure.issue("Issue")
    def test_should_fail(self):
        assert 4 == 5, "Assert fail to check failed test result"

    @pytest.mark.skip(reason='SKIP reason')
    def test_skip(self):
        assert 4 == 5, "Assert fail to check failed test result"
