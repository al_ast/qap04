import requests


class TestImdbRequests:
    def test_get_actor_ratings(self):
        url = "https://imdb8.p.rapidapi.com/title/get-ratings"
        headers = {
            'x-rapidapi-host': 'imdb8.p.rapidapi.com',
            'x-rapidapi-key': 'a8db052937msh0270012ac6741ecp19503ejsn8c3272a5383d'
        }

        params = {"tconst": 'tt9174582'}
        response = requests.get(url, headers=headers, params=params)

        assert response.status_code == 200, f"Expected 200 status code but was: {response.status_code}"
