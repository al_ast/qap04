import random
import string

import allure
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from lesson_17_23_12_2021.class_materials.pages.login_page import LoginPage
from lesson_17_23_12_2021.class_materials.pages.product_page import ProductsPage


# @pytest.hookimpl(tryfirst=True, hookwrapper=True)
# def pytest_runtest_makereport(item, call):
#     # execute all other hooks to obtain the report object
#     outcome = yield
#     rep = outcome.get_result()
#
#     # set a report attribute for each phase of a call, which can
#     # be "setup", "call", "teardown"
#
#     setattr(item, "rep_" + rep.when, rep)
#
#
# @pytest.fixture(autouse=True)
# def navigate_to_login_page(chromedriver, request):
#     login_page = LoginPage(chromedriver)
#     login_page.navigate()
#     yield
#
#     if request.node.rep_call.failed:
#         test_name = request.node.name
#         chromedriver.save_screenshot(f"/home/alex/work_folder/qap04/{test_name}.png")
#     chromedriver.quit()

@pytest.fixture(autouse=True)
def navigate_to_login_page(chromedriver, request):
    login_page = LoginPage(chromedriver)
    login_page.navigate()


@pytest.fixture()
def chromedriver():
    chrome_options = Options()
    chrome_options.add_argument('--headless')

    return webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)


@pytest.fixture()
def login_page(chromedriver):
    return LoginPage(chromedriver)


@pytest.fixture()
def products_page(chromedriver):
    return ProductsPage(chromedriver)


@pytest.fixture(autouse=True)
def tests_cleanup(request, chromedriver):
    yield
    screenshot_name = f"{request.node.name}.png"
    chromedriver.save_screenshot(screenshot_name)

    allure.attach("Test is finished", allure.attachment_type.TEXT)
    allure.attach.file(screenshot_name, attachment_type=allure.attachment_type.PNG)

    chromedriver.quit()
