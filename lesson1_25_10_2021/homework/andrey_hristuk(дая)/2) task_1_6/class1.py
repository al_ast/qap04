# Написать класс Human(человек).
# Который может ходить(walk) и принимает кол-во километров, которое надо пройти.
# Метод должен печатать на экран: “I went {provided number} kms”

class Human:
    def walk(self, kms):
        print('I went ', kms, 'kms')
