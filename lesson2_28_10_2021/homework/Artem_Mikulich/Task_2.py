# Apply all math operations e.g +,-, **, % e.t.c to the variable itself
x = 6
x += 4
print(x)
x = 4
x -= 2
print(x)
x = 6
x /= 2
print(x)
x = 7
x *= 2
print(x)
x = 8
x **=2
print(x)
x = 9
x %= 2
print(x)
x = 10
x //= 4
print(x)