# Read user input from the console
# For negative number print: “exact number is negative”
# For positive number print whether it is odd or even: e.g “exact number is odd”
# For strings print: “Only numbers are supported”
x = input("Enter any number ")
try:
    x_1 = int(x)
except ValueError:
    print("Only numbers are supported")
else:
    if x_1 < 0:
        print("Number is negative")
    elif (x_1 % 2) == 0:
        print("Number is even")
    else:
        print("Number is odd")




