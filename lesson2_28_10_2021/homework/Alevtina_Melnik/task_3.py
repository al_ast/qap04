# Task3
# print all comparison operations result
# e.g
# print(5 > 9)
# print(2 != 3)
# e.t.c
print(2 < 1)
print(2 > 1)
print(2 <= 1)
print(2 >= 1)
print(2 != 1)
print(2 != 2)
print(2 == 2)
print(2 == 1)