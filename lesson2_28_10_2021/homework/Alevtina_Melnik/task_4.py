import sys

input_number = input("Enter value: ")
try:
    number = int(input_number)
except ValueError:
    print("Only numbers are supported")
    sys.exit()
if number < 0:
    print('Exact number is negative')
elif number > 0:
    if number % 2 == 0:
        print("Exact number is even")
    else:
        print("Exact number is odd")