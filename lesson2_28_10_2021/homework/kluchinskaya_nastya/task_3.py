# print all comparison operations result
print(5 > 3)  # true
print(5 < 3)  # false
print(5 >= 4)  # true
print(5 <= 4)  # false
print(5 <= 5)  # true
print(5 == 5)  # true
print(5 == 4)  # false
print(5 != 5)  # false
print(5 != 4)  # true