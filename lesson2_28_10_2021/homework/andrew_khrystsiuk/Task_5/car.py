# Create a car with color, model, current mileage, max mileage.

class Car:

    def __init__(self, color, model):
        print(f'This is {color} {model} car.')
        self.color = color
        self.model = model

    def drive(self, kms):
        print('I have drive', kms, 'kms')
