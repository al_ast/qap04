# By default max mileage=current mileage + 1000
# Car should have 2 methods:
# 1.print_info: which will print all information about car e.g “This is color model car”
# 2. drive. Which accepts kms to drive. Method should print any message every km it has drive.
# If it is more or equals max mileage then car is broken and should print it

from car import Car
car1 = Car('Red', 'Ford', )
current_mileage = 10
print(f'My current mileage is {current_mileage}')
max_mileage = current_mileage + 1000
print(f'My max mileage is {max_mileage}')
while True:
    kms = input('Input number of kilometers: ')
    current_mileage += int(kms)
    if not kms.isnumeric() or kms <= str(0):
        print('input correct number')
    car1.drive(kms)
    if current_mileage != max_mileage:
        for kms in range(current_mileage):
            print('My current mileage is', kms + 1)
            if kms >= max_mileage:
                print('I’m broken because of my current mileage is maximum', max_mileage, 'kms')
                break
    if current_mileage == max_mileage:
        for kms in range(current_mileage):
            print('My current mileage is', kms + 1)
        print('I’m broken because of my current mileage is maximum', max_mileage, 'kms')
        break
