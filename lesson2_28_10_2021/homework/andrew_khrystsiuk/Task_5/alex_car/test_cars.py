from lesson2_28_10_2021.homework.andrew_khrystsiuk.Task_5.alex_car.car import \
    Car

black_bmv = Car("black", "bmv", 100)
red_opel = Car("red", "opel", 100, 105)

black_bmv.print_info()
red_opel.print_info()

# red_opel.drive(9)
red_opel.drive(2)
red_opel.drive(3)
red_opel.drive(1)
red_opel.print_info()

