# Create a car with color, model, current mileage, max mileage.

class Car:

    def __init__(self, color, model, current_mileage=0, max_mileage=None):
        self.color = color
        self.model = model
        self.current_mileage = current_mileage

        if max_mileage is None:
            self.max_mileage = self.current_mileage + 1000
        else:
            self.max_mileage = max_mileage

    def print_info(self):
        print(f"Color:{self.color}. Model:{self.model}. Current mileage: "
              f"{self.current_mileage}. Max mile_age:{self.max_mileage}")

    def drive(self, kms):
        mileage_message = "Current mileage:{}. Max mileage:{}"

        for current_km in range(kms):
            if self.current_mileage < self.max_mileage:
                self.current_mileage += 1
                message = "Drive 1 km finished. " + mileage_message.format(
                    self.current_mileage, self.max_mileage)
                print(message)
            else:
                message = "Car is broken. " + mileage_message.format(
                    self.current_mileage, self.max_mileage)
                print(message)
                break
