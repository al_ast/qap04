# Read user input from the console
# For negative number print: “exact number is negative”
# For positive number print whether it is odd or even: e.g “exact number is odd”
# For strings print: “Only numbers are supported”

while True:
    input_value = input('Enter number:')
    try:
        number = int(input_value)
    except ValueError:
        print('Only members are supported')
    else:
        if number < 0:
            print(f'{number} is negative number')
        elif number >= 0:
            if number % 2 == 0:
                print(f'{number} is even number')
            else:
                print(f'{number} is odd number')
