# Print all comparison operations result

print(1 < 3)
print(3 < 1)
print(4 > 2)
print(2 > 4)
print(1 == 1)
print(1 == 0)
print(5 >= 5)
print(4 >= 5)
print(6 <= 7)
print(7 <= 6)
print(1 != 2)
print(1 != 1)
