# Apply all math operations e.g +,-, **, % e.t.c to the variable itself

x = 5
x += 3
print(x)
x -= 3
print(x)
x *= 3
print(x)
x /= 3
print(x)
x **= 3
print(x)
x //= 3
print(x)
x %= 3
print(x)
