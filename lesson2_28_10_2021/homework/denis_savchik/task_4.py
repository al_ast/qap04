#Read user input from the console
#For negative number print: “exact number is negative”
#For positive number print whether it is odd or even: e.g “exact number is odd”
#For strings print: “Only numbers are supported”

variable = input("Enter something pls ")
try:
    variable_1 = int(variable)
except ValueError:
    print("Only numbers are supported")
else:
    if variable_1 < 0:
        print("exact number is negative")
    elif (variable_1 % 2) == 0 :
        print("exact number is even")
    else:
        print("exact number is odd")