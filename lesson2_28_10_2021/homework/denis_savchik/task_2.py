#Apply all math operations e.g +,-, **, % e.t.c to the variable itself

var = 5
var += 3
print(var)
var = 7
var -= 1
print(var)
var = 3
var *= 2
print(var)
var = 27
var /= 3
print(var)
var = 225
var **= 5
print(var)
var = 1000
var //= 7
print(var)
var = 21
var %= 5
print(var)