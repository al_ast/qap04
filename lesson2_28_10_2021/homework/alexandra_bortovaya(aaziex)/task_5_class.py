'''
Create a car with color, model, current mileage, max mileage. By default max mileage=current mileage + 1000
Car should have 2 methods:
1. print_info: which will print all information about car e.g “This is color model car”
2. drive. Which accepts kms to drive. Method should print any message every km it has drive.
If it is more or equals max mileage then car is broken and should print it
'''


class Car:

    def __init__(self, color, model, mileage, max_mileage=None):
        self.color = color
        self.model = model
        self.mileage = mileage
        if max_mileage is None:
            self.max_mileage = self.mileage + 1000
        else:
            self.max_mileage = max_mileage

    def print_info(self):
        print(
            f'\nInformation about the car:\n\tColor: {self.color}\n\tModel: {self.model}\n'
            f'\tCurrent mileage: {self.mileage}\n\tMax mileage: {self.max_mileage}'
        )

    def drive(self, km):
        while km > 0:
            if self.mileage < self.max_mileage:
                self.mileage += 1
                print(f'1 more km is driven, my current mileage is {self.mileage}')
                km -= 1
            else:
                print(f'I’m broken because my current mileage is maximum ({self.max_mileage})')
                break
