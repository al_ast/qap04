# Apply all math operations e.g +,-, **, % e.t.c to the variable itself

x = 5
x += 2
print(x)

x = 5
x -= 2
print(x)

x = 5
x *= 2
print(x)

x = 5
x /= 2
print(x)

x = 5
x %= 2
print(x)

x = 5
x //= 2
print(x)

x = 5
x **= 2
print(x)
