'''
Read user input from the console
For negative number print: “exact number is negative”
For positive number print whether it is odd or even: e.g “exact number is odd”
For strings print: “Only numbers are supported”
'''


x = input('Enter your number: ')

try:
    x = int(x)
except ValueError:
    print('Only integers are supported')
else:
    if x < 0:
        print(f'{x} is negative')
    elif x % 2 == 0:
        print(f'{x} is even')
    else:
        print(f'{x} is odd')
