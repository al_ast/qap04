"""
Реализовать калькулятор с 4 методами:
Сумма, вычитаниеб , умножение, деление.
Метод принимает 2 аргумента и возвращает результат.
Невалидные данные должны быть обработаны
"""


class Calculator:
    def validate_numbers(self, a, b):
        is_valid_number_a = isinstance(a, int)
        is_valid_number_b = isinstance(b, int)

        if not is_valid_number_a or not is_valid_number_b:
            raise Exception("Not valid number")

    def summ(self, a, b):
        self.validate_numbers(a, b)
        print(a+b)

    def sub(self, a, b):
        self.validate_numbers(a, b)
        return a - b


calc = Calculator()
calc.summ(1,2)
