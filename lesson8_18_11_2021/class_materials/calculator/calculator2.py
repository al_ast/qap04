"""
# Реализовать калькулятор с 4 методами:
# Сумма, вычитаниеб , умножение, деление.
# Метод принимает 2 аргумента и возвращает результат.
# Невалидные данные должны быть обработаны
"""


class Calculator:
    def is_valid(self, a, b):
        is_valid_number_a = isinstance(a, int) or isinstance(a, float)
        is_valid_number_b = isinstance(b, int) or isinstance(b, float)

        return is_valid_number_a and is_valid_number_b

    def summ(self, a, b):
        if self.is_valid(a, b):
            return a + b

    def subtract(self, a, b):
        if self.is_valid(a, b):
            return a - b
