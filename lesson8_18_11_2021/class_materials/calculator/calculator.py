"""
Реализовать калькулятор с 4 методами:
Сумма, вычитаниеб , умножение, деление.
Метод принимает 2 аргумента и возвращает результат.
Невалидные данные должны быть обработаны
"""


class Calculator:
    def valdate_numbers(self, number1, number2):
        is_valid_number_1 = isinstance(number1, int) \
                            or isinstance(number1, float)

        is_valid_number_2 = isinstance(number2, int) \
                            or isinstance(number2, float)

        if is_valid_number_1 and is_valid_number_2:
            print("numbers are valid")
        else:
            raise Exception("Not valid numbers")

    def summ(self, a, b):
        self.valdate_numbers(a, b)

        return a + b

    def subtract(self, a, b):
        self.valdate_numbers(a, b)

        return a - b

    def multiply(self, a, b):
        self.valdate_numbers(a, b)

        return a * b


calculator = Calculator()
print(calculator.summ(2, 1))
print(calculator.subtract(2, 1))
print(calculator.multiply(2, 3))
