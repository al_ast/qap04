"""
Вектор
Реализуйте класс, описывающий математический вектор размерности n.

Примеры:
Создание и базовые арифметические операции (сложение, вычитание, умножение)

#>>> Vector(1, 2)
Vector(1, 2)
#>>> Vector(5, 6) + Vector(1, 3)
Vector(6, 9)

Оператор @ для скалярного умножения
#>>> Vector(1, 2) @ Vector(9, 2)
11

Оператор * для умножения на скаляр и векторного умножения
#>>> Vector(2, 3, -1) * Vector(5, 1, 5)
Vector(16, -15, -13)
_________________________________________
"""


class Vector:
    def __init__(self, *args):
        # Можно найти решение получше
        # Преорбразовываем координаты в список
        # Даже если на вход придет кортеж из спика
        if isinstance(args[0], list):
            self.coordinates = args[0]
        else:
            self.coordinates = list(args)

    def add_vector(self, vector):
        if len(self.coordinates) != len(vector.coordinates):
            raise Exception("Vectors has different length")

        coordinates = []
        for index in range(len(self.coordinates)):
            summ = vector.coordinates[index] + self.coordinates[index]
            coordinates.append(summ)

        return Vector(coordinates)

    def scalar_product(self, vector):
        if len(self.coordinates) != len(vector.coordinates):
            raise Exception("Vectors has different length")

        result = 0
        for index in range(len(self.coordinates)):
            result += self.coordinates[index] * vector.coordinates[index]

        # ТОже самое но без индекса. Это вариант предпочтителниее
        # Можешьп почитать про zip
        for v1_coordinate, v2_coordinate in zip(self.coordinates,
                                                vector.coordinates):
            result += v1_coordinate * v2_coordinate

        return result

    def mult_vector_scalar(self, scalar):
        coordinates = []
        for coordinate in self.coordinates:
            coordinates.append(coordinate * scalar)

        return Vector(coordinates)


vector1 = Vector(1, 2)
vector2 = Vector(3, 4)
print(vector1.scalar_product(vector2))

vector3 = vector1.add_vector(vector2)
print(vector3.coordinates)

# vector1.add_vector(Vector(3, 4, 5))
vector4 = vector1.mult_vector_scalar(3)
print(vector4.coordinates)
