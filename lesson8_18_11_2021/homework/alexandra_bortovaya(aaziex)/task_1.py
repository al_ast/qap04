"""
Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы,
успеваемость (массив из пяти элементов).

Создать класс School:
Добавить возможно для добавления студентов в школу
Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
Добавить возможность вывода учеников заданной группы
Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)
"""


class Students:
    def __init__(self, surname, initials, group, marks):
        self.surname = surname
        self.initials = initials
        self.group = group
        self.marks = marks


class School:
    def __init__(self, school_students):
        self.school_students = school_students

    def add_students(self, new_student):
        self.school_students.append(new_student)

    def specific_marks(self):
        print(f"Students that have only 5 and 6 marks:")
        for student in self.school_students:
            marks_list = student.marks
            if marks_list.count(5) + marks_list.count(6) == len(marks_list):
                print(f"\t{student.surname} {student.initials}, group #{student.group}")

    def print_by_group(self, group):
        print(f"Students from group {group}:")
        for student in self.school_students:
            if group == student.group:
                print(f"\t{student.surname} {student.initials}")

    def automatic_exam(self):
        print("Students with average mark >=7:")
        for student in self.school_students:
            average = sum(student.marks) / len(student.marks)
            if average >= 7:
                print(f"\t{student.surname} {student.initials}")


student1 = Students('Smith', 'JD', 'QAP04', [7, 7, 9, 2, 7])
student2 = Students('Brown', 'LT', 'QAP05', [5, 5, 6, 5, 5])
student3 = Students('Hayes', 'MP', 'QAP03', [7, 7, 5, 10, 7])
student4 = Students('Fitzgerald', 'AH', 'QAP04', [10, 7, 9, 2, 7])

our_school_students = []
our_school = School(our_school_students)

our_school.add_students(student1)
our_school.add_students(student2)
our_school.add_students(student3)
our_school.add_students(student4)
our_school.print_by_group('QAP04')
our_school.automatic_exam()
our_school.specific_marks()
print("\n")

student5 = Students('Shepherd', 'JD', 'QAP04', [7, 7, 9, 2, 7])
student6 = Students('Fowler', 'LT', 'QAP05', [5, 5, 6, 5, 5])
student7 = Students('Barlow', 'MP', 'QAP03', [7, 7, 5, 10, 7])

another_school_students = []
another_school = School(another_school_students)

another_school.add_students(student5)
another_school.add_students(student6)
another_school.add_students(student7)
another_school.print_by_group('QAP04')
another_school.automatic_exam()
another_school.specific_marks()
