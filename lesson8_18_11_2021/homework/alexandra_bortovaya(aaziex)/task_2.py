"""
Вектор
Реализуйте класс, описывающий математический вектор размерности n.

Примеры:
Создание и базовые арифметические операции (сложение, вычитание, умножение)

#>>> Vector(1, 2)
Vector(1, 2)
#>>> Vector(5, 6) + Vector(1, 3)
Vector(6, 9)

Оператор @ для скалярного умножения
#>>> Vector(1, 2) @ Vector(9, 2)
11

Оператор * для умножения на скаляр и векторного умножения
#>>> Vector(2, 3, -1) * Vector(5, 1, 5)
Vector(16, -15, -13)
_________________________________________
"""


class Vector:
    def add_vector(self, *args):
        vector = []
        for arg in args:
            vector.append(arg)
        return vector

    def sum_vectors(self, v_1, v_2):
        sum_vector = []
        index = 0
        while index < len(v_1):
            sum_vector.append(v_1[index] + v_2[index])
            index += 1
        return sum_vector

    def scalar_product(self, v_1, v_2):
        index = 0
        scalar_vectors = 0
        while index < len(v_1):
            scalar_vectors += v_1[index] * v_2[index]
            index += 1
        return scalar_vectors

    def mult_vector_scalar(self, v_1, scalar):
        index = 0
        multiplied_scalar = []
        while index < len(v_1):
            multiplied_scalar.append(v_1[index] * scalar)
            index += 1
        return multiplied_scalar


vector_use = Vector()
vector_1 = vector_use.add_vector(2, 3, 4)
vector_2 = vector_use.add_vector(6, 1, 3)
print(f"{vector_1=}")
print(f"{vector_2=}")

sum = vector_use.sum_vectors(vector_1, vector_2)
print(f"{vector_1} + {vector_2} = {sum}")

scalar = vector_use.scalar_product(vector_1, vector_2)
print(f"{vector_1} @ {vector_2} = {scalar}")

mult_scalar = 5
mult_scalar_to_vector = vector_use.mult_vector_scalar(vector_1, mult_scalar)
print(f"{vector_1} * {mult_scalar} = {mult_scalar_to_vector}")
