"""
Создайте класс Students, содержащий поля: фамилия, номер группы,
успеваемость (массив из пяти элементов).

Создать класс School:
Добавить возможность для добавления студентов в школу
Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
Добавить возможность вывода учеников заданной группы
Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)"""


class Student:
    def __init__(self, surname, number_group, marks):
        self.surname = surname
        self.number_group = number_group
        self.marks = marks

    def __repr__(self):
        return f'{self.surname}, {self.number_group}, {self.marks}'


class School:
    def __init__(self, students):
        self.students = students

    def add_new_student(self, student):
        self.students.append(student)

    def students_marks_only(self, marks):
        students = self.students.copy()
        for student in self.students:
            for mark in student.marks:
                if mark not in marks:
                    students.remove(student)
                    break

        return students

    def get_students_by_group(self, number_group):
        students = []
        for student in self.students:
            if student.number_group == number_group:
                students.append(student)

        return students

    def passes_without_exam(self, mark=7):
        students = []
        for student in self.students:
            student_average = sum(student.marks) / len(student.marks)
            if student_average >= mark:
                students.append(student)

        return students


student1 = Student("Petrov", "QAP", [8, 7, 8, 9, 7])
student2 = Student("Ivanov", "JA", [5, 5, 6, 5, 6])
student3 = Student("Smith", "MG", [7, 7, 7, 7, 7])
student4 = Student("Stown", "MG", [6, 6, 5, 5, 6])

school1 = School([student1, student2, student3])
print(school1.students)

school1.add_new_student(student4)
print(school1.students)

students_with_grade_5_and_6_only = school1.students_marks_only([5, 6])
print(students_with_grade_5_and_6_only)

students_with_group = school1.get_students_by_group("MG")
print(students_with_group)

students_with_grade_7_only = school1.students_marks_only([7])
print(students_with_grade_7_only)

print(school1.passes_without_exam())
