"""
Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы,
успеваемость (массив из пяти элементов).

Создать класс School:

Добавить возможно для добавления студентов в школу
Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
Добавить возможность вывода учеников заданной группы
Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)
"""


class Students:

    def __init__(self, last_name, init, group_num, marks):
        self.last_name = last_name
        self.init = init
        self.group_num = group_num
        self.marks = marks


class School:

    def __init__(self, school_students):
        self.school_students = school_students

    def add_student(self, new_student):
        self.school_students.append(new_student)

    def students_with_marks_5_and_6(self):
        print('Students which have 5 or 6')
        for student in self.school_students:
            if student.marks.count(5) + student.marks.count(6) == len(student.marks):
                print(f'{student.last_name, student.group_num}')

    def group_students(self, group):
        print('\nStudents in one group:')
        for student in self.school_students:
            if group == student.group_num:
                print(f'{student.last_name, student.init}')

    def students_automate(self):
        print('\nStudents candidate on automate exam')
        for student in self.school_students:
            if sum(student.marks) / len(student.marks) >= 7:
                print(f'{student.last_name, student.init}')


student1 = Students('Ivanov', 'II', 'QA42', [6, 6, 7, 4, 6])
student2 = Students('Petrov', 'OV', 'QA42', [6, 10, 7, 7, 6])
student3 = Students('Kennedy', 'AG', 'QA20', [6, 6, 6, 6, 5])
student4 = Students('Smirnov', 'BJ', 'QA21', [6, 5, 5, 5, 5])
student5 = Students('Leon', 'HY', 'QA21', [6, 6, 6, 5, 5])
student6 = Students('Morozov', 'TR', 'QA42', [7, 8, 7, 8, 7])
student7 = Students('Pavlov', 'NN', 'QA20', [8, 9, 7, 6, 7])

students = [student1, student2, student3, student4, student5, student6]

school1 = School(students)
school1.add_student(student7)
school1.students_with_marks_5_and_6()
school1.students_automate()
school1.group_students("QA42")
