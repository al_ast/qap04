class Students:
    def __init__(self, surname, name, number_of_group, progress):
        self.surname = surname
        self.name = name
        self.number_of_group = number_of_group
        self.progress = progress

    def __repr__(self):
        return f"{self.name}"


class School:
    def __init__(self, students):
        self.my_students = students

    def add_students(self, std):
        self.my_students.extend(std)


    def print_students_with_group(self, number_group):
        for student in self.my_students:
            if student.number_of_group == number_group:
                print(student)

    def print_progress(self, name):
        for student in self.my_students:
            if student.name == name:
                print(student.progress)

     # def print_students_with_marks_7_or_8(self,):
     #     for student in self.my_students:
     #         for mark in student.progress:
     #             if mark not in list:
     #                 student.remove(student)
     #                 break
     #                 print(student.progress)
    def students_with_automate(self):
        print(f'Students candidate on automate ')
        for student in self.my_students:
            if sum(student.progress) / len(student.progress) > 7:
                print(student.progress)
