class Student:
    def __init__(self, last_name, initials, number_group, progress):
        self.last_name = last_name
        self.initials = initials
        self.number_group = number_group
        self.progress = progress

student_1 = Student("Karavaev", "P.Y.", 10, [5, 5, 5, 6, 6, 6])
student_2 = Student("Varon'ka", "E.A.", 12, [6, 3, 3, 2, 1, 2])
student_3 = Student("Voinilko", "D.M.", 12, [10, 10, 8, 9, 10, 10])
class School:
    def __init__(self, list_of_student):
        self.list_of_student = list_of_student
    def add_student(self, student):
        self.list_of_student.append(student)
    def print_Student_with_5_or_6(self):
        for student in self.list_of_student:
            if (student.progress).count(5) + (student.progress).count(6) == 6:
                print(f"{student.last_name} in group {student.number_group} have grade 5 or 6")
    def print_student_in_group(self, number):
        for student in self.list_of_student:
            if student.number_group == number:
                print(f"{student.last_name} in group {number}")
    def print_student_with_automate(self):
        for student in self.list_of_student:
            if sum(student.progress) / len(student.progress) >= 7:
                print(f"student with qrade 7 or more: {student.last_name}")

school_1 = School([])
school_1.add_student(student_1)
school_1.add_student(student_2)
school_1.add_student(student_3)

school_1.print_Student_with_5_or_6()

school_1.print_student_in_group(10)

school_1.print_student_with_automate()
