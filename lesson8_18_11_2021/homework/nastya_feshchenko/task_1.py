# Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы, успеваемость (массив из пяти элементов).
class Student:
    def __init__(self, first_name, last_name, group, marks):
        self.first_name = first_name
        self.last_name = last_name
        self.group = group
        self.marks = marks

student1 = Student("Nastya", "Feshchenko", "QAP04", [10, 9, 8, 7, 8])
student2 = Student("Sasha", "Zhukovskaya", "BA01", [9, 9, 7, 7])
marks = student1.marks
studentlist = [student1, student2]


# Создать класс School:
class School:
    def __init__(self, list_students):
        self.list_students = list_students

    # Добавить возможность для добавления студентов в школу
    def add_student(self, student):
        studentlist.append(student)

    # Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
    def marks_5_or_6(self):
        for student in self.list_students:
            if (student.marks).count(5) + (student.marks).count(6) == 5:
                print(f"{student.last_name} из группы {student.group} имеет оценки 5 и 6.")

    # Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)
    def high_average_score(self):
        for student in self.list_students:
            if sum(student.marks) / float(len(student.marks)) >= 7:
                print(f'Средний балл >= 7 имеет студент: {student.last_name}')

school1 = School(studentlist)

student3 = Student("Anna", "Ivanova", "QAP04", [8, 5, 8, 6, 5, 6])
student4 = Student("Katerina", "Petrova", "BA01", [9, 4, 6, 9, 9])
student5 = Student("Caroline", "Forbs", "BA01", [6, 6, 6, 6, 6])

school1.add_student(student3)
school1.add_student(student4)
school1.add_student(student5)
print(school1.list_students)

for student in school1.list_students:
    print(student.first_name)

# Добавить возможность вывода учеников заданной группы
     # группа QAP04
for student in school1.list_students:
    if student.group == "QAP04":
        print(student.first_name)

    #группа BA01
for student in school1.list_students:
    if student.group == "BA01":
        print(student.first_name)

school1.marks_5_or_6()
school1.high_average_score()


