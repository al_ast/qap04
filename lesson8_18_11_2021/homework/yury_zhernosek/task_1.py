"""
Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы,
успеваемость (массив из пяти элементов).

Создать класс School:

Добавить возможно для добавления студентов в школу
Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
Добавить возможность вывода учеников заданной группы
Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)

"""

class Students:
    def __init__(self, last_name, initials, number_group, progress):
        self.last_name = last_name
        self.initials = initials
        self.number_group = number_group
        self.progress = progress

class Scholl:
    def __init__(self, list_students):
        self.list_students = list_students

    def add_student(self,student):
        self.list_students.append(student)

    def print_only_5_or_6(self):
        for student in self.list_students:
            if (student.progress).count(5) + (student.progress).count(6) == 5:
                print(f'{student.last_name} in group {student.number_group} have only 5 or 6')

    def print_students_in_group(self,num):
        for student in self.list_students:
            if student.number_group == num:
                print(f'{student.last_name} in group {num}')

    def print_good_students(self):
        for student in self.list_students:
            if sum(student.progress) / float(len(student.progress)) >= 7:
                print(f'Average score >= 7: {student.last_name}')

student1 = Students('Zhernosek','Y.V',5,[1,3,7,2,5])
student2 = Students('Koval','D.F',2,[10,10,10,10,9])
student3 = Students('Poupkou','A.N',1,[6,6,6,6,6])
student4 = Students('Wertinskiy','D.M',4,[5,5,5,5,5])
student5 = Students('Shklowski','J.D',4,[9,9,9,9,9])

school_1 = Scholl([])
school_1.add_student(student1)
school_1.add_student(student2)
school_1.add_student(student3)
school_1.add_student(student4)
school_1.add_student(student5)
school_1.print_only_5_or_6()
school_1.print_students_in_group(2)
school_1.print_good_students()