# """
# Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы,
# успеваемость (массив из пяти элементов).
#
# Создать класс School:
# Добавить возможно для добавления студентов в школу
# Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
# Добавить возможность вывода учеников заданной группы
# Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)
# """


class Students:
    def __init__(self, lastname, group, marks):

        self.lastname = lastname
        self.group = group
        self.marks = marks


class School:
    def __init__(self, students):
        self.students = students

    def add_student(self, new_student):
        self.students.append(new_student)

    def filter_on_marks(self, first_mark, second_mark):
        print(f'Students that have marks {first_mark} or {second_mark}: ')
        for student in self.students:
            marks_list = student.marks
            if marks_list.count(first_mark) + marks_list.count(second_mark) == len(marks_list):
                print(f"{student.lastname}, group {student.group}")

    def filter_on_group(self, group):
        print(f'Students from group {group}: ')
        for student in self.students:
            if group == student.group:
                print(f'{student.lastname}')

    def not_passing_exam(self, auto_mark):
        print('Students that are not passing through exam during their average marks: ')
        for student in self.students:
            average = sum(student.marks)/len(student.marks)
            if average >= auto_mark:
                print(f'{student.lastname}, group {student.group}')


print(70*'_', '\n')

student1 = Students('A.M. Ivanov', '11', [4, 5, 2, 1, 9])
student2 = Students('K.L. Bear', '12', [6, 5, 5, 6, 6])
student3 = Students('A.T. Pine', '12', [8, 9, 6, 8, 9])

students = [student1]
school = School(students)

school.add_student(student2)
school.add_student(student3)

school.filter_on_marks(5, 6)
school.filter_on_group('11')
school.not_passing_exam(7)

print(70*'_', '\n')

student5 = Students('K.K. Petrov', '11', [7, 8, 8, 8, 7])
student6 = Students('W.T. Johnson', '12', [6, 5, 5, 5, 5])
student7 = Students('F.S. Riddle', '11', [10, 9, 10, 9, 10])

more_students = [student5, student6]
second_school = School(more_students)

second_school.add_student(student7)

second_school.filter_on_marks(7, 8)
second_school.filter_on_group('12')
second_school.not_passing_exam(9)
