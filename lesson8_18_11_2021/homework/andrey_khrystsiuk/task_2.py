class Vector:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f"|{str(self.x)} , {str(self.y)} , {str(self.z)}|"

    def __add__(self, add):
        return Vector(self.x+add.x, self.y+add.y, self.z+add.z)

    def __sub__(self, sub):
        return Vector(self.x-sub.x, self.y-sub.y, self.z-sub.z)

    def __mul__(self, mul):
        return Vector(self.x * mul.x, self.y * mul.y, self.z * mul.z)

    def __matmul__(self, matmul):
        return


vector1 = Vector(1, 10, 15)
vector2 = Vector(0, 10, 20)
vector3 = Vector(-10, 1, 10)

print(vector1 + vector2)
print(vector1 - vector3)
print(vector1 * vector2)
# print(vector1 @ vector3) I've tried to realise how to make matrix multiply without any side libraries, but didn't
# found any useful info on this topic, so i want to understand how to make the right method of solving this



