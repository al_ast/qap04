from statistics import mean
# Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы
# успеваемость (массив из пяти элементов).
#
# Создать класс School:
#
# Добавить возможно для добавления студентов в школу
# Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
# Добавить возможность вывода учеников заданной группы
# Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)
class Students:
    def __init__(self, surname, number_of_group, rating):
        self.surname = surname
        self.number_of_group = number_of_group
        self.rating = rating

student1 = Students('Petrov A.C.', 1, [5, 5, 5, 5, 5])
student2 = Students('Ivanova S.G', 1, [8, 10, 10, 7, 9])
student3 = Students('Babaev I.I', 2, [8, 8, 9, 9, 9])
student4 = Students('Sidorova E.U.', 2, [5, 6, 6, 5, 5])
students = [student1, student2, student3, student4]


class School:
    def __init__(self, school_students):
        self.school_students = school_students

    def append_to_school(self, students):
        self.school_students.extend(students)

    def print_surname(self):
        for i in self.school_students:
            marks_list = i.rating
            if marks_list.count(5) + marks_list.count(6) == len(marks_list):
                print(f'Scores equal only 5 or 6 - {i.surname}, {i.number_of_group}')

    def print_group(self, school_group):
        self.school_group = school_group
        for i in self.school_students:
            if i.number_of_group == school_group:
                print(f'Students of {i.number_of_group} group {i.surname}')

    def automat(self):
        for i in self.school_students:
            if mean(i.rating) >=7:
                print(f'students with an average score > 7 : {i.surname}')


school_141 = School([])
school_141.append_to_school([student1, student2, student3, student4])
school_141.print_surname()
school_141.print_group(2)
school_141.automat()

