# Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы,
# успеваемость (массив из пяти элементов).
#
# Создать класс School:
#
# Добавить возможно для добавления студентов в школу
# Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
# Добавить возможность вывода учеников заданной группы
# Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)

class Student:
    def __init__(self, name, group_number, marks):
        self.name = name
        self.group_number = group_number
        self.progress = marks

    def __repr__(self):
        return f'{self.name}, {self.group_number}, {self.progress}'


class School:
    def __init__(self, students):
        self.students = students

    def add(self, student):
        self.students.append(student)

    def get_students_with_grades_only(self, grades):
        students = self.students.copy()
        for student in self.students:
            for mark in student.progress:
                # Проверяем, есть ли хотя бы одна оценка не равная ожидаемому
                # Если есть хотя бы одна неподходящая оценка, то этот студент
                # нам не подходит
                if mark not in grades:
                    students.remove(student)
                    break

        return students

    def get_students_by_group(self, group_number):
        students = []
        for student in self.students:
            if student.group_number == group_number:
                students.append(student)

        return students

    def get_students_no_need_to_pass_exam_based_on_average_mark(self, mark=7):
        students = []
        for student in self.students:
            student_average = sum(student.progress) / len(student.progress)
            if student_average >= mark:
                students.append(student)

        return students


student1 = Student('Yana Shafar', '974003', [7, 7, 7, 7, 7])
student2 = Student('Ksusha Voskr', '974004', [9, 9, 8, 9, 8])
student3 = Student('Anna Smirn', '974001', [6, 5, 5, 5, 6])

print(f"Информация о студенте с использованием repr:{student1}")
print(f"Информация о студенте с использованием repr:{student2}")
print(f"Информация о студенте с использованием repr:{student2}")

school1 = School([student1, student2])
print(f"Информация о студентах по школе:{school1.students}")

school1.add(student3)
print(f"Информация о студентах по школе:{school1.students}")

students_with_grade_5_and_6_only = school1.get_students_with_grades_only([5, 6])
students_wth_grade_7_only = school1.get_students_with_grades_only([7])

print(f"Students with 5 and 6 only:{students_with_grade_5_and_6_only}")
print(f"Students with 7 only:{students_wth_grade_7_only}")

students_with_group = school1.get_students_by_group('974003')
print(f"974003 group:{students_with_group}")

print(f"974004 group:{school1.get_students_by_group('974004')}")

print(school1.get_students_no_need_to_pass_exam_based_on_average_mark(mark=6))
