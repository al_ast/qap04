# Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы,
# успеваемость (массив из пяти элементов).
#
# Создать класс School:
#
# Добавить возможно для добавления студентов в школу
# Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
# Добавить возможность вывода учеников заданной группы
# Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)

class Student:
    def __init__(self, name, group_number, progress):
        self.name = name
        self.group_number = group_number
        self.progress = progress

    def __repr__(self):
        return f"{self.name}"

    def print_info(self):
        print(f'Информация о студенте: {self.name}, {self.group_number}, {self.progress}')

    def print_name(self):
        print(f'Имя студента: {self.name}')

    def print_group(self):
        print(f'Группа: {self.group_number}')

    def print_progress(self):
        print(f'Успеваемость: {self.progress}')

    # def student_automat(self):


class School:
    def __init__(self, students):
        self.students = students

    def add(self, student):
        self.students.append(student)

    def grades_5_or_6(self):
        for student in self.students:
            if (5 in student.progress) or (6 in student.progress):
                print(f'{student.name} {student.group_number}')

    def get_student_group(self, group_number):
        for student in self.students:
            if student.group_number == group_number:
                print(f'{student.name}')

    def student_automat(self):
        for student in self.students:
            student_average = sum(student.progress)/len(student.progress)
            if student_average >= 7:
                print(f'{student.name}')


student1 = Student('Yana Shafar', '974003', [8, 9, 8, 8, 8])
student2 = Student('Ksusha Voskr', '974004', [9, 9, 8, 9, 8])
student3 = Student('Anna Smirn', '974001', [6, 5, 5, 5, 6])

student1.print_info()
student2.print_info()
student1.print_name()
student2.print_group()
student2.print_progress()
student2.print_progress()

school1 = School([student1, student2])
print(school1.students)

school1.add(student3)
print(school1.students)

school1.grades_5_or_6()

school1.get_student_group('974003')
school1.get_student_group('974004')

school1.student_automat()


