"""
https://restful-booker.herokuapp.com/apidoc/index.html
"""

import json

import requests

# # GET
# url = "https://restful-booker.herokuapp.com/booking"
# response = requests.get(url)
# data = json.loads(response.text)
#
# url = "https://restful-booker.herokuapp.com/booking/1"
# response = requests.get(url)
# data1 = json.loads(response.text)

# # POST
# url = "https://restful-booker.herokuapp.com/auth"
# data = {
#     "username": "admin",
#     "password": "password123"
# }
# response = requests.post(url, data)
# data2 = json.loads(response.text)

url = "https://restful-booker.herokuapp.com/booking"
data = {
    "firstname": "Jimq",
    "lastname": "Brown",
    "totalprice": 111,
    "depositpaid": True,
    "bookingdates": {
        "checkin": "2018-01-01",
        "checkout": "2019-01-01"
    },
    "additionalneeds": "Breakfast"
}

response = requests.post(url, data)
data3 = json.loads(response.text)
a = 0

# url = "https://restful-booker.herokuapp.com/booking/1"
# response = requests.get(url)
# data1 = json.loads(response.text)
