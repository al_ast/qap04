#https://reqres.in

import requests
import json


class TestAPI:
    def test_user_email(self):
        url = "https://reqres.in/api/users/1"
        data = {
            "email": "george.bluth@reqres.in"
        }
        response = requests.get(url, data=data)
        data_response = json.loads(response.text)
        user_email = data_response["data"]["email"]

        assert user_email == "george.bluth@reqres.in", "This user's email is 'george.bluth@reqres.in'"
        assert response.status_code == 200, f'Expected code 200, got: {response.status_code}.'

    def test_user_first_and_last_name(self):
        url = "https://reqres.in/api/users/1"
        data = {
            "first_name": "George",
            "last_name": "Bluth"
        }
        response = requests.get(url, data=data)
        data_response = json.loads(response.text)
        user_first_name = data_response["data"]["first_name"]
        user_last_name = data_response["data"]["last_name"]
        assert user_first_name == "George" and user_last_name == "Bluth", "Wrong user name or last name"

    def test_user_not_exist(self):
        url = "https://reqres.in/api/users/100"
        response = requests.get(url)

        assert response.status_code == 404, f'Expected code 404, got {response.status_code}.'

    def test_create_user(self):
        url = "https://reqres.in/api/users"
        data = {
            "first_name": "Andrew",
            "avatar": "https://reqres.in/img/faces/5-image.jpg",
        }

        response = requests.post(url, data)
        assert response.status_code == 201, f'Expected code 201, got {response.status_code}.'

    def test_login(self):
        url = "https://reqres.in/api/login"
        data = {
            "email": "eve.holt@reqres.in",
            "password": "aquasky123"}

        response = requests.post(url, data)
        data_response = json.loads(response.text)
        assert data_response.get('token'), f"Got no token"
