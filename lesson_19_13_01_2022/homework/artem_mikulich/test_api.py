import requests
import json

class TestAPI:

    def test_create_user(self):
        url = "https://reqres.in/api/users"
        request_data = {
            "name": "morpheus",
            "job": "leader"
        }

        response = requests.post(url, request_data)

        assert response.status_code == 201, f"expected status code of server 201  but was {response.status_code}"

        response_data = json.loads(response.text)

        assert len(response_data) == 4, f"Expected {response_data} with the number of fields 4 but was {len(response_data)}"

    def test_user_name(self):
        url = "https://reqres.in/api/users"
        request_data = {
            "name": "morpheus",
            "job": "leader"
            }

        response = requests.post(url, request_data)
        response_data = json.loads(response.text)

        assert response_data["name"] == 'morpheus', f"expected name morpheus but was {response_data['name']}"


    def test_booking_status_code(self):
        url = "https://restful-booker.herokuapp.com/booking"
        response = requests.get(url)

        assert response.status_code == 200, f"expected status code of server 200 but was: {response.status_code}"

    def test_single_user_first_name(self):
        url = "https://reqres.in/api/users/2"
        response = requests.get(url)
        response_data = json.loads(response.text)
        response_data_first_name = response_data["data"]["first_name"]

        assert response_data_first_name == "Janet", "must have a name Janet"

    def test_single_user_last_name(self):
        url = "https://reqres.in/api/users/2"
        response = requests.get(url)
        response_data = json.loads(response.text)
        response_data_last_name = response_data["data"]["last_name"]

        assert response_data_last_name == "Weaver", "must have a last name Weaver"

    def test_id(self):
        user_id = 4
        url = "https://reqres.in/api/register"
        request_data = {
            "email": "eve.holt@reqres.in",
            "password": "pistol"
            }
        response = requests.post(url, request_data)
        response_data = json.loads(response.text)
        response_data_id = response_data['id']
        assert user_id == response_data_id, f"expected {user_id} but was {response_data_id}"

    def test_token(self):
        url = "https://reqres.in/api/login"
        request_data = {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
            }
        response = requests.post(url, request_data)
        response_data = json.loads(response.text)

        assert response_data.get("token"), f"expected token, but was {response_data.get('token')}"