import requests
import json


class TestApplicationProgrammingInterface:
    def test_login_with_valid_credentials(self):
        url = 'https://reqres.in/api/register'
        valid_credentials = {
            "email": "eve.holt@reqres.in",
            "password": "pistol"
        }
        response = requests.post(url=url, data=valid_credentials)
        assert response.status_code == 200, f"Expected status code = 200, but was {response.status_code}"

    def test_login_with_invalid_credentials(self):
        url = 'https://reqres.in/api/register'
        invalid_credentials = {
            "email": "sydney@fife"
        }
        response = requests.post(url=url, data=invalid_credentials)
        assert response.status_code == 400, "Expected status code = 400  but was {response.status_code}"

    def test_status_code(self):
        url = "https://restful-booker.herokuapp.com/booking"
        response = requests.get(url)

        assert response.status_code == 200, f"Expected status code: 200 but was {response.status_code}"

    def test_user_name(self):
        url = "https://reqres.in/api/users"
        request_data = {
            "name": "morpheus",
            "job": "leader"
        }
        response = requests.post(url, request_data)
        response_data = json.loads(response.text)
        assert response_data["name"] == 'morpheus', f"Expected name morpheus, but was {response_data['name']}"

    def test_non_existent_user(self):
        url = "https://reqres.in/api/users/23"
        response = requests.get(url)

        assert response.status_code == 404, f'Expected code: 404, but was {response.status_code}.'