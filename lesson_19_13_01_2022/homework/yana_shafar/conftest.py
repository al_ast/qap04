import pytest


@pytest.fixture()
def valid_credentials():
    return {
        "email": "eve.holt@reqres.in",
        "password": "pistol"
    }


@pytest.fixture()
def invalid_credentials():
    return {
        "email": "sydney@fife"
    }


@pytest.fixture()
def user_data():
    return {
        "name": "morpheus",
        "job": "leader"
    }


@pytest.fixture()
def login_credentials():
    return {
        "email": "eve.holt@reqres.in",
        "password": "cityslicka"
    }
