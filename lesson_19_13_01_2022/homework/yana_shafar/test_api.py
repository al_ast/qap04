import requests


class TestApi:
    def test_register_valid_user(self, valid_credentials):
        url = 'https://reqres.in/api/register'

        response = requests.post(url=url, data=valid_credentials)

        assert response.status_code == 200, f'Expected 200,but was: {response.status_code}'
        assert len(response.json()) == 2, f'Expected 2, but was: {len(response.json())}'

    def test_register_invalid_user(self, invalid_credentials):
        url = 'https://reqres.in/api/register'

        response = requests.post(url=url, data=invalid_credentials)

        assert response.status_code == 400, f'Expected 400,but was: {response.status_code}'

    def test_user_list_with_pagination(self):
        page = 2
        url = f'https://reqres.in/api/users?page={page}'

        response = requests.get(url=url)

        assert response.status_code == 200, f'Expected 200,but was: {response.status_code}'
        assert page == response.json().get('page'), f'Expected {page}, but was {response.json().get("page")}'
        assert response.json().get('per_page') == len(response.json().get('data')), \
            f'Expected {response.json().get("per_page")}, but was {len(response.json().get("data"))}'

    def test_get_single_user(self):
        user_id = 2
        url = f'https://reqres.in/api/users/{user_id}'

        response = requests.get(url=url)

        assert response.status_code == 200, f'Expected 200,but was: {response.status_code}'
        assert user_id == response.json().get('data').get('id'), \
            f"Expected {user_id}, but was {response.json().get('data').get('id')}"

    def test_create_user(self, user_data):
        url = 'https://reqres.in/api/users'

        response = requests.post(url=url, data=user_data)

        assert response.status_code == 201, f'Expected 201,but was: {response.status_code}'
        assert user_data.get('name') == response.json().get('name'), \
            f"Expected {user_data.get('name')}, but was {response.json().get('name')}"
        assert user_data.get('job') == response.json().get('job'), \
            f"Expected {user_data.get('job')}, but was {response.json().get('job')}"

    def test_login_user(self, login_credentials):
        url = 'https://reqres.in/api/login'

        response = requests.post(url=url, data=login_credentials)

        assert response.status_code == 200, f'Expected 200,but was: {response.status_code}'
        assert response.json().get('token'), f"Expected token, but was: {response.json().get('token')}"
