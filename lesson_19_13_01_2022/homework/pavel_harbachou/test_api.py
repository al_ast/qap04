# Написать как можно больше апи тестов(минимум 6) используя
# pytest и requests
# Сайт для тестирования https://reqres.in
import json
from datetime import timedelta, datetime
import requests


class TestAPI:
    def test_is_id_not_empty(self):
        response = requests.get('https://reqres.in/api/users?page=2')
        data = json.loads(response.text)
        list_of_id = []
        for i in data['data']:
            if i['id']:
                list_of_id.append(i['id'])
        assert len(list_of_id) > 0, "Can't be empty id"

    def test_is_avatar_correct(self):
        response = requests.get('https://reqres.in/api/users/2')
        data = json.loads(response.text)
        assert data['data']['avatar'] == "https://reqres.in/img/faces/2-image.jpg", 'Not correct avatar'

    def test_is_user_not_found(self):
        response = requests.get('https://reqres.in/api/users/23')
        assert response.status_code == 404, 'User must be not found'

    def test_is_created_time_true(self):
        actual_time = datetime.now() - timedelta(minutes=180)
        actual_time_in_view_for_comparison = actual_time.strftime('%Y-%m-%dT%H:%M:%S')
        credentials = {
            " name ": "morpheus",
            " job ": "leader"
        }
        response = requests.post('https://reqres.in/api/users', data=credentials)
        data = json.loads(response.text)
        assert actual_time_in_view_for_comparison in data['createdAt'], 'Time of create is wrong'

    def test_is_updated_time_true(self):
        actual_time = datetime.now() - timedelta(minutes=180)
        actual_time_in_view_for_comparison = actual_time.strftime('%Y-%m-%dT%H:%M:%S')
        credentials = {
            "name": "morpheus",
            "job": "resident"
        }
        response = requests.put('https://reqres.in/api/users/2', data=credentials)
        data = json.loads(response.text)
        assert actual_time_in_view_for_comparison in data['updatedAt'], 'Time of update is wrong'

    def test_login_with_valid_credentials(self):
        valid_credentials = {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
        }
        response = requests.post('https://reqres.in/api/login', data=valid_credentials)
        data = json.loads(response.text)
        assert data['token'] == 'QpwL5tke4Pnpja7X4', 'Login unsuccessful'
