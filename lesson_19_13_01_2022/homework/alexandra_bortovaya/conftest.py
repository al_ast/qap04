import pytest

from lesson_19_13_01_2022.homework.alexandra_bortovaya.booker_requests import BookerApiRequests


@pytest.fixture()
def booker_requests():
    return BookerApiRequests()


@pytest.fixture()
def get_existing_booking(booker_requests):
    new_booking = booker_requests.get_booking_ids()
    return new_booking[0]["bookingid"]
