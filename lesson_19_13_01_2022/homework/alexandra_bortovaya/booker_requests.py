import json

import requests


class BookerApiRequests:
    URL = "https://restful-booker.herokuapp.com{}"

    def create_token(self, username="admin", password="password123"):
        url = self.URL.format("/auth")
        data = {
            "username": username,
            "password": password
        }
        response = requests.post(url, data)
        return json.loads(response.text)

    def get_booking_ids(self):
        url = self.URL.format("/booking")
        response = requests.get(url)
        return json.loads(response.text)

    def get_booking(self, booking_id):
        url = self.URL.format(f"/booking/{booking_id}")
        response = requests.get(url)
        return json.loads(response.text)

    def create_booking(self,
                       firstname="Jim",
                       lastname="Brown",
                       totalprice=1000,
                       depositpaid=True,
                       checkin="2018-01-01",
                       checkout="2019-01-01",
                       additionalneeds="Breakfast"
                       ):
        url = self.URL.format("/booking")
        data = {
            "firstname": firstname,
            "lastname": lastname,
            "totalprice": totalprice,
            "depositpaid": depositpaid,
            "bookingdates": {
                "checkin": checkin,
                "checkout": checkout
            },
            "additionalneeds": additionalneeds
        }
        response = requests.post(url, data)
        return response, data

    def health_check(self):
        url = self.URL.format("/ping")
        response = requests.get(url)
        return response.status_code
