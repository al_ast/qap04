class TestBooker:
    def test_health_check(self, booker_requests):
        response_code = booker_requests.health_check()

        assert response_code == 201, f"Response code is different from expected ({response_code} instead of 201)"

    def test_create_token(self, booker_requests):
        response_data = booker_requests.create_token()

        assert "token" in response_data.keys(), f"No token was created, response data: {response_data}"

    def test_booking_list_is_not_empty(self, booker_requests):
        response_data = booker_requests.get_booking_ids()

        assert len(response_data) > 0, "Booking list is empty"

    def test_create_booking(self, booker_requests):
        response, sent_data = booker_requests.create_booking()
        response_code = response.status_code
        assert response_code == 200, f"Response code is different from expected ({response_code} instead of 200)"

        response_data = response.json()
        assert len(response_data.get("bookingid", "")) > 0, "Response doesn't contain booking id for newly created user"

    def test_response_params_for_create_booking(self, booker_requests):
        response, sent_data = booker_requests.create_booking(firstname="Morgan",
                                                             lastname="Duncan",
                                                             totalprice=2500,
                                                             depositpaid=True,
                                                             checkin="2021-03-01",
                                                             checkout="2021-03-21",
                                                             additionalneeds="Breakfast")
        response_code = response.status_code
        assert response_code == 200, f"Response code is different from expected ({response_code} instead of 200)"

        response_data = response.json()
        assert response_data == sent_data, "Response params are not equal to sent params"

    def test_get_booking(self, booker_requests, get_existing_booking):
        booking_id = get_existing_booking
        response_data = booker_requests.get_booking(booking_id)
        expected_keys = ["firstname", "lastname", "totalprice", "depositpaid", "bookingdates", "additionalneeds"]
        bookingdates_expected_keys = ["checkin", "checkout"]
        actual_keys = list(response_data.keys())
        bookingdates_actual_keys = list(response_data["bookingdates"].keys())

        assert expected_keys == actual_keys and bookingdates_expected_keys == bookingdates_actual_keys, \
            "Response does not contain all required booking parameters"
