# Написать как можно больше апи тестов(минимум 6) используя
# pytest и requests
# Сайт для тестирования https://reqres.in

import requests
import json

class TestAPI:
    def test_user_does_not_exist(self):
        url = "https://reqres.in/api/users/23"
        response = requests.get(url)

        assert response.status_code == 404, f'Expected code 404, but got code: {response.status_code}.'

    def test_single_user_first_name(self):
        url = "https://reqres.in/api/users/2"
        data = {
               "first_name": "Janet",
               "last_name": "Weaver"},
        response = requests.get(url, data=data)
        data_response = json.loads(response.text)
        first_name = data_response["data"]["first_name"]

        assert first_name == "Janet", "This user's first name should be Janet."
        assert response.status_code == 200, f'Expected code 200, but got code: {response.status_code}.'

    def test_single_resource(self):
        url = "https://reqres.in/api/unknown/2"
        data = {
                "id": 2,
                "name": "fuchsia rose"},

        response = requests.get(url, data=data)
        assert response.status_code == 200, f'Expected code 200, but got code: {response.status_code}.'

    def test_resource_does_not_exist(self):
        url = "https://reqres.in//api/unknown/23"
        response = requests.get(url)

        assert response.status_code == 404, f'Expected code 404, but got code: {response.status_code}.'

    def test_create_user(self):
        url = "https://reqres.in/api/users"
        data = {
            "name": "morpheus",
            "job": "leader"}

        response = requests.post(url, data)
        assert response.status_code == 201, f'Expected code 201, but got code: {response.status_code}.'

    def test_successful_login(self):
        url = "https://reqres.in/api/login"
        data = {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"}

        response = requests.post(url, data)
        data_response = json.loads(response.text)
        assert data_response.get('token'), f"Expected a token, but {data_response.get('token')} was given"
