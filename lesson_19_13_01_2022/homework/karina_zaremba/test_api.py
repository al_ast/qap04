import json
import requests


class TestAPI:
    def test_single_user_first_name(self):
        url = "https://reqres.in/api/users/2"
        response = requests.get(url)
        data = json.loads(response.text)
        response_data_first_name = data["data"]["first_name"]
        assert response_data_first_name == "Janet", "Expected first name Janet"

    def test_login(self):
        url = "https://reqres.in/api/login"
        data = {
                 "email": "eve.holt@reqres.in",
                 "password": "cityslicka"
                }
        response = requests.post(url, data)
        data_response = json.loads(response.text)
        assert data_response.get('token'), f"Not token"

    def test_register_test_id(self):
        id_user = 4
        url = "https://reqres.in/api/register"
        requests_data = {
            "email": "eve.holt@reqres.in",
            "password": "pistol"
        }
        response = requests.post(url, requests_data)
        response_data = json.loads(response.text)
        response_data_id = response_data["id"]
        assert id_user == response_data_id, f"must be {id_user}, but was {response_data_id}"

    def test_resource_does_not_exist(self):
        url = "https://reqres.in//api/unknown/23"
        response = requests.get(url)
        assert response.status_code == 404, f'Expected code 404, but real is: {response.status_code}.'

    def test_single_not_found(self):
        url = "https://reqres.in/api/users"
        data = {
             "name": "morpheus",
             "job": "leader"
                }
        response = requests.post(url, data)
        data_response = json.loads(response.text)
        assert data_response["job"] == "leader", f"expected job is leader, but was [data_response{'job'}]"
        
    def test_list_users(self):
        url = "https://reqres.in/api/users?page=2"
        response = requests.get(url)
        assert response.status_code == 200, f"expected code 200, but was {response}"



