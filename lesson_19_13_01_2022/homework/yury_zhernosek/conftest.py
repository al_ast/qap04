import pytest
import requests


@pytest.fixture()
def valid_credentials():
    credentials = {
        'email': 'eve.holt@reqres.in',
        'password': 'pistol'
    }
    return credentials


@pytest.fixture()
def invalid_credentials():
    credentials = {
        'email': 'sydney@fife'
    }
    return credentials


@pytest.fixture()
def get_response():
    return requests.get('https://reqres.in/')
