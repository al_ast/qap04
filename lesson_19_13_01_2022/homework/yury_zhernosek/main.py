import requests


class TestAPI:

    def test_for_server_error(self, get_response):
        assert get_response.status_code < 500, f'Server error: {get_response.status_code}'

    def test_for_not_redirect(self, get_response):
        assert not get_response.is_redirect, 'Redirected'

    def test_for_text_content_not_empty(self, get_response):
        assert get_response.text != '', 'Text is empty'

    def test_for_encoding_utf8(self, get_response):
        assert get_response.encoding == 'utf-8', f'Use encoding type: {get_response.encoding}'

    def test_login_with_valid_credentials(self, valid_credentials):
        url = 'https://reqres.in/api/login'
        response = requests.post(url, valid_credentials)
        assert 300 > response.status_code >= 200, 'Not logged in with valid data'

    def test_register_with_invalid_credentials(self, invalid_credentials):
        url = 'https://reqres.in/api/register'
        response = requests.post(url, invalid_credentials)
        assert response.status_code >= 200, 'Login with invalid data'

