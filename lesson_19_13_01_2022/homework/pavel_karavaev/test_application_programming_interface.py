import requests


class TestApplicationProgrammingInterface:

    def test_registration_with_valid_credentials(self):
        valid_credentials_for_registration = {
            "email": "eve.holt@reqres.in",
            "password": "pistol"
        }

        url = 'https://reqres.in/api/register'

        response = requests.post(url=url, data=valid_credentials_for_registration)

        assert response.status_code == 200, "registration not successful!"

    def test_registration_with_invalid_credentials(self):
        invalid_credentials_for_registration = {
            "email": "sydney@fife"
        }

        url = 'https://reqres.in/api/register'

        response = requests.post(url=url, data=invalid_credentials_for_registration)

        assert response.status_code == 400, "login successful with invalid credentials!"

    def test_of_obtaining_a_token_during_authorization(self):
        valid_credentials_for_login = {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
        }

        url = 'https://reqres.in/api/login'

        response = requests.post(url=url, data=valid_credentials_for_login)

        assert len(response.json().get('token')) != '', "token not found!"

    def test_create_user(self):
        credentials_for_create_user = {
            "name": "morpheus",
            "job": "leader"
        }

        url = 'https://reqres.in/api/users'

        response = requests.post(url=url, data=credentials_for_create_user)

        assert len(response.json().get('id')) != '', "user id not found!"

    def test_get_user_information(self):

        user_id = 2

        url = f'https://reqres.in/api/users/{user_id}'

        response = requests.get(url=url)

        assert user_id == response.json().get('data').get('id'), \
            "an unexpected user was received"

    def test_get_non_existent_user(self):

        url = 'https://reqres.in/api/unknown/23'

        response = requests.get(url=url)

        assert response.status_code == 404, f"expected error 404, but was {response.status_code}"
