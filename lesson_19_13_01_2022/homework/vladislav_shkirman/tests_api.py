import requests
import json


class TestApi:

    def test_get_users(self, get_users_data):
        users_list = [dictionary.get('first_name') + ' ' + dictionary.get('last_name')
                      for dictionary in get_users_data]
        assert len(users_list) > 0, "No users found in the list"

    def test_get_user_emails(self, get_users_data):
        emails_list = []
        actual_emails_count = 0
        user_data = None
        for user_data in get_users_data:
            actual_emails_count = len([1 for user_data in get_users_data if 'email' in user_data])
            emails_list.append(user_data.get("email"))
        assert len(emails_list) == actual_emails_count, f'Email is missing for {user_data.get("id")}'

    def test_get_not_existing_user(self, get_users_url):
        not_existing_user_id = '99'
        response_not_existing_user = requests.get(get_users_url + not_existing_user_id)
        assert response_not_existing_user.status_code == 404, \
            f'No data is expected for this user with id {not_existing_user_id}'

    def test_user_created(self, get_users_url):
        request_data = {
            "name": 'vlad',
            "job": 'qa engineer'
        }
        response_user_created = requests.post(get_users_url, request_data)
        user_data = json.loads(response_user_created.text)
        assert response_user_created.status_code == 201, \
            f'Response code {response_user_created.status_code} is not correct, code 201 - "user created" is expected'
        assert user_data["name"] == request_data["name"], \
            f'Requested name {request_data["name"]} is not found in response'

    def test_user_updated(self, get_users_url):
        existing_user_id = '2'
        update_user_url = get_users_url + existing_user_id
        request_user_data_to_update = {
            "name": "vlad",
            "job": "automation qa engineer"
        }
        response_user_updated = requests.patch(update_user_url, request_user_data_to_update)
        update_user_data = json.loads(response_user_updated.text)
        assert update_user_data.get("name") == request_user_data_to_update["name"], \
            f'Requested name {request_user_data_to_update["name"]} is not found in response'
        assert update_user_data.get("job") == request_user_data_to_update["job"], \
            f'Requested job {request_user_data_to_update["job"]} is not found in response'

    def test_login(self, login):
        request_data_to_login = {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
        }
        response_login = requests.post(login, request_data_to_login)
        login_data = json.loads(response_login.text)
        assert response_login.status_code == 200, \
            f'Response code {response_login.status_code} is not correct, code 200 - OK is expected'
        assert len(login_data["token"]) > 0, f'Token is not received. Error {response_login.status_code} returned'
