import requests
import json
import pytest


@pytest.fixture(autouse=True)
def api_setup():
    global users_url
    global update_user_url
    users_url = 'https://reqres.in/api/users/'


@pytest.fixture
def get_users_data():
    response_users_list = requests.get(users_url)
    users_list_data = json.loads(response_users_list.text)["data"]
    return users_list_data


@pytest.fixture
def get_users_url():
    return users_url


@pytest.fixture
def login():
    login_url = "https://reqres.in/api/login"
    return login_url
