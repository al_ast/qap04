from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def _wait(self, locator, timeout=10):
        by_type = self._define_by_type(locator)
        WebDriverWait(self.driver, timeout).until(
            expected_conditions.presence_of_element_located((by_type, locator))
        )

    def _define_by_type(self, locator):
        if "/" in locator:
            return By.XPATH

        return By.CSS_SELECTOR

    def _find_element(self, locator):
        self._wait(locator)
        by_type = self._define_by_type(locator)

        return self.driver.find_element(by_type, locator)

    def click(self, locator):
        element = self._find_element(locator)
        element.click()

    def hover(self, locator):
        element = self._find_element(locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    def enter_text(self, locator, text):
        element = self._find_element(locator)
        element.send_keys(text)
