from lesson_14_13_12_2021.home_work.alexandra_bortovaya.base_page import BasePage


class CartPage(BasePage):
    _QUANTITY_LOCATOR = "//a[text()='{}']/ancestor::tr//span[contains(@class,'basket__{}')]"
    _CHECKOUT_LOCATOR = "//button[contains(@class, 'button__order') and @data-ga_category='Ordering']"
    _USERNAME_LOCATOR = "//input[contains(@class,'input-user-name')]"
    _EMAIL_LOCATOR = "//input[contains(@class,'input-user-email')]"
    _DELIVERY_TYPE_LOCATOR = "//input[contains(@class,'delivery_type') and @value='{}']/parent::label"

    def change_product_quantity(self, product_name, action):
        self.click(self._QUANTITY_LOCATOR.format(product_name, action))

    def proceed_to_checkout(self):
        self.click(self._CHECKOUT_LOCATOR)

    def add_personal_data(self, email, username=None):
        self.enter_text(self._USERNAME_LOCATOR, username)
        self.enter_text(self._EMAIL_LOCATOR, email)

    def choose_delivery_type(self, value):
        self.click(self._DELIVERY_TYPE_LOCATOR.format(value))
