import time

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_14_13_12_2021.home_work.alexandra_bortovaya.cart_page import CartPage
from lesson_14_13_12_2021.home_work.alexandra_bortovaya.catalog_page import CatalogPage
from lesson_14_13_12_2021.home_work.alexandra_bortovaya.category_page import CategoryPage
from lesson_14_13_12_2021.home_work.alexandra_bortovaya.common_header import CommonHeader
from lesson_14_13_12_2021.home_work.alexandra_bortovaya.product_page import ProductPage

driver = webdriver.Chrome(ChromeDriverManager().install())

CATEGORY = "Товары для дома"
SUB_CATEGORY = "Декор"
SUB_SUBCATEGORY = ("Картины, постеры", "Свечи, подсвечники")
PRODUCT = ("Картина Orlix Разноцветные листья / CA-12819", "Набор свечей Белбогемия Provence 560129/02 / 37191 (4шт)")
URL = "https://www.21vek.by/"

driver.get(URL)
driver.maximize_window()

common_header = CommonHeader(driver)
common_header.open_catalog_menu()
common_header.hover_category_from_catalog_menu(CATEGORY)
common_header.open_sub_category_from_catalog_menu(SUB_CATEGORY)

category_page = CategoryPage(driver)
category_page.open_sub_category(SUB_SUBCATEGORY[0])

catalog_page = CatalogPage(driver)
catalog_page.open_product_page_by_name(PRODUCT[0])

product_page = ProductPage(driver)
product_page.add_product_to_cart()

driver.back()
driver.back()

category_page.open_sub_category(SUB_SUBCATEGORY[1])
category_page.add_to_cart_by_product_name(PRODUCT[1])

common_header.open_cart()

cart_page = CartPage(driver)
cart_page.change_product_quantity(PRODUCT[1], "plus")
cart_page.proceed_to_checkout()
cart_page.add_personal_data("sasha-test@gmail.com", "Alexandra")
cart_page.choose_delivery_type("self")

time.sleep(10)
