from lesson_14_13_12_2021.home_work.alexandra_bortovaya.base_page import BasePage


class ProductPage(BasePage):
    _ADD_TO_CART_LOCATOR = "//div[contains(@class,'item-buyzone')]//button[@data-ga_action='add_to_cart']"

    def add_product_to_cart(self):
        self.click(self._ADD_TO_CART_LOCATOR)
