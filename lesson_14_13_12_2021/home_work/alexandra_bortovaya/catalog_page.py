from lesson_14_13_12_2021.home_work.alexandra_bortovaya.base_page import BasePage


class CatalogPage(BasePage):
    _PRODUCT_NAME_LOCATOR = "//span[text()='{}']"

    def open_product_page_by_name(self, value):
        self.click(self._PRODUCT_NAME_LOCATOR.format(value))
