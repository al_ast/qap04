from lesson_14_13_12_2021.home_work.alexandra_bortovaya.base_page import BasePage


class CategoryPage(BasePage):
    _SUB_CATEGORY_LOCATOR = "//a[contains(@class,'sub__header') and contains(text(),'{}')]"
    _ADD_TO_CART_LOCATOR_PART1 = "//span[text()='{}']/ancestor::div[contains(@class,'item_data')]"
    _ADD_TO_CART_LOCATOR_PART2 = "/following-sibling::div//button"
    _ADD_TO_CART_LOCATOR = _ADD_TO_CART_LOCATOR_PART1 + _ADD_TO_CART_LOCATOR_PART2

    def open_sub_category(self, value):
        self.click(self._SUB_CATEGORY_LOCATOR.format(value))

    def add_to_cart_by_product_name(self, value):
        self.click(self._ADD_TO_CART_LOCATOR.format(value))
