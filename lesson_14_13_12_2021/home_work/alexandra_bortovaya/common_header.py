from lesson_14_13_12_2021.home_work.alexandra_bortovaya.base_page import BasePage


class CommonHeader(BasePage):
    _CATALOG_MENU_LOCATOR = "//button[contains(@class,'catalogButton')]"
    _CATEGORY_LOCATOR = "//span[contains(@class,'categoryName') and contains(text(),'{}')]"
    _CART_LOCATOR = "//a[contains(@class,'headerCartBox')]"

    def open_catalog_menu(self):
        self.click(self._CATALOG_MENU_LOCATOR)

    def hover_category_from_catalog_menu(self, value):
        self.hover(self._CATEGORY_LOCATOR.format(value))

    def open_sub_category_from_catalog_menu(self, value):
        self.click(self._CATEGORY_LOCATOR.format(value))

    def open_cart(self):
        self.click(self._CART_LOCATOR)
