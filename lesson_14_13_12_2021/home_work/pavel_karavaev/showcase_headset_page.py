from base_page import BasePage


class ShowcaseHeadSetsPage(BasePage):
    def click_more_detailed_hyperx_cloudx(self):
        self.click_more_detailed("//a[@href='/ru/headsets/cloud-stinger-s-gaming-headset']")

    def click_more_detailed_hyperX_cloyd_flight(self):
        self.click_more_detailed("//a[@href='/ru/headsets/cloud-flight-wireless-gaming-headset-ps5-ps4']")

    def click_more_detailed_hyperx_cloud_buds(self):
        self.click_more_detailed("//article//a[@href='/ru/headsets/cloud-buds-bluetooth-wireless-headphones']")

    def click_more_detailed_cloud_2_wireless(self):
        self.click_more_detailed("//article//a[@href='/ru/headsets/cloud-gaming-headset?partnum=HHSC2X-BA-RD%2FG']")

    def click_more_detailed_cloud_stingers_s(self):
        self.click_more_detailed("//article//a[@href='/ru/headsets/cloud-stinger-s-gaming-headset']")
