import time

from base_page import BasePage
class MainPage(BasePage):
    def agree_cookie(self):
        COOKIE_BUTTON_LOCATOR = "//button[@id='onetrust-accept-btn-handler']"

        self.wait(COOKIE_BUTTON_LOCATOR, 10)
        self.click(COOKIE_BUTTON_LOCATOR)

    def click_product_button(self):
        PRODUCT_BUTTON_LOCATOR = "//a[@aria-controls='nav-primary']"
        self.click(PRODUCT_BUTTON_LOCATOR)

    def click_header_category(self, value):
        HEADER_BUTTONS_LOCATOR = f"//div[2]//div//a[@target='_self' and contains(text(),'{value}')]"
        self.click(HEADER_BUTTONS_LOCATOR)

    def choose_language(self):
        _CHOOSE_LANGUAGE_BUTTON_LOCATOR = "//nav//div[4]//a[@href='javascript: void(0)']"
        _RUSSIAN_LANGUAGE_LOCATOR = "//a[contains(@data-track-label,'Russia')]"
        self.click(_CHOOSE_LANGUAGE_BUTTON_LOCATOR)
        self.click(_RUSSIAN_LANGUAGE_LOCATOR)




