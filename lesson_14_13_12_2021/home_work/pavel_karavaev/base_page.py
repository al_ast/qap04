from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from webdriver_manager import driver


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def determining_type_locator(self, locator):
        if "//" in locator:
            return By.XPATH
        else:
            return By.CSS_SELECTOR

    def wait(self, locator, timeout):
        by_type = self.determining_type_locator(locator)
        WebDriverWait(self.driver, timeout).until(
            EC.presence_of_element_located((by_type, locator)))


    def click(self, locator):
        by_type = self.determining_type_locator(locator)
        element = self.driver.find_element(by_type, locator)
        element.click()

    def enter_text(self, locator, text):
        by_type = self.determining_type_locator(locator)
        element = self.driver.find_element(by_type, locator)
        element.send_keys(text)

    def get_text(self, locator):
        by_type = self.determining_type_locator(locator)

        element = self.driver.find_element(
            by_type, locator)

        return element.text

    def refresh(self):
        self.driver.refresh()

    def click_more_detailed(self, locator):
        MORE_DETAILS_BUTTON_LOCATOR = f"{locator}"
        self.click(MORE_DETAILS_BUTTON_LOCATOR)

    # def hover_cursor(self, locator):
    #     action = ActionChains(driver)
    #     by_type = self.determining_type_locator(locator)
    #     element = self.driver.find_element(by_type, locator)
    #     action.move_to_element(element).perform()    НЕ ПОНЯЛ КАК НАВЕСТИ КУРСОР ЧТОБ БЫЛ ЭФФЕКТ В БРАУЗЕРЕ
