from base_page import BasePage


class HeadsetPage(BasePage):
    def click_buy_button(self):
        BUY_BUTTON_LOCATOR = "//a[@class='e-btn']"
        self.click(BUY_BUTTON_LOCATOR)

    def click_introduction_button(self):
        _INTRODUCTION_BUTTON_LOCATOR = "//a[@href='#product-features']"
        self.click(_INTRODUCTION_BUTTON_LOCATOR)


    def click_specification_button(self):
        _SPECIFICATION_BUTTON_LOCATOR = "//a[@href='#specifications']"
        self.click(_SPECIFICATION_BUTTON_LOCATOR)

    def click_official_partners_button(self):
        OFFICIAL_PARTNERS_BUTTON_LOCATOR = "//p/a[@href='/ru/wheretobuy']"
        self.click(OFFICIAL_PARTNERS_BUTTON_LOCATOR)
