from base_page import BasePage


class PcPage(BasePage):
    def click_middle_button(self, value):
        MIDDLE_BUTTON_LOCATOR = f"//ul//li//h4[contains(text(),'{value}')]"
        self.click(MIDDLE_BUTTON_LOCATOR)

