import time

from base_page import BasePage


class PartnersPage(BasePage):
    def click_country_list(self):
        COUNTRY_LIST_LOCATOR = "//div//e-icon"
        self.click(COUNTRY_LIST_LOCATOR)
        time.sleep(1)
    def click_belarus_button(self):
        BELARUS_BUTTON_LOCATOR = "//li[@data-index='8']"
        self.click(BELARUS_BUTTON_LOCATOR)
        time.sleep(1)
    def chose_partners(self):
        PARTNERS_BUTTON_LOCATOR = "//a[@href='https://5element.by/search/?q=hyperx&tab=products']"
        self.click(PARTNERS_BUTTON_LOCATOR)
        time.sleep(1)