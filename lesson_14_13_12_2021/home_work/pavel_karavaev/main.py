import time

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from headset_page import HeadsetPage
from partners_page import PartnersPage
from showcase_headset_page import ShowcaseHeadSetsPage
from main_page import MainPage
from pc_page import PcPage

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://www.hyperxgaming.com/ru")

main_page = MainPage(driver)
main_page.agree_cookie()
main_page.choose_language()

main_page.click_product_button()
time.sleep(2)

main_page.click_header_category("PC")

pc_page = PcPage(driver)
pc_page.click_middle_button("Гарнитуры")

showcase_headset_page = ShowcaseHeadSetsPage(driver)
showcase_headset_page.click_more_detailed_hyperx_cloudx()

headset_page = HeadsetPage(driver)
headset_page.click_introduction_button()
time.sleep(2)

headset_page.click_specification_button()
time.sleep(2)

headset_page.click_buy_button()
time.sleep(2)
headset_page.click_official_partners_button()

partners_page = PartnersPage(driver)
partners_page.click_country_list()
partners_page.click_belarus_button()
partners_page.chose_partners()
time.sleep(5)
