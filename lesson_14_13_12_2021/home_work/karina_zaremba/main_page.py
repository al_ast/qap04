from BasePage import BasePage

class Login(BasePage):
    LOGINPAGE_LOCATOR = "//a[@class='h-drop h-user']"
    USERNAME_LOCATOR = "//input[@name='login']"
    PASSWORD_LOCATOR = "//input[@type='password']"
    BUTTON_LOCATOR = "//div[@class='modal-popup modal-login']//form/div[4]"

    def click_personal_account(self):
        self.click_element(self.LOGINPAGE_LOCATOR)

    def enter_user_name(self, value):
        self.enter_text(self.USERNAME_LOCATOR, value)

    def enter_password(self, value):
        self.enter_text(self.PASSWORD_LOCATOR, value)

    def click_login_button(self):
        self.click_element(self.BUTTON_LOCATOR)