import time

from lesson_14_13_12_2021.home_work.karina_zaremba.BasePage import BasePage


class SearchModal(BasePage):
    OPEN_SEARCH_INPUT_LOCATOR = "//input[@placeholder='Поиск товара' and @required='required']"
    SEARCH_INPUT_LOCATOR = "//form[@action='/search']//input[@type='text']"
    SEARCH_BUTTON_LOCATOR = "//button[@class='multi-search-header__submit']//span"

    def search(self, text):
        self.click_element(self.OPEN_SEARCH_INPUT_LOCATOR)
        self.enter_text(self.SEARCH_INPUT_LOCATOR, text)
        self.click_element(self.SEARCH_BUTTON_LOCATOR)
