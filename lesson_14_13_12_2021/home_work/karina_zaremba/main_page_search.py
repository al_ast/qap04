from BasePage import BasePage


class Search(BasePage):
    SEARCH_LOCATOR = "//form/input[@type='text' and @required='required']"
    BUTTON_SEARCH_LOCATOR = "//div[@class='h-search']//button[@type='submit']"

    def enter_product_name(self, value):
        self.enter_text(self.SEARCH_LOCATOR, value)
        self.click_element(self.BUTTON_SEARCH_LOCATOR)


#я перепробовала миллион вариантов клика button_search_locator, но у меня не получается
# кликнуть на сам поиск :(
# даже подключала from selenium.webdriver.common.keys import Keys чтобы ввести текст и автоматом
# нажать enter, но не получилось. Можете, в readme помочь с этим моментом?


