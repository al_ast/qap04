from base_page import BasePage


class ProductAccessoriesPage(BasePage):
    PLACE_AN_ORDER_BUTTON_LOCATOR = "//div[@title='Заказать товар']"

    def place_an_order(self):
        place_an_order_locator = self.PLACE_AN_ORDER_BUTTON_LOCATOR
        self.click(place_an_order_locator)
