from base_page import BasePage


class CartPage(BasePage):
    CONFIRM_ORDER_BUTTON_LOCATOR = "//input[@value='Подтвердить заказ']"
    INPUT_USER_FULL_NAME_LOCATOR = "//input[@id='fio']"
    INPUT_USER_EMAIL_LOCATOR = "//input[@id='poshta']"
    INPUT_USER_CITY_LOCATOR = "//input[@id='punkt_calk']"

    def confirm_order(self):
        confirm_order_locator = self.CONFIRM_ORDER_BUTTON_LOCATOR
        self.click(confirm_order_locator)

    def enter_user_full_name(self, user_full_name):
        user_full_name_locator = self.INPUT_USER_FULL_NAME_LOCATOR
        self.enter_text(user_full_name_locator, user_full_name)

    def enter_user_email(self, user_email):
        user_email_locator = self.INPUT_USER_EMAIL_LOCATOR
        self.enter_text(user_email_locator, user_email)

    def enter_user_city(self, user_city):
        user_city_locator = self.INPUT_USER_CITY_LOCATOR
        self.enter_text(user_city_locator, user_city)
