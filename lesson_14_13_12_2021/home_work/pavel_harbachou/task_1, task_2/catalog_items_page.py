from selenium.webdriver.common.by import By
from base_page import BasePage


class CatalogItemsPage(BasePage):
    PRODUCT_LOCATOR = "//img[contains(@alt,'{}')]"

    def select_product(self, product_name):
        product_locator = self.PRODUCT_LOCATOR.format(product_name)
        self.click(product_locator)
