# Задание1: Переделать домашку 13 к виду из урока 14
# Добавить пару своих новых действий или страниц к уже
# существующим
#
# Задание2*:
# кликнуть по элементу используя джава скрипт экзекьютор
# driver.execute_script

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from main_page import MainPage
from catalog_items_page import CatalogItemsPage
from product_page import ProductPage
from product_accessories_page import ProductAccessoriesPage
from cart_page import CartPage


driver = webdriver.Chrome(ChromeDriverManager().install())
url = 'https://sila.by/'
driver.get(url)
main_page = MainPage(driver)
main_page.select_category('Стиральные машины')

catalog_items_page = CatalogItemsPage(driver)
catalog_items_page.select_product('SAMSUNG WW70A6S23AN/LP')

product_page = ProductPage(driver)
product_page.add_to_cart()

product_accessories_page = ProductAccessoriesPage(driver)
product_accessories_page.place_an_order()

cart_page = CartPage(driver)
cart_page.confirm_order()
cart_page.enter_user_full_name('My Fullname')
cart_page.enter_user_email('My email')
cart_page.enter_user_city('My city')

driver.get(url)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
main_page.go_mobile_version()
