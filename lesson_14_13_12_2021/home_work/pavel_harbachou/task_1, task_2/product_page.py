from base_page import BasePage


class ProductPage(BasePage):
    ADD_TO_CART_BUTTON_LOCATOR = "//div[@title='В КОРЗИНУ!']"

    def add_to_cart(self):
        place_an_order_locator = self.ADD_TO_CART_BUTTON_LOCATOR
        self.click(place_an_order_locator)
