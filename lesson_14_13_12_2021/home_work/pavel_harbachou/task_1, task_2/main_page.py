from base_page import BasePage


class MainPage(BasePage):
    CATEGORY_LOCATOR = "//div[@class='header_menu__item']//a[text()='{}']"
    MOBILE_VERSION_BUTTON_LOCATOR = "//a[@class='mob_ver']"

    def select_category(self, category_name):
        category_locator = self.CATEGORY_LOCATOR.format(category_name)
        self.click_by_js(category_locator)

    def go_mobile_version(self):
        mobile_version_locator = self.MOBILE_VERSION_BUTTON_LOCATOR
        self.click(mobile_version_locator)
