from base_page import BasePage

class Iphone(BasePage):
    IPHONE13_LOCATOR = "//span[@class='chapternav-label' and text()='iPhone 13']"

    def click_iphone(self):
        self.click(self.IPHONE13_LOCATOR)