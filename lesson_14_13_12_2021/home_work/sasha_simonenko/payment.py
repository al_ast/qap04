from base_page import BasePage

class Payment(BasePage):
    WHERE_TO_BUY_LOCATOR = "//a[@href='/by/buy/']"

    def click_where_to_buy(self):
        self.click(self.WHERE_TO_BUY_LOCATOR)
