import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from mainpage import MainPage
from iphone import Iphone
from payment import Payment
from resellers import Resellers

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.apple.com/by/")
driver.maximize_window()

mainpage = MainPage(driver)
mainpage.click_iphone_button()
time.sleep(5)

iphone = Iphone(driver)
iphone.click_iphone()
time.sleep(5)

payment = Payment(driver)
payment.click_where_to_buy()
time.sleep(5)

resellers = Resellers(driver)
resellers.click_resellers_button()
time.sleep(5)

