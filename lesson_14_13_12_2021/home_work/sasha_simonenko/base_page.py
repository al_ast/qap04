from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def quit(self):
        self.driver.quit()

    def click(self,locator):
        element = self.driver.find_element(By.XPATH, locator)
        element.click()

    def refresh(self):
        self.driver.refresh()

    def _wait(self, locator, time_out=10):
        by_type = self._define_locator_type(locator)
        WebDriverWait(self.driver, time_out).until(EC.presence_of_element_located((by_type, locator)))