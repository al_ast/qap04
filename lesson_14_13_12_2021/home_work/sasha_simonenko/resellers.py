from base_page import BasePage

class Resellers(BasePage):
    FULL_LIST_OF_RESELLERS = "//a[text()='Find the full list of resellers']"

    def click_resellers_button(self):
        self.click(self.FULL_LIST_OF_RESELLERS)