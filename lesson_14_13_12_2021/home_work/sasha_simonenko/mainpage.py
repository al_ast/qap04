from base_page import BasePage

class MainPage(BasePage):
    IPHONE_LOCATOR = "//a[@class = 'ac-gn-link ac-gn-link-iphone']"

    def click_iphone_button(self):
        self.click(self.IPHONE_LOCATOR)