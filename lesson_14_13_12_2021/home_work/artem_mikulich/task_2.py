from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.hltv.org/")
driver.maximize_window()
matches_locator = "//a[@class='navmatches']"
hltv_logo_locator = "//div[@class='hltv-logo-container']"

matches = driver.find_element(By.XPATH, matches_locator)
driver.execute_script("arguments[0].click();", matches)
hltv_logo = driver.find_element(By.XPATH, hltv_logo_locator)
driver.execute_script("arguments[0].click();", hltv_logo)

