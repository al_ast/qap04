import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from cart_page import CartPage
from catalog_page import CatalogPage
from main_page import MainPage
from product_page import ProductPage
from related_services_page import ServicesPage

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://www.21vek.by/")
driver.maximize_window()


main_page = MainPage(driver)
main_page.click_category("Телевизоры")
time.sleep(2)

catalog_page = CatalogPage(driver)
catalog_page.click_product("Телевизор Horizont 32LE5511D")
time.sleep(2)

product_page = ProductPage(driver)
product_page.click_in_cart()
time.sleep(2)
product_page.click_cart()
time.sleep(2)

cart_page = CartPage(driver)
cart_page.click_back_to_products()
time.sleep(2)

service_page = ServicesPage(driver)
service_page.click_button_strong_protection()
time.sleep(2)
service_page.click_main()
service_page.quit()




