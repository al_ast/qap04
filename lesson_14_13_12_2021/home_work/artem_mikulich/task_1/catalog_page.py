

from base_page import BasePage


class CatalogPage(BasePage):
    USER_PRODUCT_LOCATOR = "//span[text()='{}']"

    def click_product(self, product_name):
        self.click(self.USER_PRODUCT_LOCATOR.format(product_name))