from selenium.webdriver.common.by import By

from base_page import BasePage


class MainPage(BasePage):
    USER_CATEGORY_LOCATOR = "//a[text()='{}']"


    def click_category(self,category_name):
        self.click(self.USER_CATEGORY_LOCATOR.format(category_name))


