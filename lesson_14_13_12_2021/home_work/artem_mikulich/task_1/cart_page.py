

from base_page import BasePage


class CartPage(BasePage):
    USER_BACK_TO_PRODUCTS_LOCATOR = "//a[@class = 'b-go_back' and text()='← Вернуться к покупкам']/ancestor::td/a"

    def click_back_to_products(self):
        self.click(self.USER_BACK_TO_PRODUCTS_LOCATOR)
