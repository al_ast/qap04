

from base_page import BasePage


class ServicesPage(BasePage):
    USER_SERVICE_LOCATOR = "//a[text() = 'Надежная защита']"
    USER_MAIN_LOCATOR = "//a[@class = 'breadcrumbs__link']"

    def click_button_strong_protection(self):
        self.click(self.USER_SERVICE_LOCATOR)
    def click_main(self):
        self.click(self.USER_MAIN_LOCATOR)


