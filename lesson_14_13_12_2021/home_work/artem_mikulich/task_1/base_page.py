from selenium.webdriver.common.by import By


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def quit(self):
        self.driver.quit()

    def click(self,locator):
        element = self.driver.find_element(By.XPATH, locator)
        element.click()

