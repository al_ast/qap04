

from base_page import BasePage


class ProductPage(BasePage):
    USER_CART_LOCATOR = "//form[@class='j-to_basket']"
    USER_CART_PAGE_LOCATOR = "//a[@class='headerCartBox']"


    def click_in_cart(self):
        self.click(self.USER_CART_LOCATOR)
    def click_cart(self):
        self.click((self.USER_CART_PAGE_LOCATOR))

