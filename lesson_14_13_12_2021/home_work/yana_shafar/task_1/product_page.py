from lesson_14_13_12_2021.home_work.yana_shafar.task_1.base_page import BasePage


class ProductPage(BasePage):
    ADD_TO_CART_LOCATOR = "//button[text()='В корзину']"
    CART_LOCATOR = "//a[@class='headerCartBox']"

    def add_to_cart(self):
        self.click(self.ADD_TO_CART_LOCATOR)

    def open_cart(self):
        self.click_with_java_script(self.CART_LOCATOR)