import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def _wait(self, locator, time_out=10):
        by_type = self._define_locator_type(locator)
        WebDriverWait(self.driver, time_out).until(EC.presence_of_element_located((by_type, locator)))

    def _define_locator_type(self, locator):
        if "//" in locator:
            return By.XPATH

        return By.CSS_SELECTOR

    def click(self, locator):
        by_type = self._define_locator_type(locator)
        self._wait(locator)
        element = self.driver.find_element(by_type, locator)
        element.click()
        time.sleep(3)

    def enter_text(self, locator, text):
        by_type = self._define_locator_type(locator)
        self._wait(locator)
        element = self.driver.find_element(by_type, locator)
        element.send_keys(text)
        time.sleep(3)

    def click_with_java_script(self, locator):
        by_type = self._define_locator_type(locator)
        self._wait(locator)
        element = self.driver.find_element(by_type, locator)
        self.driver.execute_script("arguments[0].click();", element)
        time.sleep(3)