from lesson_14_13_12_2021.home_work.yana_shafar.task_1.base_page import BasePage


class CheckoutPage(BasePage):
    EMAIL_INPUT_LOCATOR = "//input[@type='email']"
    DELIVERY_ADDRESS_INPUT_LOCATOR = "//input[@name='data[addr]']"
    PHONE_NUMBER_LOCATOR = "//input[@type='tel']"
    CONFIRM_THE_ORDER_LOCATOR = "//button[@id='j-basket__confirm']"

    def enter_email(self, value):
        self.enter_text(self.EMAIL_INPUT_LOCATOR, value)

    def enter_delivery_address(self, value):
        self.enter_text(self.DELIVERY_ADDRESS_INPUT_LOCATOR, value)

    def enter_phone_number(self, value):
        self.enter_text(self.PHONE_NUMBER_LOCATOR, value)

    def click_confirm_the_order_button(self):
        self.click(self.CONFIRM_THE_ORDER_LOCATOR)