from lesson_14_13_12_2021.home_work.yana_shafar.task_1.base_page import BasePage


class CartPage(BasePage):
    INCREASE_IN_THE_NUMBER_LOCATOR = "//span[@id='j-plus-8519419']"
    CHECKOUT_BUTTON = "//button[@class='g-button cr-button__order j-ga_track']"

    def click_plus_button(self):
        self.click(self.INCREASE_IN_THE_NUMBER_LOCATOR)

    def click_checkout_button(self):
        self.click(self.CHECKOUT_BUTTON)