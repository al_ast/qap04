from lesson_14_13_12_2021.home_work.yana_shafar.task_1.base_page import BasePage


class CatalogPage(BasePage):
    PRODUCER_OF_PRODUCT_LOCATOR = "//dl[@class='b-filter-attr j-producers']//child::label[@title='{}']"
    FILTER_BUTTON_LOCATOR = "//button[@id='j-filter__btn']"
    PRODUCT_LOCATOR = "//span[text()='{}']"

    def select_producer_of_product(self, producer_name):
        locator = self.PRODUCER_OF_PRODUCT_LOCATOR.format(producer_name)
        self.click(locator)

    def click_filter_button(self):
        self.click(self.FILTER_BUTTON_LOCATOR)

    def select_product(self, product_name):
        locator = self.PRODUCT_LOCATOR.format(product_name)
        self.click(locator)
