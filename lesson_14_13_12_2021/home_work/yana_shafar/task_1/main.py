from webdriver_manager.chrome import ChromeDriverManager

from selenium import webdriver

from lesson_14_13_12_2021.home_work.yana_shafar.task_1.cart_page import CartPage
from lesson_14_13_12_2021.home_work.yana_shafar.task_1.catalog_page import CatalogPage
from lesson_14_13_12_2021.home_work.yana_shafar.task_1.checkout_page import CheckoutPage
from lesson_14_13_12_2021.home_work.yana_shafar.task_1.main_page import MainPage
from lesson_14_13_12_2021.home_work.yana_shafar.task_1.product_page import ProductPage

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://www.21vek.by/")

main_page = MainPage(driver)
main_page.select_category('Ноутбуки')

catalog_page = CatalogPage(driver)
catalog_page.select_producer_of_product('Asus')
catalog_page.click_filter_button()
catalog_page.select_product('Игровой ноутбук Asus ROG Strix G17 G713QE-HX023')

product_page = ProductPage(driver)
product_page.add_to_cart()
product_page.open_cart()

cart_page = CartPage(driver)
cart_page.click_plus_button()
cart_page.click_checkout_button()

checkout_page = CheckoutPage(driver)
checkout_page.enter_email('abcdefg1@gmail.com')
checkout_page.enter_delivery_address('ул. Комммунистическая 22')
checkout_page.enter_phone_number('+375445678912')
checkout_page.click_confirm_the_order_button()