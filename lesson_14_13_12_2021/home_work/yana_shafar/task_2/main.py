from webdriver_manager.chrome import ChromeDriverManager

from selenium import webdriver

from lesson_14_13_12_2021.home_work.yana_shafar.task_2.catalog_page import CatalogPage
from lesson_14_13_12_2021.home_work.yana_shafar.task_2.main_page import MainPage

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://www.21vek.by/")

main_page = MainPage(driver)
main_page.select_category('Пылесосы')

catalog_page = CatalogPage(driver)
catalog_page.add_to_cart()
catalog_page.open_cart()
