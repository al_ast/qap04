from lesson_14_13_12_2021.home_work.yana_shafar.task_2.base_page import BasePage


class CatalogPage(BasePage):
    ADD_TO_CART_BUTTON_LOCATOR = "//button[text()='В корзину']"
    PRODUCT_CART_LOCATOR = "//a[@class='headerCartBox']"

    def add_to_cart(self):
        self.click(self.ADD_TO_CART_BUTTON_LOCATOR)

    def open_cart(self):
        self.click_with_java_script(self.PRODUCT_CART_LOCATOR)