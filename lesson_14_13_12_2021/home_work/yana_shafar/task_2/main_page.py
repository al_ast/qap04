from lesson_14_13_12_2021.home_work.yana_shafar.task_2.base_page import BasePage


class MainPage(BasePage):
    CATEGORY_LOCATOR = "//a[text()='{}']"

    def select_category(self, category_name):
        locator = self.CATEGORY_LOCATOR.format(category_name)
        self.click(locator)