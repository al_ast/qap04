from base_page import BasePage


class BookPage(BasePage):
    _PUT_TO_CART_LOCATOR = "//span[contains(text(),'Положить в')]"

    def put_to_cart(self):
        self.click(self._PUT_TO_CART_LOCATOR)
