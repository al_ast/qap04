from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def _define_locator_type(self, locator):
        if "//" in locator:
            return By.XPATH

        return By.CSS_SELECTOR

    def _wait(self, locator, time_out=20):
        by_type = self._define_locator_type(locator)
        WebDriverWait(self.driver, time_out).until(
            EC.presence_of_element_located(
                (by_type, locator))
        )

    def __init__(self, driver):
        self.driver = driver

    def click(self, locator):
        by_type = self._define_locator_type(locator)

        self._wait(locator)
        element = self.driver.find_element(by_type, locator)
        element.click()

    def enter_data(self, locator, data):
        by_type = self._define_locator_type(locator)

        self._wait(locator)
        element = self.driver.find_element(by_type, locator)
        element.send_keys(data)

    def get_text(self, locator):
        by_type = self._define_locator_type(locator)

        self._wait(locator)
        element = self.driver.find_element(by_type, locator)
        return element.text
