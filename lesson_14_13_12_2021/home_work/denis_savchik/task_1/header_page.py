from base_page import BasePage


class HeaderPage(BasePage):
    _CART_HEADER_LOCATOR = "//u[text()='Корзина']"
    _ENTER_DATA_TO_SEARCH_LOCATOR = "#top-s"
    _CLICK_SEARCH_LOCATOR = ".top-panel__search__btn__item"

    def check_cart(self):
        self.click(self._CART_HEADER_LOCATOR)

    def start_search(self, sought):
        self.enter_data(self._ENTER_DATA_TO_SEARCH_LOCATOR, sought)
        self.click(self._CLICK_SEARCH_LOCATOR)


