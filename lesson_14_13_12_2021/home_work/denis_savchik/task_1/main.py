# Task 2 with seleniumWD
# Задание2:
# Выбрать любой сайт
# Написать 5-6 действий на разных страницах используя локаторы
# см пример в class_materials/pages
# Улучшить с использованеим наследования
# Использовать готовый класс кликер


from book_page import BookPage
from books_category_page import BooksCategoryPage
from cart_page import CartPage
from genre_book_page import GenreBookPage
from header_page import HeaderPage
from main_page import MainPage
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://oz.by/")

main_page = MainPage(driver)
books_category_page = BooksCategoryPage(driver)
genre_book_page = GenreBookPage(driver)
book_page = BookPage(driver)
header_page = HeaderPage(driver)
cart_page = CartPage(driver)


main_page.select_category("Книги")
books_category_page.select_genre("Художественная литература")
genre_book_page.select_book("Гарри Поттер и философский камень")
book_page.put_to_cart()
header_page.check_cart()
cart_page.enter_phone_number("292081884")

driver.back()
header_page.start_search("Ведьмак")
print(header_page.get_text("//a[contains(@href,'https://oz.by/search/?bst=1')]"))

a = 0




