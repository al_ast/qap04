from base_page import BasePage


class GenreBookPage(BasePage):
    _BOOK_LOCATOR = "//img[@alt='{}']/ancestor::a"

    def select_book(self, book_name):
        locator = self._BOOK_LOCATOR.format(book_name)
        self.click(locator)
