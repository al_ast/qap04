from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://musicmarket.by/")
driver.maximize_window()

guitar_locator = "//img[contains(@alt,'Fender CD-60S Natural')]"

guitar = driver.find_element(By.XPATH, guitar_locator)
driver.execute_script("arguments[0].click();", guitar)



