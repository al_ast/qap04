from base_page import BasePage


class Cart(BasePage):
    EMAIL_INPUT_LOCATOR = "//input[@id='elm_']"
    CITY_INPUT_LOCATOR = "//input[@id='elm_23']"
    ORDER_BUTTON_LOCATOR = "//button[@id='place_order']"
    DELIVERY_LOCATOR = "//input[@value='17']"
    PAYMENT_LOCATOR = "//input[@value='26']"

    def input_email(self, email):
        email_input = self.EMAIL_INPUT_LOCATOR
        self.enter_text(email_input, email)

    def input_city(self, city):
        city_input = self.CITY_INPUT_LOCATOR
        self.enter_text(city_input, city)

    def confirm_order(self, confirmation):
        confirm_order = self.ORDER_BUTTON_LOCATOR.format(confirmation)
        self.click(confirm_order)

    def manage_delivery_details(self, way_of_delivery, way_of_payment):
        choose_delivery = self.DELIVERY_LOCATOR.format(way_of_delivery)
        self.click(choose_delivery)
        choose_payment = self.PAYMENT_LOCATOR.format(way_of_payment)
        self.click(choose_payment)
