from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome(ChromeDriverManager().install())


class BasePage:

    def __init__(self):
        self.driver = driver

    def click(self, locator):
        element = WebDriverWait(driver, 100).until(
            EC.presence_of_element_located((By.XPATH, f"{locator}"))
                                           )
        driver.execute_script("arguments[0].click();", element)

    def enter_text(self, locator, value):
        element = WebDriverWait(driver, 100).until(
            EC.presence_of_element_located((By.XPATH, f"{locator}"))
        )
        element.send_keys(value)
