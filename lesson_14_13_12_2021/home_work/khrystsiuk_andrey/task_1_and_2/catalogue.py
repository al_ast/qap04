from base_page import BasePage


class Catalogue(BasePage):
    CATALOG_LOCATOR = "//img[@title='Akai Pro MPK Mini MK3 | Купить MIDI клавиатуру на djshop.by']"

    def open_product_from_catalog(self):
        self.click("//img[@title='Akai Pro MPK Mini MK3 | Купить MIDI клавиатуру на djshop.by']")
