from main_page import MainPage
from catalogue import Catalogue
from product_page import PageProduct
from cart import Cart


dj_shop_main_page = MainPage()
dj_shop_main_page.open_main_page('https://djshop.by')
dj_shop_main_page.close_ad('Close ad window')
dj_shop_main_page.choose_category('MIDI Keyboards')

dj_shop_catalogue = Catalogue()
dj_shop_catalogue.open_product_from_catalog()

dj_shop_product_page = PageProduct()
dj_shop_product_page.add_to_cart('Adding to cart')
dj_shop_product_page.go_to_cart('Going to cart')

dj_shop_cart = Cart()
dj_shop_cart.input_email('sample_email@gmail.com')
dj_shop_cart.input_city('Minsk')
dj_shop_cart.confirm_order('Order confirmed')
dj_shop_cart.manage_delivery_details('By courier', 'By card while delivered')
