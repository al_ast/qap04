from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.onliner.by/")
driver.maximize_window()

iframe_element = WebDriverWait(driver, 100).until(
            EC.presence_of_element_located((By.XPATH, "//iframe[@name='chats-helper-iframe']"))
    )
driver.switch_to.frame(iframe_element)
text_of_element = driver.find_element(By.XPATH, "//body[contains(text(), 'This')]").text
print(text_of_element)



