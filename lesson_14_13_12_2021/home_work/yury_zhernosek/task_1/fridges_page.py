from product_page import ProductPage

class FridgesPage(ProductPage):

    FRIDGE_LOCATOR = "//span[text()='{}']"

    def select_fridge(self, name_fridge):
        locator = self.FRIDGE_LOCATOR.format(name_fridge)
        self.click(locator)