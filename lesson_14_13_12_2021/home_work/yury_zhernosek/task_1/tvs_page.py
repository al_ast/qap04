from product_page import ProductPage

class TvsPage(ProductPage):

    TV_LOCATOR = "//span[text()='{}']"

    def select_tv(self, name_tv):
        locator = self.TV_LOCATOR.format(name_tv)
        self.click(locator)