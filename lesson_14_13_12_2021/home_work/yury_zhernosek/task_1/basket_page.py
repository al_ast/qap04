from base_page import BasePage

class BasketPage(BasePage):

    PLACING_ORDER_BUTTON_LOCATOR = "//span[text()='Оформить заказ']"
    PROMOCODE_LOCATOR = "//input[@placeholder='Промокод']"
    ACCEPT_CODE_BUTTON_LOCATOR = "//span[text()='Применить код']"

    def click_placing_order(self):
        self.click(self.PLACING_ORDER_BUTTON_LOCATOR)

    def enter_promocode(self, promocode):
        self.enter_text(self.PROMOCODE_LOCATOR, promocode)
        self.click(self.ACCEPT_CODE_BUTTON_LOCATOR)