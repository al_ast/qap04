from base_page import BasePage

class AccountPage(BasePage):

    BUTTON_ENTER_LOCATOR = "//span[text()='Войти']"

    def click_enter_button(self):
        self.wait(self.BUTTON_ENTER_LOCATOR)
        self.click(self.BUTTON_ENTER_LOCATOR)