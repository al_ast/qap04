from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    CATEGORY_LOCATOR = "//a[text()='{}']"
    BASKET_BUTTON_LOCATOR = "//div[@class='headerCart']"
    ACCOUNT_BUTTON_LOCATOR = "//span[text()='Аккаунт']"

    def __init__(self, driver):
        self.driver = driver

    def click(self, locator):
        self.wait(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.click()

    def open_site(self, url):
        self.driver.get(url)

    def enter_text(self, locator, text):
        self.wait(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.send_keys(text)

    def open_homepage(self):
        self.driver.get('https://www.21vek.by/')

    def select_category(self, category):
        locator = self.CATEGORY_LOCATOR.format(category)
        self.wait(locator)
        self.click(locator)

    def click_basket_button(self):
        self.wait(self.BASKET_BUTTON_LOCATOR)
        self.click(self.BASKET_BUTTON_LOCATOR)

    def click_account_button(self):
        self.wait(self.ACCOUNT_BUTTON_LOCATOR)
        self.click(self.ACCOUNT_BUTTON_LOCATOR)

    def wait(self, locator, time_out=5):
        WebDriverWait(self.driver, time_out).until(EC.presence_of_element_located((By.XPATH, locator)))