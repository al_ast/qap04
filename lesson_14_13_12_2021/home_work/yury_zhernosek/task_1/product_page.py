from base_page import BasePage

class ProductPage(BasePage):

    BUTTON_ADD_TO_BASKET_LOCATOR = "//span[text()='{}']//ancestor::li//button[text()='В корзину']|" \
                                   "//h1[text()='{}']//ancestor::div//button[text()='В корзину']"

    def click_add_to_basket(self, name_product):
        locator = self.BUTTON_ADD_TO_BASKET_LOCATOR.format(name_product, name_product)
        self.click(locator)