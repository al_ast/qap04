# Сайт https://www.21vek.by/
# У меня только через дебаг работает

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from home_page import HomePage
from tvs_page import TvsPage
from fridges_page import FridgesPage
from account_page import AccountPage
from account_form import AccountForm
from basket_page import BasketPage

driver = webdriver.Chrome(ChromeDriverManager().install())

home_page = HomePage(driver)
tvs_page = TvsPage(driver)
fridges_page = FridgesPage(driver)
basket_page = BasketPage(driver)
account_page = AccountPage(driver)
account_form = AccountForm(driver)

driver.maximize_window()

driver.get('https://www.21vek.by/')
home_page.select_category('Телевизоры')
tvs_page.select_tv('Телевизор Horizont 32LE5511D')
tvs_page.select_category('Холодильники')
fridges_page.click_add_to_basket('Холодильник с морозильником LG GA-B419SLGL')
fridges_page.click_account_button()
account_page.click_enter_button()
account_form.enter_email('urazernosek@gmail.com')
account_form.enter_password('asfsagrsgd34')
# account_form.click_enter_button()
account_form.click_close_button()
fridges_page.click_basket_button()
basket_page.enter_promocode('Some promocode')
basket_page.click_placing_order()