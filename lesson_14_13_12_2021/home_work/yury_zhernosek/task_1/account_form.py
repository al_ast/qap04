from base_page import BasePage

class AccountForm(BasePage):

    INPUT_LOCATOR = "//input[@label='{}']"
    BUTTON_ENTER_LOCATOR = "//button[text()='Войти']"
    CLOSE_BUTTON_LOCATOR = "//div[@aria-label='Закрыть']"

    def enter_email(self,email):
        locator = self.INPUT_LOCATOR.format('Электронная почта')
        self.enter_text(locator, email)

    def enter_password(self, password):
        locator = self.INPUT_LOCATOR.format('Пароль')
        self.enter_text(locator, password)

    def click_enter_button(self):
        self.click(self.BUTTON_ENTER_LOCATOR)

    def click_close_button(self):
        self.click(self.CLOSE_BUTTON_LOCATOR)