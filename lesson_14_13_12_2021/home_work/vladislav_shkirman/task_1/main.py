"""
Задание1: Переделать домашку 13 к виду из урока 14
Добавить пару своих новых действий или страниц к уже
существующим
"""
from header import Header
from catalog_items_page import CatalogItemsPage
from product_page import ProductPage
from cart_page import CartPage

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(ChromeDriverManager().install())

url = 'https://www.21vek.by/'
main_page = Header(driver)
main_page.open_homepage(url)
main_page.maximize_window()
main_page.open_category('Телевизоры')

catalog_page = CatalogItemsPage(driver)
catalog_page.open_product_page('Телевизор Samsung UE24N4500AU')

product_page = ProductPage(driver)
product_page.add_to_cart()
product_page.open_cart_page()

cart_page = CartPage(driver)
cart_page.delete_item('Телевизор Samsung UE24N4500AU')

main_page.search_item('смартфон')
main_page.apply_price_filter(450, 500)
main_page.get_first_n_item_names(3)

main_page.close_browser()
