from base_page import BasePage


class CartPage(BasePage):
    DELETE_ITEM = "//a[text()='{}']/ancestor::tr//a[contains(@id,'delete')]"

    def delete_item(self, item_name):
        self.click_element(self.DELETE_ITEM.format(item_name))
