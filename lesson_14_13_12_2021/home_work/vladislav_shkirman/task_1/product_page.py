from header import Header


class ProductPage(Header):
    ADD_TO_CART = "//form[@class='j-to_basket']"

    def add_to_cart(self):
        self.click_element(self.ADD_TO_CART)
