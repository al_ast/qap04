from base_page import BasePage


class CatalogItemsPage(BasePage):
    ITEM_NAME_LOCATOR = "//span[text()='{}']"

    def open_product_page(self, item_name):
        self.click_element(self.ITEM_NAME_LOCATOR.format(item_name))
