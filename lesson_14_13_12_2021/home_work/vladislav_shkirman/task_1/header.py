from base_page import BasePage


class Header(BasePage):
    CATEGORY = "//a[text()='{}']"
    CART_BUTTON = "//span[@class='headerCartIcon']"
    SEARCH_INPUT = "//input[@id='catalogSearch']"
    SEARCH_BUTTON = "//button[contains(@class,'CatalogSearch')]"
    NAME_ITEM = "//span[@class='result__name']"
    PRICE_FROM = "//input[@name='filter[price][from]']"
    PRICE_TO = "//input[@name='filter[price][to]']"
    FILTER_BUTTON = "//button[@id='j-filter__btn']"

    def open_category(self, category_name):
        self.click_element(self.CATEGORY.format(category_name))

    def open_cart_page(self):
        self.click_element(self.CART_BUTTON)

    def search_item(self, user_input):
        self.enter_text(self.SEARCH_INPUT, user_input)
        self.click_element(self.SEARCH_BUTTON)

    def get_first_n_item_names(self, number):
        print(*self.get_text(self.NAME_ITEM)[:number], sep='\n')

    def apply_price_filter(self, price_from, price_to):
        self.enter_text(self.PRICE_FROM, price_from)
        self.enter_text(self.PRICE_TO, price_to)
        self.click_element(self.FILTER_BUTTON)

