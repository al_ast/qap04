from base_page import BasePage


class MainPage(BasePage):
    EXPLORE = "//a[@href='/feed/explore']"
    SUBSCRIPTIONS = "//a[@href='/feed/subscriptions']"
    LIBRARY = "//a[@href='/feed/library']"
    HISTORY = "//a[href='/feed/history']"

    def click_explore(self):
        self.click(self.EXPLORE)

    def click_subscriptions(self):
        self.click(self.SUBSCRIPTIONS)

    def click_library(self):
        self.click(self.LIBRARY)

    def go_history(self):
        self.click(self.HISTORY)


