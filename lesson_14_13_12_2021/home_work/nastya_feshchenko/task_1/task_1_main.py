import time

from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver

from base_page import BasePage
from main_page import MainPage
from explore_page import Explore
from trends_page import Trends
from subscriptions_page import Subscriptions

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.youtube.com")

main_page = MainPage(driver)
main_page.click_explore()

explore_page = Explore(driver)
explore_page.click_trends()

trends_page = Trends(driver)
trends_page.play_video("MORGENSHTERN - Домой (Official Video, 2021)")

time.sleep(5)

trends_page.back_to_main()
time.sleep(5)
main_page.click_subscriptions()

subscriptions_page = Subscriptions(driver)
subscriptions_page.manage_channels()

time.sleep(5)
