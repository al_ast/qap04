from base_page import BasePage
from main_page import MainPage

class Explore(BasePage):
    TRENDING = "//a[@id='destination-content-root']"

    def click_trends(self):
        self.click(self.TRENDING)
