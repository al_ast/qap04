from base_page import BasePage
from main_page import MainPage


class Trends(BasePage):
    VIDEO = "//a[@id='video-title']"

    def play_video(self, title):
        self.click(self.VIDEO)
