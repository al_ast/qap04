from base_page import BasePage

class Subscriptions(BasePage):
    MANAGE_CHANNELS = "//a[@href='/feed/channels']"

    def manage_channels(self):
        self.click(self.MANAGE_CHANNELS)
