from lesson_14_13_12_2021.class_materials.BasePage import BasePage


class LoginPage(BasePage):
    USER_NAME_INPUT_LOCATOR = "#user-name"
    USER_PASSWORD_LOCATOR = "#password"
    LOGIN_BUTTON_LOCATOR = "#login-button"

    def enter_user_name(self, value):
        self.enter_text(
            self.USER_NAME_INPUT_LOCATOR, value)

    def enter_password(self, value):

        self.enter_text(
            self.USER_PASSWORD_LOCATOR, value
        )

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)
