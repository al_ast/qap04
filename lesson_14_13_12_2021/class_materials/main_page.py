from lesson_14_13_12_2021.class_materials.BasePage import BasePage


class MainPage(BasePage):
    TITLE_LOCATOR = ".header_secondary_container"
    SHOPPING_CART_LOCATOR = ".shopping_cart_link"

    def get_title(self):
        return self.get_text(self.TITLE_LOCATOR)

    def click_cart(self):
        self.click(self.SHOPPING_CART_LOCATOR)

    def click_filter(self):
        self.click("locator")
