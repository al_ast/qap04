from webdriver_manager.chrome import ChromeDriverManager

from lesson_14_13_12_2021.class_materials.login_page import LoginPage
from selenium import webdriver

from lesson_14_13_12_2021.class_materials.main_page import MainPage

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://www.saucedemo.com/")

login_page = LoginPage(driver)

login_page.enter_user_name("standard_user")
login_page.enter_password("secret_sauce")
login_page.click_login_button()

main_page = MainPage(driver)

title = main_page.get_title()
print(title)

main_page.click_cart()
