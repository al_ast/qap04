#- In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.
firstname = "Ivan"
lastname = "Ivanov"
name1 = "{} {}"
print(firstname, lastname)
print(name1.format(lastname, firstname))



# Replace “  # ” to “/” in the 'www.my_site.com#about'
x = "www.my_site.com#about"
print(x.replace('#', '/'))