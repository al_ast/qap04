number = 3
date = "01.11.2021"
themes = "conditions, loops, string formats"
status = "finished"

print(f"Lesson number: {number}. Date: {date}. Themes: {themes}. Status: {status}")
print(f"Lesson number: {3}. Date: 01.11.2021. Themes: conditions, loops, string formats. Status: finished")

string_template = "Lesson number: {}. Date: {}. Themes: {}. Status: {}."
print(string_template.format(3, "01.11.2021", "conditions, loops, string formats", "finished"))
print(string_template.format(number, date, themes, status))

string_template = "Lesson number: {0}. Date: {1}. Themes: {2}. Status: {3}."
print(string_template.format(number, date, themes, status))

string_template = "Lesson number: {2}. Date: {0}. Themes: {3}. Status: {1}."
print(string_template.format(date, status, number, themes ))

string_template = "Lesson number: {info[0]}. Date: {info[1]}. Themes: {info[2]}. Status: {info[3]}."
print(string_template.format(info=[number, date, themes, status]))