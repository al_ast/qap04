# -- Replace “#” to “/” in the 'www.my_site.com#about'

string = 'www.my_site.com#about'
print(string.replace('#', '/'))

# -- In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.
# -e.g
# -“Petr Ivanov” => “Ivanov Petr”

string_name = 'Vlad Shkirman'
split = string_name.split()
print(f'\n{split[1]} {split[0]}')