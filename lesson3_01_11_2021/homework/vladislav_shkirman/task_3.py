# -3**
# -Быки и коровы
# -
# -В классическом варианте игра рассчитана на двух игроков.
# Каждый из игроков задумывает и записывает тайное 4-значное число
# -с неповторяющимися цифрами. Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
# -Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое противнику.
# -Противник сообщает в ответ,
# сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров)
# -и сколько угадано вплоть до позиции в тайном числе (то есть количество быков).
# -При игре против компьютера игрок вводит комбинации одну за другой, пока не отгадает всю последовательность.
# -Ваша задача реализовать программу, против которой можно сыграть в "Быки и коровы"
# -
# -Пример
# -Загадано число 3219
# ->>> 2310
# -Две коровы, один бык
# ->>> 3219
# -Вы выиграли!

from ClassGenerateNumber import GenerateRandomNumber

computer_number = (GenerateRandomNumber().random_number())
print(str(computer_number))
secret_number_list = list(computer_number)
bull = 0
cow = 0
while bull != 4:
    bull = 0
    cow = 0
    guess = list(str(input("Enter the number: ")))
    for i, word in enumerate(guess):
        if guess[i] in secret_number_list:
            if guess[i] == secret_number_list[i]:
                bull += 1
                if bull == 4:
                    print("You've won!")
                    break
            else:
                cow += 1
    print(f'cow quantity: {cow} ')
    print(f'bull quantity: {bull} ')
