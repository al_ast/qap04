import random


class GenerateRandomNumber:
    def random_number(self):
        secret_number = ''
        while len(secret_number) != 4:
            n = (random.randint(0, 9))
            if str(n) not in secret_number:
                secret_number += str(n)
        return secret_number
