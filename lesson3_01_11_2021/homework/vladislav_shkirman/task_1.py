# 1. Given lesson information(up to you which information).
# Print it using f-strings and .format every possible way you know

lesson = 2
topic = 'Git'
minute = 5

string_example = 'On lesson {} we were discussing the topic {} during {} min'
print('Simple format with ".format" method without order:')
print(string_example.format(3, 'conditions', 12))

print('\nSimple format with "f" method without order:')
string_example = f'On lesson {lesson} we were discussing the topic {topic} during {minute} min'
print(string_example.format(lesson, topic, minute))
print('\nSimple format with more parameters passed:')
print(string_example.format(1, 'math operations', 40, 'test', 1, 'parameters'))

string_example = 'On lesson {0} we were discussing the topic {1} during {2} min'
print('\nSimple format with sequence order:')
print(string_example.format(lesson, topic, minute))

string_example = 'On lesson {2} we were discussing the topic {0} during {1} min'
print('\nSimple format with random order:')
print(string_example.format(lesson, topic, minute))

string_example = 'On lesson {1} we were discussing the topic {1} during {2} min'
print('\nSimple format with duplicate order:')
print(string_example.format(lesson, topic, minute))

string_example = 'On lesson {info[0]} we were discussing the topic {info[1]} during {info[2]} min'
print("\nFormat with array as parameter:")
print(string_example.format(info=[lesson, topic, minute]))