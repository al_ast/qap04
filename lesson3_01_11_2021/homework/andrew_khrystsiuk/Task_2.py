# - Replace “#” to “/” in the 'www.my_site.com#about'
# - In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.

web = "www.my_site.com#about"
web1 = web.replace("#", "/")
print(web1)

name = "Andrew Vitalievich"
name1 = name.split()
print(f"{name1[1]} {name1[0]}")
