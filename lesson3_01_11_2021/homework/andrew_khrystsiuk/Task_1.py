# Given lesson information(up to you which information).
# Print it using f-strings and .format every possible way you know

color = "red"
model = "ford"
current_mileage = "10"
max_mileage = f"{int(current_mileage) + int(1000)}"

print("\nThis is", color, model, "car.", "It's current mileage is", current_mileage, ", max mileage is", max_mileage)

stings = "\nThis is {} {} car. It's current mileage is {}, max mileage is {}"
print(stings.format(color, model, current_mileage, max_mileage))

stings = "\nThis is {} {} car. It's current mileage is {}, max mileage is {}"
print(stings.format("red", "ford", "10", f"{int(current_mileage) + int(1000)}"))

stings = "\nThis is {0} {1} car. It's current mileage is {2}, max mileage is {3}"
print(stings.format(color, model, current_mileage, max_mileage))

stings = f"\nThis is {color}, {model} car. It's current mileage is {current_mileage}, max mileage is {max_mileage}"
print(stings)
