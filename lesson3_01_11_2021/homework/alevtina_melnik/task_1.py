Name = "Alevtina"
Like = "read books"
Number_books_month = 3

#1 Способ
Information = "My names is {}. I like {}, number of books read per month {}"
print(Information.format("Alevtina","read books",3))
print(Information.format(Name,Like,Number_books_month))

#2 Способ с корректнм текстом
Information = "My names is {0}. I like {1}, number of books read per month {2}"
print(Information.format(Name,Like,Number_books_month))

#2 Способ с некорректным текстом из-за перестановки индексов
Information = "My names is {2}. I like {0}, number of books read per month {1}"
print(Information.format(Name,Like,Number_books_month))

#3 Способ
Information = "My names is {info[0]}. I like {info[1]}, number of books read per month {info[2]}"
print(Information.format(info=[Name,Like,Number_books_month]))

#4 Способ
print(f"My names is {Name}. I like {Like}, number of books read per month {Number_books_month}")