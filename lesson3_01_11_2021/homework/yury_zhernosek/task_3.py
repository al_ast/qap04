# 3**
# Быки и коровы
#
# В классическом варианте игра рассчитана на двух игроков. Каждый из игроков задумывает и записывает тайное 4-значное число
# с неповторяющимися цифрами. Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
# Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое противнику.
# Противник сообщает в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров)
# и сколько угадано вплоть до позиции в тайном числе (то есть количество быков).
# При игре против компьютера игрок вводит комбинации одну за другой, пока не отгадает всю последовательность.
# Ваша задача реализовать программу, против которой можно сыграть в "Быки и коровы"


import random

computer_number = 0
while len(set(str(computer_number))) != 4:
    computer_number = random.randint(1000, 9999)
computer_number = str(computer_number)
print(computer_number)                               #Added for test. Deleted after test
bull = 0
while bull != 4:
    cow = 0
    bull = 0
    player_number = input('Your number: ')
    while len(set(str(player_number))) != 4:
        print('Error! Invalid number!')
        player_number = input('Your number: ')
    for number in player_number:
        if number in computer_number:
            cow += 1
    for i in range(4):
        if player_number[i] == computer_number[i]:
            bull += 1
            cow -= 1
    if bull < 4:
        print(f'Value cows: {cow}. Value bulls: {bull}. Try again!')
    else:
        print("Congratulations! You won!")