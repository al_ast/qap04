#2*.
# Replace “#” to “/” in the 'www.my_site.com#about'
# In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.

site='www.my_site.com#about'
site=site.replace('#', '/')
print(site)
name='Petr Ivanov'
first_word = name[:name.find(' ')]
second_word = name[name.find(' ') + 1:]
print(second_word + ' ' + first_word)