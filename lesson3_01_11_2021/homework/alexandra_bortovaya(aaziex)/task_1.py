"""
Given lesson information(up to you which information).
Print it using f-strings and .format every possible way you know
Note: Don't copy code from the class materials. Try to read it only and write on your own
"""

number = 3
date = "01.11.2020"
themes = "Git branches, init method, string formats, conditionals, loops"
status = "Done"

print(f"Lesson number: {number}. Date: {date}. Themes: {themes}. Status: {status}.")

template = "Lesson number: {}. Date: {}. Themes: {}. Status: {}."
print(template.format(3, "01.11.2020", "Git branches, init method, string formats, conditionals, loops", "Done"))
print(template.format(number, date, themes, status))

template = "Lesson number: {1}. Date: {3}. Themes: {0}. Status: {2}."
print(template.format(themes, number, status, date))

template = "Lesson number: {info[1]}. Date: {info[3]}. Themes: {info[0]}. Status: {info[2]}."
print(template.format(info=[themes, number, status, date]))


