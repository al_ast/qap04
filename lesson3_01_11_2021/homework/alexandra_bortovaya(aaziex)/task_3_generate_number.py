import random


class GenerateNumber:

    def generate_number(self):
        number = ""
        while len(number) != 4:
            character = str(random.randint(0, 9))
            if character not in number:
                number += character
        return number
