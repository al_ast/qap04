"""
Быки и коровы
В классическом варианте игра рассчитана на двух игроков. Каждый из игроков задумывает и записывает тайное 4-значное
число с неповторяющимися цифрами. Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое противнику.
Противник сообщает в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров)
и сколько угадано вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой, пока не отгадает всю последовательность.
Ваша задача реализовать программу, против которой можно сыграть в "Быки и коровы"
"""
from task_3_generate_number import GenerateNumber


generated = GenerateNumber()
computer_number = generated.generate_number()
print(f"Hint: computer number = {computer_number}")

bull = 0

while bull != 4:
    cow = 0
    bull = 0
    user_number = input("Enter your number: ")
    for number in user_number:
        if number in computer_number:
            cow += 1
    for i in range(4):
        if user_number[i] == computer_number[i]:
            bull += 1
            cow -= 1
    if bull < 4:
        if bull == 1:
            bull_text = "bull"
        else:
            bull_text = "bulls"
        if cow == 1:
            cow_text = "cow"
        else:
            cow_text = "cows"
        print(f"{bull} {bull_text}, {cow} {cow_text}, try again!")
    else:
        print("Congratulations! You won!")
