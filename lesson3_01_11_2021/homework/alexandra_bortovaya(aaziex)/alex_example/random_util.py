import random


def generate_random_number_with_unique_digits(length=4):
    number = ""
    while len(number) != length:
        character = str(random.randint(0, 9))
        if character not in number:
            number += character
    return number
