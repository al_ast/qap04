"""
Быки и коровы
В классическом варианте игра рассчитана на двух игроков. Каждый из игроков задумывает и записывает тайное 4-значное
число с неповторяющимися цифрами. Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое противнику.
Противник сообщает в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров)
и сколько угадано вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой, пока не отгадает всю последовательность.
Ваша задача реализовать программу, против которой можно сыграть в "Быки и коровы"
"""

from random_util import generate_random_number_with_unique_digits

number_to_guess = generate_random_number_with_unique_digits()
print(f"Hint: Number to guess = {number_to_guess}")


def get_single_or_plural_form(word, count):
    if count == 1:
        return f"{count} {word}"

    return f"{count} {word}s"


while True:
    cows_count = 0
    bulls_count = 0
    user_number = input("Enter your number: ")

    for i in range(4):
        if user_number[i] == number_to_guess[i]:
            bulls_count += 1
        elif user_number[i] in number_to_guess:
            cows_count += 1

    if bulls_count == 4:
        print("Congratulations! You won!")
        break
    else:
        print(f"{get_single_or_plural_form('bull', bulls_count)}, "
              f"{get_single_or_plural_form('cow', cows_count)}")
