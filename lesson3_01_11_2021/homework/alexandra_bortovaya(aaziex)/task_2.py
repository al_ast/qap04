"""
- Replace “#” to “/” in the 'www.my_site.com#about'
- In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.

e.g
“Petr Ivanov” => “Ivanov Petr”
"""

site = "www.my_site.com#about"
print(site.replace("#", "/"))

full_name = "Petr Ivanov"
splitted_name = full_name.split()
print(f"\n{splitted_name[1]} {splitted_name[0]}")
