name="Pavel."
date= "01.11.2021."
themes="lesson 3."
status="full completed."
print("\nMy name is", name, "Date:", date, "Themes:", themes, "Status:", status)
#
stings="\nMy name is {} Date: {} Themes: {} Status: {}"
print(stings.format(name, date, themes, status))
#
stings="\nMy name is {}. Date: {}. Themes: {}. Status: {}"
print(stings.format("Pavel", "01.11.2021", "lesson 3", "full completed"))
#
stings="\nMy name is {0} Date: {1} Themes: {2} Status: {3}"
print(stings.format(name, date, themes, status))
#
stings=f"\nMy name is {name} Date: {date} Themes: {themes} Status: {status}"
print(stings)
