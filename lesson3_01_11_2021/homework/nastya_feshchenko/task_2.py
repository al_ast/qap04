#2*.

#- Replace “#” to “/” in the 'www.my_site.com#about'


#- In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.

#e.g

#“Petr Ivanov” => “Ivanov Petr”



site = "www.my_site.com#about"

edited_site = site.replace("#", "/")

print(edited_site)



firstname = str("Johny")

lastname = str("Depp")

print(str(firstname), str(lastname))

print(str(lastname), str(firstname))