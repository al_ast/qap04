#Given lesson information(up to you which information).

#Print it using f-strings and .format every possible way you know

#Note: Don't copy code from the class materials. Try to read it only and write on your own


#Example:

#print(f"Lesson nuber: {number}. Date: {date}. Themes:{themese}. Status:{status}

#"Lesson numer: 3. Date: 01.11.2021. Themes: string formats. Status: in progress"



name = "Nastya"

age = 24

hobby = "watching movies"



string_template = "My name is {}. I’m {} years old. My hobby is {}."

print("Simple format method without order:")

print(string_template.format("Nastya", 24, "watching movies"))

print(string_template.format(name, age, hobby))



string_template = "My name is {}. I’m {} years old. My hobby is {}."

print("\nSimple format method without order with more parameters:")

print(string_template.format("Nastya", 24, "watching movies", 100, "abc", 777))

print(string_template.format(name, age, hobby))



print("\nSimple format method with sequence order:")

string_template = "My name is {0}. I'm {1} years old. My hobby is {2}."

print(string_template.format(name, age, hobby))



print("\nSimple format method with random order:")

string_template = "My name is {1}. I'm {2} years old. My hobby is {0}."

print(string_template.format(name, age, hobby))



print("\nSimple format method with duplicate index:")

string_template = "My name is {1}. I'm {2} years old. My hobby is {2}."

print(string_template.format(name, age, hobby))



print("\nFormat with array as parameter:")

string_template = "My name is {info[0]}. I'm {info[1]} years old. My hobby is {info[2]}."

print(string_template.format(info=[name, age, hobby]))