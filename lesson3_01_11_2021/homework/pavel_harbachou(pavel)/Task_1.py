#Given lesson information(up to you which information).
#Print it using f-strings and .format every possible way you know
#Note: Don't copy code from the class materials. Try to read it only and write on your own


lesson_number = 3
date = '01.11.2021'
theme = 'string formats'
status = 'ended'

text = 'Lesson number : {} . Today {} . Theme of lesson {} . Lesson status : {} .'
print(text.format(3, '01.11.2021', 'string formats', 'ended'))
print(text.format(lesson_number, date, theme, status))

text = 'Lesson number : {} . Today {} . Theme of lesson {} . Lesson status : {} .'
print(text.format(3, '01.11.2021', 'string formats', 'ended', 22))
print(text.format(lesson_number, date, theme, status))

text = 'Lesson number : {0} . Today {1} . Theme of lesson {2} . Lesson status : {3} .'
print(text.format(3, '01.11.2021', 'string formats', 'ended'))
print(text.format(lesson_number, date, theme, status))

text = 'Lesson number : {0} . Today {0} . Theme of lesson {2} . Lesson status : {2} .'
print(text.format(3, '01.11.2021', 'string formats', 'ended'))
print(text.format(lesson_number, date, theme, status))

text = 'Lesson number : {info[0]} . Today {info[1]} . Theme of lesson {info[2]} . Lesson status : {info[3]} .'
print(text.format(info = [lesson_number, date, theme, status]))

print(f'Lesson number : {lesson_number} . Today {date} . Theme of lesson {theme} . Lesson status : {status} .')



