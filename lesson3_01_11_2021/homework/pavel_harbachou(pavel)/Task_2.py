#- Replace “#” to “/” in the 'www.my_site.com#about'
#- In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.

text = 'www.my_site.com#about'
print(text.replace('#' , '/'))

text = 'Petrov Ivan'
print(text.replace('Ivan' , 'Petrov') .replace('Petrov', 'Ivan', 1))

