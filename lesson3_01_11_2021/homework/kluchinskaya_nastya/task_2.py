# - Replace “#” to “/” in the 'www.my_site.com#about'
# - In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.
# - Заменить “#” на “/” в 'www.my_site.com#about '
# - В строке “Иван Иванов” поменяйтесь словами. Может быть предоставлена любая строка с именем и фамилией.

site = "www.my_site.com#about"
print(site.replace("#", "/"))

full_name = "Petr Ivanov"
split_name = full_name.split()
print(f"\n{split_name[1]} {split_name[0]}")