# Given lesson information(up to you which information).
# Print it using f-strings and .format every possible way you know
# Note: Don't copy code from the class materials. Try to read it only and write on your own
number = 3
date = "The first of November"
theme = "String formats"

lesson_template = "The lesson number: {}. Date of the lesson: {}. The theme of the lesson: {}"
print(lesson_template.format(3, "The first of November", "string formats"))
print(lesson_template.format(number, date, theme))
print(lesson_template.format(3, "The first of November", "string formats", 55, "people in the class"))
print(f"The lesson number: {number}. Date of the lesson: {date}. The theme of the lesson: {theme}")
print(f"The lesson number: %d. Date of the lesson: %s. The them of the lesson: %s" % (number, date, theme))

lesson_template = "The lesson number: {0}. Date of the lesson: {1}. The theme of the lesson: {2}"
print(lesson_template.format(number, date, theme))

lesson_template = "The lesson number: {2}. Date of the lesson: {0}. The theme of the lesson: {1}"
print(lesson_template.format(number, date, theme))

lesson_template = "The lesson number: {info[0]}. Date of the lesson: {info[1]}. The theme of the lesson: {info[2]}"
print(lesson_template.format(info=[number, date, theme]))