# Given lesson information(up to you which information).Print it using f-strings and .format every possible way you know

school = "TMS"
specialization = "QA automation"
group = "QAP04"
cabinet = "805"

string_template = "\nHello,student! Welcome to {}.You will be learn on {}.Ur group is {}. " \
                  "The first lesson will be hold in {} classroom"
print(string_template.format("TMS", "QA automation", "QAP04", 805))
print(string_template.format(school, specialization, group, cabinet))


string_template = "\nHello,student! Welcome to {}.You will be learn on {}.Ur group is {}. " \
                  "The first lesson will be hold in {} classroom"
print(f"\nHello,student! Welcome to {school}.You will be learn on {specialization}.Ur group is {group}."
      f"The first lesson will be hold in {cabinet} classroom")


string_template = "\nHello,student! Welcome to {1}.You will be learn on {1}.Ur group is {0}. " \
                  "The first lesson will be hold in {3} classroom"
print(string_template.format(school, specialization, group, cabinet))


string_template = "\nHello,student! Welcome to {info[0]}.You will be learn on {info[1]}.Ur group is {info[2]}. " \
                  "The first lesson will be hold in {info[3]} classroom"
print(string_template.format(info = [school,specialization,group, cabinet]))
