# Replace “  # ” to “/” in the 'www.my_site.com#about'

site_name = "www.my_site.com#about"
site_name = site_name.replace("#", "/")
print(site_name)

# In the string “Ivan Ivanov” swap words. Any string
# with firstname and lastname could be provided.

name = "Dzianis Savchik"
print(name)
name_rever = name.split()
name = list(reversed(name_rever))
print(" ".join(name))
