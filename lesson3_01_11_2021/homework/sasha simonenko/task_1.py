number = 3
date = '01.11.2021'
theme = 'string formats'
status = 'in progress'

print(f'Lesson number: {number}. Date: {date}. Theme: {theme}. Status: {status}')

template = 'Lesson number: {}. Date: {}. Theme: {}. Status: {}'
print(template.format(3, '01.11.2021', 'string formats', 'in progress'))

print(template.format(number, date, theme, status))

template = 'Lesson number: {1}. Date: {2}. Theme: {3}. Status: {0}.'
print(template.format(status, number, date, theme))

template = 'Lesson number: {info[3]}. Date: {info[0]}. Theme: {info[2]}. Status: {info[1]}'
print(template.format(info=[date, status, theme, number]))

