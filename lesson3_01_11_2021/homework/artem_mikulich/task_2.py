#1)In the string “Ivan Ivanov” swap words. Any string with firstname and lastname could be provided.
#2)Replace “#” to “/” in the 'www.my_site.com#about'.
full_name = "Artem Mikulich"
swap_names = full_name.split()
swap_names = list(reversed(swap_names))
print(" ".join(swap_names))

site_name = "www.my_site.com#about"
site_name = site_name.replace("#", "/")
print(site_name)