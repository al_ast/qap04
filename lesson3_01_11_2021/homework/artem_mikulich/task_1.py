#Given lesson information(up to you which information).Print it using f-strings and .format every possible way you know
name = "Artem"
age = 20
hobby = "football"
education = "physicist, engineer"
course = 4
string_template = "\nMy name is: {}, I'm : {}, My hobby is: {}, By education I am: {}, My last course {}  "
print(string_template.format(name, age, hobby, education, course))
string_template = f"\nMy name is: {name}, I'm : {age}, My hobby is: {hobby}, By education I am: {education}, My last course {course}  "
print(string_template)

first_planet = "Mercury"
second_planet = "Venus"
third_planet = "Earth"
fourth_planet = "Mars"
fifth_planet = "Jupiter"
sixth_planet = "Saturn"
seventh_planet = "Uranium"
eighth_planet = "Neptune"
string_template = f"\nList of planets of the solar system from the sun : {fifth_planet}, {second_planet}, {third_planet}, {fourth_planet}, {first_planet}, {sixth_planet}, {seventh_planet}, {eighth_planet}"
print(string_template)

planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranium", "Neptune"]
string_template = f"We live on a planet: {planets[2]}"
print(string_template)


name1 = "Atrem"
age1 = 20
sity = "Mogilev"
string_template1 = "My name is {0}, I'm: {1}, I was born in the city: {2}"
print(string_template1.format(name1, age1, sity))