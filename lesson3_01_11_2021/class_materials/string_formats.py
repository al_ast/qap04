name = "Alex"
age = 26
people_in_the_class = 14

string_template = "My names is: {}. I'm: {} years old. There are {} people in the class"
print("Simple format method without order:")
print(string_template.format("Alex", 26, 14))
print(string_template.format(name, age, people_in_the_class))
####################################################################

string_template = "My names is: {}. I'm: {} years old. There are {} people in the class"
print("\nSimple format method without order with more parameters:")
print(string_template.format("Alex", 26, 14, 100, "abc", 777))
print(string_template.format(name, age, people_in_the_class))
####################################################################

print("\nSimple format method with sequence order:")
string_template = "My names is: {0}. I'm: {1} years old. There are {2} people in the class"
print(string_template.format(name, age, people_in_the_class))
####################################################################

print("\nSimple format method with random order:")
string_template = "My names is: {1}. I'm: {2} years old. There are {0} people in the class"
print(string_template.format(name, age, people_in_the_class))
####################################################################

print("\nSimple format method with duplicate index:")
string_template = "My names is: {1}. I'm: {2} years old. There are {2} people in the class"
print(string_template.format(name, age, people_in_the_class))
####################################################################

print("\nFormat with array as parameter:")
string_template = "My names is: {info[0]}. I'm: {info[1]} years old. There are {info[2]} people in the class"
print(string_template.format(info=[name, age, people_in_the_class]))
####################################################################
