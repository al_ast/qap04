if True:
    print("True")

if False:
    print("False")

if 5 > 3:
    print("5 > 3")

a = 5
b = 4
if a > 3:
    print("a>3")
else:
    print("a <=3")

if a > 3:
    print("a > 3")
else:
    if a > 2:
        print("a > 2")

if a > 5 and b < 4:
    print("double condition")