from class_weapon import Weapon
from class_ammunition import Ammunition
from class_robot import Robot

sword = Weapon("Sword", 500, 600)
gun = Weapon("Gun", 2000, 1000)

armour = Ammunition("Armour", 1000, 1500)
shield = Ammunition("Shield", 1000, 800)

robot1 = Robot("Optimus", sword, shield)
robot2 = Robot("Bumblebee", gun, armour)

print(robot1)
print(robot2)
