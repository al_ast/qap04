from class_robot import Robot
from class_weapon import Weapone
from class_armor import Armor

gun = Weapone("blaster", 100, 60)

armor = Armor("exoskeleton", 100, 30)

robot = Robot("bumblebee", gun, armor)

print(robot)