# Шахматный король ходит по горизонтали, вертикали и диагонали,
# но только на 1 клетку. Даны две различные клетки шахматной доски,
# определите, может ли король попасть с первой клетки на вторую одним ходом.
# Пример
# Cell 1 coordinates:
# >>> 4, 4
# Cell 2 coordinated:
# >>> 5, 5
# YES
#
# Конь
# Определите, может ли конь попасть с первой клетки на вторую одним ходом.
#
# Ладья
# Определите, может ли ладья попасть с первой клетки на вторую одним ходом.
#
# Ферзь
# Определите, может ли ферзь попасть с первой клетки на вторую одним ходом.
#
# Слон
# определите, может ли слон попасть с первой клетки на вторую одним ходом.


class Shahmaty:
    @staticmethod
    def king(x, y, x1, y1):
        a = abs(x1 - x)
        b = abs(y1 - y)
        if a == 1 or b == 1:
            print('King can')
        else:
            print('King cant')


    @staticmethod
    def horse(x, y, x1, y1):
        a = abs(x1 - x)
        b = abs(y1 - y)
        if a == 2 and b == 1:
            print('Can')
        elif b == 2 and a == 1:
            print('Horse can')
        else:
            print('Horse cant')

    @staticmethod
    def rook(x, y, x1, y1):
        a = abs(x1 - x)
        b = abs(y1 - y)
        if (a == 0 and b != 0) or (b == 0 and a != 0):
            print('Rook can')
        else:
            print('Rook cant')

    @staticmethod
    def queen(x, y, x1, y1):
        a = abs(x1 - x)
        b = abs(y1 - y)
        if (a == 0 and b != 0) or (b == 0 and a != 0) or a == b:
            print('Queen can')
        else:
            print('Queen cant')

    @staticmethod
    def elephant(x, y, x1, y1):
        a = abs(x1 - x)
        b = abs(y1 - y)
        if a == b:
            print('Elephant can')
        else:
            print('Elephant cant')