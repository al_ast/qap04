class Robot:

    def __init__(self, name, guns, armors, health):
        self.name = name
        self.guns = guns
        self.armors = armors
        self.health = health
        self.full_armor = 0
        self.full_damage = 0
        for armor in armors:
            self.full_armor += armor.strength
        for gun in guns:
            self.full_damage += gun.damage

    def __repr__(self):
        return f'Guns: {self.guns}. Armor: {self.armors}'

    def hit(self, first_robot, second_robot):
        last_damage = 0
        if second_robot.health - first_robot.full_damage > 0:
            if second_robot.full_armor > 0:
                if second_robot.full_armor > first_robot.full_damage:
                    second_robot.full_armor = second_robot.full_armor - first_robot.full_damage
                    print(f'The {second_robot.name} has {second_robot.full_armor} armor and {second_robot.health} hp left')
                else:
                    last_damage = first_robot.full_damage - second_robot.full_armor
                    second_robot.full_armor = 0
            else:
                if last_damage > 0:
                    second_robot.health = second_robot.health - last_damage
                else:
                    second_robot.health = second_robot.health - first_robot.full_damage
                    print(f'The {second_robot.name} has {second_robot.health} hp left')
        else:
            print(f'{second_robot.name} dead')