from lesson9_22_11_2021.homework.yury_zhernosek.alex_example.robot import Robot
from lesson9_22_11_2021.homework.yury_zhernosek.class_armor import Armor
from lesson9_22_11_2021.homework.yury_zhernosek.class_gun import Gun

gun1 = Gun('AK-47', 30)
gun2 = Gun('Rocket launcher', 200)

armor1 = Armor('Helmet', 100)
armor2 = Armor('Chestplate', 300)

robot1 = Robot('Optimus', [gun2, gun1], [], 1000)
robot2 = Robot('Bumblebee', [gun2], [armor2], 1100)

while robot2.health > 0:
    robot1.hit(robot2)
    print("_____________")
robot1.hit(robot2)
# while robot1.health > 0:
#     robot2.hit(robot1)
#     print("_____________")
# robot2.hit(robot1)