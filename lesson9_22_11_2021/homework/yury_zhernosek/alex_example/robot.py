class Robot:

    def __init__(self, name, guns, armors, health):
        self.name = name
        self.guns = guns
        self.armors = armors
        self.health = health
        self._full_armor = 0
        self._full_damage = 0

        for armor in armors:
            self._full_armor += armor.strength
        for gun in guns:
            self._full_damage += gun.damage

    def __repr__(self):
        return f'Guns: {self.guns}. Armor: {self.armors}'

    def hit(self, robot):
        print(f"{robot.name} is going to take {self._full_damage} damage")
        # Можно убарть этот if
        # и поместить принт из 20 строчки в 36
        if robot.health <= 0:
            print(f"{robot.name} Already dead")
            return

        robot._full_armor -= self._full_damage

        if robot._full_armor < 0:
            robot.health -= abs(robot._full_armor)
            robot._full_armor = 0

        if robot.health <= 0:
            print(f"{robot.name}: dead")
        else:
            print(f"{robot.name} health:{robot.health}.\n"
                  f"Armor:{robot._full_armor}.")
