class Chess:

    def user_input(self):
        try:
            self.x1 = int(input('Print x coordinate : '))
            self.y1 = int(input('Print y coordinate : '))
        except ValueError:
            print('Invalid data')
            exit()

    def check_user_input(self):
        valid_number1 = isinstance(self.x1, (int))
        valid_number2 = isinstance(self.y1, (int))
        if not valid_number1 or not valid_number2:
            print('Invalid data')
            exit()
        if 0 < self.x1 < 9 and 0 < self.y1 <9:
            return
        else:
            print('Invalid data')
            exit()


    def king(self, x: int, y: int):
        self.user_input()
        self.check_user_input()
        if (x - self.x1 == 1 or x - self.x1 == -1 or x - self.x1 == 0) and\
                (y - self.y1 == 1 or y - self.y1 == -1 or y - self.y1 == 0):
            print('YES')
        else:
            print('NO')

    def knight(self, x: int, y: int):
        self.user_input()
        self.check_user_input()
        if (x - 1 == self.x1 or x + 1 == self.x1) and (y - 2 == self.y1 or y + 2 == self.y1):
            print('YES')
        elif (x - 2 == self.x1 or x + 2 == self.x1) and (y - 1 == self.y1 or y + 1 == self.y1):
            print('YES')
        else:
            print('NO')

    def castle(self, x: int, y: int):
        self.user_input()
        self.check_user_input()
        if x == self.x1 or y == self.y1:
            print('YES')
        else:
            print('NO')

    def queen(self, x: int, y: int):
        self.user_input()
        self.check_user_input()
        if abs(x - self.x1) <= 1 and abs(y - self.y1) <= 1 or x == self.x1 or y == self.y1:
            print('YES')
        else:
            print('NO')

    def bishop(self, x: int, y: int):
        self.user_input()
        self.check_user_input()
        if abs(x - self.x1) == abs(y - self.y1):
            print('YES')
        else:
            print('NO')