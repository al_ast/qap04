from lesson9_22_11_2021.homework.pavel_harbachou.alex_examples.advanced.base_figure import \
    BaseFigure

# castle is the same with rock
class RockAdvanced(BaseFigure):
    def __init__(self, x, y):
        super().__init__(x, y)

    def is_can_move(self, x, y):
        if not self.is_points_valid(x, y):
            return False

        # Проверяем что мы не стоим на той же клетке
        if self.x == x and self.y == y:
            return False

        if self.x != x and self.y != y:
            return False

        return True
