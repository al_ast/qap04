# Тут можно почитать про наследование в питоне
class BaseFigure:
    def __init__(self, x, y):
        if not self.is_points_valid(x, y):
            raise Exception("X or Y not valid")

        self.x = x
        self.y = y

    def is_points_valid(self, x, y):
        # Проверяем что x не меньше 1, не больше 8 и int
        if not (isinstance(x, int) and 1 <= x <= 8):
            return False

        # Проверяем что y не меньше 1 и int
        if not (isinstance(y, int) and 1 <= y <= 8):
            return False

        return True

