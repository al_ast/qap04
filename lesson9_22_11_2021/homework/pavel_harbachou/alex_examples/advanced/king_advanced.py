from lesson9_22_11_2021.homework.pavel_harbachou.alex_examples.advanced.base_figure import \
    BaseFigure


class KingAdvanced(BaseFigure):
    def __init__(self, x, y):
        super().__init__(x, y)

    def is_can_move(self, x, y):
        if not self.is_points_valid(x, y):
            return False

        # Проверяем что мы не стоим на той же клетке
        if self.x == x and self.y == y:
            return False

        diff_x = abs(self.x - x)
        diff_y = abs(self.y - y)

        # Проверяем что растояние между клетками 1
        if diff_x > 1 or diff_y > 1:
            return False

        return True
