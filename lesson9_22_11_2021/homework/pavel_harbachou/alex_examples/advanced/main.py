from lesson9_22_11_2021.homework.pavel_harbachou.alex_examples.advanced.king_advanced import \
    KingAdvanced
from lesson9_22_11_2021.homework.pavel_harbachou.alex_examples.advanced.rock_advanced import \
    RockAdvanced

king = KingAdvanced(1, 3)
print(f"King Can move: {king.is_can_move(2, '3')}")

rock = RockAdvanced(1, 4)
print(f"Rock can move: {rock.is_can_move(1, 8)}")
