class King:
    def __init__(self, x, y):
        if not self._is_points_valid(x, y):
            raise Exception("X or Y not valid")

        self.x = x
        self.y = y

    def _is_points_valid(self, x, y):
        # Проверяем что x не меньше 1, не больше 8 и int
        if not (isinstance(x, int) and 1 <= x <= 8):
            return False

        # Проверяем что y не меньше 1 и int
        if not (isinstance(y, int) and 1 <= y <= 8):
            return False

        return True

    def is_can_move(self, x, y):
        if not self._is_points_valid(x, y):
            return False

        # Проверяем что мы не стоим на той же клетке
        if self.x == x and self.y == y:
            return False

        diff_x = abs(self.x - x)
        diff_y = abs(self.y - y)

        # Проверяем что растояние между клетками 1
        if diff_x > 1 or diff_y > 1:
            return False

        return True
