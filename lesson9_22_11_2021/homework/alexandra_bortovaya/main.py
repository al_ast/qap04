from task_1 import Chess


game_1 = Chess()
print("========КОРОЛЬ========")
game_1.king([2, 2], [2, 3])  # YES
game_1.king([2, 2], [3, 2])  # YES
game_1.king([2, 2], [3, 3])  # YES
game_1.king([2, 2], [1, 1])  # YES
game_1.king([2, 2], [2, 4])  # NO
game_1.king([2, 2], [4, 4])  # NO
print("========ЛАДЬЯ========")
game_1.rook([2, 2], [2, 5])  # YES
game_1.rook([2, 2], [4, 2])  # YES
game_1.rook([2, 2], [3, 3])  # NO
game_1.rook([2, 2], [1, 4])  # NO
print("========СЛОН========")
game_1.elephant([2, 2], [4, 4])  # YES
game_1.elephant([2, 4], [3, 3])  # YES
game_1.elephant([2, 3], [4, 1])  # YES
game_1.elephant([2, 4], [4, 4])  # NO
game_1.elephant([2, 2], [2, 5])  # NO
print("========ФЕРЗЬ========")
game_1.queen([2, 3], [4, 5])  # YES
game_1.queen([1, 3], [1, 1])  # YES
game_1.queen([4, 4], [1, 4])  # YES
game_1.queen([2, 2], [4, 3])  # NO
print("========КОНЬ========")
game_1.horse([1, 2], [3, 3])  # YES
game_1.horse([4, 4], [2, 3])  # YES
game_1.horse([3, 1], [1, 2])  # YES
game_1.horse([4, 4], [2, 1])  # NO
game_1.horse([3, 1], [1, 5])  # NO
