"""Шахматный король ходит по горизонтали, вертикали и диагонали, но только на 1 клетку.
Даны две различные клетки шахматной доски, определите, может ли король попасть с первой клетки на вторую одним ходом.
Король +
Лвдья +
Конь +
Ферзь +
Слон +
"""


class Chess:
    def print_no(self, from_cell, to_cell):
        print(f"NO, you are not able to go from {from_cell} to {to_cell}")

    def print_yes(self, from_cell, to_cell):
        print(f"YES, you are able to go from {from_cell} to {to_cell}")

    def staight_line(self, from_cell, to_cell):
        if from_cell[0] == to_cell[0] and from_cell[1] != to_cell[1]:
            return True
        elif from_cell[0] != to_cell[0] and from_cell[1] == to_cell[1]:
            return True
        else:
            return False

    def diagonal_line(self, from_cell, to_cell):
        if from_cell[0] - from_cell[1] == to_cell[0] - to_cell[1]:
            return True
        elif from_cell[0] + from_cell[1] == to_cell[0] + to_cell[1]:
            return True
        else:
            return False

    def king(self, from_cell, to_cell):
        if abs(from_cell[0] - to_cell[0]) > 1 or abs(from_cell[1] - to_cell[1]) > 1:
            self.print_no(from_cell, to_cell)
        else:
            self.print_yes(from_cell, to_cell)

    def rook(self, from_cell, to_cell):
        if self.staight_line(from_cell, to_cell):
            self.print_yes(from_cell, to_cell)
        else:
            self.print_no(from_cell, to_cell)

    def elephant(self, from_cell, to_cell):
        if self.diagonal_line(from_cell, to_cell):
            self.print_yes(from_cell, to_cell)
        else:
            self.print_no(from_cell, to_cell)

    def queen(self, from_cell, to_cell):
        if self.staight_line(from_cell, to_cell) or self.diagonal_line(from_cell, to_cell):
            self.print_yes(from_cell, to_cell)
        else:
            self.print_no(from_cell, to_cell)

    def horse(self, from_cell, to_cell):
        if abs(from_cell[0] - to_cell[0]) >= 1 and abs(from_cell[1] - to_cell[1]) == 1:
            self.print_yes(from_cell, to_cell)
        elif abs(from_cell[0] - to_cell[0]) == 1 and abs(from_cell[1] - to_cell[1]) >= 1:
            self.print_yes(from_cell, to_cell)
        else:
            self.print_no(from_cell, to_cell)
