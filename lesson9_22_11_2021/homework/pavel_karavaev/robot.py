class Robot:
    def __init__(self, name, health, weapon, armor, helm):
        self.name = name
        self.health = health
        self.weapon = weapon
        self.armor = armor
        self.helm = helm
    def __repr__(self):
        return f"name: {self.name}" \
               f"\nhealth: {self.health}" \
               f"\nhelm: {self.helm}" \
               f"\nweapon: {self.weapon}" \
               f"\narmor: {self.armor}" \
