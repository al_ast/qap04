class Weapon:
    def __init__(self, name, damage):
        self.name = name
        self.damage = damage
    def __repr__(self):
        return self.name

mace = Weapon("mace", 350)
toothpick = Weapon("toothpick", 999)
