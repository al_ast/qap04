class Armor:
    def __init__(self, name, block):
        self.block = block
        self.name = name
    def __repr__(self):
        return self.name

jellyfish_head = Armor("jellyfish_head", 999)
lion_armor = Armor("lion_armor", 1799)
