from robot import Robot


class Weapon:
    def __init__(self, weapon_class, weapon_grade, damage_sec):
        self.weapon_class = weapon_class
        self.weapon_grade = weapon_grade
        self.damage_sec = damage_sec

    def __repr__(self):
        return f'it has a {self.weapon_grade} grade {self.weapon_class} that deals {self.damage_sec} points of damage'
