class Robot:
    def __init__(self, name, weapon, armor, robot_health):
        self.name = name
        self.weapon = weapon
        self.armor = armor
        self.robot_health = robot_health

    def __repr__(self):
        return self.name
