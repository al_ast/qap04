from robot import Robot
from weapon import Weapon
from armor import Armor
import time


robots_in_tournament = []
robots_health = []
robots_damage = []
text = open("rules.txt").read()
print(text)
time.sleep(10)
print('\n')
while len(robots_in_tournament) != 2:

    gun = Weapon(str(input('The name of my weapon will be: ')), int(input('It has a grade: ')),
                 int(input('It will deal amount of damage about: ')))
    armor = Armor(str(input('My armor name is: ')), int(input('Armor grade is: ')),
                  int(input('The amount of damage it will protect me from is: ')))
    robot = Robot(str(input('You know my robot by as: ')), gun, armor, int(input('Its health amount is: ')))
    damage = int(robot.weapon.damage_sec * robot.weapon.weapon_grade)
    health = int(robot.robot_health + robot.armor.armor_grade * robot.armor.protection_sec)

    if 0 <= gun.weapon_grade > 5 or 250 < gun.damage_sec > 1000 or 0 < armor.armor_grade > 5 \
            or 500 < armor.protection_sec > 1500 or 750 < robot.robot_health > 2500:
        print('You have to read rules first.')
        exit()

    print(f'\nThis is {robot}, {gun}, {armor}.\nThe {robot} can deal {damage} damage in one hit,'
          f' and can survive {health} points of damage.\n')

    if len(robots_in_tournament) < 1:
        print(f'Now you should create the second robot for tournament, than we can start the fight.\n')
    robots_in_tournament.append(robot)
    robots_health.append(health)
    robots_damage.append(damage)

    if len(robots_in_tournament) == 2:
        time.sleep(3)
        print('Let`s start the fight!')
        time.sleep(3)
        while robots_health[0] != 0 and robots_health[1] != 0:

            robots_health[0] = robots_health[0] - robots_damage[1]
            robots_health[1] = robots_health[1] - robots_damage[0]

            if robots_health[0] < 0:
                robots_health[0] = 0
            if robots_health[1] < 0:
                robots_health[1] = 0

            print(f'{robots_in_tournament[1].name} hits {robots_in_tournament[0].name}! {robots_in_tournament[0].name} '
                  f'have {robots_health[0]} HP now.\nContinue fighting!\n')
            time.sleep(0.7)
            if robots_health[0] == 0:
                print(f'{robots_in_tournament[0].name} is dead now.')
                print(f'{robots_in_tournament[1].name} won the fight!')
                continue

            print(f'{robots_in_tournament[0].name} hits {robots_in_tournament[1].name}! {robots_in_tournament[1].name} '
                  f'have {robots_health[1]} HP now.\nContinue fighting!\n')
            time.sleep(0.7)

            if robots_health[1] == 0:
                print(f'{robots_in_tournament[1].name} is dead now.')
                print(f'{robots_in_tournament[0].name} won!')
                continue
        time.sleep(60)
        break
