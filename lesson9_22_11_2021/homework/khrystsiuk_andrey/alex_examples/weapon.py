class Weapon:
    _min_grade = 1
    _max_grade = 5

    _min_damage = 500
    _max_damage = 1000

    def __init__(self, weapon_class, grade, damage):
        if grade < self._min_grade or grade > self._max_grade:
            raise Exception(f"Not valid armor grade:{grade}")

        if damage < self._min_damage or damage > \
                self._max_damage:
            raise Exception(f"Not valid damage per sec:{damage}")

        self.weapon_class = weapon_class
        self.weapon_grade = grade
        self.damage = damage

    def __repr__(self):
        return f'it has a {self.weapon_grade} grade {self.weapon_class} that deals {self.damage} points of damage'
