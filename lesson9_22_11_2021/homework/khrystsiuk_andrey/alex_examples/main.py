from lesson9_22_11_2021.homework.khrystsiuk_andrey.alex_examples.robots_arena import RobotsArena
from robot import Robot
from weapon import Weapon
from armor import Armor


robot1 = Robot("Robot1", Weapon(1, 2, 500), Armor("helmet", 2, 100), 2000)
robot2 = Robot("Robot2", Weapon(3, 3, 550), Armor("helmet", 1, 200), 1200)
robot3 = Robot("Robot3", Weapon(5, 1, 600), Armor("helmet", 3, 150), 1900)
robot4 = Robot("Robot4", Weapon(2, 1, 700), Armor("helmet", 4, 250), 1800)
robot5 = Robot("Robot5", Weapon(4, 1, 600), Armor("helmet", 2, 70), 2000)
robot6 = Robot("Robot6", Weapon(1, 4, 590), Armor("helmet", 5, 90), 1000)

robots_arena = RobotsArena([robot1, robot2, robot3, robot4, robot5, robot6])
robots_arena.start_random_fight()
