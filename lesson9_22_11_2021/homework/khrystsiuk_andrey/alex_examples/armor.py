class Armor:
    _min_grade = 1
    _max_grade = 5

    _min_protection = 50
    _max_protection = 500

    def __init__(self, armor_name, grade, protection):
        if grade < self._min_grade or grade > self._max_grade:
            raise Exception(f"Not valid armor grade:{grade}")

        if protection < self._min_protection or protection > \
                self._max_protection:
            raise Exception(f"Not valid protection per sec:{protection}")

        self.armor_name = armor_name
        self.armor_grade = grade
        self.protection = protection

    def __repr__(self):
        return f'its armor is {self.armor_grade} grade {self.armor_name} that' \
               f' have {self.protection} protection points'
