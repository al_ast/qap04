class Robot:
    _min_health = 750
    _max_health = 2500

    def __init__(self, name, weapon, armor, health):
        if health < self._min_health or health > self._max_health:
            raise Exception(f"Health is not valid:{health}")

        self.name = name
        self.weapon = weapon
        self.armor = armor
        self.health = health

    def __repr__(self):
        return self.name

    def hit(self, robot):
        print(f"{robot.name} is going to take {self.weapon.damage} damage from {self.name}")

        robot.armor.protection -= self.weapon.damage

        if robot.armor.protection < 0:
            robot.health -= abs(robot.armor.protection)
            robot.armor.protection = 0

        if robot.health <= 0:
            print(f"{robot.name}: dead")
        else:
            print(f"{robot.name} health:{robot.health}.\n"
                  f"Armor:{robot.armor.protection}.")

    def is_alive(self):
        return self.health > 0
