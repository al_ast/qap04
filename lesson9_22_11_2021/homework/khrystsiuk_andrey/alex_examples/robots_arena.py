import random


class RobotsArena:
    def __init__(self, robots):
        self.robots = robots

    def add_robot(self, robot):
        self.robots.append(robot)

    def start_random_fight(self):
        while len(self.robots) != 1:
            # Создаем рандомный порядок роботов для ударов
            current_fight_robots = self.robots.copy()
            random.shuffle(current_fight_robots)

            # Робот бьет следующего по списку
            for index in range(len(current_fight_robots)):
                first_robot = current_fight_robots[index]

                second_robot_index = index + 1 if len(current_fight_robots) > index + 1 else 0
                second_robot = current_fight_robots[second_robot_index]

                if first_robot.is_alive():
                    first_robot.hit(second_robot)

            # Оставляем только живых
            self.robots = [r for r in current_fight_robots if r.is_alive()]

    def ask_to_create_robots(self, robots_count):
        # Тут можно добить логику считывания из консоли
        pass
