from robot import Robot


class Armor:
    def __init__(self, armor_name, armor_grade, protection_sec):
        self.armor_name = armor_name
        self.armor_grade = armor_grade
        self.protection_sec = protection_sec

    def __repr__(self):
        return f'its armor is {self.armor_grade} grade {self.armor_name} that' \
               f' have {self.protection_sec} protection points'
