class Robot:
    def __init__(self, name, ammunition, weapon):
        self.name = name
        self.ammunition = ammunition
        self.weapon = weapon

    def __repr__(self):
        return f'Robot {self.name} has characteristics like a {self.ammunition} and {self.weapon}'