from robot import Robot
from ammunition import Ammunition
from weapon import Weapon

armor1 = Ammunition('silver helmet', 75)
armor2 = Ammunition('bronze helmet', 68)


sword1 = Weapon('gold sword', '95%')
sword2 = Weapon('siver sword', '85%')

robot1 = Robot('Valli', [sword2], [armor2])
robot2 = Robot('Fenya', [sword1], [armor1])

print(robot1)
print(robot2)

