from Robot import Robot
from Ammunition import Ammunition
from Weapons import Weapons


#создаю экземпляры класса
gun = Weapons("Револьвер", 100, 17)
sword = Weapons("Железный меч", 35, 10)
print(gun.type, gun.power, gun.level)
lats = Ammunition("Латы", 1000)
print(lats.ammunition_type, lats.endurance)

John = Robot("John", gun, lats)
print(f"Робота зовут {John} и у него есть: {gun} и {lats}")
print(John.name, John.weapon, John.ammunition)


