class Robot:
    def __init__(self, name, weapon, ammunition):
        self.name = name
        self.weapon = weapon
        self.ammunition = ammunition

    def __repr__(self):
        return f"The robot {self.name} has a {self.weapon} and a {self.ammunition}."