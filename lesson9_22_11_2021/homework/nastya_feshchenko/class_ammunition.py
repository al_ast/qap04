class Ammunition:
    def __init__(self, name, power, durability):
        self.name = name
        self.power = power
        self.durability = durability

    def __repr__(self):
        return self.name