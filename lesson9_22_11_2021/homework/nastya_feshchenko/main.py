from class_weapon import Weapon
from class_ammunition import Ammunition
from class_robot import Robot

# оружие
sword = Weapon("Sword", 700, 700)
gun = Weapon("Gun", 1000, 1200)

# аммуниция
armour = Ammunition("Armour", 800, 1000)
shield = Ammunition("Shield", 200, 400)

# робот
robot1 = Robot("Wall-e", sword, shield)
robot2 = Robot("Robocop", gun, armour)


print(robot1)
print(robot2)