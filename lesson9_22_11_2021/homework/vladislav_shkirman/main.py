"""
2. Создайте класс Робот
создайте 2 типа оружия: меч, автомат
Создайте 2 типа амуниции: броня, шлем
Добавьте оружию и амуниции свои характеристики(например урон, прочность)

Создайте своего робота с каким либо оружием
(может быть несколкьо и брони может быть несколько. Так же может быть ничего)
Выыедите весь арсенал робота на экран
"""

from class_weapon import Weapon
from class_ammunition import Ammunition
from class_robot import Robot

weapon1 = Weapon('Sword', 18).get_weapon()
weapon2 = Weapon('Machine_gun', 120).get_weapon()
weapon3 = Weapon(weapon1, weapon2).get_weapon()

ammunition1 = Ammunition('Armor', 55).get_ammunition()
ammunition2 = Ammunition('Helmet', 20).get_ammunition()
ammunition3 = Ammunition(ammunition1, ammunition2).get_ammunition()

robot1 = Robot(weapon1, ammunition1).get_robot()
robot_with_both_weapons = Robot(weapon3, ammunition2).get_robot()
robot_with_both_ammunition = Robot(weapon2, ammunition3).get_robot()
robot_without_weapon = Robot([], ammunition2).get_robot()

print(f'Robot with one weapon: {robot1}')
print(f'Robot with two weapons: {robot_with_both_weapons}')
print(f'Robot with one weapon along with armor and helmet: {robot_with_both_ammunition}')
print(f'Robot has no weapons: {robot_without_weapon}')
