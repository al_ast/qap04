class Weapon:

    def __init__(self, name, damage):
        self.name = name
        self.damage = damage

    def get_weapon(self):
        return [self.name, self.damage]
