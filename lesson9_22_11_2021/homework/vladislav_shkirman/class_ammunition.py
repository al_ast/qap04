class Ammunition:

    def __init__(self, name, durability):
        self.name = name
        self.durability = durability

    def get_ammunition(self):
        return [self.name, self.durability]
