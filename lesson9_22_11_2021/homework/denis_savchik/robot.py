class Robot:

    def __init__(self, name, health_point, weapon=0, armor=0):
        self.name = name
        self.hp = health_point
        self.weapon = weapon
        self.armor = armor

    def __repr__(self):
        return self.name

    def get_arsenal(self):
        return [self.weapon, self.armor]
