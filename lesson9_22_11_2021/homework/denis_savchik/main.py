# 2. Создайте класс Робот
# создайте 2 типа оружия: меч, автомат
# Создайте 2 типа амуниции: броня, шлем
# Добавьте оружию и амуниции свои характеристики(например урон, прочность)
#
# Создайте своего робота с каким либо оружием
# (может быть несколько и брони может быть несколько. Так же может быть ничего)
# Выведите весь арсенал робота на экран


from robot import Robot
from armor import Armor
from weapon import Weapon


sword = Weapon(100, 500, "Sword")
automate = Weapon(50, 1000, "Automate")

helmet = Armor("Helmet", 1000, 30)
armor = Armor("Armor", 2000, 100)

robot2 = Robot("Boll", 1500, sword, [armor, helmet])
robot1 = Robot("Wally", 1000, automate, [armor])

print(robot1.get_arsenal())