class Weapon:

    def __init__(self, damage, durability, name):
        self.name = name
        self.damage = damage
        self.durability = durability

    def __repr__(self):
        return self.name
