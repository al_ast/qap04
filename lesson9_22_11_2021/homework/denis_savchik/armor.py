class Armor:

    def __init__(self, name, durability, protection):
        self.name = name
        self.durability = durability
        self.protection = protection

    def __repr__(self):
        return self.name