import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://www.21vek.by")
actions = ActionChains(driver)

christmas_trees_locator = "//a[@href='https://www.21vek.by/christmas_trees/']"
christmas_trees = driver.find_element(By.XPATH, christmas_trees_locator)
actions.click(christmas_trees).perform()

christmas_tree_1 = driver.find_element(By.XPATH,
                                       "//span[text()='Ель искусственная Green Year Зеленая New (1.2м)']")
actions.click(christmas_tree_1).perform()
time.sleep(1)

add_to_basket = driver.find_element(By.XPATH, "//div/form[1]/button[@data-ga_action='add_to_cart']")
actions.click(add_to_basket).perform()
time.sleep(1)

basket_button = driver.find_element(By.XPATH, "//span[@class='headerCartIcon']")
driver.execute_script("arguments[0].click();", basket_button)
time.sleep(1)

delete_from_basket = driver.find_element(By.XPATH, "//a[@id='j-delete-4038647']")
driver.execute_script("arguments[0].click();", delete_from_basket)
time.sleep(1)

main_page_button = driver.find_element(By.XPATH, "//a[@class='logotypeImg']")
driver.execute_script("arguments[0].click();", main_page_button)
time.sleep(3)

driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

iframe = "//iframe[@id='vkwidget1']"
vk_link_iframe = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, iframe))
)
driver.switch_to.frame(vk_link_iframe)

vk_link = driver.find_element(By.XPATH, "//a[@class='wcommunity_name_link']")
time.sleep(3)
actions.click(vk_link).perform()
time.sleep(3)
