

from selenium import webdriver
from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("http://ww12.demoaut.com")
driver.maximize_window()

# iframe + js usage

iframe_locator = "//iframe[@id='master-1']"
iframe_element = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, iframe_locator))
    )

driver.switch_to.frame(iframe_element)

demo_software_button_locator = "//div[@id='e3']/a"
demo_software_button_element = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, demo_software_button_locator))
)
driver.execute_script("arguments[0].click();", demo_software_button_element)

# actions usage

actions = ActionChains(driver)

element_to_click_locator = "//a[@onclick='showPolicy();']"
element_to_click = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, element_to_click_locator))
    )
title_element_locator = "//div[@class='header']"
title_element = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, title_element_locator))
    )
actions.click(element_to_click).perform()                                      # Открывает Privacy Policy
actions.double_click(title_element).key_down(Keys.CONTROL).send_keys('c')\
    .key_up(Keys.CONTROL).perform()                                            # Выделяет и копирует заголовок страницы

driver.execute_script("window.scrollBy(0,1500);")
driver.execute_script("window.close();")
