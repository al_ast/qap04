"""
Использовать следующие действия не менее 3 раз. Для iframe можно 1 раз:
    -   actions
    -   click element inside iframe. then outside
        (например кликнуть рассылки тут http://ww12.demoaut.com/)
    -   js scripts
"""
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://oz.by/")
driver.maximize_window()


def find_element_by_locator(locator):
    return WebDriverWait(driver, 10).until(
        expected_conditions.presence_of_element_located((By.XPATH, locator))
    )

# Actions


actions = ActionChains(driver)

banner_menu_element = find_element_by_locator("//a[contains(@class,'offers-slider')]//span[text()='Новый год']")
banner_button_element = find_element_by_locator("//span[contains(@class,'offers-slider') and text()='Выбрать подарки']")
actions.click(banner_menu_element).click(banner_button_element).perform()

open_menu_element = find_element_by_locator("//strong[@class='main-nav__header']")
menu_element = find_element_by_locator("//a[contains(@class,'menu-link') and contains(text(),'Развлечения')]")
sub_menu_element = find_element_by_locator("//a[contains(text(),'Алмазные вышивки')]")

actions.move_to_element(open_menu_element).move_to_element(menu_element).click(sub_menu_element).perform()

go_to_the_top_of_page_element = find_element_by_locator("//a[contains(@class,'up-btn')]")
actions.move_to_element(go_to_the_top_of_page_element).click(go_to_the_top_of_page_element).perform()

# JS scripts

logo_element = find_element_by_locator("//a[contains(@class,'top-panel__logo')]")
driver.execute_script("arguments[0].click();", logo_element)

search_input_element = find_element_by_locator("//input[@id='top-s']")
search_button_element = find_element_by_locator("//button[@class='top-panel__search__btn']")

driver.execute_script("arguments[0].value='декор'", search_input_element)
driver.execute_script("arguments[0].click();", search_button_element)

# iframe

driver.get("https://phptravels.com/demo/")

live_chat_iframe = find_element_by_locator("//iframe[@title='LiveChat chat widget']")
driver.switch_to.frame(live_chat_iframe)

live_chat_open_button = find_element_by_locator("//button[contains(@aria-label,'Open LiveChat')]")
live_chat_open_button.click()

live_chat_input_field = find_element_by_locator("//input[@id='name']")
live_chat_input_field.send_keys("Alexandra")

minimize_chat_button = find_element_by_locator("//button[@aria-label='Minimize window']")
minimize_chat_button.click()

driver.switch_to.default_content()

navigate_lo_login_page_button = find_element_by_locator("//a[contains(@class,'sign-in')]")
navigate_lo_login_page_button.click()

driver.quit()
