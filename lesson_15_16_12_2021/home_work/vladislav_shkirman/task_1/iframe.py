"""
Task1:
Выбрать любой сайт:

Использовать следующие действия не менее 3 раз. Для iframe можно 1 раз:
    -   actions
    -   click elelemnt inside iframe. then outside
        (например кликнуть рассылки тут http://ww12.demoaut.com/)
    -   js scripts
"""

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

iframe_locator = "//iframe"
demo_software_locator = "//span[@aria-label='Demo Software']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get('http://ww12.demoaut.com/')
driver.maximize_window()
iframe_element = WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((By.XPATH, iframe_locator))
)

driver.switch_to.frame(iframe_element)

demo_software_element = WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((By.XPATH, demo_software_locator))
)

demo_software_element.click()

driver.switch_to.default_content()
