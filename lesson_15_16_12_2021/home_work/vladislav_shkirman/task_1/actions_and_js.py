"""
Task1:
Выбрать любой сайт:

Использовать следующие действия не менее 3 раз. Для iframe можно 1 раз:
    -   actions
    -   click elelemnt inside iframe. then outside
        (например кликнуть рассылки тут http://ww12.demoaut.com/)
    -   js scripts
"""

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

header_news_locator = "//span[text()='Новости' and contains(@class,'navigation')]"
drop_down_technology_locator = "//a[text()='Технологии' and contains(@class,'navigation')]"
search_input_locator = "//input[@class='fast-search__input']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get('https://www.onliner.by/')
driver.maximize_window()

print("Title - " + driver.execute_script("return document.title"))

header_news = driver.find_element(By.XPATH, header_news_locator)
drop_down_technology = driver.find_element(By.XPATH, drop_down_technology_locator)

actions = ActionChains(driver)
actions.move_to_element(header_news).perform()
driver.execute_script("arguments[0].click();", drop_down_technology)

search_input_element = driver.find_element(By.XPATH, search_input_locator)

actions.click(search_input_element).send_keys('ноутбук').perform()
actions.key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL).\
    key_down(Keys.BACKSPACE).send_keys('видеокарта').perform()
