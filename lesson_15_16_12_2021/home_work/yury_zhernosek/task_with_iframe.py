from base_page import BasePage

IFRAME_LOCATOR = '//iframe'
IFRAME_BUTTON_LOCATOR = '//span[text()="Demo Software"]'
BUTTON_LOCATOR = '//a[text()="Privacy Policy"]'


class TaskWithIframe(BasePage):

    def click_in_iframe(self, iframe_locator, button_locator):
        iframe_element = self._element(iframe_locator)
        self.driver.switch_to.frame(iframe_element)
        self.click(button_locator)
        self.driver.switch_to.default_content()

    def execute(self):
        self.click_in_iframe(IFRAME_LOCATOR, IFRAME_BUTTON_LOCATOR)
        self.click(BUTTON_LOCATOR)
