from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

from base_page import BasePage

SHOP_BUTTON_LOCATOR = '//a[contains(text(),"МАГАЗИН")]'
RECOMMENDED_BUTTON_LOCATOR = '//div[@class="content"]//a[contains(text(),"Рекомендации")]'
INPUT_NAME_ACCOUNT_LOCATOR = '#input_username'
INPUT_PASSWORD_LOCATOR = '#input_password'
ENTER_BUTTON_LOCATOR = '//span[text()="Войти"]'
FIND_GAME_INPUT_LOCATOR = '#store_search'


class TaskWithActions(BasePage):

    def execute(self):
        actions = ActionChains(self.driver)
        shop_button_element = self._element(SHOP_BUTTON_LOCATOR)
        recommended_button_element = self._element(RECOMMENDED_BUTTON_LOCATOR)

        actions.move_to_element(shop_button_element).click(recommended_button_element).perform()

        input_name_account_element = self._element(INPUT_NAME_ACCOUNT_LOCATOR)
        input_password_element = self._element(INPUT_PASSWORD_LOCATOR)
        enter_button_element = self._element(ENTER_BUTTON_LOCATOR)

        actions.send_keys_to_element(input_name_account_element, 'Login'). \
            send_keys_to_element(input_password_element, 'Password').click(enter_button_element).perform()

        self.open_site('https://store.steampowered.com')

        find_game_input_element = self._element(FIND_GAME_INPUT_LOCATOR)

        actions.send_keys_to_element(find_game_input_element, 'Sea').pause(2).send_keys(Keys.DOWN).\
            send_keys(Keys.ENTER).perform()
