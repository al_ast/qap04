'''
Выбрать любой сайт:

Использовать следующие действия не менее 3 раз. Для iframe можно 1 раз:
    -   actions
    -   click elelemnt inside iframe. then outside
        (например кликнуть рассылки тут http://ww12.demoaut.com/)
    -   js scripts
'''

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from task_with_actions import TaskWithActions
from task_with_iframe import TaskWithIframe
from task_with_js import TaskWithJs

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()

task1 = TaskWithActions(driver)
task2 = TaskWithIframe(driver)
task3 = TaskWithJs(driver)

task1.open_site('https://steamcommunity.com')
task1.execute()

task2.open_site('http://ww12.demoaut.com/')
task2.execute()

task3.open_site('https://av.by/')
task3.execute()