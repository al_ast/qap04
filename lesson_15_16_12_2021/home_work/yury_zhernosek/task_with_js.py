from base_page import BasePage

ALL_CAR_BUTTON_LOCATOR = '//span[text()="Все марки"]'
SELECTED_CAR_LOCATOR = '//span[text()="Porsche"]'
MODEL_CAR_LOCATOR = '//span[text()="Модель"]'
SELECTED_MODEL_CAR_LOCATOR = '//button[text()="Cayenne"]'


class TaskWithJs(BasePage):

    def click_with_js(self, locator):
        element = self._element(locator)
        self.driver.execute_script("arguments[0].click();", element)

    def execute(self):
        self.click_with_js(ALL_CAR_BUTTON_LOCATOR)
        self.click_with_js(SELECTED_CAR_LOCATOR)
        self.click_with_js(MODEL_CAR_LOCATOR)
        self.click_with_js(SELECTED_MODEL_CAR_LOCATOR)