import time

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

# (например кликнуть рассылки тут http://ww12.demoaut.com/)

url = "http://ww12.demoaut.com/"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get(url)

iframe_locator = "//iframe"
block_iframe = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, iframe_locator)))
driver.switch_to.frame(block_iframe)

rent_button_locator = "//span[@aria-label='Аренда 1 с Онлайн']"
rent_button = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH, rent_button_locator)))
rent_button.click()
time.sleep(5)

driver.switch_to.default_content()



