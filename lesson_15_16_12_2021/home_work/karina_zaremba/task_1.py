import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait


course_locator = "//ul[@class='t228__list']//a[@href='/kursy-programmirovaniya']"
offline_courses_locator = "//div[@class='t686__container t-container']/descendant::a[1]"
online_courses_locator = "//div[@class='t686__container t-container']/descendant::a[2]"
logo_locator = "//div[@class='t228__leftcontainer']"
phone_locator = "//input[@name='Phone']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://teachmeskills.by/")

course_element = driver.find_element(By.XPATH, course_locator)
actions = ActionChains(driver)
actions.click(course_element).perform()

offline = driver.find_element(By.XPATH, offline_courses_locator)
online = driver.find_element(By.XPATH, online_courses_locator)
actions.move_to_element(offline).move_to_element(online).click(offline).perform()

logo = driver.find_element(By.XPATH, logo_locator)
actions.click(logo).perform()

enter_phone_number = driver.find_element(By.XPATH, phone_locator)
actions.click(enter_phone_number).perform()
time.sleep(2)

#########                 js
contact_locator = "//li[@class='t228__list_item']//a[text()='Контакты']"
main_page_locator = "//div[@class='t228__leftcontainer']/a"

contact_page = driver.find_element(By.XPATH, contact_locator)
driver.execute_script("arguments[0].click();", contact_page)

main_page = driver.find_element(By.XPATH, main_page_locator)
driver.execute_script("arguments[0].click();", main_page)
time.sleep(5)

driver.refresh()
### iframe

ifrmame_locator = "//iframe[@id='roistat-lh-popup-iframe']"
input_name_locator = "//tr//input[@type='text']"
input_phone_number = "//tr//input[@type='tel']"

form_window_iframe = WebDriverWait(driver, 30).until(By.XPATH, ifrmame_locator)
driver.switch_to.frame(form_window_iframe)

