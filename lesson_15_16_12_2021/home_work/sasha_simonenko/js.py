import time

from selenium import webdriver
from selenium.webdriver.common.by import By

from webdriver_manager.chrome import ChromeDriverManager

TV = "//span[text()='Телевизоры']"
samsung_tv = "//span[text()='Телевизор Samsung QE65QN85AAU']"
price_chart = "//a[contains(text(),'График цен')]"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.onliner.by/")


TV = driver.find_element(By.XPATH, "//span[text()='Телевизоры']")
driver.execute_script("arguments[0].click();",TV)

samsung_tv = driver.find_element(By.XPATH,"//span[text()='Телевизор Samsung QE65QN85AAU']")
driver.execute_script("arguments[0].click();",samsung_tv)

price_chart = driver.find_element(By.XPATH, "//a[contains(text(),'График цен')]")
driver.execute_script("arguments[0].click();",price_chart)
time.sleep(5)
