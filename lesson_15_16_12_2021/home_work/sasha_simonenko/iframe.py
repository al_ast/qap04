from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

open_chat_iframe = "//iframe"
open_chat_button = "//p[text()='Leave a message']"
demo_header_locator = "//a[text()='Demo']"


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://phptravels.com/demo/")

open_chat = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, open_chat_iframe)))

driver.switch_to.frame(open_chat)

open_chat = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, open_chat_button)))

open_chat.click()
demo_header = driver.find_element(By.XPATH, demo_header_locator)

driver.switch_to.default_content()
driver.execute_script("arguments[0].click();", demo_header)
demo_header.click()


