import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://phptravels.com/demo/")

product = "//span[text()='Product']"
integrations = "//a[@class='lvl-1 link nav-link' and text()='Integrations']"

features = "//span[text()='Features']"
tours_module = "//a[@class='lvl-1 link nav-link' and text()='Tours Module']"

features1 = "//span[text()='Features']"
cars_module = "//a[@class='lvl-1 link nav-link' and text()='Cars Module']"


product_element = driver.find_element(By.XPATH,product)
int_element = driver.find_element(By.XPATH, integrations)

actions = ActionChains(driver)
actions.move_to_element(product_element).click(int_element).perform()
time.sleep(5)

feat_element = driver.find_element(By.XPATH,features)
tm_element = driver.find_element(By.XPATH,tours_module)

actions2 = ActionChains(driver)
actions2.move_to_element(feat_element).click(tm_element).perform()
time.sleep(5)

features1_element = driver.find_element(By.XPATH,features1)
cars_module_element = driver.find_element(By.XPATH,cars_module)

actions3 = ActionChains(driver)
actions3.move_to_element(features1_element).click(cars_module_element).perform()
time.sleep(4)


