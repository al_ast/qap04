from lesson_15_16_12_2021.home_work.pavel_harbachou.base_page import BasePage

iframe_task = BasePage()
iframe_task.open_site('http://ww12.demoaut.com/')
IFRAME_LOCATOR = '//iframe'
PRIVACY_POLICY_LOCATOR = "//a[contains(@onclick,'Policy')]"
MIDDLE_BUTTON_LOCATOR = '//div[@id="e2"]'

iframe = iframe_task.find_element_with_presense_wait(IFRAME_LOCATOR)
iframe_task.switch_to_frame(iframe)
demo_software_button = iframe_task.find_element(MIDDLE_BUTTON_LOCATOR)
print(demo_software_button.text)
iframe_task.switch_to_default_element()
privacy_policy = iframe_task.find_element(PRIVACY_POLICY_LOCATOR)
privacy_policy.click()
