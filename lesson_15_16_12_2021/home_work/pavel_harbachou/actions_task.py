# Выбрать любой сайт:
#
# Использовать следующие действия не менее 3 раз. Для iframe можно 1 раз:
#     -   actions
#     -   click element inside iframe. then outside
#         (например кликнуть рассылки тут http://ww12.demoaut.com/)
#     -   js scripts

from lesson_15_16_12_2021.home_work.pavel_harbachou.base_page import BasePage, actions

action_task = BasePage()
# 1
MERCHANDISE_LOCATOR = '//a[@href="#!" and text()="Merchandise"]'
OVERVIEW_LOCATOR = '//a[@href="/merchandise/"]'

action_task.open_site('https://www.sylvansport.com/')
open_merchandise = action_task.find_element(MERCHANDISE_LOCATOR)
open_overview = action_task.find_element(OVERVIEW_LOCATOR)
actions.move_to_element(open_merchandise).click(open_overview).perform()
# 2
HEADER_SEARCH_BUTTON_LOCATOR = '//div[@class="action-container"]/*[@aria-label="Show search bar"]'

action_task.open_site('https://ausopen.com/')
open_header_search_button = action_task.find_element(HEADER_SEARCH_BUTTON_LOCATOR)
actions.click(open_header_search_button).send_keys('abc').perform()
# 3
HEADER_SHOP_LOCATOR = '//a[@href="https://www.australianopenshop.com/" and @data-menu="Shop"]'

open_header_shop = action_task.find_element(HEADER_SHOP_LOCATOR)
actions.double_click(open_header_shop).perform()
