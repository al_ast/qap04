from lesson_15_16_12_2021.home_work.pavel_harbachou.base_page import BasePage

js_task = BasePage()
js_task.open_site('https://mail.ru/')
INPUT_EMAIL_WINDOW_LOCATOR = '//input[contains(@class,"email-input")]'
ENTER_PASSWORD_BUTTON_LOCATOR = '//button[@data-testid="enter-password"]'
REMEMBER_EMAIL_CHECKBOX_BUTTON_LOCATOR = '//input[@id="saveauth"]'
INPUT_PASSWORD_WINDOW_LOCATOR = '//input[contains(@class,"password-input")]'
# 1
open_input_email_window = js_task.find_element(INPUT_EMAIL_WINDOW_LOCATOR)
open_input_email_window.send_keys('My_email')
open_enter_password_button = js_task.find_element(ENTER_PASSWORD_BUTTON_LOCATOR)
js_task.js_click(open_enter_password_button)
# 2
open_remember_email_checkbox_button = js_task.find_element(REMEMBER_EMAIL_CHECKBOX_BUTTON_LOCATOR)
js_task.js_click(open_remember_email_checkbox_button)
# 3
open_input_password_window = js_task.find_element(INPUT_PASSWORD_WINDOW_LOCATOR)
js_task.js_send_my_password(open_input_password_window)
