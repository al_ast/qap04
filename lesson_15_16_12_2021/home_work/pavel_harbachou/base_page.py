from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(ChromeDriverManager().install())
actions = ActionChains(driver)


class BasePage:
    def open_site(self, url):
        driver.get(url)
        driver.maximize_window()

    def find_element(self, locator):
        element = driver.find_element(By.XPATH, locator)
        return element

    def find_element_with_presense_wait(self, locator):
        element = WebDriverWait(driver, 10).until(
            expected_conditions.presence_of_element_located((By.XPATH, locator))
        )
        return element

    def switch_to_default_element(self):
        return driver.switch_to.default_content()

    def switch_to_frame(self, locator):
        return driver.switch_to.frame(locator)

    def js_click(self, locator):
        return driver.execute_script("arguments[0].click();", locator)

    def js_send_my_password(self, locator):
        return driver.execute_script("arguments[0].value='My password'", locator)
