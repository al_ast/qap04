from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://oz.by/")

driver.maximize_window()

books_widget_locator = "//a[@class='menu-link-action main-nav__list__item ' and text()='Книги']"
books_genre_locator = "//a[@href='/books/topic16.html']"

books_widget_element = driver.find_element(By.XPATH, books_widget_locator)
books_genre_element = driver.find_element(By.XPATH, books_genre_locator)
actions = ActionChains(driver)

actions.move_to_element(books_widget_element)\
    .move_to_element(books_widget_element)\
    .click(books_genre_element).perform()

driver.quit()
