from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

take_credit_iframe_locator = "//iframe"
take_credit_locator = "//span[text()='Займер Без Процентов']"
privacy_policy_locator = "//a[@onclick='showPolicy();']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("http://ww12.demoaut.com/")
driver.maximize_window()

iframe = driver.find_element(By.XPATH, take_credit_iframe_locator)

driver.switch_to.frame(iframe)

element = driver.find_element(By.XPATH, take_credit_locator)
element.click()
driver.execute_script("window.scrollTo(0, 1080)")

driver.switch_to.default_content()

element_out_iframe = driver.find_element(By.XPATH, privacy_policy_locator)
element_out_iframe.click()

driver.quit()
