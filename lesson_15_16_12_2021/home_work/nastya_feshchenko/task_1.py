# Task1:
# Выбрать любой сайт:
#
# Использовать следующие действия не менее 3 раз. Для iframe можно 1 раз:
#     -   actions
#     -   click element inside iframe. then outside
#         (например кликнуть рассылки тут http://ww12.demoaut.com/)
#     -   js scripts


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver import ActionChains
import time

driver = webdriver.Chrome(ChromeDriverManager().install())

# ACTIONS

# 1.

driver.maximize_window()
driver.get("http://www.journ.bsu.by")

departaments = driver.find_element(By.XPATH, "//li[@class='dj-up itemid499 parent separator']")
mezhnar = driver.find_element(By.XPATH, "//a[text()='Международной журналистики']")

actions = ActionChains(driver)
actions.move_to_element(departaments)
actions.click(mezhnar)
actions.perform()
time.sleep(5)


# 2.

driver.maximize_window()
driver.get("https://www.gismeteo.by/weather-minsk-4248/month/")

more = driver.find_element(By.XPATH, "//div[@class='subnav-menu-item dropdown-handle']")
weekends = driver.find_element(By.XPATH, "//a[text()='Выходные']")

actions = ActionChains(driver)
actions.move_to_element(more)
actions.click(more)
actions.click(weekends)
actions.perform()
time.sleep(5)


# 3.

driver.maximize_window()
driver.get("https://yandex.by")

more = driver.find_element(By.XPATH, "//div[text()='ещё']")
video = driver.find_element(By.XPATH, "//div[@class='services-new__item-title' and text()='Видео']")

actions = ActionChains(driver)
actions.move_to_element(more)
actions.click(more)
actions.click(video)
actions.perform()
time.sleep(5)
#########################################################



# JS SCRIPTS

# 1.
driver.get("https://www.imdb.com")

menu_locator = "//*[@title='Open Navigation Drawer']"
menu = driver.find_element(By.XPATH, menu_locator)
driver.execute_script("arguments[0].click();", menu)
menu.click()

time.sleep(5)


# 2.
driver.get("https://pikabu.ru")

best = driver.find_element(By.XPATH, "//a[@href='/best']")
driver.execute_script("arguments[0].click();", best)
best.click()

time.sleep(5)


# 3.
driver.get("https://myshows.me/")

ratings = driver.find_element(By.XPATH, "//a[@href='/ratings/']")
driver.execute_script("arguments[0].click();", ratings)
ratings.click()

time.sleep(5)
###################################################################



#iframe

driver.get("https://www.21vek.by")

chat = WebDriverWait(driver, 20).until(
   EC.presence_of_element_located((By.XPATH, "//iframe[@id='cto_sub_ifr_px']")))

driver.switch_to.frame(chat)

open_chat = WebDriverWait(driver, 20).until(
   EC.presence_of_element_located((By.XPATH, "//span[@class='lt-label-block__txt']")))
open_chat.click()

time.sleep(5)
driver.switch_to.default_content()
