from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

iframe_locator = "//iframe"
element_locator = "//span[text()='Займер Без Процентов']"
privacy_locator = "//a[@onclick='showPolicy();']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("http://ww12.demoaut.com/")
driver.maximize_window()

iframe = driver.find_element(By.XPATH, iframe_locator)
driver.switch_to.frame(iframe)
element = driver.find_element(By.XPATH, element_locator)
element.click()
driver.execute_script("window.scrollTo(0, 1080)")
driver.switch_to.default_content()
element_out_iframe = driver.find_element(By.XPATH, privacy_locator)
element_out_iframe.click()
driver.quit()


