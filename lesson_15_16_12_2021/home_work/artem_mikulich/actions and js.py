from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.hltv.org/")
driver.maximize_window()

matches_locator = "//a[@class='navmatches']"
top_tier_locator = "//div[text()='Top tier']"
stats_locator = "//a[@class='navstats']"
player_s1mple_locator = "//a[@href='/stats/players/7998/s1mple' and @class='name']"


matches = driver.find_element(By.XPATH, matches_locator)
driver.execute_script("arguments[0].click()", matches)
top_tier = driver.find_element(By.XPATH, top_tier_locator)
driver.execute_script("arguments[0].click()", top_tier)

stats = driver.find_element(By.XPATH, stats_locator)
driver.execute_script("arguments[0].click()", stats)
player = driver.find_element(By.XPATH, player_s1mple_locator)


action = ActionChains(driver)
action.move_to_element(player).click(player).perform()
