import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

gift_certificates_locator = "//span[text()='Подарочные сертификаты']"
presents_for_women_locator = "//a[text()='Подарки для женщин']"
swimming_with_a_dolphin_locator = "//p[text()='Плавание с дельфином']"
buy_certificate_button_locator = "//a[@id='set-order-btn']"
cart_locator = "//a[@class='basketLink']"
arrange_as_a_gift_button_locator = "//a[text()='Оформить в подарок']"
write_congratulatory_text_locator = "//a[text()='Написать поздравительный текст']"
signature_locator = "//input[@id='greeting-from-5019395']"
chat_iframe_locator = "//iframe[@id='carrot-messenger-collapsed-frame']"
chat_element_locator = "//div[@class='carrotquest-messenger-collapsed-right_bottom']"
about_the_company_locator = "//a[@href='https://daroo.by/o-servise']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://daroo.by/")
actions = ActionChains(driver)

gift_certificates_element = driver.find_element(By.XPATH, gift_certificates_locator)
presents_for_women_element = driver.find_element(By.XPATH, presents_for_women_locator)

actions.move_to_element(gift_certificates_element).click(presents_for_women_element).perform()

swimming_with_a_dolphin_element = driver.find_element(By.XPATH, swimming_with_a_dolphin_locator)

actions.context_click(on_element=swimming_with_a_dolphin_element).perform()
actions.click(swimming_with_a_dolphin_element).perform()
time.sleep(5)

buy_certificate_button = driver.find_element(By.XPATH, buy_certificate_button_locator)
driver.execute_script("arguments[0].click();", buy_certificate_button)

cart = driver.find_element(By.XPATH, cart_locator)
driver.execute_script("arguments[0].click();", cart)

arrange_as_a_gift = driver.find_element(By.XPATH, arrange_as_a_gift_button_locator)
driver.execute_script("arguments[0].click();", arrange_as_a_gift)

write_congratulatory_text = driver.find_element(By.XPATH, write_congratulatory_text_locator)
driver.execute_script("arguments[0].click();", write_congratulatory_text)
time.sleep(5)

driver.execute_script("document.getElementById('greeting-from-5019395').value='Yana'")

chat_iframe = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.XPATH, chat_iframe_locator)))

driver.switch_to.frame(chat_iframe)

chat_element = driver.find_element(By.XPATH, chat_element_locator)
chat_element.click()

time.sleep(5)

driver.switch_to.default_content()

about_the_company = driver.find_element(By.XPATH, about_the_company_locator)
about_the_company.click()
time.sleep(5)

