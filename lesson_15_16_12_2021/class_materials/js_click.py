from selenium import webdriver
from selenium.webdriver.common.by import By

from webdriver_manager.chrome import ChromeDriverManager

chat_iframe_locator = "//iframe[contains(@title, 'Chat')]"
open_chat_locator = "//div[@role='main']"
demo_header_locator = "//a[text()='Demo']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://phptravels.com/demo/")

demo_header = driver.find_element(By.XPATH, demo_header_locator)
driver.execute_script("arguments[0].click();", demo_header)
demo_header.click()
