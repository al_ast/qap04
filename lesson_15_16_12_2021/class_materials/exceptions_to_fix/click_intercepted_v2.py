from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())


sofa_locator = "//a[text()='Диваны']"
account_locator = "//span[text()='Аккаунт']"

driver.maximize_window()
driver.get("https://www.21vek.by/")

account_element = driver.find_element(By.XPATH, account_locator)
account_element.click()

sofa_header = driver.find_element(By.XPATH, sofa_locator)
sofa_header.click()
