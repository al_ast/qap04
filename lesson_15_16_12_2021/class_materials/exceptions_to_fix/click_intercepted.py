from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.21vek.by/")

account_locator = "//span[text()='Корзина']"
account_element = driver.find_element(account_locator)

account_element.click()
