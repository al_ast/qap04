from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())


support_block_locator = "span.lt-label-block__txt"
laptops_locator = "//a[text()='Ноутбуки']"

driver.maximize_window()
driver.get("https://www.21vek.by/")

laptops_header = driver.find_element(By.XPATH, laptops_locator)
laptops_header.click()

WebDriverWait(driver, 15).until(
            expected_conditions.presence_of_element_located(
                (By.CSS_SELECTOR, support_block_locator))
        )

support_section = driver.find_element(By.CSS_SELECTOR, support_block_locator)
support_section.click()


