from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

company_locator = "//span[text()='Company']"
partners_company_menu_option_locator = "//a[contains(@href,'partners')]"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://phptravels.com/demo/")

company_element = driver.find_element(By.XPATH, company_locator)
partners_element = driver.find_element(By.XPATH, partners_company_menu_option_locator)

actions = ActionChains(driver)
actions.move_to_element(company_element).click(partners_element).perform()

# or
# actions.move_to_element(company_element)
# actions.click(partners_element)
# actions.perform()
