from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

chat_iframe_locator = "//iframe[contains(@title, 'Chat')]"
open_chat_locator = "//div[@role='main']"
demo_header_locator = "//a[text()='Demo']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://phptravels.com/demo/")

open_chat_iframe = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, chat_iframe_locator))
)

driver.switch_to.frame(open_chat_iframe)

open_chat_element = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, open_chat_locator))
)

open_chat_element.click()


driver.switch_to.default_content()

demo_header = driver.find_element(By.XPATH, demo_header_locator)
driver.execute_script("arguments[0].click();", demo_header)
demo_header.click()
