import pytest


@pytest.mark.usefixtures("")
class TestLoginPage:
    @pytest.mark.slow
    def test_login_with_not_valid_credentials(self, login_page):
        login_page.login("not valid user name", "not valid password")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")

        assert 3 == 5, "msg"

    def test_should_fail(self):
        assert 4 == 5, "Assert fail to check failed test result"
