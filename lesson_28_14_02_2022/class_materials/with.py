# https://pythonworld.ru/osnovy/with-as-menedzhery-konteksta.html
with open('newfile.txt', 'w', encoding='utf-8') as g:
    d = int(input())
    print('1 / {} = {}'.format(d, 1 / d), file=g)

# Файл будет закрыт после чтения

