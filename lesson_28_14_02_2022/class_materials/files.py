# os.getcwd()

# f = open(filename, mode)
# r: open an existing file for a read operation.
# w: open an existing file for a write operation. If the file already contains some data then it will be overridden.
# a:  open an existing file for append operation. It won’t override existing data.
#  r+:  To read and write data into the file. The previous data in the file will not be deleted.
# w+: To write and read data. It will override existing data.
# a+: To append and read data from the file. It won’t override existing data.

#new_file = open("new_example.txt", "w+")
file = open("example.txt", "r")
data = file.read()
for row in file:
    #r = row.replace("\n", "")
    print(row)
file.close()

file = open("example.txt", "w")
file.write("Alex write")
file.close()


# all_lines = file.readlines()
# print(all_lines)