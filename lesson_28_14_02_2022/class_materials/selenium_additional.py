# Submit file
# https://demo.guru99.com/test/upload/
import os

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/upload/")

path = os.path.abspath("example.txt")

file_input = driver.find_element(By.XPATH, "//input[contains(@id, 'uploadfile')]")
file_input.send_keys(path)

# Alerts
# "https://demo.guru99.com/test/delete_customer.php"
# alert_obj = driver.switch_to.alert
# alert_obj.accept() – used to accept the Alert
# alert_obj.dismiss() – used to cancel the Alert
# alert.send_keys() – used to enter a value in the Alert text box.
# alert.text() – used to retrieve the message included in the Alert pop-up.
