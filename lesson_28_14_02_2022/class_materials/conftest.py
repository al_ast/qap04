import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(autouse=True)
def chromedriver():
    webdriver.Chrome(ChromeDriverManager().install())
