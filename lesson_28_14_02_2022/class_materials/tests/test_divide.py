import time


class TestDivide:
    def test_divide_1(self):
        time.sleep(5)

        assert 4 - 2 == 2

    def test_divide_2(self):
        time.sleep(5)

        assert 4 - 2 == 3

    def test_divide_3(self):
        time.sleep(5)

        assert 4 - 2 == 2


