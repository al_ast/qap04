from lesson_28_14_02_2022.home_work.alexandra_bortovaya.task_1.base_page import BasePage
from lesson_28_14_02_2022.home_work.alexandra_bortovaya.task_1.working_with_files import WorkingWithFiles


class FileUploadPage(BasePage, WorkingWithFiles):
    URL = "https://demo.guru99.com/test/upload/"
    FILE_INPUT_LOCATOR = "//input[@id='uploadfile_0']"

    def open_page(self):
        self.open_url(self.URL)

    def create_and_upload_file(self):
        test_file_path = self.create_file_for_upload()
        self.enter_data(self.FILE_INPUT_LOCATOR, test_file_path)

    def get_uploaded_file_name(self):
        uploaded_file_path = self.get_input_value(self.FILE_INPUT_LOCATOR)
        uploaded_file_name = uploaded_file_path.split("\\")[-1]
        return uploaded_file_name
