import os


class WorkingWithFiles:
    TEST_FILE_NAME = "test_file"

    def create_file_for_upload(self):
        file = open(self.TEST_FILE_NAME, "w")
        file.write("Some file content\nMore file content")
        return os.path.abspath(self.TEST_FILE_NAME)

    def delete_file(self):
        os.remove(self.TEST_FILE_NAME)
