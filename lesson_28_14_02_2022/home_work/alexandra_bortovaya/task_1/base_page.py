from selenium.webdriver.common.by import By


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def _define_by_type(self, locator):
        if "/" in locator:
            return By.XPATH
        return By.CSS_SELECTOR

    def _find_element_by_locator(self, locator):
        by_type = self._define_by_type(locator)
        return self.driver.find_element(by_type, locator)

    def open_url(self, url):
        self.driver.get(url)

    def enter_data(self, locator, data):
        element = self._find_element_by_locator(locator)
        element.send_keys(data)

    def get_input_value(self, locator):
        element = self._find_element_by_locator(locator)
        return element.get_attribute("value")
