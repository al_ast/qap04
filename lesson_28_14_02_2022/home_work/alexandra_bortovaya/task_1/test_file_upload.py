import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from lesson_28_14_02_2022.home_work.alexandra_bortovaya.task_1.file_upload_page import FileUploadPage


@pytest.fixture()
def setup():
    global file_upload_page

    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chromedriver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
    file_upload_page = FileUploadPage(chromedriver)

    file_upload_page.open_page()

    yield
    chromedriver.quit()
    file_upload_page.delete_file()


@pytest.mark.usefixtures("setup")
class TestFileUpload:
    def test_file_upload(self):
        file_upload_page.create_and_upload_file()
        actual_uploaded_file_name = file_upload_page.get_uploaded_file_name()
        expected_uploaded_file_name = file_upload_page.TEST_FILE_NAME

        assert actual_uploaded_file_name == expected_uploaded_file_name, f"Uploaded file name is not correct, " \
                                                                         f"expected {expected_uploaded_file_name} " \
                                                                         f"but got {actual_uploaded_file_name}"
