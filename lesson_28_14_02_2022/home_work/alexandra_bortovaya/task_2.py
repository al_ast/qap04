"""Удалить покупателя по этой ссылке https://demo.guru99.com/test/delete_customer.php
Так же нужно принять алерт (либо отклонить)"""

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://demo.guru99.com/test/delete_customer.php")

customer_id_input = driver.find_element(By.XPATH, "//input[@name='cusid']")
submit_button = driver.find_element(By.XPATH, "//input[@name='submit']")

customer_id_input.send_keys("1022215")
submit_button.click()

alert_obj = driver.switch_to.alert
alert_obj.accept()
alert_obj.accept()
