from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://demo.guru99.com/test/delete_customer.php")
id_input = driver.find_element(By.XPATH, "//input[@name='cusid']")
id_input.send_keys("123")
submit_button = driver.find_element(By.XPATH, "//input[@name='submit']")
submit_button.click()
alert_obj = driver.switch_to.alert

# accept if you sure

alert_obj.accept()
alert_obj.accept()

# or dismiss if needed

# alert_obj.dismiss()
