from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/upload/")
created_file = open("myFile.txt", "w+")
file_path = "C:\qap04\lesson_28_14_02_2022\home_work\pavel_karavaev\myFile.txt"
file_input = driver.find_element(
    By.XPATH, "//input[contains(@id,'uploadfile')]"
)
file_input.send_keys(file_path)
created_file.close()
