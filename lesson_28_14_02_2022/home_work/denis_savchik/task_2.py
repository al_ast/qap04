from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/delete_customer.php")

submit_button_locator = "//input[@type='submit']"
input_place_locator = "//input[@type='text']"

submit_button_element = driver.find_element(By.XPATH, submit_button_locator)
input_place_element = driver.find_element(By.XPATH, input_place_locator)

input_place_element.send_keys("24")
submit_button_element.click()
alert = driver.switch_to.alert
alert.accept()
