from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import os

input_button_locator = "//input[@id='uploadfile_0']"

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/upload/")

generated_file = open("upload_file.txt", "w+")
generated_file.write("A loong time ago...")
generated_file.close()

file_path = os.path.abspath("/home/dzianis/Documents/homework/28/upload_file.txt")

input_button_element = driver.find_element(By.XPATH, input_button_locator)
input_button_element.send_keys(file_path)
