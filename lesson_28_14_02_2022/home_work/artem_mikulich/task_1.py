"""1.Создать файл используя питон и загрузить его
в https://demo.guru99.com/test/upload/ испоьзуя в селениум
Проверить, что имя заруженного файла появилось"""

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import os

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/upload/")
create_file = open("file.txt", "w+")
create_file.write("Hello my name is Artem")
path = os.path.abspath("file.txt")
file_input = driver.find_element(By.XPATH, "//input[@id='uploadfile_0']")
file_input.send_keys(path)
create_file.close()



