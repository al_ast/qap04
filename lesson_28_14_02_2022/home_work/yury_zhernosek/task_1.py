from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


file_name = 'new_file.txt'
file_path = f"C:\TeachMeSkills\qap04\lesson_28_14_02_2022\home_work\yury_zhernosek\{file_name}"
file = open(file_path, "a")
file.close()

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://demo.guru99.com/test/upload/")

upload_button_element = driver.find_element(By.XPATH, "//input[@type='file']")
upload_button_element.send_keys(file_path)

driver.close()