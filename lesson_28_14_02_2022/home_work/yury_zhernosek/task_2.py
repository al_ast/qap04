from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://demo.guru99.com/test/delete_customer.php")

input_field_element = driver.find_element(By.XPATH, "//input[@type='text']")
input_field_element.send_keys("2563452")

submit_button_element = driver.find_element(By.XPATH, "//input[@type='submit']")
submit_button_element.click()

alert_obj = driver.switch_to.alert
alert_obj.accept()
alert_obj.accept()

driver.close()
