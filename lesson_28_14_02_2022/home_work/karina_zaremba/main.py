import os
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/upload/")
path = os.path.abspath("file")

file = open("file", "w")
file.write("You must write some information")
file.close()


file_input = driver.find_element(By.XPATH, "//input[contains(@id, 'uploadfile')]")
file_input.send_keys(path)
time.sleep(1)
