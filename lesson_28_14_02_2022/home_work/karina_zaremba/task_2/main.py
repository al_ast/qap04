import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/delete_customer.php")

input_id = driver.find_element(By.XPATH, "//input[@name='cusid']")
input_id.send_keys("01")
element = driver.find_element(By.XPATH, "//input[@name='submit']")
element.click()
alert_obj = driver.switch_to.alert
alert_obj.accept()

time.sleep(2)
