# Удалить покупателя по этой ссылке
# Так же жуно принять алерт(либо отклонить)
# "https://demo.guru99.com/test/delete_customer.php"
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
url = "https://demo.guru99.com/test/delete_customer.php"
driver.get(url)
text_field_locator = '//input[@type="text"]'
text_field = driver.find_element(By.XPATH, text_field_locator)
text_field.send_keys('123')
submit_button_locator = '//input[@type="submit"]'
submit_button = driver.find_element(By.XPATH, submit_button_locator)
submit_button.click()
alert = driver.switch_to.alert
alert.accept()
alert.accept()
