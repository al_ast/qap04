# Создать файл используя питон и загрузить его
# в https://demo.guru99.com/test/upload/ испоьзуя в селениум
# Проверить, что имя заруженного файла появилось
import os.path
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
url = 'https://demo.guru99.com/test/upload/'
file_to_upload = open('file to upload.txt', 'w+')
file_to_upload.write("Added text to file")
driver.get(url)
input_locator = '//input[@id="uploadfile_0"]'
path = os.path.abspath('file to upload.txt')
input_file = driver.find_element(By.XPATH, input_locator)
input_file.send_keys(path)
