import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(ChromeDriverManager().install())

new_file = open("new_file.txt", "w+")
new_file.write("Hello world")
new_file.close()

driver.get("https://demo.guru99.com/test/upload/")

file_path = os.path.abspath("new_file.txt")
file_upload = driver.find_element(By.XPATH, "//input[@class='upload_txt']")
file_upload.send_keys(file_path)
