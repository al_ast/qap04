from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://demo.guru99.com/test/delete_customer.php")

input_form = driver.find_element(By.XPATH, "//input[@name='cusid']")
input_form.send_keys("5")

submit_button = driver.find_element(By.XPATH, "//input[@name='submit']")
submit_button.click()

alert = driver.switch_to.alert
alert.accept()
