import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/upload/")
new_file = open("example.txt", "w+")
file_path = r"C:\Users\User\PycharmProjects\alex_clone\qap04\lesson_28_14_02_2022\home_work\homework28\example"

file_input = driver.find_element(By.XPATH, "//input[contains(@id, 'uploadfile')]")
file_input.send_keys(file_path)
new_file.close()
