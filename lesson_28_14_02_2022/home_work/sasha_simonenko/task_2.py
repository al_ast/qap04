from selenium import webdriver
import os
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://demo.guru99.com/test/delete_customer.php")

customer_id = driver.find_element(By.XPATH, "//input[@name='cusid']")
customer_id.send_keys("abc")
submit_button = driver.find_element(By.XPATH, "//input[@name='submit']")
submit_button.click()

alert_obj = driver.switch_to.alert
alert_obj.dismiss()
