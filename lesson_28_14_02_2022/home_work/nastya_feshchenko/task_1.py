# 1.Создать файл используя питон и загрузить его
# в https://demo.guru99.com/test/upload/ испоьзуя в селениум
# Проверить, что имя заруженного файла появилось

import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(ChromeDriverManager().install())

new_file = open("nastyas_file.txt", "w+")
new_file.write("May the force be with you")
new_file.close()

driver.get("https://demo.guru99.com/test/upload/")

path = os.path.abspath("nastyas_file.txt")
file_input = driver.find_element(By.XPATH, "//input[@class='upload_txt']")
file_input.send_keys(path)



# 2. Удалить покупателя по этой ссылке
# Также нужно принять алерт (либо отклонить)
# "https://demo.guru99.com/test/delete_customer.php"

driver.get("https://demo.guru99.com/test/delete_customer.php")

input_form = driver.find_element(By.XPATH, "//input[@name='cusid']")
input_form.send_keys("123456")

submit = driver.find_element(By.XPATH, "//input[@name='submit']")
submit.click()

alert_obj = driver.switch_to.alert
alert_obj.accept()
