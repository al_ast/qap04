from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import os

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get('https://demo.guru99.com/test/upload/')
UPLOAD_LOCATOR = "//input[contains(@id, 'uploadfile')]"

with open("vlad_file.txt", "w") as file_to_upload:
    file_to_upload.write("lesson 28 homework by Vlad")

file_input = driver.find_element(By.XPATH, UPLOAD_LOCATOR)
file_location = os.path.abspath("vlad_file.txt")
file_input.send_keys(file_location)
attribute = file_input.get_attribute('value')
actual_file_name_uploaded = attribute.split("\\")[-1]

if actual_file_name_uploaded == "vlad_file.txt":
    os.remove(file_location)
    print('\nFile uploaded successfully')
else:
    raise Exception('Uploaded file is not equal to expected one')
