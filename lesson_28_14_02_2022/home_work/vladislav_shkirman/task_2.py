"""2. Удалить покупателя по этой ссылке
Так же жуно принять алерт(либо отклонить)
"https://demo.guru99.com/test/delete_customer.php"
"""

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://demo.guru99.com/test/delete_customer.php")

CUSTOMER_LOCATOR = "//input[@name='cusid']"
SUBMIT_LOCATOR = "//input[@name='submit']"

customer_id_input = driver.find_element(By.XPATH, CUSTOMER_LOCATOR)
submit_button_element = driver.find_element(By.XPATH, SUBMIT_LOCATOR)

customer_id_input.send_keys('42')
submit_button_element.click()

alert = driver.switch_to.alert
alert.accept()
alert.accept()

customer_id_input = driver.find_element(By.XPATH, CUSTOMER_LOCATOR)
submit_button_element = driver.find_element(By.XPATH, SUBMIT_LOCATOR)

customer_id_input.send_keys('43')
submit_button_element.click()
alert.dismiss()
