# pip install selenium
# pip install webdriver-manager

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())

# Navigate to url
driver.get("https://www.saucedemo.com/")

# find_element: найти элемент по локатору
# By - тип локатора. может быть By.XPATH, By.CSS. и производные, по айди, классу и.т.д
# send_keys - ввести текст
# click - нажать

user_name_input = driver.find_element(By.XPATH, "//input[@id='user-name']")
user_name_input.send_keys("standard_user")

password_input = driver.find_element(By.XPATH, "//input[@id='password']")
password_input.send_keys("secret_sauce")

login_button = driver.find_element(By.XPATH, "//input[@id='login-button']")
login_button.click()

title = driver.find_element(By.XPATH, "//span[@class='title']")
print(f"Title is:{title.text}")

not_exists_element = driver.find_element(By.XPATH, "//h23")

# main methods
# click()
# .text
# send_keys()
# wait for element
element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, "//input[@id='login-button']"))
    )