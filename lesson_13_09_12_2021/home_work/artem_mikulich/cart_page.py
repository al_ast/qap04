from selenium.webdriver.common.by import By

from base_page import BasePage


class CartPage(BasePage):
    USER_BACK_TO_PRODUCTS_LOCATOR = "//a[@class = 'b-go_back' and text()='← Вернуться к покупкам']/ancestor::td/a"

    def click_back_to_products(self):
        locator = self.driver.find_element(By.XPATH, self.USER_BACK_TO_PRODUCTS_LOCATOR)
        locator.click()
