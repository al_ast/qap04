from selenium.webdriver.common.by import By

from base_page import BasePage


class MainPage(BasePage):
    USER_CATEGORY_LOCATOR = "//a[text()='{}']"
    USER_TV_LOCATOR = "//span[text()='{}']"


    def click_category(self,categoty_name):
        locator = self.driver.find_element(By.XPATH, self.USER_CATEGORY_LOCATOR.format(categoty_name))
        locator.click()

    def click_product(self,product_name):
        locator = self.driver.find_element(By.XPATH,self.USER_TV_LOCATOR.format(product_name))
        locator.click()




