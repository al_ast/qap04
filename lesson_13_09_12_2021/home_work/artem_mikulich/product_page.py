from selenium.webdriver.common.by import By

from base_page import BasePage


class ProductPage(BasePage):
    USER_CART_LOCATOR = "//form[@class='j-to_basket']"
    USER_CART_PAGE_LOCATOR = "//a[@class='headerCartBox']"


    def click_in_cart(self):
        locator = self.driver.find_element(By.XPATH, self.USER_CART_LOCATOR)
        locator.click()
    def click_cart(self):
        locator = self.driver.find_element(By.XPATH, self.USER_CART_PAGE_LOCATOR)
        locator.click()

