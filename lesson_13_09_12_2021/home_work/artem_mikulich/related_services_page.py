from selenium.webdriver.common.by import By

from base_page import BasePage


class ServicesPage(BasePage):
    USER_SERVICE_LOCATOR = "//a[text() = 'Надежная защита']"
    USER_MAIN_LOCATOR = "//a[@class = 'breadcrumbs__link']"

    def click_button_strong_protection(self):
        locator = self.driver.find_element(By.XPATH, self.USER_SERVICE_LOCATOR)
        locator.click()
    def click_main(self):
        locator = self.driver.find_element(By.XPATH,self.USER_MAIN_LOCATOR)
        locator.click()

