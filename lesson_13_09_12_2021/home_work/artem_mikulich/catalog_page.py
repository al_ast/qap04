from selenium.webdriver.common.by import By

from base_page import BasePage


class CatalogPage(BasePage):
    USER_PRODUCT_LOCATOR = "//span[text()='{}']"

    def click_product(self, product_name):
        locator = self.driver.find_element(By.XPATH, self.USER_PRODUCT_LOCATOR.format(product_name))
        locator.click()