from base_page import BasePage


class Cart(BasePage):
    EMAIL_INPUT_LOCATOR = "//input[@name='user_data[email]']"
    CITY_INPUT_LOCATOR = "//input[@name='user_data[s_city]']"
    ORDER_BUTTON_LOCATOR = "//button[@id='place_order']"

    def input_email(self, email):
        email_input = self.EMAIL_INPUT_LOCATOR
        self.enter_text(email_input, email)

    def input_city(self, city):
        city_input = self.CITY_INPUT_LOCATOR
        self.enter_text(city_input, city)

    def confirm_order(self, confirmation):
        confirm_order = self.ORDER_BUTTON_LOCATOR.format(confirmation)
        self.click(confirm_order)
