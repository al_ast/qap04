from base_page import BasePage


class MainPage(BasePage):
    CATEGORY_LOCATOR = "//a[@class='ty-menu__item-link' and contains(text(), 'MIDI клавиатуры')]"
    AD_WINDOW_CLOSE_LOCATOR = "//button[@title='Close (Esc)' and @class='mfp-close']"

    def open_main_page(self, url):
        self.driver.get(url)
        self.driver.maximize_window()

    def close_ad(self, ad_window):
        ad_window_close = self.AD_WINDOW_CLOSE_LOCATOR.format(ad_window)
        self.click(ad_window_close)

    def choose_category(self, category):
        category = self.CATEGORY_LOCATOR.format(category)
        self.click(category)
