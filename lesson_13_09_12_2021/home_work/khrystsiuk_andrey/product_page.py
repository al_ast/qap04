from base_page import BasePage
import time


class PageProduct(BasePage):
    BUTTON_LOCATOR = "//button[contains(text(), 'Добавить в корзину')]"
    GO_TO_CART_LOCATOR = "//a[@href='https://djshop.by/checkout.html']"

    def add_to_cart(self, adding):
        add_locator = self.BUTTON_LOCATOR.format(adding)
        self.click(add_locator)

    def go_to_cart(self, go_to_checkout):
        go_to_cart = self.GO_TO_CART_LOCATOR.format(go_to_checkout)
        self.click(go_to_cart)
