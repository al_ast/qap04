from driver import *
class ShowcaseOfCourses(BasePage):
    BUTTON = "//div//strong[contains(text(),'{}')]"
    def choose_a_direction(self, direction):
        button_of_direction = self.BUTTON.format(direction)
        button = self.find_element(button_of_direction)
        self.click(button)

    def button_of_gift_certificates(self):
        element = self.driver.find_element(By.XPATH, "//a[@href='https://teachmeskills.by/podarochnye-sertificate-courses']")
        self.click(element)