from driver import *
class CourseEntryPage(BasePage):
    _PROMO_CODE_INPUT_LOCATOR ="//div[@data-input-lid='1620035966578']//input"
    _PHONE_NUMBER_INPUT_LOCATOR ="//form[@id='form162785043']//input[@type='tel']"

    def input_phome_number(self, number_phone):
        input_number_phone = self.find_element("//form[@id='form162785043']//input[@type='tel']")
        self.input_text(input_number_phone, number_phone)
    def input_promo_code(self, promo_code):
        input_promocode = self.find_element("//div[@data-input-lid='1620035966578']//input")
        self.input_text(input_promocode, promo_code)
    def button_sign_up(self):
        element = self.find_element("//form[@id='form162785043']//button")
        self.click(element)