import time

from driver import BasePage
from main_page import MainHeader
from courses_page import CoursesPage
from showcase_of_courses_page import ShowcaseOfCourses
from course_entry_page import CourseEntryPage

url = "https://teachmeskills.by/"
driver = BasePage()
driver.open_link(url)
time.sleep(1)

main_header = MainHeader()
main_header.select_button("Курсы")
time.sleep(1)

courses_page = CoursesPage()
courses_page.choose_the_type_of_study("Офлайн")
time.sleep(1)

showcase_of_courses = ShowcaseOfCourses()
showcase_of_courses.choose_a_direction("Python")
time.sleep(1)

courses_entry_page = CourseEntryPage()
courses_entry_page.input_phome_number(259149320)
courses_entry_page.input_promo_code("СУПЕР ПРОМО")
