from driver import BasePage
class MainHeader(BasePage):
    BUTTON = "//div[3]//a[@class='t-menu__link-item' and text()='{}']"
    def select_button(self, button_name):
        header_button_locator = self.BUTTON.format(button_name)
        header_button = self.find_element(header_button_locator)
        self.click(header_button)
