from driver import *
class CoursesPage(BasePage):
    BUTTON = "//div[contains(text(),'{}')]/ancestor::div[4]"
    def choose_the_type_of_study(self, type_of_study):
        button_of_your_chose_locator = self.BUTTON.format(type_of_study)
        button = self.find_element(button_of_your_chose_locator)
        self.click(button)

    def button_of_gift_certificates(self):
        element = self.driver.find_element(By.XPATH, "//a[@href='https://teachmeskills.by/podarochnye-sertificate-courses']")
        self.click(element)
