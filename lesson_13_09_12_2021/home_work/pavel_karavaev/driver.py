from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.by import By

class BasePage:
    driver = webdriver.Chrome(ChromeDriverManager().install())

    def open_link(self, url):
        self.driver.get(url)

    def find_element(self, locator):
        element = self.driver.find_element(By.XPATH, f"{locator}")
        return element

    def click(self, element):
        element.click()

    def input_text(self, element, value):
        element.send_keys(value)

    def online_support_button(self):
        element = self.driver.find_element(By.XPATH, "//div/div[@class='t825__btn']")
        self.click(element)

