from page import Page
from selenium.webdriver.common.by import By


class CatalogItemsPage(Page):
    ITEM_NAME_LOCATOR = "//span[text()='{}']"

    def open_product_page(self, item_name):
        item_locator = self.driver.find_element(By.XPATH, self.ITEM_NAME_LOCATOR.format(item_name))
        item_locator.click()
