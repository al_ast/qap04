"""
Переделать домашнее задание2 из урока12
Используя реальный метод клик у вебдрайвера и сам вебдрайвер
"""
import time
from header import Header
from catalog_items_page import CatalogItemsPage
from product_page import ProductPage
from cart_page import CartPage

main_page = Header()
main_page.open_homepage()
main_page.open_category('Телевизоры')
time.sleep(1)

catalog_page = CatalogItemsPage()
catalog_page.open_product_page('Телевизор Samsung UE24N4500AU')
time.sleep(3)

product_page = ProductPage()
product_page.add_to_cart()
product_page.open_cart_page()
time.sleep(5)

cart_page = CartPage()
cart_page.delete_item('Телевизор Samsung UE24N4500AU')
time.sleep(2)

main_page.close_browser()
