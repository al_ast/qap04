from page import Page
from selenium.webdriver.common.by import By


class Header(Page):
    CATEGORY = "//a[text()='{}']"
    CART_BUTTON = "//span[@class='headerCartIcon']"

    def open_category(self, category_name):
        category_locator = self.driver.find_element(By.XPATH, self.CATEGORY.format(category_name))
        category_locator.click()

    def open_cart_page(self):
        cart_button_locator = self.driver.find_element(By.XPATH, self.CART_BUTTON)
        cart_button_locator.click()
