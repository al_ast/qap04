from header import Header
from selenium.webdriver.common.by import By


class ProductPage(Header):
    ADD_TO_CART = "//form[@class='j-to_basket']"

    def add_to_cart(self):
        cart_locator = self.driver.find_element(By.XPATH, self.ADD_TO_CART)
        cart_locator.click()
