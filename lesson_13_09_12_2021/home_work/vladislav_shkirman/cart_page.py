from page import Page
from selenium.webdriver.common.by import By


class CartPage(Page):
    DELETE_ITEM = "//a[text()='{}']/ancestor::tr//a[contains(@id,'delete')]"

    def delete_item(self, item_name):
        delete_locator = self.driver.find_element(By.XPATH, self.DELETE_ITEM.format(item_name))
        delete_locator.click()
