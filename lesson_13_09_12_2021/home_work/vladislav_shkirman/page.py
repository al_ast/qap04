from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(ChromeDriverManager().install())


class Page:

    def __init__(self):
        self.driver = driver

    def open_homepage(self):
        self.driver.get('https://www.21vek.by/')

    def close_browser(self):
        self.driver.quit()
