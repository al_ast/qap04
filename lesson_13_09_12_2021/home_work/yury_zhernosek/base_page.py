from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

class BasePage:

    driver = webdriver.Chrome(ChromeDriverManager().install())

    def click(self, locator):
        element = self.driver.find_element(By.XPATH, f"{locator}")
        element.click()

    def open_site(self, url):
        self.driver.get(url)

    def enter_text(self, locator, text):
        element = self.driver.find_element(By.XPATH, f"{locator}")
        element.send_keys(text)

    def open_homepage(self):
        self.driver.get('https://www.21vek.by/')