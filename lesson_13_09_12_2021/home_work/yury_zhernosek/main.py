'Сайт https://www.21vek.by/'

from main_page import MainPage
from fridge_page import FridgePage
from tv_page import TvPage
from browser import Browser
from account_page import AccountPage
from account_form import AccountForm

main_page = MainPage()
fridge_page = FridgePage()
tv_page = TvPage()
browser = Browser()
account_page = AccountPage()
account_form = AccountForm()

browser.open_site('https://www.21vek.by/')
main_page.select_category('Телевизоры')
tv_page.select_tv('Телевизор Horizont 32LE5511D')
tv_page.open_homepage()
main_page.select_category('Холодильники')
fridge_page.select_fridge('Холодильник с морозильником LG GA-B419SLGL')
fridge_page.open_homepage()
main_page.click_account_button()
account_page.click_enter_button()
account_form.enter_email('urazernosek@gmail.com')
account_form.enter_password('12312314wdsddf')
account_form.click_enter_button()