from base_page import BasePage

class MainPage(BasePage):

    CATEGORY = "//a[text()='{}']"

    def select_category(self, category_name):
        selected_category = self.CATEGORY.format(category_name)
        self.click(selected_category)

    def click_account_button(self):
        self.click("//span[text()='Аккаунт']")