from base_page import BasePage

class TvPage(BasePage):

    TV = "//span[text()='{}']"

    def select_tv(self, name_tv):
        selected_tv = self.TV.format(name_tv)
        self.click(selected_tv)