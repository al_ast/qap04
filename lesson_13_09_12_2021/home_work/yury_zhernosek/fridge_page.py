from base_page import BasePage

class FridgePage(BasePage):

    FRIDGE = "//span[text()='{}']"

    def select_fridge(self, name_fridge):
        selected_fridge = self.FRIDGE.format(name_fridge)
        self.click(selected_fridge)