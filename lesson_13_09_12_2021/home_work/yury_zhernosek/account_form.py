from base_page import BasePage

class AccountForm(BasePage):

    INPUT = "//input[@label='{}']"

    def enter_email(self,email):
        locator = self.INPUT.format('Электронная почта')
        self.enter_text(locator, email)

    def enter_password(self, password):
        locator = self.INPUT.format('Пароль')
        self.enter_text(locator, password)

    def click_enter_button(self):
        self.click("//button[text()='Войти']")