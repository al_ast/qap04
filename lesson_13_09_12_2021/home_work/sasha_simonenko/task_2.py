import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get("https://www.onliner.by/")

catalog = driver.find_element(By.XPATH, "//a[@class= 'b-main-navigation__link' and @href='https://catalog.onliner.by/']")
catalog.click()

electronics = driver.find_element(By.XPATH, "//span[@class= 'catalog-navigation-classifier__item-title-wrapper' and text()='Электроника']")
electronics.click()
time.sleep(5)

gadgets = driver.find_element(By.XPATH,"//div[@class='catalog-navigation-list__aside-title' and text()=' Гаджеты ']")
gadgets.click()
time.sleep(5)

superprices = driver.find_element(By.XPATH, "//a[contains(@href,'https://catalog.onliner.by/superprice')"
                                       " and @class='catalog-navigation__bubble catalog-navigation__bubble_primary']")
superprices.click()
time.sleep(5)

airpods = driver.find_element(By.XPATH, "//span[contains(text(),'Наушники Apple AirPods 3')]/ancestor::a")
airpods.click()
time.sleep(5)

basket_button = driver.find_element(By.XPATH,"//a[@data-shop-id=('256') and "
                                             "@class='button-style button-style_base-alter product-aside__item-button button-style_expletive']")
basket_button.click()
time.sleep(5)


