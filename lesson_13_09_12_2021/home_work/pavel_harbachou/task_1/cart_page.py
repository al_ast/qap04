from base_page import BasePage


class CartPage(BasePage):
    CONFIRM_ORDER_BUTTON = "//input[@value='Подтвердить заказ']"

    def confirm_order(self):
        locator = self.CONFIRM_ORDER_BUTTON
        self.click(locator)
