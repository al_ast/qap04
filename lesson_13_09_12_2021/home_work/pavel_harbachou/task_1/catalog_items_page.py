from selenium.webdriver.common.by import By
from base_page import BasePage


class CatalogItemsPage(BasePage):
    PRODUCT = "//img[contains(@alt,'{}')]"

    def select_product(self, product_name):
        product_locator = self.PRODUCT.format(product_name)
        self.click(product_locator)
