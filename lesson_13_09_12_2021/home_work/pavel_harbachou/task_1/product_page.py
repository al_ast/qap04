from base_page import BasePage


class ProductPage(BasePage):
    ADD_TO_CART_BUTTON = "//div[@title='В КОРЗИНУ!']"

    def add_to_cart(self):
        locator = self.ADD_TO_CART_BUTTON
        self.click(locator)
