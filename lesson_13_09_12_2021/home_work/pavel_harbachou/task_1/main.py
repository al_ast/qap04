from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from main_page import MainPage
from catalog_items_page import CatalogItemsPage
from product_page import ProductPage
from product_accessories_page import ProductAccessoriesPage
from cart_page import CartPage


driver = webdriver.Chrome(ChromeDriverManager().install())
url = 'https://sila.by/'
driver.get(url)
main_page = MainPage(driver)
main_page.select_category('Стиральные машины')

catalog_items_page = CatalogItemsPage(driver)
catalog_items_page.select_product('SAMSUNG WW70A6S23AN/LP')

product_page = ProductPage(driver)
product_page.add_to_cart()

product_accessories_page = ProductAccessoriesPage(driver)
product_accessories_page.place_an_order()

cart_page = CartPage(driver)
cart_page.confirm_order()

driver.get(url)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
main_page.go_mobile_version()
