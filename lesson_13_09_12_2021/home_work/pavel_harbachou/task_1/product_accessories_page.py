from base_page import BasePage


class ProductAccessoriesPage(BasePage):
    PLACE_AN_ORDER_BUTTON = "//div[@title='Заказать товар']"

    def place_an_order(self):
        locator = self.PLACE_AN_ORDER_BUTTON
        self.click(locator)
