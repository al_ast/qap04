from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def wait(self, locator):
        locator_type = self.define_locator_type(locator)
        WebDriverWait(self.driver, 10).until(expected_conditions.presence_of_element_located((locator_type, locator)))

    def define_locator_type(self, locator):
        if '//' in locator:
            return By.XPATH
        else:
            return By.CSS_SELECTOR

    def click(self, locator):
        locator_type = self.define_locator_type(locator)
        self.wait(locator)
        element = self.driver.find_element(locator_type, locator)
        element.click()

    def find_element(self, locator):
        element = self.driver.find_element(By.XPATH, f"{locator}")
        return element
