from selenium.webdriver.common.by import By
from page import Page
#Вводим пароль

class Password(Page):
    PASSWORD_LOCATOR = "//input[@type='password']"

    def input_pass(self, password):
        input_password = self.driver.find_element(By.XPATH, self.PASSWORD_LOCATOR.format(password))
        input_password.send_keys(password)