from selenium.webdriver.common.by import By
from page import Page

class Button(Page):
    BUTTON_LOCATOR = "//div[@class='modal-popup modal-login']//form/div[4]"

    def click_button(self):
        button = self.driver.find_element(By.XPATH, self.BUTTON_LOCATOR)
        button.click()