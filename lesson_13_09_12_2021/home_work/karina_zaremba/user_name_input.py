from selenium.webdriver.common.by import By
from page import Page
#Шаг2: попап открыт, вводим логин

class UserName(Page):

    USERNAME_LOCATOR = "//input[@name='login']"

    def login_input(self, login):
        login_locator = self.driver.find_element(By.XPATH, self.USERNAME_LOCATOR.format(login))
        login_locator.send_keys(login)

