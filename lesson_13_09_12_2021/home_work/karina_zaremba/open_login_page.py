from selenium.webdriver.common.by import By
from page import Page

class OpenLoginPage(Page):
    LOGINPAGE_LOCATOR = "//a[@class='h-drop h-user']"

    def open_login_page(self):
        login_page = self.driver.find_element(By.XPATH, self.LOGINPAGE_LOCATOR)
        login_page.click()