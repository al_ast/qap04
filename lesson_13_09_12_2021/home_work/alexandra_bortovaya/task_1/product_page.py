from driver import Driver


class ProductPage(Driver):
    ADD_TO_CART = "//div[contains(@class,'item-buyzone')]//button[@data-ga_action='add_to_cart']"

    def add_product_to_cart(self):
        add_to_cart = self.find_element(self.ADD_TO_CART)
        self.click(add_to_cart)
