from driver import Driver


class Cart(Driver):
    QUANTITY = "//span[contains(@class,'j-basket__{}')]"
    CHECKOUT = "//button[contains(@class, 'button__order') and @data-ga_category='Ordering']"
    USERNAME = "//input[contains(@class,'input-user-name')]"
    EMAIL = "//input[contains(@class,'input-user-email')]"

    def change_product_quantity(self, change_action):
        quantity_locator = self.QUANTITY.format(change_action)
        quantity = self.find_element(quantity_locator)
        self.click(quantity)

    def proceed_to_checkout(self):
        checkout = self.find_element(self.CHECKOUT)
        self.click(checkout)

    def add_personal_data(self, email, username=None):
        username_field = self.find_element(self.USERNAME)
        email_field = self.find_element(self.EMAIL)
        self.enter_text(username_field, username)
        self.enter_text(email_field, email)

