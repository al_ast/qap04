from driver import Driver


class CommonHeader(Driver):
    CATEGORY = "//a[text()='{}']"
    CART = "//a[contains(@class,'headerCartBox')]"

    def select_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        category = self.find_element(category_locator)
        self.click(category)

    def open_cart(self):
        cart = self.find_element(self.CART)
        self.click(cart)
