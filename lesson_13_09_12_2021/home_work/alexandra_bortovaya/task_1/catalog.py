from driver import Driver


class Catalog(Driver):
    PRODUCT = "//span[text()='{}']/parent::a"

    def open_product_page_by_name(self, product_name):
        product_locator = self.PRODUCT.format(product_name)
        product = self.find_element(product_locator)
        self.click(product)
