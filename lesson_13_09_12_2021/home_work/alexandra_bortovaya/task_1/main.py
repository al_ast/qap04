"""
Выбрать любой сайт
Написать 5-6 действий на разных страницах используя локаторы
Улучшить с использованеим наследования
Использовать готовый класс кликер
"""
import time

from cart import Cart
from catalog import Catalog
from common_header import CommonHeader
from driver import Driver
from product_page import ProductPage

url = "https://www.21vek.by/"
driver = Driver()
driver.open_site(url)

common_header = CommonHeader()
common_header.select_category("Ёлки")

catalog = Catalog()
catalog.open_product_page_by_name("Ель живая HD Nordic Trees Nordmann Датская Премиум (1.2-1.4м, в горшке)")

time.sleep(5)
product_page = ProductPage()
product_page.add_product_to_cart()
time.sleep(5)
common_header.open_cart()

cart = Cart()
cart.change_product_quantity("plus")
cart.proceed_to_checkout()
time.sleep(5)
cart.add_personal_data("sasha-test@gmail.com", "Alexandra")

time.sleep(5)
