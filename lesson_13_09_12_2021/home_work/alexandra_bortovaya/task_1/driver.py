from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class Driver:
    driver = webdriver.Chrome(ChromeDriverManager().install())

    def open_site(self, url):
        self.driver.get(url)

    def find_element(self, locator):
        element = self.driver.find_element(By.XPATH, f"{locator}")
        return element

    def click(self, element):
        element.click()

    def enter_text(self, element, value):
        element.send_keys(value)
