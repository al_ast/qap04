from class_clicker import Clicker
from class_main_page import MainPage

class Explore(MainPage):
    CATEGORIES = "//a[@id='destination-content-root']"

    def choose_category(self, category):
        category_locator = self.CATEGORIES.format(category)
        clicker = Clicker()
        clicker.click(category_locator)