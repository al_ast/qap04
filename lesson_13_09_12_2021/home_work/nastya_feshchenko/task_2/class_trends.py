from class_clicker import Clicker
from class_main_page import MainPage


class Trends(MainPage):
    VIDEOS = "//div[@id='dismissible']"

    def choose_video(self, video):
        video_locator = self.VIDEOS.format(video)
        clicker = Clicker()
        clicker.click(video_locator)