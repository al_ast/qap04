# https://www.youtube.com

from class_clicker import Clicker
from class_main_page import MainPage
from class_explore import Explore
from class_trends import Trends


main_page = MainPage()
main_page.go_navigator("Навигатор")

explore_page = Explore()
explore_page.choose_category("В тренде")

trends_page = Trends()
trends_page.choose_video("Почему в России пытают / вДудь")


