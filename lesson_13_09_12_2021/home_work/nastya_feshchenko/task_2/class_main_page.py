from class_clicker import Clicker

class MainPage:
    NAVIGATOR = "//a[@href='/feed/explore']"
    SUBSCRIPTIONS = "//a[@href='/feed/subscriptions']"
    LIBRARY = "//a[@href='/feed/library']"
    HISTORY = "//a[href='/feed/history']"

    def __init__(self):
        self.clicker = Clicker()

    def go_navigator(self, navigator):
        navigator_locator = self.NAVIGATOR.format(navigator)
        clicker = Clicker()
        clicker.click(navigator_locator)

    def go_subscriptions(self, subscriptions):
        subscriptions_locator = self.SUBSCRIPTIONS.format(subscriptions)
        clicker = Clicker()
        clicker.click(subscriptions_locator)

    def go_library(self, library):
        library_locator = self.LIBRARY.format(library)
        clicker = Clicker()
        clicker.click(library_locator)

    def go_history(self, history):
        history_locator = self.HISTORY.format(history)
        clicker = Clicker()
        clicker.click(history_locator)