import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

url = "https://onliner.by/"
driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get(url)

catalogue = driver.find_element(By.XPATH, "//a[@href='https://catalog.onliner.by/']")
catalogue.click()

category = driver.find_element(By.XPATH, "//a[@href='https://catalog.onliner.by/christmasdecor?utm_source=catalog_tile&utm_medium=christmasdecor']")
category.click()

rug = driver.find_element(By.XPATH, "//a[@href='https://catalog.onliner.by/christmasdecor/serpantin/serp1850114']")
rug.click()

rug_add_to_cart = driver.find_element(By.XPATH, "//a[@class='button-style button-style_base-alter product-aside__item-button button-style_expletive']")
rug_add_to_cart.click()

time.sleep(5)

cart = driver.find_element(By.XPATH, "//div//a[@href='https://cart.onliner.by/']")
cart.click()

time.sleep(100)