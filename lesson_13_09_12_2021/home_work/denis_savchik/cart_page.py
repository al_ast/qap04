from base_page import BasePage


class CartPage(BasePage):
    _INPUT_PHONE_NUMBER_LOCATOR = "//input[@id='formInputLoginPhone']"
    _CHECKOUT_ORDER_LOCATOR = "//button[@id='checkout-block-submit']"

    def enter_phone_number(self, number):
        self.enter_data(self._INPUT_PHONE_NUMBER_LOCATOR, number)

    def checkout_order(self):
        self.click(self._CHECKOUT_ORDER_LOCATOR)
