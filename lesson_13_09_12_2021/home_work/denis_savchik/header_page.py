from base_page import BasePage


class HeaderPage(BasePage):
    _CART_HEADER_LOCATOR = "//u[text()='Корзина']"

    def check_cart(self):
        self.click(self._CART_HEADER_LOCATOR)
        
