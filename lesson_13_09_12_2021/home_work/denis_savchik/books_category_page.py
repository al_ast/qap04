from base_page import BasePage


class BooksCategoryPage(BasePage):
    _BOOKS_CATEGORY_LOCATOR = "//span[@class='landing-nav-list__title' and text()='{}']"

    def select_genre(self, category_name):
        locator = self._BOOKS_CATEGORY_LOCATOR.format(category_name)
        self.click(locator)

