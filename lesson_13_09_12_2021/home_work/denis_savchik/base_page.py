from selenium.webdriver.common.by import By


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def click(self, locator):
        element = self.driver.find_element(By.XPATH, locator)
        element.click()

    def enter_data(self, locator, data):
        element = self.driver.find_element(By.XPATH, locator)
        element.send_keys(data)
