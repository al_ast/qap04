from lesson_13_09_12_2021.home_work.yana_shafar.driver import Driver


class BusketPage(Driver):

    def checkout (self):
        checkout_locator = "//button[@id='j-basket__ok']"
        checkout = self.find_element(checkout_locator)
        self.click(checkout)