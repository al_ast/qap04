import time

from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


class Driver:
    _driver = webdriver.Chrome(ChromeDriverManager().install())

    def open_site(self, url):
        self._driver.get(url)

    def find_element(self, locator):
        element = self._driver.find_element(By.XPATH, locator)
        return element

    def click(self, element):
        element.click()
        time.sleep(1)

    def click_button(self, element):
        self._driver.execute_script("arguments[0].click();", element)
        time.sleep(1)

