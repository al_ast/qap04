from lesson_13_09_12_2021.home_work.yana_shafar.busket_page import BusketPage
from lesson_13_09_12_2021.home_work.yana_shafar.catalog_page import CatalogPage
from lesson_13_09_12_2021.home_work.yana_shafar.checkout_page import CheckoutPage
from lesson_13_09_12_2021.home_work.yana_shafar.driver import Driver
from lesson_13_09_12_2021.home_work.yana_shafar.main_page import MainPage
from lesson_13_09_12_2021.home_work.yana_shafar.product_page import ProductPage

url = "https://www.21vek.by/"
driver = Driver()
driver.open_site(url)

main_page = MainPage()
main_page.select_category("Телевизоры")

catalog_page = CatalogPage()
catalog_page.select_product('Телевизор Horizont 32LE5511D')

product_page = ProductPage()
product_page.add_to_busket()
product_page.click_busket()

busket_page = BusketPage()
busket_page.checkout()

checkout_page = CheckoutPage()
checkout_page.confirm_the_order()
