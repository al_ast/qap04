from lesson_13_09_12_2021.home_work.yana_shafar.driver import Driver


class CheckoutPage(Driver):

    def confirm_the_order(self):

        confirm_locator = "//button[@id='j-basket__confirm']"

        confirm = self.find_element(confirm_locator)
        self.click(confirm)