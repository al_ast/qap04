from lesson_13_09_12_2021.home_work.yana_shafar.driver import Driver


class MainPage(Driver):
    CATEGORY = "//a[text()='{}']"

    def select_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        category = self.find_element(category_locator)
        self.click(category)
