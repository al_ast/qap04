from selenium.webdriver.common.by import By

from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.base_page import BasePage


class ProductPage(BasePage):
    CART_ICON_LOCATOR = "//a[@class='headerCartBox']"
    ADD_TO_CART_BUTTON_LOCATOR = "//button[text()='В корзину']"

    def add_to_cart(self):
        self.click(By.XPATH, self.ADD_TO_CART_BUTTON_LOCATOR)

    def click_cart_icon(self):
        self.click_with_java_script(By.XPATH, self.CART_ICON_LOCATOR)
