import time


class BasePage:
    def __init__(self, driver):
        self._driver = driver

    def open_site(self, url):
        self._driver.get(url)

    def click(self, by_type, locator):
        # by_type = e.g By.XPATH
        element = self._driver.find_element(by_type, locator)
        element.click()
        time.sleep(1)

    # click_js is a really used name for it
    # Renamed from click_button
    def click_with_java_script(self, by_type, locator):
        element = self._driver.find_element(by_type, locator)
        self._driver.execute_script("arguments[0].click();", element)
        time.sleep(1)
