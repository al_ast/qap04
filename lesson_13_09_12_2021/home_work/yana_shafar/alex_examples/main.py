from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver

from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.cart_page import CartPage
from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.catalog_page import CatalogPage
from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.checkout_page import CheckoutPage
from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.main_page import MainPage
from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.product_page import ProductPage

driver = webdriver.Chrome(ChromeDriverManager().install())
url = "https://www.21vek.by/"

main_page = MainPage(driver)
main_page.open_site(url)
main_page.select_category("Телевизоры")

catalog_page = CatalogPage(driver)
catalog_page.select_product('Телевизор Horizont 32LE5511D')

product_page = ProductPage(driver)
product_page.add_to_cart()
product_page.click_cart_icon()

cart_page = CartPage(driver)
cart_page.checkout()

checkout_page = CheckoutPage(driver)
checkout_page.confirm_the_order()
