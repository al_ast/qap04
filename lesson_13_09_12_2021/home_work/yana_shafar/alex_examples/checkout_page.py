from selenium.webdriver.common.by import By

from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.base_page import BasePage


class CheckoutPage(BasePage):
    CONFIRM_LOCATOR = "//button[@id='j-basket__confirm']"

    def confirm_the_order(self):
        self.click(By.XPATH, self.CONFIRM_LOCATOR)
