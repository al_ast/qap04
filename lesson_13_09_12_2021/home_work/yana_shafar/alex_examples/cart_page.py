from selenium.webdriver.common.by import By

from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.base_page import BasePage


class CartPage(BasePage):
    CHECKOUT_LOCATOR = "//button[@id='j-basket__ok']"

    def checkout(self):
        self.click(By.XPATH, self.CHECKOUT_LOCATOR)
