from selenium.webdriver.common.by import By

from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.base_page import BasePage


class MainPage(BasePage):
    CATEGORY_LOCATOR = "//a[text()='{}']"

    def select_category(self, category_name):
        locator = self.CATEGORY_LOCATOR.format(category_name)
        self.click(By.XPATH, locator)
