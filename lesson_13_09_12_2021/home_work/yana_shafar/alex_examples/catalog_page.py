from selenium.webdriver.common.by import By

from lesson_13_09_12_2021.home_work.yana_shafar.alex_examples.base_page import BasePage


class CatalogPage(BasePage):
    PRODUCT_LOCATOR = "//span[text()='{}']"

    def select_product(self, product_name):
        locator = self.PRODUCT_LOCATOR.format(product_name)
        self.click(By.XPATH, locator)
