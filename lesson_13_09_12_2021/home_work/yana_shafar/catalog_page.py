from lesson_13_09_12_2021.home_work.yana_shafar.driver import Driver


class CatalogPage(Driver):
    PRODUCT = "//span[text()='{}']"

    def select_product(self, product_name):
        product_locator = self.PRODUCT.format(product_name)
        product = self.find_element(product_locator)
        self.click(product)
