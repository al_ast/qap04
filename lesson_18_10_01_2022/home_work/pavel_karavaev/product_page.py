from selenium.common.exceptions import TimeoutException

from base_page import BasePage


class ProductPage(BasePage):
    PRODUCT_LOCATOR_WITHOUT_ITEM_NAME = "//*[contains(text(),'{}')]"
    PRODUCTS_PRICES_LOCATOR = "//*[@class='inventory_item_price']"

    def get_count_element(self, item_name):
        try:
            count_product = self.find_elements(self.PRODUCT_LOCATOR_WITHOUT_ITEM_NAME.format(item_name))
            return len(count_product)
        except TimeoutException:
            return 0

    def get_prices_of_all_product(self):
        try:
            prices_elements_list = self.find_elements(self.PRODUCTS_PRICES_LOCATOR)
            prices_list = []
            for element in prices_elements_list:
                prices_list.append(element.text)
                if len(prices_list) > 0:
                    return prices_list
        except TimeoutException:
            return False
