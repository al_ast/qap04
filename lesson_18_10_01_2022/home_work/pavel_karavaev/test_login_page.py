import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from login_page import LoginPage


@pytest.fixture()
def setup_login_page():
    global login_page
    global driver

    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    login_page.navigate()

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_login_page")
class TestLoginPage:
    @pytest.mark.wrong_credentials
    def test_login_with_invalid_credentials(self):
        login_page.login("nonexistent_username", "nonexistent_password")

        assert login_page.is_opened(), "login page not opening with invalid credentials"

    @pytest.mark.true_credentials
    def test_login_with_valid_credentials(self):
        login_page.login("standard_user", "secret_sauce")

        assert login_page.is_product_page_opening(), "product page not opening with valid credentials"

# pytest C:\qap04\lesson_18_10_01_2022\home_work\pavel_karavaev -m "wrong_credentials" - для запуска теста только с
# неправильными данными
