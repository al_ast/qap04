import pytest


class TestCalculator:
    @pytest.mark.parametrize("first_number,second_number,actually",
                             [
                                 (4, 3, 7),
                                 (2, 6, 8),
                                 (5, 3, 8),
                                 (7, 10, 17)
                             ])
    def test_sum(self, first_number, second_number, actually):
        assert first_number + second_number == actually
