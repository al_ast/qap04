from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def _get_locator_by_type(self, locator):
        if "//" in locator:
            return By.XPATH
        else:
            return By.CSS_SELECTOR

    def _element(self, locator, timeout=5):
        locator_by_type = self._get_locator_by_type(locator)

        return WebDriverWait(self.driver, timeout).until(EC.presence_of_element_located((locator_by_type, locator)))

    def enter_text(self, locator, value):
        element = self._element(locator)
        element.send_keys(value)

    def click(self, locator):
        element = self._element(locator)
        element.click()

    def get_text(self, locator):
        element = self._element(locator)
        return element.text

    def open_url(self, url):
        self.driver.get(url)

    def is_element_present(self, locator):
        try:
            element = self._element(locator)
            return True
        except TimeoutException:
            return False

    def find_elements(self, locator, timeout=10):
        by_type = self._get_locator_by_type(locator)
        return WebDriverWait(self.driver, timeout).until(
            EC.presence_of_all_elements_located((by_type, locator))
        )