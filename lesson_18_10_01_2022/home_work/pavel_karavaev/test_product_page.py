import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from product_page import ProductPage
from login_page import LoginPage


@pytest.fixture(scope="session")
def setup_product_page():
    global product_page
    global driver

    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    product_page = ProductPage(driver)

    login_page.navigate()
    login_page.login("standard_user", "secret_sauce")

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_product_page")
class TestProductPage:
    def test_count_of_backpack(self):
        assert product_page.get_count_element("Backpack") > 0, "No one backpack on page"

    def test_get_price_of_product(self):
        price = product_page.get_prices_of_all_product()
        assert price, "one of price empty"
