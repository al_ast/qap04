import pytest


class TestLoginPage:
    @pytest.mark.invalid
    def test_login_page_with_invalid_credentials(self, login_page):
        login_page.login_data('not_valid_user_name', "not_valid_user_password")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    @pytest.mark.valid
    def test_login_page_with_valid_credentials(self, login_page, product_page):
        login_page.login_data("standard_user", "secret_sauce")

        assert product_page.is_opened(), "User should be redirected to the product with valid credentials"
