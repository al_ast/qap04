import pytest


class TestCalculator:

    @pytest.mark.parametrize("first_number, second_number, expected",
                             [
                                (1, 0, 1),
                                (6, 1, 7),
                                (-10, 17, 7)
                             ])
    def test_action(self, first_number, second_number, expected):
        assert first_number + second_number == expected, "invalid value"

