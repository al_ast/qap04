from base_page import BasePage
# проверить, что рюкзаков > 0


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[@class='title']"
    ALL_PRODUCTS_LOCATOR = "//div[contains(text(), '{}')]"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def is_product_presented(self, product_name):
        return self.is_element_present(self.ALL_PRODUCTS_LOCATOR.format(product_name))

