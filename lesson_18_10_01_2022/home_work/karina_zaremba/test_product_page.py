import pytest


@pytest.mark.usefixtures("login")
class TestProductPage:
    @pytest.mark.number_of_products
    def test_product_presence(self, product_page):
        presented_product = product_page.is_product_presented("Backpack")
        assert presented_product, "No such products on the page"

# проверить все маркеры: python -m C:\Users\Karina\Desktop\QA\COPY Hometasks\Lesson17,18 -m number_of_products
# проверить все марки кроме "number_of_products" ----> python -m C:\Users\Karina\Desktop\QA\COPY Hometasks\Lesson17,18 -m not "number_of_products"