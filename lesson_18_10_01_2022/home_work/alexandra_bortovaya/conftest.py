import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.login_page import LoginPage
from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.product_page import ProductPage


@pytest.fixture(autouse=True)
def setup_test_login_page(chromedriver, login_page, product_page):
    login_page.navigate()
    chromedriver.maximize_window()

    yield
    chromedriver.quit()


@pytest.fixture()
def login(login_page):
    login_page.login(LoginPage.STANDARD_USER_LOGIN, LoginPage.VALID_USER_PASSWORD)


@pytest.fixture()
def chromedriver():
    return webdriver.Chrome(ChromeDriverManager().install())


@pytest.fixture()
def login_page(chromedriver):
    return LoginPage(chromedriver)


@pytest.fixture()
def product_page(chromedriver):
    return ProductPage(chromedriver)
