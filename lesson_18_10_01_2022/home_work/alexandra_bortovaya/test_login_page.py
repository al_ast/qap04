import pytest

from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.login_page import LoginPage


class TestLoginPage:
    @pytest.mark.parametrize("username,password", [
        (LoginPage.STANDARD_USER_LOGIN, LoginPage.INVALID_USER_PASSWORD),
        (LoginPage.INVALID_USER_LOGIN, LoginPage.VALID_USER_PASSWORD),
        (LoginPage.INVALID_USER_LOGIN, LoginPage.INVALID_USER_PASSWORD),
        (LoginPage.LOCKED_OUT_USER_LOGIN, LoginPage.VALID_USER_PASSWORD)
    ])
    def test_login_with_invalid_credentials(self, username, password, login_page):
        login_page.login(username, password)

        assert login_page.is_page_opened(), "Login page is not opened, user with invalid credentials must not be logged"

    @pytest.mark.parametrize("username,password", [
        (LoginPage.STANDARD_USER_LOGIN, ""),
        ("", LoginPage.VALID_USER_PASSWORD),
        ("", "")
    ])
    def test_login_with_empty_credentials(self, username, password, login_page):
        login_page.login(username, password)

        assert login_page.is_page_opened(), "Login page is not opened, user with invalid credentials must not be logged"

    @pytest.mark.smoke
    def test_login_with_valid_credentials(self, login_page, product_page):
        login_page.login(LoginPage.STANDARD_USER_LOGIN, LoginPage.VALID_USER_PASSWORD)

        assert product_page.is_page_opened(), "Product page is not opened, user with valid credentials must be logged"
