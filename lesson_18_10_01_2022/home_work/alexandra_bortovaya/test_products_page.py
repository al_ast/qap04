import pytest


@pytest.mark.usefixtures("login")
class TestProductsPage:
    @pytest.mark.smoke
    def test_products_are_present(self, product_page):
        assert product_page.get_products_count() > 0, "There are no products on page"

    def test_all_products_names_exist(self, product_page):
        products_amount = product_page.get_products_count()
        product_names_amount = product_page.get_product_names_count()
        assert products_amount == product_names_amount, f"One or more product names are empty ({products_amount} " \
                                                        f"products, {product_names_amount} product names)"

    def test_all_product_descriptions_exist(self, product_page):
        products_amount = product_page.get_products_count()
        product_descriptions_amount = product_page.get_product_descriptions_count()
        assert product_descriptions_amount == products_amount, f"One or more product descriptions are empty " \
                                                               f"({products_amount} products, " \
                                                               f"{product_descriptions_amount} product descriptions)"

    @pytest.mark.smoke
    def test_all_products_have_add_to_cart_button(self, product_page):
        products_amount = product_page.get_products_count()
        product_buttons_amount = product_page.get_add_to_cart_buttons_count()
        assert product_buttons_amount == products_amount, f"One or more products have no add to cart button " \
                                                          f"({products_amount} products, " \
                                                          f"{product_buttons_amount} product buttons)"

    def test_all_product_prices_exist(self, product_page):
        products_amount = product_page.get_products_count()
        product_prices_amount = product_page.get_product_prices_count()
        assert product_prices_amount == products_amount, f"One or more products have no price ({products_amount} " \
                                                         f"products, {product_prices_amount} product prices)"

    def test_all_products_prices_are_valid(self, product_page):
        is_prices_valid, invalid_prices = product_page.is_all_product_prices_valid()
        assert is_prices_valid, f"There are invalid prices on product page: {invalid_prices}"
