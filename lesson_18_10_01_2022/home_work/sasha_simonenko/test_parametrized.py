import pytest


class TestCalculator:
    @pytest.mark.parametrize("first_number,second_number,expected_sum",
                             [
                                 (48, 29, 77),
                                 (6, 6, 12),
                                 (67, 33, 100),
                                 (20, 20, 40)
                             ])
    def test_sum(self, first_number, second_number, expected_sum):
        assert first_number + second_number == expected_sum
