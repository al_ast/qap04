import pytest


class TestLoginPage:
    @pytest.mark.login
    def test_only_the_login_string(self, login_page, products_page):
        login_page.enter_user_name("standard_user")
        assert products_page.is_opened() == False

    @pytest.mark.password
    def test_only_the_password_string(self, login_page, products_page):
        login_page.enter_password("secret_sauce")
        assert products_page.is_opened() == False

    @pytest.mark.entrance
    def test_with_all_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")
        assert products_page.is_opened(), "Product page will be open"

