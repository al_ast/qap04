import pytest


@pytest.mark.usefixtures("products_page")
class TestProductPage:
    @pytest.mark.product
    def test_is_element_present(self, products_page):
        assert products_page.is_element_present("t-shirts") < 2, "There are only 2 t-shirts"

