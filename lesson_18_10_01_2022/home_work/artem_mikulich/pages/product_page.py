from lesson_16_20_12_2021.home_work.artem_mikulich.base_page import BasePage

class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

