from lesson_18_10_01_2022.home_work.artem_mikulich.pages.base_page import BasePage

class LoginPage(BasePage):
    URL = "https://www.saucedemo.com/"

    LOGIN_LOGO_LOCATOR = "//div[@class='login_logo']"
    USER_NAME_INPUT_LOCATOR = "//input[@id='user-name']"
    PASSWORD_INPUT_LOCATOR = "//input[@id='password']"
    LOGIN_BUTTON_LOCATOR = "//input[@id='login-button']"

    def is_opened(self):
        return self.is_element_present(self.LOGIN_LOGO_LOCATOR)

    def enter_user_name(self, user_name):
        self.enter_text(self.USER_NAME_INPUT_LOCATOR, user_name)

    def enter_password(self, password):
        self.enter_text(self.PASSWORD_INPUT_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def login(self, user_name, password):
        self.enter_user_name(user_name)
        self.enter_password(password)
        self.click_login_button()

    def navigate(self):
        self.open_url(self.URL)