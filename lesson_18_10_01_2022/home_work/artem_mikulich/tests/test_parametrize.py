import pytest

class TestCalculator:
    @pytest.mark.math
    @pytest.mark.parametrize(
        "first_number, second_number, expected",
        [
            (5, 2, 7),
            (10, 5, 15),
            (3, 2, 2)

        ])
    def test_sum(self, first_number, second_number, expected):
        assert first_number + second_number == expected, f"{first_number} + {second_number} not equal {expected}"
