import pytest


class TestLoginPage:

    @pytest.mark.marker1
    @pytest.mark.parametrize('username',
                             [
                                 'standard_user',
                                 'locked_out_user',
                                 'problem_user',
                                 'performance_glitch_user'
                             ]
    )
    def test_login_with_valid_data(self, login_page, products_page, username):
        login_page.login(username, 'secret_sauce')
        assert products_page.is_opened(), 'After login it should open product page'

    @pytest.mark.marker2
    def test_login_with_invalid_data(self,login_page):
        login_page.login('invalid username', 'invalid password')
        assert login_page.is_opened(), 'The user must not login with invalid credentials'
