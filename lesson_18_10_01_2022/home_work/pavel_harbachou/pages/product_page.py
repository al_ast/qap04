import re
from lesson_17_23_12_2021.home_work.pavel_harbachou.pages.base_page import BasePage


class ProductPage(BasePage):
    INVENTORY_ITEM_NAMES_LOCATOR = '//div[@class="inventory_item_name"]'
    INVENTORY_ITEM_PRICES_LOCATOR = '//div[@class="inventory_item_price"]'
    PRODUCT_LOGO_LOCATOR = "//span[text()='Products']"

    def get_t_shirts_count(self):
        t_shirts_count = self.get_element_count('T-Shirt', self.INVENTORY_ITEM_NAMES_LOCATOR)
        return t_shirts_count

    def get_jackets_count(self):
        jacket_count = self.get_element_count('Jacket', self.INVENTORY_ITEM_NAMES_LOCATOR)
        return jacket_count

    def check_not_empty_price(self):
        inventory_prices = self.find_elements(self.INVENTORY_ITEM_PRICES_LOCATOR)
        if len(inventory_prices) == 6:
            return True
        return False

    def is_price_format_valid(self, price):
        match = re.fullmatch("^\$[0-9]+\.[0-9]{2}", price)
        if match and price != "$0.00":
            return True
        return False

    def is_opened(self):
        return self.is_element_present(self.PRODUCT_LOGO_LOCATOR)
