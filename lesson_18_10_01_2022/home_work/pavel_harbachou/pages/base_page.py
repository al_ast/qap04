from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def wait(self, locator):
        locator_type = self._define_locator_type(locator)
        WebDriverWait(self.driver, 10).until(expected_conditions.presence_of_element_located((locator_type, locator)))

    def _define_locator_type(self, locator):
        if '//' in locator:
            return By.XPATH
        else:
            return By.CSS_SELECTOR

    def click(self, locator):
        locator_type = self._define_locator_type(locator)
        self.wait(locator)
        element = self.driver.find_element(locator_type, locator)
        element.click()

    def click_by_js(self, locator):
        locator_type = self._define_locator_type(locator)
        self.wait(locator)
        element = self.driver.find_element(locator_type, locator)
        self.driver.execute_script("arguments[0].click();", element)

    def enter_text(self, locator, text):
        locator_type = self._define_locator_type(locator)
        self.wait(locator)
        element = self.driver.find_element(locator_type, locator)
        element.send_keys(text)

    def find_element(self, locator):
        locator_type = self._define_locator_type(locator)
        self.wait(locator)
        element = self.driver.find_element(locator_type, locator)
        return element

    def find_elements(self, locator):
        locator_type = self._define_locator_type(locator)
        self.wait(locator)
        elements = self.driver.find_elements(locator_type, locator)
        return elements

    def open_site(self, url):
        self.driver.get(url)
        self.driver.maximize_window()

    def is_element_present(self, locator):
        try:
            self.find_element(locator)
            return True
        except TimeoutException:
            return False

    def get_element_count(self, name_of_element, element_locator):
        elements = self.find_elements(element_locator)
        element_count = 0
        for element in elements:
            if name_of_element in element.text:
                element_count += 1
        return element_count
