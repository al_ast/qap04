import pytest


class TestProductPage:
    @pytest.mark.is_product_presence
    def test_is_any_t_shirt_present(self, product_page):
        assert product_page.get_t_shirts_count() > 0, 'No one t_shirt on page'

    @pytest.mark.is_product_presence
    def test_is_any_jackets_present(self, product_page):
        assert product_page.get_jackets_count() > 0, 'No one jacket on page'
