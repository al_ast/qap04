import pytest
# Для запуска всех тестов с пометкой login :
# python -m pytest C:\Users\User\PycharmProjects\qap04\lesson_18_10_01_2022\home_work\pavel_harbachou -m login
# Для запуска всех тестов кроме тестов с пометкой login :
# python -m pytest C:\Users\User\PycharmProjects\qap04\lesson_18_10_01_2022\home_work\pavel_harbachou -m "not login"


class TestLoginPage:
    @pytest.mark.login
    @pytest.mark.parametrize('user_name , password',
                             [
                                 ("standard_user", "secret_sauce"),
                                 ("problem_user", "secret_sauce"),
                                 ("performance_glitch_user", "secret_sauce")
                             ])
    def test_login_with_valid_data(self, user_name, password, product_page, login_page):
        login_page.login(user_name, password)
        assert product_page.is_opened(), 'Products page should be opened after login with valid data'
