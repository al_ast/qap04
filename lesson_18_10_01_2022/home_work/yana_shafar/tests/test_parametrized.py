import pytest


class TestCalculator:
    @pytest.mark.sum
    @pytest.mark.parametrize(
        "first_number, second_number, expected_result",
        [
            (25, 16, 41),
            (5, 8, 13),
            (9, 7, 16)
        ]
    )
    def test_sum(self, first_number, second_number, expected_result):
        assert first_number + second_number == expected_result

# запустить тесты по маркеру sum:
# python -m pytest D:\TeachMeSkills\qap04\lesson_18_10_01_2022\home_work\yana_shafar -m "sum"
# запустить вс тесты кроме маркера valid
# python -m pytest D:\TeachMeSkills\qap04\lesson_18_10_01_2022\home_work\yana_shafar -m "not valid"
