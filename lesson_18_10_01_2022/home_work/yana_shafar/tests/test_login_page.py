import pytest


class TestLoginPage:
    @pytest.mark.invalid
    def test_login_with_not_valid_credentials(self, login_page):
        login_page.login("not_valid", "not_valid")
        assert login_page.is_opened(), "User cannot login with not valid credentials"

    @pytest.mark.valid
    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")
        assert products_page.is_opened(), "Product page must be open after login with valid credentials"
