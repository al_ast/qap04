import pytest


@pytest.mark.usefixtures("login")
class TestProductPage:
    @pytest.mark.products
    def test_amount_of_products(self, products_page):
        assert products_page.amount_of_products("Backpack") > 0, "There are no backpacks on the page"

    @pytest.mark.prices
    def test_prices_of_products(self, products_page):
        assert products_page.get_prices_of_products(), "The price of some product is empty"
