# Придумать свои маркеры.
# Запустить тесты по маркеру
# Запустить все тесты кроме указанного маркера
#
# Написать абсолютно любой параметризированный тест
#
# Попробовать Несколько conftest.py в разных местах, pytest.ini

import pytest

class TestCalculator:
    @pytest.mark.maths
    @pytest.mark.parametrize(
        "first_number,second_number,expected",
        [
            (10, 2, 8),
            (24, 4, 20),
            (3, 5, 0)
        ])
    def test_substraction(self, first_number, second_number, expected):
        assert first_number - second_number == expected, f"{first_number} - {second_number} doesn't equal {expected_number}"

    @pytest.mark.maths
    @pytest.mark.parametrize(
        "number, expected",
        [
            (2, 8),
            (4, 64),
            (5, 1255)
        ])
    def test_exponentation(self, number, expected):
        assert number**3 == expected, f"{number}**3 doesn't equal {expected}"

    @pytest.mark.good
    def test_good(self):
        assert 1 == 1, "1 doesn't equal 1"

    @pytest.mark.bad
    def test_bad(self):
        assert 2 + 4 == 6, "2 + 4 does not equal 6"


# запустить тесты по маркеру maths
# pytest /Users/nastassia/Desktop/branch3/qap04/lesson_18_10_01_2022/home_work/nastya_feshchenko -m maths

# запустить все тесты, кроме маркера bad
# pytest /Users/nastassia/Desktop/branch3/qap04/lesson_18_10_01_2022/home_work/nastya_feshchenko -m "not bad"

