import pytest


class TestLoginPage:
    @pytest.mark.parametrize("user_name,password",
                             [
                                 ("", ""),
                                 (" ", " "),
                                 ("not_valid", "not_valid"),
                                 ("$!@#$ %^&*()", " !@#$%^&*()_")
                             ])
    def test_login_with_not_valid_credentials(self, login_page, user_name, password):
        login_page.login("not valid", "not valid")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    @pytest.mark.slow
    def test_slow(self):
        assert "slow" == "this is slow test"

    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")

        assert products_page.is_opened(), "User should be redirected to the product page after login with valid creds"

    @pytest.mark.fast
    def test_fast(self):
        assert "fast" == "this is fast test"
