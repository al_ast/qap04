import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_17_23_12_2021.class_materials.pages.login_page import LoginPage
from lesson_17_23_12_2021.class_materials.pages.product_page import ProductsPage


@pytest.fixture(autouse=True)
def navigate_to_login_page(chromedriver):
    login_page = LoginPage(chromedriver)
    login_page.navigate()

    yield
    chromedriver.quit()


@pytest.fixture()
def chromedriver():
    return webdriver.Chrome(ChromeDriverManager().install())


@pytest.fixture()
def login_page(chromedriver):
    return LoginPage(chromedriver)


@pytest.fixture()
def products_page(chromedriver):
    return ProductsPage(chromedriver)
