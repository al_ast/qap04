class Worker:
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary

    def print_info(self):
        print(f"{self.name=}.{self.salary=}")


class Company:
    def __init__(self, title, workers):
        self.workers = workers
        self.title = title

    def print_workers(self):
        for worker in self.workers:
            worker.print_info()

    def print_workers_count(self):
        print(len(self.workers))

    def print_exact_worker_salary(self, index):
        print(self.workers[index].salary)

    def hire_workers(self, workers):
        self.workers.extend(workers)


alex = Worker("Alex", "100")
dima = Worker("Dima", "200")
yula = Worker("Yula", "150")

all_workers = [alex, dima, yula]

apple = Company("Apple", all_workers)

apple.print_workers_count()
apple.print_workers()
apple.print_exact_worker_salary(2)

microsoft = Company("Microsoft", [])
microsoft.print_workers()
microsoft.hire_workers([yula, dima, alex])
microsoft.print_workers()
