"""
Создайте класс book с именем книги, автором, кол-м страниц, зарезервирована ли книга или нет.
Создайте класс пользователь который может брать книгу, возвращать, бронировать.
Если другой пользователь хочет взять зарезервированную книгу (или которую уже кто-то читает - надо ему про это сказать).
"""


class Book:
    def __init__(self, book_name, author, num_of_pages, is_reserved=False, is_taken=False, occupied_by=None):
        self.book_name = book_name
        self.author = author
        self.num_of_pages = num_of_pages
        self.is_reserved = is_reserved
        self.is_taken = is_taken
        self.occupied_by = occupied_by


book_1 = Book("Dune", "Frank Herbert", 704)
book_2 = Book("Pride and Prejudice", "Jane Austen", 384)
book_3 = Book("The Picture of Dorian Gray", "Oscar Wilde", 320)


class Reader:
    is_available = True

    def __init__(self, reader_name):
        self.reader_name = reader_name

    def is_book_available(self, book):
        if book.is_reserved:
            print(f"{self.reader_name}, '{book.book_name}' is already reserved by {book.occupied_by}.")
            self.is_available = False
        elif book.is_taken:
            print(f"{self.reader_name}, '{book.book_name}' is already taken by {book.occupied_by}.")
            self.is_available = False
        else:
            self.is_available = True

    def reserve_book(self, book):
        self.is_book_available(book)
        if self.is_available:
            print(f"{self.reader_name}, '{book.book_name}' is now reserved by you.")
            book.is_reserved = True
            book.occupied_by = self.reader_name

    def take_book(self, book):
        self.is_book_available(book)
        if self.is_available:
            print(f"{self.reader_name}, you can take '{book.book_name}'.")
            book.is_taken = True
            book.occupied_by = self.reader_name

    def return_book(self, book):
        if self.reader_name == book.occupied_by:
            print(f"{self.reader_name}, thanks for returning '{book.book_name}'.")
            book.is_taken = False
            book.is_reserved = False
        else:
            print(f"{self.reader_name}, you're a cheater! This book is not reserved or taken by you!")


reader_1 = Reader("Lexi")
reader_2 = Reader("James")

reader_1.reserve_book(book_1)
reader_1.take_book(book_2)
reader_2.take_book(book_1)
reader_2.return_book(book_1)
reader_1.return_book(book_1)
reader_2.take_book(book_1)
