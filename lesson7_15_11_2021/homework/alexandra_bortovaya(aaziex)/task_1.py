"""
Реализовать калькулятор с 4 методами: Сумма, вычитание , умножение, деление.
Метод принимает 2 аргумента и возвращает результат. Невалидные данные должны быть обработаны
"""


class Calculator:

    def validate_numbers(self, number1, number2):
        is_valid_number_1 = isinstance(number1, (int, float))
        is_valid_number_2 = isinstance(number2, (int, float))

        if not is_valid_number_1:
            raise Exception("1st number is not valid")
        elif not is_valid_number_2:
            raise Exception("2nd number is not valid")

    def addition(self, a, b):
        self.validate_numbers(a, b)
        return a + b

    def subtraction(self, a, b):
        self.validate_numbers(a, b)
        return a - b

    def multiplication(self, a, b):
        self.validate_numbers(a, b)
        return a * b

    def division(self, a, b):
        self.validate_numbers(a, b)
        if b == 0:
            raise Exception("Division by zero is not allowed")
        else:
            return round(a/b, 2)


calc_try = Calculator()
print(calc_try.addition(1, 2))
print(calc_try.subtraction(1, 2))
print(calc_try.multiplication(1, 2))
print(calc_try.division(1, 2))
print(calc_try.division(1, 0))
print(calc_try.addition('1', 2))
print(calc_try.addition(1, 'abc'))
