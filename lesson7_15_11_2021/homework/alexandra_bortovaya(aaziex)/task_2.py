"""
Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
Класс студен описан следующим образом:

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

Необходимо понять у кого больше всего денег и вернуть его имя.
Если у студентов денег поровну вернуть: “all”.
"""


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money


student1 = Student('Mary', 100)
student2 = Student('James', 100)
student3 = Student('Thomas', 150)

all_students = [student1, student2, student3]


class MoneyCount:
    def count_money(self, students):
        money_list = []
        for student in students:
            money_list.append(student.money)
        if money_list.count(money_list[0]) == len(money_list):
            print("All")
        else:
            max_amount = 0
            student_name = ''
            for student in students:
                if student.money > max_amount:
                    max_amount = student.money
                    student_name = student.name

            print(f"{student_name} has more money than others ({max_amount}).")


group = MoneyCount()
group.count_money(all_students)
