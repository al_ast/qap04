"""2.
Создайте класс Students, содержащий поля: фамилия и инициалы, номер группы,
успеваемость (массив из пяти элементов).

Создать класс School:

Добавить возможность для добавления студентов в школу - готово
Добавить возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 5 или 6.
Добавить возможность вывода учеников заданной группы - done
Добавить возможность вывода учеников претендующих на автомат(средний балл >= 7)"""


class Students:

    def __init__(self, name, group, progress):
        self.name = name
        self.group = group
        self.progress = progress

    def get_info_list(self):
        students_info = [self.name, self.group, self.progress]
        return students_info

    def __repr__(self):
        return self.name


class School:

    def __init__(self, list_of_students):
        self.list_of_students = list_of_students

    def admission(self, student):
        self.list_of_students.append(student)

    def remove(self, student, group):
        if student.group == group:
            self.list_of_students.remove(student)

    def get_list_of_students(self):
        return self.list_of_students

    def print_names(self):
        for student in self.list_of_students:
            print(student.name)

    def print_group(self, group):
        for student in self.list_of_students:
            if student.group == group:
                print(student)


student_denis = Students("Denis", "1A", [5, 7, 8, 7, 8])

student_artem = Students("Artem", "1B", [2, 10, 5, 9, 9])

school1 = School([])

school1.admission(student_denis)

school1.admission(student_artem)

school1.print_group("1A")
