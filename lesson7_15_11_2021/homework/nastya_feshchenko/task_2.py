# 2.
# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Необходимо понять у кого больше всего денег и вернуть его имя.
# Если у студентов денег поровну вернуть: “all”.

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

student1 = Student("Jane", 200)
student2 = Student("Alex", 250)
student3 = Student("Oscar", 320)
student4 = Student("Jessica", 235)

students = [student1, student2, student3, student4]

# у кого больше денег?
max_amount = 0

for student in students:
    if student.money > max_amount:
        max_amount = student.money

print(max_amount)


# если у всех студентов денег поровну
money = []
for student in students:
    money.append(student.money)

count = money.count(money[0])

if count == len(students):
    print("all")

print(money)
print(count)


