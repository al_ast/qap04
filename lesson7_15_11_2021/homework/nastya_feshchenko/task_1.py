# 1.
# Реализовать калькулятор с 4 методами:
# Сумма, вычитание , умножение, деление.
# Метод принимает 2 аргумента и возвращает результат.
# Невалидные данные должны быть обработаны

class Calculator:
    def validate_numbers(self, number1, number2):
        is_valid_number1 = isinstance(number1, int)
        is_valid_number2 = isinstance(number2, int)
        if is_valid_number1 and is_valid_number2:
            print("Numbers are valid")
        else:
            raise Exception(f"The numbers aren't valid")

    def summ(self, a, b):
        self.validate_numbers(a, b)

        return a + b

    def subtract(self, a, b):
        self.validate_numbers(a, b)

        return a - b

    def multiply(self, a, b):
        self.validate_numbers(a, b)

        return a * b

    def divide(self, a, b):
        self.validate_numbers(a, b)

        return a / b


calc = Calculator()
a = calc.summ(5, 2)
print(a)
b = calc.subtract(3, 2)
print(b)
c = calc.multiply(4, 8)
print(c)
d = calc.divide(9, 3)
print(d)
