# Реализовать калькулятор с 4 методами:
# Сумма, вычитаниеб , умножение, деление.
# Метод принимает 2 аргумента и возвращает результат.
# Невалидные данные должны быть обработаны
class Calculator:
    def valid_number(self, number1, number2):
        valid_number1 = isinstance(number1, (int, float))
        valid_number2 = isinstance(number2, (int, float))
        if not valid_number1 or not valid_number2:
            print('Not valid')
            exit()

    def sum(self, a, b):
        self.valid_number(a, b)
        print(a + b)

    def sub(self, a, b):
        self.valid_number(a, b)
        print(a - b)

    def mul(self, a, b):
        self.valid_number(a, b)
        print(a * b)

    def div(self, a , b):
        self.valid_number(a, b)
        try:
            print(a / b)
        except ZeroDivisionError:
            print('cannot be divisible by 0')

calc = Calculator()
calc.div(2, 0)



