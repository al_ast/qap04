# Создайте класс book с именем книги, автором, кол-м страниц,
# зарезервирована ли книга или нет.
# Создайте класс пользователь который может брать книгу, возвращать, бронировать.
# Если другой пользователь хочет взять зарезервированную
# книгу(или которую уже кто-то читает - надо ему про это сказать).
class Book:
    def __init__(self, name: str, author: str, number_of_pages: int, reserve: bool, available: bool):
        self.name = name
        self.author = author
        self.number_of_pages = number_of_pages
        self.reserve = reserve
        self.available = available
        self.name_of_user = ''


spark_of_life = Book('Spark of life', 'Remarque', 438, False, False)
great_expectations = Book('Great expectations', 'Dickens', 500, False, False)
the_catcher_in_the_rye = Book('The catcher in the rye', 'Salinger', 320, False, False)


class User:
    def __init__(self, user_name: str):
        self.user_name = user_name

    def take_a_book(self, name_of_book: Book):
        if name_of_book.available:
            print(f'This book is taken by {name_of_book.name_of_user}')
        else:
            name_of_book.available = True
            name_of_book.name_of_user = self.user_name
            print('You take this book')

    def return_a_book(self, name_of_book:Book):
        if name_of_book.available:
            name_of_book.available = False
            name_of_book.name_of_user = ''
            print('This book is returned')
        else:
            print("This book isn't reserved")

    def reserve_a_book(self, name_of_book: Book):
        if not name_of_book.reserve:
            name_of_book.reserve = True
            print('You reserve this book')
            name_of_book.name_of_user = self.user_name
        else:
            print(f'This book is reserved by {name_of_book.name_of_user}')

user1 = User('Alex')
user2 = User('Clinton')
user1.take_a_book(spark_of_life)
user2.take_a_book(spark_of_life)
user1.return_a_book(spark_of_life)
user2.reserve_a_book(great_expectations)
user1.reserve_a_book(great_expectations)
