"""
1.
Реализовать калькулятор с 4 методами:
Сумма, вычитаниеб , умножение, деление.
Метод принимает 2 аргумента и возвращает результат.
Невалидные данные должны быть обработаны
"""


class Calculator:

    def is_valid_number(self, number1, number2):
        is_valid_number_1 = isinstance(number1, int) or isinstance(number1, float)
        is_valid_number_2 = isinstance(number2, int) or isinstance(number2, float)

        if is_valid_number_1 and is_valid_number_2:
            pass
        else:
            raise Exception('Not valid numbers')

    def sum(self, a, b):
        self.is_valid_number(a, b)

        return a + b

    def subs(self, a, b):
        self.is_valid_number(a, b)

        return a - b

    def multiplication(self, a, b):
        self.is_valid_number(a, b)

        return a * b

    def division(self, a, b):
        self.is_valid_number(a, b)
        if b == 0:
            raise ZeroDivisionError('Dividing by zero')

        return a / b


my_numbers = Calculator()
print(my_numbers.sum(1, 10))
print(my_numbers.subs(5, 14))
print(my_numbers.multiplication(3, 2))
print(my_numbers.division(12, 3))
