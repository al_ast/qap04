# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Класс студен описан следующим образом:
#
# class Student:
#     def __init__(self, name, money):
#         self.name = name
#         self.money = money
#
# Необходимо понять у кого больше всего денег и вернуть его имя.
# Если у студентов денег поровну вернуть: “all”.

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money


student1 = Student("A", 90)
student2 = Student("B", 90)
student3 = Student("C", 90)
student4 = Student("D", 90)

students = [student1, student2, student3, student4]
moneys = []
for student in students:
    moneys.append(student.money)

count = moneys.count(moneys[0])
if count == len(students):
    print("all")
else:
    for student in students:
        if student.money > max_amount:
            max_amount = student.money
    print(max_amount)

print(count)
print(moneys)

# max_amount = 0
# for student in students:
#     if student.money > max_amount:
#         max_amount = student.money
# print(max_amount)

# print(student.money)