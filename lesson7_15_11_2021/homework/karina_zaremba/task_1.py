# Реализовать калькулятор с 4 методами:
# Сумма, вычитаниеб , умножение, деление.
# Метод принимает 2 аргумента и возвращает результат.
# Невалидные данные должны быть обработаны


class Calculator:

    def is_valid(self, num1, num2):
        is_valid_numbers_num1 = isinstance(num1, int) or isinstance(num1, float)
        is_valid_numbers_num2 = isinstance(num2, int) or isinstance(num2, float)
        if not is_valid_numbers_num1 and not is_valid_numbers_num2:
            print("not valid")

    # def __init__(self, num1, num2):
    #     self.num1 = num1
    #     self.num2 = num2
    def add(self, a, b):
        self.is_valid(a, b)
        return (a + b)
    def mul(self, a, b):
        self.is_valid(a, b)
        return (a * b)
    def div(self, a, b):
        self.is_valid(a, b)
        try:
           return (a/b)
        except ZeroDivisionError:
            print("Mistake")
    def sub(self, a, b):
        self.is_valid(a, b)
        return (a - b)


calc = Calculator()
# result = calc.add(1, 3)
# print(result)
#
# result = calc.mul(1, 5)
# print(result)

result = calc.div(1, 0)
print(result)

# result = calc .sub(10, 2)
# print(result)

