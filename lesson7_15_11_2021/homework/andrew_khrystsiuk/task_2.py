# 2.
# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Класс студен описан следующим образом:
# Необходимо понять у кого больше всего денег и вернуть его имя.
# Если у студентов денег поровну вернуть: “all”.


class Student:

    def __init__(self, name, money):
        self.name = name
        self.money = money


student1 = Student('Andrew', 100)
student2 = Student('Alex', 100)
student3 = Student('Dmitry', 100)

students = [student1, student2, student3]
moneys = []
for student in students:
    moneys.append(student.money)
print(moneys)

count = moneys.count(student.money)
max_amount = 0
for student in students:
    if count == len(moneys):
        print("all")
        break

    elif student.money > max_amount:
        max_amount = student.money
        print(f'{student.name} have {max_amount}')
