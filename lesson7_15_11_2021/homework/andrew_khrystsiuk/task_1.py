
class Calculator:

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def validate(self, a, b):
        is_valid_number_1 = isinstance(a, int) or isinstance(a, float)
        is_valid_number_2 = isinstance(b, int) or isinstance(b, float)

        if is_valid_number_1 and is_valid_number_2:
            print('numbers are valid')
        else:
            raise Exception('Numbers are invalid')

    def summ(self):
        self.validate(a, b)
        return self.a + self.b

    def sub(self):
        self.validate(a, b)
        return self.a - self.b

    def mult(self):
        self.validate(a, b)
        return self.a * self.b

    def div(self):
        self.validate(a, b)
        if self.b == 0:
            return 'Division by zero!'
        else:
            return self.a / self.b


a = 10
b = 2

print("1. Sum")
print("2. Subtraction")
print("3. Multiplication")
print("4. Division")
choice1 = int(input('Input choice: '))
calc1 = Calculator(a, b)
if choice1 == int(1):
    print(calc1.summ())
elif choice1 == int(2):
    print(calc1.sub())
elif choice1 == int(3):
    print(calc1.mult())
elif choice1 == int(4):
    print(calc1.div())
else:
    print('Incorrect input')
