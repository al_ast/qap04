a = 100
print(a)

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

student1 = Student('Sasha', 100)
student2 = Student('Vika', 110)
student3 = Student('Ira', 140)

students = [student1, student2, student3]
moneys = []
for student in students:
    moneys.append(student.money)

count = moneys.count(moneys[0])
if count == len(students):
    print('all')
else:
    max_amount = 0

    for student in students:
        if student.money > max_amount:
            max_amount = student.money

    print(max_amount)