class Calculator:
    def check(self, value):
        is_number_valid = isinstance(value, int) or isinstance((value, float))

        if not is_number_valid:
            raise  Exception(f'{value} is not valid number')

    def summ(self, a, b):

        self.check(a)
        self.check(b)

        return a + b

    def subtract(self, a, b):
        self.check(a)
        self.check(b)

        return a - b

    def multiply(self, a, b):
        self.check(a)
        self.check(b)

        return a * b

    def devide(self, a, b):
        self.check(a)
        self.check(b)


        if b == 0:
            raise  Exception('devide by zero is not allowed')

        return a / b

calc = Calculator()
print(calc.summ(2,8))
print(calc.subtract(3,9))
print(calc.multiply(5,5))
print(calc.devide(10,1))