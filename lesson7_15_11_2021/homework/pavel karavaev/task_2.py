class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money
    def print_student_name(self):
        print(self.name)
    def print_student_money(self):
        print(self.money)
    def print_info(self):
        print(f"{self.name}.{self.money}")

student_1 = Student("pavel", 100)
student_2 = Student("vlad", 200)
student_3 = Student("dima", 40)


all_students = [student_1, student_2, student_3]
money = []

for student in all_students:
    money.append(student.money)

count = money.count(money[0])

if count == len(all_students):
    print("all equally")
else:
    max_money = 0
    for student in all_students:
        if student.money > max_money:
            max_money = student.money
            name_student = student.name
    print(f"{name_student} have money: {max_money}")
