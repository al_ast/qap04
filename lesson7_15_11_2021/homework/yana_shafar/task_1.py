# Реализовать калькулятор с 4 методами:
# Сумма, вычитаниеб , умножение, деление.
# Метод принимает 2 аргумента и возвращает результат.
# Невалидные данные должны быть обработаны


class Calculator:

    def sum(self, a, b):
        if isinstance(a, int) and isinstance(b, int):
            return a + b
        else:
            return f'Only numbers are allow'

    def subtruction(self, a, b):
        if isinstance(a, int) and isinstance(b, int):
            return a - b
        else:
            return f'Only numbers are allow'

    def multiplication(self, a, b):
        if isinstance(a, int) and isinstance(b, int):
            return a * b
        else:
            return f'Only numbers are allow'

    def division(self, a, b):
        if isinstance(a, int) and isinstance(b, int):
            if b == 0:
                return f'You cannot divide by zero'
            else:
                return a / b
        else:
            return f'Only numbers are allow'


my_calculator = Calculator()

print(my_calculator.sum(34, 'ab'))
print(my_calculator.multiplication(34, 17))
print(my_calculator.subtruction(34, 17))
print(my_calculator.division(34, 0))