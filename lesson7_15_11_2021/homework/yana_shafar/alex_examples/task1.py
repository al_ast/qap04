# Реализовать калькулятор с 4 методами:
# Сумма, вычитаниеб , умножение, деление.
# Метод принимает 2 аргумента и возвращает результат.
# Невалидные данные должны быть обработаны


class Calculator:
    def _check_is_number_format_valid(self, value):
        is_number_valid = isinstance(value, int) or isinstance(value, float)

        if not is_number_valid:
            raise Exception(f"{value}: is not valid number")

    def sum(self, a, b):

        self._check_is_number_format_valid(a)
        self._check_is_number_format_valid(b)

        return a + b

    def subtract(self, a, b):
        self._check_is_number_format_valid(a)
        self._check_is_number_format_valid(b)

        return a - b

    def multiply(self, a, b):
        self._check_is_number_format_valid(a)
        self._check_is_number_format_valid(b)

        return a * b

    def devide(self, a, b):
        self._check_is_number_format_valid(a)
        self._check_is_number_format_valid(b)

        if b == 0:
            raise Exception("Devide by zero is not allowed")

        return a / b
