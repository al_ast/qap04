# Создайте класс book с именем книги, автором, кол-м страниц,
# зарезервирована ли книга или нет.
# Создайте класс пользователь который может брать книгу, возвращать, бронировать.
# Если другой пользователь хочет взять зарезервированную
# книгу(или которую уже кто-то читает - надо ему про это сказать).


class Book:
    def __init__(self, name, author, pages, is_reserved_by="",
                 is_present=True):
        self.name = name
        self.author = author
        self.pages = pages
        self.is_reserved_by = is_reserved_by
        self.is_present = is_present

    def is_available(self, user_name):
        is_available_based_on_reserved = \
            self.is_reserved_by == "" or self.is_reserved_by == user_name

        return self.is_present and is_available_based_on_reserved


class User:
    def __init__(self, name):
        self.name = name

    def take(self, book):
        if book.is_available(self.name):
            book.is_reserved_by = self.name
            book.is_present = False
            print(f"Taking the book: {book.name}")
            return

        print(f"Book is reserved or not present:{book.name}")

    def return_the_book(self, book):
        book.is_present = True
        book.is_reserved_by = ""
        print(f"Book is returned:{book.name}")

    def reserve(self, book):
        if book.is_available(self.name):
            book.is_reserved_by = self.name
            print(f"Reserve the book:{book.name}")

            return

        print(f"Book is reserved or not present:{book.name}")


book1 = Book('Book1', 'Author1', 5)
book2 = Book('Book2', 'Author2', 10)
book3 = Book('Book3', 'Author3', 15)

user1 = User('Yana')
user2 = User('Sophiya')

user1.reserve(book1)
user1.take(book1)
user2.take(book1)
user1.return_the_book(book1)
user2.reserve(book1)
user1.take(book1)
user1.take(book3)
