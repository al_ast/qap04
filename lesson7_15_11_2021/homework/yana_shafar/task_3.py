# Создайте класс book с именем книги, автором, кол-м страниц,
# зарезервирована ли книга или нет.
# Создайте класс пользователь который может брать книгу, возвращать, бронировать.
# Если другой пользователь хочет взять зарезервированную
# книгу(или которую уже кто-то читает - надо ему про это сказать).


class Book:
    def __init__(self, name, author, pages, is_reserved=False, is_read=False):
        self.name = name
        self.author = author
        self.pages = pages
        self.is_reserved = is_reserved
        self.is_read = is_read


class User:
    def __init__(self, name):
        self.name = name

    def take(self, book):
        if not book.is_reserved:
            book.is_read = True
            book.is_reserved = True
            print(f'{self.name} takes {book.name}')
        else:
            print(f'Book is reserved')

    def give_away(self, book):
        book.is_read = False
        book.is_reserved = False

    def reserve(self, book):
        if not book.is_reserved:
            book.is_reserved = True
        else:
            print(f'Book is reserved')


book1 = Book('Book1', 'Author1', 5, False, False)
book2 = Book('Book2', 'Author2', 10, False, False)
book3 = Book('Book3', 'Author3', 15, False, False)

user1 = User('Yana')
user2 = User('Sophiya')

user1.take(book1)
user2.take(book1)
user1.give_away(book1)
user2.reserve(book1)
user1.take(book1)