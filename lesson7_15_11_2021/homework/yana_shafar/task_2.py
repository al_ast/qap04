# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Класс студен описан следующим образом:
#
# class Student:
#     def init(self, name, money):
#         self.name = name
#         self.money = money
#
# Необходимо понять у кого больше всего денег и вернуть его имя.
# Если у студентов денег поровну вернуть: “all”.

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money


def get_key(d, value):
    for k, v in d.items():
        if v == value:
            return k


def analyze(student_list):
    student_dict = {}
    for student in student_list:
        student_dict[student.name] = student.money

    student_money_set=set(student_dict.values())
    if len(student_money_set) != len(student_dict.values()):
        return f'all'
    max_money = max(student_dict.values())
    max_student = get_key(student_dict, max_money)

    return max_student


student1 = Student('Yana', 100)
student2 = Student('Dasha', 95)
student3 = Student('Ksusha', 120)
student4 = Student('Alexey', 107)
student5 = Student('Yaroslav', 125)

student_list = [student1, student2, student3, student4, student5]

print(analyze(student_list))