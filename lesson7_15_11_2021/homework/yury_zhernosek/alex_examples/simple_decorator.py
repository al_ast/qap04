import time


def execution_time_decorator(function_to_check):
    start_time_seconds = int(time.time())
    print("До выполнения функции")
    function_to_check()
    print("После выполения функции")

    end_time_seconds = int(time.time())

    print(f"Время выполнения секунд:{end_time_seconds - start_time_seconds}")


@execution_time_decorator
def my_simple_function():
    time.sleep(1)
    print("Print from the function when finished")


