"""
Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
Класс студен описан следующим образом:

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

Необходимо понять у кого больше всего денег и вернуть его имя.
Если у студентов денег поровну вернуть: “all”.
"""

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

class Info:

    def print_money(self):
        max_money = 0
        for student in list_students:
            list_money.append(student.money)
        if len(list_money) == list_money.count(list_money[1]):
            print('All')
        else:
            for student in list_students:
                if student.money >= max_money:
                    max_money = student.money
                    name_student = student.name
            print(f'{name_student} have max money: {max_money}$')

student1 = Student('Andrei', 60)
student2 = Student('Yura', 60)
student3 = Student('Kolya', 100)
list_students = [student1,student2,student3]
list_money = []
test = Info()
test.print_money()