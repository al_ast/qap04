'''
Создайте класс book с именем книги, автором, кол-м страниц,
зарезервирована ли книга или нет.
Создайте класс пользователь который может брать книгу, возвращать, бронировать.
Если другой пользователь хочет взять зарезервированную
книгу(или которую уже кто-то читает - надо ему про это сказать).
'''
class Book:

    def __init__(self, name, author, count_page, reserved):
        self.name = name
        self.author = author
        self.count_page = count_page
        self.reserved = reserved



class User:

    def __init__(self, list_you_book):
        self.list_you_book = list_you_book

    def take_book(self,number_book):
        if number_book.reserved == 'Yes':
            print('Book reserved!')
        else:
            number_book.reserved = 'Yes'
            (self.list_you_book).append(number_book.name)
            print('You take this book!')
    def return_book(self,number_book):
        if number_book.name in self.list_you_book:
            number_book.reserved = 'No'
            self.list_you_book.remove(number_book.name)
            print('You return book!')
        else:
            print('You dont have this book!')
    def reserve_book(self,number_book):
        if number_book.reserved == 'Yes':
            print('Book reserved!')
        else:
            number_book.reserved = 'Yes'
            (self.list_you_book).append(number_book.name)
            print('You reserved this book!')
    def print_list_book_user(self):
        print(self.list_you_book)

book1 = Book('Book1', 'Pushkin1', 100, 'Yes')
book2 = Book('Book2', 'Pushkin2', 200, 'No')
book3 = Book('Book3', 'Pushkin3', 120, 'Yes')
book4 = Book('Book4', 'Pushkin4', 210, 'No')
book5 = Book('Book5', 'Pushkin5', 90, 'No')
user1 = User([])
user2 = User([])
user1.take_book(book2)
user2.take_book(book3)
user1.return_book(book2)
user2.return_book(book1)
user1.reserve_book(book5)
user1.return_book(book5)