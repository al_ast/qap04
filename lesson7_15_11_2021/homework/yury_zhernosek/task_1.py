"""
Реализовать калькулятор с 4 методами:
Сумма, вычитаниеб , умножение, деление.
Метод принимает 2 аргумента и возвращает результат.
Невалидные данные должны быть обработаны
"""

class Calculator:

    def check_valid(self, a, b):
        valid_number_int = isinstance(a, int)
        valid_number_float = isinstance(a, float)
        if not valid_number_float and not valid_number_int:
            raise Exception('A not valid')
        else:
            valid_number_int = isinstance(b, int)
            valid_number_float = isinstance(b, float)
            if not valid_number_float and not valid_number_int:
                raise Exception('B not valid')

    def summ(self, a, b):
        self.check_valid(a, b)
        print(f'{a}+{b} = {a+b}')

    def dif(self, a, b):
        self.check_valid(a, b)
        print(f'{a}-{b} = {a-b}')

    def multiplication(self, a, b):
        self.check_valid(a, b)
        print(f'{a}*{b} = {a*b}')

    def division(self, a, b):
        self.check_valid(a, b)
        print(f'{a}/{b} = {a/b}')

test = Calculator()
test.summ(2,2)
test.division(2, 4)
test.dif(10,5)
test.multiplication(2,8)