'''
метод sum(a,b) принимает 2 числа и возвращает их сумму.
Написать декоратор, который возвращает ошибку
если переданы нечисловые параметры(например строка)

примерно такое:

@validate_int_parameters
def sum(a,b)

sum(3, "1") => ошибка
sum(4, 2) = > 6
'''
def sum(a, b):
    valid_number_int = isinstance(a, int)
    valid_number_float = isinstance(a, float)
    if not valid_number_float and not valid_number_int:
        print(f'{a}+{b}=Error')
        return
    else:
        valid_number_int = isinstance(b, int)
        valid_number_float = isinstance(b, float)
        if not valid_number_float and not valid_number_int:
            print(f'{a}+{b}=Error')
            return
        print(f'{a}+{b}={a+b}')
sum(2,2)
sum(2,'2')
sum('2',2)