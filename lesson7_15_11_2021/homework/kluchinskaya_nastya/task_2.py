# 2.
# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Класс студен описан следующим образом:
#
# class Student:
#     def __init__(self, name, money):
#         self.name = name
#         self.money = money
#
# Необходимо понять у кого больше всего денег и вернуть его имя.
# Если у студентов денег поровну вернуть: “all”.


class Students:
    def __init__(self, name, money):
        self.name = name
        self.money = money


student1 = Students("Rob", 150)
student2 = Students("Maks", 150)
student3 = Students("Bob", 150)

students = [student1, student2, student3]

moneys = []
for student in students:
    moneys.append(student.money)

count = moneys.count(moneys[0])

if count == len(students):
    print("all")
else:
    max_amount = 0
    student_name = ""
    for student in students:
        if student.money > max_amount:
            max_amount = student.money
            student_name = student.name
    print(f"{student_name} has more money than others ({max_amount}).")