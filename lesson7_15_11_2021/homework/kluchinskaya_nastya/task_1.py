# 1.
# Реализовать калькулятор с 4 методами:
# Сумма, вычитаниеб , умножение, деление.
# Метод принимает 2 аргумента и возвращает результат.
# Невалидные данные должны быть обработаны


class Calculator:
    def validate_numbers(self, number1, number2):
        is_valid_number_1 = isinstance(number1, int) or isinstance(number1, float)

        is_valid_number_2 = isinstance(number2, int) or isinstance(number2, float)

        if is_valid_number_1 and is_valid_number_2:
            print("numbers are valid")
        else:
            raise Exception("Not valid numbers")

    def multiply(self, a, b):
        print(a * b)

    def summ(self, a, b):
        print(a + b)

    def divide(self, a, b):
        # if isinstance(a, int) and isinstance(b, int):
        if b == 0:
            return f'You cannot divide by zero'
        else:
            print(a / b)

    def sub(self, a, b):
        print(a - b)


calk1 = Calculator()
print(calk1.multiply(2, 3))
print(calk1.summ(2, 3))
print(calk1.divide(2, 2))
print(calk1.sub(2, 3))