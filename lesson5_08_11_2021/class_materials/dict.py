students = {"Student1": "name1", "Student2": "name2"}
print(students["Student1"])

students["Student1"] = "new_name_1"
print(students["Student1"])

# print(students["Student3"])

new_dict = students.copy()
print(new_dict)

print(new_dict.get("Students", None))

print(new_dict.items())
print(new_dict.keys())

students.update({"Student3": "name3", "Student4": "name4"})
print(students)