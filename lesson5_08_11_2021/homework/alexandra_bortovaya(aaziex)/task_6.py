"""
6**
Создайте программу, которая, принимая массив имён, возвращает строку описывающая количество лайков (как в Facebook).

Примеры:
likes() -> "no one likes this"
likes("Ann") -> "Ann likes this"
likes("Ann", "Alex") -> "Ann and Alex like this"
likes("Ann", "Alex", "Mark") -> "Ann, Alex and Mark like this"
likes("Ann", "Alex", "Mark", "Max") -> "Ann, Alex and 2 others like this"

Подсказка: *args
"""


def likes(*argv):
    if not argv:
        print("No one likes this")
    else:
        like_list = []

        for arg in argv:
            like_list.append(arg)

        if len(like_list) == 1:
            print(f"{like_list[0]} likes this")
        elif len(like_list) == 2:
            print(f"{like_list[0]} and {like_list[1]} like this")
        elif len(like_list) == 3:
            print(f"{like_list[0]}, {like_list[1]} and {like_list[2]} like this")
        else:
            print(f"{like_list[0]}, {like_list[1]} and {len(like_list) - 2} others like this")


likes()
likes("Ann")
likes("Ann", "Alex")
likes("Ann", "Alex", "Mark")
likes("Ann", "Alex", "Mark", "Max")
likes("Ann", "Alex", "Mark", "Max", "Sasha", "Jane", "Tom", "Chris")

