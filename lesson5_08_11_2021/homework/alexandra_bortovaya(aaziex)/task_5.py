"""
5*: Шифр цезаря
Шифр Цезаря — один из древнейших шифров.
При шифровании каждый символ заменяется другим,
отстоящим от него в алфавите на фиксированное число позиций.

см картинку шифр цезаря

Примеры:
hello world! -> khoor zruog!
this is a test string -> ymnx nx f yjxy xywnsl

Напишите две функции - encode и decode принимающие как параметр строку и число - сдвиг.
"""


def define_encode_alphabet(shift):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    encode_alphabet = {}
    index = 0

    while True:
        if index < len(alphabet) - shift:
            value = alphabet[index + shift]
        elif index < len(alphabet):
            value = alphabet[abs(len(alphabet) - (index + shift))]
        else:
            break

        encode_alphabet.update({alphabet[index]: value})
        index += 1

    return encode_alphabet


def define_decode_alphabet(shift):
    encode_alphabet = define_encode_alphabet(shift)
    decode_alphabet = {}

    for key in encode_alphabet:
        decode_alphabet.update({encode_alphabet[key]: key})

    return decode_alphabet


def encode(user_input, shift):
    encode_alphabet = define_encode_alphabet(shift)
    encoded_string = list(user_input)

    for index in range(len(user_input)):
        if encoded_string[index] in encode_alphabet:
            encoded_string[index] = encode_alphabet[encoded_string[index]]

    print("".join(encoded_string))


def decode(user_input, shift):
    decode_alphabet = define_decode_alphabet(shift)
    decoded_string = list(user_input)

    for index in range(len(user_input)):
        if decoded_string[index] in decode_alphabet:
            decoded_string[index] = decode_alphabet[decoded_string[index]]

    print("".join(decoded_string))


encode("123 this is a test string!", 5)
decode("123 ymnx nx f yjxy xywnsl!", 5)
