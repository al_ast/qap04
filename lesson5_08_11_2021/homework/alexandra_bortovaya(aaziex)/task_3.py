"""
3. Напишите функцию, которая принимает на вход одномерный массив и два числа
- размеры выходной матрицы. На выход программа должна подавать матрицу
нужного размера, сконструированную из элементов массива.
   reshape([1, 2, 3, 4, 5, 6], 2, 3) =>
   [
       [1, 2, 3],
       [4, 5, 6]
   ]
   reshape([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2) =>
   [
       [1, 2],
       [3, 4],
       [5, 6],
       [7, 8]
   ]
"""


def reshape(user_input, lines, columns):
    matrix = []
    while True:
        matrix_element = user_input[:columns]
        matrix.append(matrix_element)
        for element in matrix_element:
            user_input.remove(element)
        if len(user_input) == 0 or len(matrix) == lines:
            break
    print(matrix)


reshape([1, 2, 3, 4, 5, 6, 7, 8, 9], 4, 2)
