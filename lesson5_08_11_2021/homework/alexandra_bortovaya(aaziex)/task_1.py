"""
1. Создайте свой словарь

Напечатайте:
все ключи
все значения
"""

color_codes = {
    "IndianRed": "#CD5C5C",
    "LightCoral": "#F08080",
    "Salmon": "#FA8072",
    "DarkSalmon": "#E9967A",
    "LightSalmon": "#FFA07A"
}

print(color_codes.keys())
print(color_codes.values())
