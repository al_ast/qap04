"""
4. Выполнить задание 3 из домашнего задания к уроку 4 используя break/continue
С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение
"""

task = "3+4-7"
answer = str(eval(task))
max_tries = 5
current_try = 0

print(f"Решите задачу: {task}=?")
user_answer = input("Ваш ответ: ")

while True:
    current_try += 1
    if user_answer == answer:
        print("Ответ правильный")
        break
    elif user_answer != str(answer) and current_try < max_tries:
        print("Неверное решение")
        user_answer = input(f"Осталось попыток: {max_tries - current_try}. Попробуйте еще раз: ")
    else:
        print("Неверное решение. У вас закончились попытки")
        break
