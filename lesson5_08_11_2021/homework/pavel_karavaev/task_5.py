acceptable_characters = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789.,?!'
s = input("text:").strip()
n = str(input("number of indent:"))
res = ''
for c in s:
    res += acceptable_characters[(acceptable_characters.index(c) + int(n)) % len(acceptable_characters)]
print('caesar cipher: "' + res + '"')
