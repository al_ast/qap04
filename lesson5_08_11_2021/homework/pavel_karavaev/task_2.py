dictionary_1 = {"a": 1, "b": 2, "c": 3}
dictionary_2 = {"c": 3, "d": 4, "e": 5}
common_dictionary = {}
general_keys = list(dictionary_1.keys()) + list(dictionary_2.keys())
for value in general_keys:
    common_dictionary[value] = [dictionary_1.get(value),dictionary_2.get(value)]
print(common_dictionary)
