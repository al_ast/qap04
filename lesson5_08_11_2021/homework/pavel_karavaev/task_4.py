invalid_attempts = 0
attempts = 3
while True:
    print(f"you have:{attempts} attempts")
    name = input("solve: 3+4-7=")
    if invalid_attempts >=2 and name != str(3+4-7):
        print("attempts ended. exit")
        break
    if name != str(3+4-7):
        print("try again")
        invalid_attempts += 1
        attempts -= 1
        continue
    elif name == str(3+4-7):
        print("good job")
        print("exit")
        break
