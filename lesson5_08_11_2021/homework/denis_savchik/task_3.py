# 3. Напишите функцию, которая принимает на вход одномерный массив и два числа
# - размеры выходной матрицы. На выход программа должна подавать матрицу
# нужного размера, сконструированную из элементов массива.

def reshape(massive, column, line):
    reshaped_massive = []
    for current in range(line):
        reshaped_massive.append(massive[:column])
        del massive[:column]
    return reshaped_massive

print(reshape([1, 2, 3, 4, 5, 6, 7, 8], 2, 4))
