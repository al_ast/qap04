# 4. Выполнить задание 3 из домашнего задания к уроку 4 используя break/continue
# С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
# Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение

# print("Решите пример: 3+4-7:")
# correct_answer = 0
# while True:
#     input_number = int(input())
#     if input_number != correct_answer:
#         print("Неверное решение")
#     else:
#         break
# print("Верно!")

# задача с количеством попыток

number_of_try = 3
current_try = 0
correct_answer = 0

print(f"У вас {number_of_try} попытки \nРешите пример: 3+4-7:")

while True:
    input_number = int(input())
    if input_number == correct_answer:
        print("Верно!")
        break
    else:
        print("Неверное решение")
    current_try += 1
    if current_try == number_of_try:
        print("У вас закончились попытки")
        break
