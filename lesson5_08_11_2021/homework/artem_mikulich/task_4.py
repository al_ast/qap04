# Выполнить задание 3 из домашнего задания к уроку 4 используя break/continue
# С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
# Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение
task = 3 + 4 - 7
answer = 0
max_attempts = 3
attempts = 0
print(f"please solve the equation: {task}")
answer_user = int(input())
while True:
    attempts -= 1
    if answer_user == answer:
        print(f"Answer is true: {answer}")
        break
    elif answer_user != answer and attempts < max_attempts:
        print("your answer is incorrect")
        answer_user = int(input("Enter again"))
    else:
        print("your answer is incorrect, try one more")
        break





