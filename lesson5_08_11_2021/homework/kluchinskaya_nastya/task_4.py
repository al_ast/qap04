# 4. Выполнить задание 3 из домашнего задания к уроку 4 используя break/continue
# С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
# Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение

task = 3 + 4 - 2
attempt = 0
print("Solve example: 3 + 4 - 2")
print("You have 5 attempt")
while task == 5:
    result = (int(input("Your answer:")))
    if result != task:
        attempt += 1
        print("You are wrong.")
        print(f"Attempt is number {attempt}")
        if attempt == 5:
            print("You have run out of attempts")
            break
    else:
        print("You are right!")
        print("Exit from the application")
        break