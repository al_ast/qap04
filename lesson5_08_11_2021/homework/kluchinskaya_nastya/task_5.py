# 5*:
# Шифр цезаря
# Шифр Цезаря — один из древнейших шифров.
# При шифровании каждый символ заменяется другим,
# отстоящим от него в алфавите на фиксированное число позиций.
#
# см картинку шифр цезаря
#
# Примеры:
# hello world! -> khoor zruog!
# this is a test string -> ymnx nx f yjxy xywnsl
#
# Напишите две функции - encode и decode принимающие как параметр строку и число - сдвиг.

alphabet = 'abcdefghijklmnopqrstuvwxyz'
list_alpha = [i for i in alphabet]


def encode(string, number):
    result = []
    for i in string:
        if i.isalpha():
            pos = list_alpha.index(i)
            result.append(list_alpha[pos + number])
            continue
        result.append(i)
    return ''.join(result)
def decode(string, number):
    result = []
    for i in string:
        if i.isalpha():
            pos = list_alpha.index(i)
            result.append(list_alpha[pos - number])
            continue
        result.append(i)
    return ''.join(result)
print(encode('hello world!', 3))
print(decode('khoor zruog!', 3))

print(encode('this is a test string', 5))
print(decode('ymnx nx f yjxy xywnsl', 5))