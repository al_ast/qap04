import numpy as np
array = np.arange(6)
print("Original array : \n", array)
modified_array = np.reshape(array, (2, 3), order='C')
print("\n Modified array : \n", modified_array)
second_array = np.arange(8)
print("Original array : \n", second_array)
modified_second_array = np.reshape(second_array, (4, 2), order='C')
print("\n Modified array : \n", modified_second_array)