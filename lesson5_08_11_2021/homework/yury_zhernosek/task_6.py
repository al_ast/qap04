list_name = ['Alex', 'Nikita', 'Yura', 'Andrei', 'Dima']
def likes(*args):
    if len(args) == 0:
        print('No one likes this!')
    elif len(args) == 1:
        print(f'{args[0]} likes this!')
    elif len(args) == 2:
        print(f'{args[0]} and {args[1]} like this!')
    elif len(args) == 3:
        print(f'{args[0]}, {args[1]} and {args[2]} like this!')
    else:
        print(f'{args[0]}, {args[1]} and {len(args)-2} others like this!')
likes('Alex', 'Nikita', 'Yura')