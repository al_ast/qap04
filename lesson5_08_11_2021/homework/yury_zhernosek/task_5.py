import string
alphabet = string.ascii_lowercase
list_alphabet = [i for i in alphabet]

def encode(string, number):
    result = []
    for letter in string:
        if letter.isalpha():
            position = list_alphabet.index(letter)
            result.append(list_alphabet[position + number])
            continue
        result.append(letter)

    return ''.join(result)


def decode(string, number):
    result = []
    for letter in string:
        if letter.isalpha():
            position = list_alphabet.index(letter)
            result.append(list_alphabet[position - number])
            continue
        result.append(letter)

    return ''.join(result)


print(encode('hello world!', 3))
print(decode('khoor zruog!', 3))

print(encode('this theme is hard', 5))
print(decode('ymnx ymjrj nx mfwi', 5))