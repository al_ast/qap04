attempt = 0
number = 22*3
print(f'\nResolve task: 22*3=?\nValue attempt: 3')
while attempt < 3:
    user_number = int(input('Answer: '))
    if user_number == number:
        print('You win!')
        break
    else:
        print('Not right.')
    attempt += 1
else:
    print('Attempts ended.')