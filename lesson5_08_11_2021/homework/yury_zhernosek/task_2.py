first_wordbook = {'a': 1, 'b': 2, 'c': 3}
second_wordbook = {'c': 3, 'd': 4, 'e': 5}
third_wordbook = {}
all_keys = list(first_wordbook.keys()) + list(second_wordbook.keys())
for key in all_keys:
    third_wordbook[key] = [first_wordbook.get(key),second_wordbook.get(key)]
print(third_wordbook)