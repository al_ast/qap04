def reshape(array, num_rows, num_colloms):
    result = []

    for row in range(num_rows):
        result.append(array[:num_colloms])
        del array[:num_colloms]

    return result


print(reshape([1, 2, 3, 4, 5, 6], 2, 3))
print(reshape([1, 2, 3, 4, 5, 6, 7, 8, ], 4, 2))