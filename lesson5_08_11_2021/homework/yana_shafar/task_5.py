alpha = 'abcdefghijklmnopqrstuvwxyz'
list_alpha = [i for i in alpha]


def encode(string, number):
    result = []
    for i in string:
        if i.isalpha():
            pos = list_alpha.index(i)
            result.append(list_alpha[pos + number])
            continue
        result.append(i)

    return ''.join(result)


def decode(string, number):
    result = []
    for i in string:
        if i.isalpha():
            pos = list_alpha.index(i)
            result.append(list_alpha[pos - number])
            continue
        result.append(i)

    return ''.join(result)


print(encode('hello world!', 3))
print(decode('khoor zruog!', 3))

print(encode('this is a test string', 5))
print(decode('ymnx nx f yjxy xywnsl', 5))
