task = "3+4-7"
tries = 5
current_try = 0
answer = str(eval(task))


print(f"Решите задачу: {task}=")
user_answer = input("Введите число: ")

while True:
    current_try += 1
    if user_answer == answer:
        print("Верно")
        break
    elif user_answer != str(answer) and current_try < tries:
        print("Неверно")
        user_answer = input(f"Осталось попыток: {tries - current_try}. Попробуйте еще раз: ")
    else:
        print("Неверное решение. У вас закончились попытки")
        break