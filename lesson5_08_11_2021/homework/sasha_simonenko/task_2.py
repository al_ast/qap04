a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
new = {}
for key in a:
    new[key] = [a.get(key), b.get(key)]
for key in b:
    if key not in new.keys():
        new[key] = [a.get(key), b.get(key)]
print(new)