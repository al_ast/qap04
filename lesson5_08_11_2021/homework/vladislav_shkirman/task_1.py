"""
1. Создайте свой словарь
Напечатайте:
все ключи
все значения
"""

my_dictionary = {"a": 1, "b": 2, "c": 3, "d": 4}
print(f'Keys: {list(my_dictionary.keys())}')
print(f'Values: {list(my_dictionary.values())}')