"""
4. Выполнить задание 3 из домашнего задания к уроку 4 используя break/continue
С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение
"""

expression = "3 + 4 - 5"
expected_answer = str(eval(expression))
tries = 0
max_tries = 4
print(f'Solve example: 3 + 4 - 5 = ?\nTotal attempts: {max_tries}\n')
while True:
    user_input = input(f'Attempt is number {tries+1}\nYour answer: ')
    if user_input != expected_answer:
        print('Wrong answer')
    else:
        print('Correct!\nExit from the application')
        break
    tries += 1
    if tries == max_tries:
        print("You've ran out of your attempts")
        break
