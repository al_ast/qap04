"""5*:
Шифр цезаря
Шифр Цезаря — один из древнейших шифров.
При шифровании каждый символ заменяется другим,
отстоящим от него в алфавите на фиксированное число позиций.

см картинку шифр цезаря

Примеры:
hello world! -> khoor zruog!
this is a test string -> ymnx nx f yjxy xywnsl

Напишите две функции - encode и decode принимающие как параметр строку и число - сдвиг.
"""

import string

alphabet = string.ascii_lowercase


def encode(text, shift):
    shifted_alphabet = alphabet[shift:] + alphabet[:shift]
    return get_result(alphabet, shifted_alphabet, text)


def decode(text, shift):
    shifted_alphabet = alphabet[shift:] + alphabet[:shift]
    return get_result(shifted_alphabet, alphabet, text)


def get_result(base, base_shifted, text):
    result = ''
    for letter in text:
        if letter in base:
            result += base_shifted[base.index(letter)]
        else:
            result += letter
    return result


print(encode('hello world!xyz', 2))
print(decode('jgnnq yqtnf!cde', 2))
