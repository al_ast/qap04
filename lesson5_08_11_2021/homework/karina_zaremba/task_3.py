# Напишите функцию, которая принимает на вход одномерный массив и два числа
# - размеры выходной матрицы. На выход программа должна подавать матрицу
# нужного размера, сконструированную из элементов массива.
#    reshape([1, 2, 3, 4, 5, 6], 2, 3) =>
#    [
#        [1, 2, 3],
#        [4, 5, 6]
#    ]
#    reshape([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2) =>
#    [
#        [1, 2],
#        [3, 4],
#        [5, 6],
#        [7, 8]
#    ]
# ([1, 2, 3, 4, 5, 6], 2(rows), 3(col))
def reshape(array, rows, columns):
    result = []

    for row in range(rows):
        result.append(array[:columns])
        del array[:columns]

    return result


print(reshape([1, 2, 3, 4, 5, 6], 3, 2))
print(reshape([1, 2, 3, 4, 5, 6, 7, 8, 9], 3, 3))


# Функция reshape() изменяет форму массива без изменения его данных.
