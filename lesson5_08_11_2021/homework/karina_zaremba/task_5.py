# Шифр цезаря
# Шифр Цезаря — один из древнейших шифров.
# При шифровании каждый символ заменяется другим,
# отстоящим от него в алфавите на фиксированное число позиций.
#
# см картинку шифр цезаря
#
# Примеры:
# hello world! -> khoor zruog!
# this is a test string -> ymnx nx f yjxy xywnsl
#
# Напишите две функции - encode и decode принимающие как параметр строку и число - сдвиг.

english_alphabet = 'abcdefghijklmnopqrstuvwxyz'

before_encode = int(input("Enter your encode number: "))          #номер, по кот будет сдвигаться текст
string_for_encode = input("Your encode message: ")            #текст до шифра
encoded_message = " "            #контейнер для итога
for letter in string_for_encode:      #преобразовываем ввод значение(норм)
    place = english_alphabet.find(letter)   #поиск буквы в алфавите -> найд-е сдвигаем на к-во номера энкода
    other_place = place + before_encode
    if letter in english_alphabet:
        encoded_message += english_alphabet[other_place]  #если в алф найд буква -> отправ в контейнер вывода
    else:
        encoded_message += letter  #если нет, то оставляем знач до шифра
print(encoded_message)

#Decode
before_decode = int(input("Enter your decode number: "))
string_for_decode = input("Your decode message: ")
decoded_message = " "
for letter in string_for_decode:
    place=english_alphabet.find(letter)
    other_place = place - before_decode
    if letter in english_alphabet:
        decoded_message += english_alphabet[other_place]
    else:
        decoded_message += letter
print(decoded_message)



# mixing = int(input('Decode step:'))
# string_for_decode = input('Input string for decode:')
# decoded_message = ''
# for element in string_for_decode:
#     place = alphabet.find(element)
#     new_place = place - mixing
#     if element in alphabet:
#         decoded_message += alphabet[new_place]
#     else:
#         decoded_message += element
# print(decoded_message)



