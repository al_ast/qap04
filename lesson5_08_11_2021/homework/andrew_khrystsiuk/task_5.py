# 5*:
# Шифр цезаря
# Шифр Цезаря — один из древнейших шифров.
# При шифровании каждый символ заменяется другим,
# отстоящим от него в алфавите на фиксированное число позиций.
# Напишите две функции - encode и decode принимающие как параметр строку и число - сдвиг.

alphabet = 'abcdefghijklmnopqrstuvwxyz'

# Encode

mixing = int(input('Encode step:'))
string_for_encode = input('Input string for encode:')
encoded_message = ''
for element in string_for_encode:
    place = alphabet.find(element)
    new_place = place + mixing
    if element in alphabet:
        encoded_message += alphabet[new_place]
    else:
        encoded_message += element
print(encoded_message)

# Decode

mixing = int(input('Decode step:'))
string_for_decode = input('Input string for decode:')
decoded_message = ''
for element in string_for_decode:
    place = alphabet.find(element)
    new_place = place - mixing
    if element in alphabet:
        decoded_message += alphabet[new_place]
    else:
        decoded_message += element
print(decoded_message)
