# 3. Напишите функцию, которая принимает на вход одномерный массив и два числа
# - размеры выходной матрицы. На выход программа должна подавать матрицу
# нужного размера, сконструированную из элементов массива.


def reshape(array, rows, columns):
    result = []

    for row in range(rows):
        result.append(array[:columns])
        del array[:columns]

    return result


print(reshape([1, 2, 3, 4, 5, 6], 3, 2))
print(reshape([1, 2, 3, 4, 5, 6, 7, 8, 9], 3, 3))

