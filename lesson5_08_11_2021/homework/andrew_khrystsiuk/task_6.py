# 6**
# Создайте программу, которая, принимая массив имён, возвращает строку описывающая количество лайков (как в Facebook).

def likes(*args):

    if not args:
        return "no one likes this"

    if len(args) == 1:
        return args[0] + " likes this"

    elif len(args) == 2:
        return args[0] + " and " + args[1] + " like this"

    elif len(args) == 3:
        return args[0] + ", " + args[1] + " and " + args[2] + " like this"

    elif len(args) > 3:
        return args[0] + ", " + args[1] + " and " + str(len(args) - 2) + " others like this"


print(likes('Ann', 'Andrew', 'Ellie', 'John'))
