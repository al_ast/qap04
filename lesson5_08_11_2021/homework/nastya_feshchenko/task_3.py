# 3. Напишите функцию, которая принимает на вход одномерный массив и два числа
# - размеры выходной матрицы. На выход программа должна подавать матрицу
# нужного размера, сконструированную из элементов массива.
#    reshape([1, 2, 3, 4, 5, 6], 2, 3) =>
#    [
#        [1, 2, 3],
#        [4, 5, 6]
#    ]
#    reshape([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2) =>
#    [
#        [1, 2],
#        [3, 4],
#        [5, 6],
#        [7, 8]
#    ]


def reshape(array, num_rows, num_colloms):
    result = []
    for row in range(num_rows):
        result.append(array[:num_colloms])
        del array[:num_colloms]
    return result
print(reshape([1, 2, 3, 4, 5, 6], 2, 3))
print(reshape([1, 2, 3, 4, 5, 6, 7, 8, ], 4, 2))


