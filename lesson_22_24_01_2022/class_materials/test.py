import mysql.connector
import pytest

# connection = mysql.connector.connect(host='localhost',
#                                      user='nastya_qap04',
#                                      password='Password12345!')
# print(connection)

import mysql.connector as mysql

# cursor = connection.cursor()
# cursor.execute("CREATE DATABASE test_db")


# Создать базу данных
# db = mysql.connect(
#     host="localhost",
#     user="root",
#     passwd="qwerty@123"
# )
#
# cursor = db.cursor()
# cursor.execute("CREATE DATABASE test_db")


# Создать таблицу
# db = mysql.connect(
#     host="localhost",
#     user="nastya_qap04",
#     passwd="Password12345!",
#     database="test_db")

# cursor = db.cursor()
# cursor.execute("CREATE TABLE users (name VARCHAR(255), user_name VARCHAR(255))")


# # Список баз банных
# cursor.execute("SHOW DATABASES")
# print(cursor.fetchall())
#
#
# # Список таблиц
# cursor.execute("SHOW TABLES")
# print(cursor.fetchall())


# Вставка в таблицу
# cursor = db.cursor()
# query = "INSERT INTO users (name, user_name) VALUES (%s, %s)"
# values = ("hello", "hello1")
# cursor.execute(query, values)
# db.commit()
# print(cursor.rowcount, "record inserted")
#
#
# # Вывести таблицу
# cursor.execute('SELECT * FROM users')
# print(cursor.fetchall())


###########################################
# Таблица из урока 21
# Create TABLE UserInfo(
#   UserId INT NULL,
#   FirstName Varchar(255),
#   LastName Varchar(255),
#   Age Int)

# Insert INTO UserInfo(UserId, FirstName, LastName, Age)
# VALUES (1, 'Dima', 'Dmitriev', 25),
# (2, 'Vasya', 'Vasiliev', 12),
# (3, 'Petya', 'Petrov', 37)

# cursor.execute("""CREATE TABLE UserInfo (
# UserId INT NULL, FirstName VARCHAR(255), LastName VARCHAR(255),
# Age Int)""")



# cursor.execute("""Insert INTO UserInfo(UserId, FirstName, LastName, Age)
# VALUES (1, 'Dima', 'Dmitriev', 25),
# (2, 'Vasya', 'Vasiliev', 12),
# (3, 'Petya', 'Petrov', 37)""")
# db.commit()
# print(cursor.rowcount, "record inserted")
#
# cursor.execute('SELECT * FROM UserInfo')
# print(cursor.fetchall())

class SqlService:
    def __init__(self, connection):
        self.connection = connection

    def execute_sql_query(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        return data




@pytest.fixture(scope="session")
def setup_connection():
    global connection
    global sql_service
    connection = connect(
        host="localhost",
        user="nastya_qap04",
        passwd="Password12345!",
        database="test_db"
    )
    sql_service = SqlService(connection)
    yield
    connection.close()


class TestUserInformation:
    def test_user_information(self):
        data = sql_service.execute_sql_query(connection, "SELECT * FROM UserInfo")

        assert len(data) > 0, "User Information should not be empty"

    def test_user_information2(self):
        data = sql_service.execute_sql_query(connection, "SELECT * FROM Credentials")

        assert len(data) > 0, "Credentials should not be empty"



