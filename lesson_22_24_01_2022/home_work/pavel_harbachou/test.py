# Установить MySql
# Создать такие же таблицы(с такими же данными) как и в уроке 20,
# только в своей локальной базею. Либо можете придумать свои таблицы
#
# Переписать запросы из урока 20, чтобы получились тесты
# Либо написать новые запросы и сделать из них тесты
#
# Минимальное кол-во тестов 5
import mysql.connector
import pytest
from lesson_22_24_01_2022.home_work.pavel_harbachou.my_sql_service import MySQLService


@pytest.fixture(scope='session')
def setup_my_sql_connection():
    global my_sql_service
    connection = mysql.connector.connect(
        host="localhost",
        user="pavel_04",
        password="Password",
        database='my_database'
    )
    my_sql_service = MySQLService(connection)


@pytest.mark.usefixtures('setup_my_sql_connection')
class TestMySQL:
    def test_is_credentials_empty(self):
        credentials = my_sql_service.sql_query('SELECT * FROM Credentials')
        assert len(credentials) > 0, 'Table Credentials is empty'

    def test_is_user_information_empty(self):
        user_information = my_sql_service.sql_query('SELECT * FROM UserInformation')
        assert len(user_information) > 0, 'Table UserInformation is empty'

    def test_endpoint_of_emails(self):
        list_of_emails = my_sql_service.sql_query('SELECT UserEmail FROM Credentials')
        emails_without_endpoint_gmail = []
        for email in list_of_emails:
            if '@gmail.com' not in email[0]:
                emails_without_endpoint_gmail.append(email)
        assert len(emails_without_endpoint_gmail) == 0, "One or more emails don't have endpoint gmail.com"

    def test_are_there_any_adults_users_in_database(self):
        age_information = my_sql_service.sql_query('SELECT Age FROM UserInformation')
        adults_users = []
        for age in age_information:
            if age[0] >= 18:
                adults_users.append(age)
        assert len(adults_users) > 0, 'No one adult user'

    def test_are_there_any_minors_users_in_database(self):
        age_information = my_sql_service.sql_query('SELECT Age FROM UserInformation')
        minor_users = []
        for age in age_information:
            if age[0] <= 18:
                minor_users.append(age)
        assert len(minor_users) > 0, 'No one minor user'
