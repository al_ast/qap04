import mysql.connector as mysql
import pytest


@pytest.fixture(scope='session')
def setup_connection():
    global db
    global cursor
    db = mysql.connect(
        host="localhost",
        user="root",
        passwd="W_j7qH~a,3c9PB5",
        database="test_db"
    )
    cursor = db.cursor()
    yield
    db.close()


class TestDataBase:
    @pytest.mark.usefixtures('setup_connection')
    def test_users_under_18(self):
        cursor.execute("SELECT LastName FROM UserInfo WHERE UserInfo.Age < 18")
        data = cursor.fetchall()
        assert len(data) == 0, f"There are no users younger than 18"

    def test_users_id(self):
        cursor.execute("SELECT UserId FROM Credentials")
        data = cursor.fetchall()
        assert len(data) != 0, "UserId should not be empty"

    def test_select_all_passwords(self):
        cursor.execute("SELECT Password FROM Credentials")
        data = cursor.fetchall()
        assert len(data) != 0, "Password list must not be empty"

    def test_users_email(self):
        cursor.execute("SELECT Credentials.UserEmail FROM Credentials;")
        data = cursor.fetchall()
        assert len(data) > 0, "User Email should not be empty"

    def test_user_credentials(self):
        cursor.execute("SELECT * from Credentials")
        data = cursor.fetchall()
        assert len(data) != 0, "User credentials should not be empty"








