class TestDb:
    def test_no_empty_passwords_exist(self, sql_service):
        query = """
                SELECT UserEmail
                FROM Credentials
                WHERE Password IS null
                """
        result = sql_service.execute_sql_query(query)

        assert len(result) == 0, f"All users must have password, users without password: {result}"

    def test_users_older_than_18(self, sql_service):
        query = """
                SELECT FirstName, LastName
                FROM UserInfo
                WHERE Age < 18
                """
        result = sql_service.execute_sql_query(query)

        assert len(result) == 0, f"All users must be older than 18, users younger than 18: {result}"

    def test_personal_info_exist(self, sql_service):
        query = """
                SELECT c.UserEmail FROM Credentials c
                LEFT JOIN UserInfo u ON u.UserId = c.UserId
                WHERE u.UserId IS null 
                """
        result = sql_service.execute_sql_query(query)

        assert len(result) == 0, f"All users must have personal data, users without personal data: {result}"

    def test_user_information(self, sql_service):
        query = "SELECT * from UserInfo"
        result = sql_service.execute_sql_query(query)

        assert len(result) != 0, "User information should not be empty"

    def test_all_emails_from_corporate_domain(self, sql_service):
        query = """
                SELECT u.FirstName, u.LastName, c.UserEmail FROM UserInfo u
                JOIN Credentials c ON u.UserId = c.UserId
                WHERE c.UserEmail NOT LIKE '%@gmail.com'
                """
        result = sql_service.execute_sql_query(query)

        assert len(result) == 0, f"All users must have gmail mail domain, users with invalid domain: {result}"

    def test_passwords_length_more_than_3(self, sql_service):
        query = """
                SELECT u.FirstName, u.LastName, c.Password
                FROM Credentials c
                JOIN UserInfo u on u.UserId = c.UserId
                """
        result = sql_service.execute_sql_query(query)
        is_passwords_length_valid = True
        users_with_short_passwords = []
        for item in result:
            if len(item[2]) <= 3:
                is_passwords_length_valid = False
                users_with_short_passwords.append(f"{item[0]} {item[1]}")

        assert is_passwords_length_valid, f"Password for the following users must be more than 3 symbols" \
                                          f" length: {users_with_short_passwords}"
