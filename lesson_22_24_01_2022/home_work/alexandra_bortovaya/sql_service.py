class SqlService:
    def __init__(self, connection):
        self.connection = connection

    def execute_sql_query(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        return cursor.fetchall()
