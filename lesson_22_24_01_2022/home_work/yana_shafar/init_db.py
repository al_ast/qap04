import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="password",
    database="test_db"
)

cursor = db.cursor()

cursor.execute("CREATE TABLE Credentials (UserId INT NULL,UserEmail Varchar(255),Password Varchar(255))")

cursor.execute("""CREATE TABLE UserInfo(
UserId INT NULL, FirstName Varchar(255), LastName Varchar(255),
Age Int)""")

cursor.execute("""Insert INTO Credentials(UserId, UserEmail, Password)
VALUES (1, 'Dima@gmail.com', 'password1'),
(2, 'Vasya@gmail.com', 'password2'),
(4, 'Petya@gmail.com', 'password3')""")
db.commit()
print(cursor.rowcount, "record inserted")

cursor.execute("""Insert INTO UserInfo(UserId, FirstName, LastName, Age)
VALUES (1, 'Dima', 'Dmitriev', 25),
(2, 'Vasya', 'Vasiliev', 12),
(5, 'Petya', 'Petrov', 37)""")
db.commit()
print(cursor.rowcount, "record inserted")





