import pytest
from mysql.connector import connect

from lesson_22_24_01_2022.home_work.yana_shafar.sql_service import SqlService


@pytest.fixture()
def setup_connection():
    global connection
    global sql_service
    connection = connect(
        host="localhost",
        user="root",
        passwd="password",
        database="test_db"
    )
    sql_service = SqlService(connection)
    yield
    connection.close()


@pytest.mark.usefixtures('setup_connection')
class TestDataBase:
    def test_user_information(self):
        data = sql_service.execute_sql_query('SELECT * FROM UserInfo')

        assert len(data) > 0, "User Information shouldn't be empty"

    def test_user_credentials(self):
        data = sql_service.execute_sql_query('SELECT * FROM Credentials')

        assert len(data) > 0, "Credentials shouldn't be empty"

    def test_user_compliance(self):
        user_info_count = sql_service.execute_sql_query('select COUNT(UserId) from userinfo')[0][0]
        common_count = sql_service.execute_sql_query('''
            select
                   COUNT(c.UserId)
            from userinfo u join credentials c on u.UserId = c.UserId;
        ''')[0][0]

        assert user_info_count == common_count, "Rows in UserInfo should be equals rows in Credentials"

    def test_invalid_email(self):
        invalid_email_count = sql_service.execute_sql_query('''
            select
                COUNT(UserEmail)
            from credentials
            where UserEmail NOT LIKE '%@%';
        ''')[0][0]

        assert invalid_email_count == 0, \
            f"Invalid email count: {invalid_email_count}. All rows must be filled"

    def test_empty_surnames(self):
        empty_surname_count = sql_service.execute_sql_query('''
        select
            COUNT(LastName)
        from userinfo
        where LastName is null;
        ''')[0][0]

        assert empty_surname_count == 0, \
            f"Empty surname count: {empty_surname_count}. All rows must be filled"

    def test_empty_user_email(self):
        empty_user_email_count = sql_service.execute_sql_query('''
        select 
            COUNT(*)
        from userinfo u left join credentials c on u.UserId = c.UserId
        where c.UserEmail is null
        ''')[0][0]

        assert empty_user_email_count == 0, "All user emails should be filled"


