class SqlService:
    def execute_sql_service(self, connection, query):
        cursor = connection.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        return data
