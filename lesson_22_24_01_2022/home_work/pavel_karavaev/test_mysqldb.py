import pytest
from mysql.connector import connect
from sql_service import SqlService


@pytest.fixture()
def setup():
    global connection
    global sql_service
    connection = connect(
        host="localhost",
        user="root",
        password="FPWLove12345",
        database="pavel_karavaev_db"
    )
    sql_service = SqlService()
    yield
    connection.close()


@pytest.mark.usefixtures("setup")
class TestDataBase:
    def test_user_information(self):
        data = sql_service.execute_sql_service(connection, "SELECT * FROM UserInfo;")
        assert len(data) > 0, "UserInfo should not be empty"

    def test_user_credentials(self):
        data = sql_service.execute_sql_service(connection, "SELECT * From Credentials;")
        assert len(data) > 0, "Credential should not be empty"

    def test_empty_user_email(self):
        data = sql_service.execute_sql_service(connection, "SELECT Credentials.UserEmail FROM Credentials;")
        assert len(data) > 0, "User Email should not be empty"

    def test_age_count(self):
        data = sql_service.execute_sql_service(connection, "SELECT COUNT(Age) FROM UserInfo "
                                                           "WHERE Age is null;")[0][0]
        assert data == 0, "not  all rows in UserInfo.Age filled"

    def test_invalid_email(self):
        data = sql_service.execute_sql_service(connection, "SELECT COUNT(UserEmail) From Credentials "
                                                           "WHERE UserEmail NOT LIKE '%@%';")[0][0]
        assert data == 0, "not all rows in Credentials.UserEmail be valid or filled"
