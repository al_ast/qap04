class TestUserInformationDB:
    def test_get_user_info(self, sql_service):
        data = sql_service.execute_query("SELECT * FROM UserInfo")
        assert len(data) > 0, "No user information found"

    def test_get_users_with_passwords(self, sql_service):
        data = sql_service.execute_query("SELECT Credentials.UserEmail "
                                         "FROM Credentials WHERE Credentials.Password is NULL")
        assert len(data) == 0, f"There is a list of users {data} without password in database"

    def test_get_user_with_different_age(self, sql_service):
        data = sql_service.execute_query("SELECT DISTINCT Age FROM UserInfo")
        assert len(data) >= 2, f"All users in database has one age"

    def test_get_users_with_email_like(self, sql_service, email='gmail'):
        data = sql_service.execute_query("SELECT UserInfo.FirstName, Credentials.UserEmail "
                                         "FROM UserInfo JOIN Credentials "
                                         "ON Credentials.UserId = UserInfo.UserId "
                                         f"WHERE Credentials.UserEmail like '%{email}%'")
        assert len(data) > 0, f"There are no users with email like '{email}' found in the database"

    def test_get_users_with_age_higher_than(self, sql_service, age=21):
        data = sql_service.execute_query("SELECT UserInfo.LastName, Credentials.UserEmail "
                                         "FROM UserInfo JOIN Credentials "
                                         "ON Credentials.UserId = UserInfo.UserId "
                                         f"WHERE UserInfo.Age >= '{age}'")
        assert len(data) > 0, f"There are no users with age higher or equal to {age}"

    def test_get_users_without_age(self, sql_service):
        data = sql_service.execute_query("SELECT credentials.UserEmail "
                                         "FROM UserInfo JOIN Credentials "
                                         "ON Credentials.UserId = UserInfo.UserId "
                                         "WHERE userinfo.Age IS NULL "
                                         "ORDER BY UserInfo.LastName DESC;")
        assert len(data) == 0, f"There are users with emails {data} without the age in database"
