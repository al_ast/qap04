import pytest
from mysql.connector import connect
from sql_service import SqlService


@pytest.fixture(scope="session")
def connection_to_test_db():
    connection = connect(
        host="localhost",
        user='vlad',
        password='Qwer1234!',
        database="test_db"
    )

    yield connection
    connection.close()


@pytest.fixture()
def sql_service(connection_to_test_db):
    return SqlService(connection_to_test_db)
