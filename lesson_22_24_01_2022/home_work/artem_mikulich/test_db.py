import pytest
from mysql.connector import connect

@pytest.fixture(scope="session")
def setup_connection():
    global connection
    global cursor
    connection = connect(host="localhost", user="artem", password="pass12345", database='test_database')
    cursor = connection.cursor()
    yield
    connection.close()
@pytest.mark.usefixtures("setup_connection")
class TestDb:
    def test_user_information(self):
        cursor.execute("SELECT * FROM Credentials")
        data = cursor.fetchall()
        assert len(data) == 3, f"Expected {data} with the number of fields 3, but was {len(data)}"

    def test_user_name(self):
        cursor.execute("SELECT * FROM UserInfo")
        data = cursor.fetchall()
        data_name = data[0][1]
        assert data_name == "Dima", f"Expected name Dima but was {data_name} "

    def test_last_name(self):
        cursor.execute("""SELECT UserInfo.LastName 
        FROM UserInfo 
        JOIN Credentials 
        ON UserInfo.UserId=Credentials.UserId
        AND Credentials.Password LIKE '%1'""")
        data = cursor.fetchall()
        assert len(data) == 1, f"People last name whose password that ends with 1 {data}"

    def test_get_users_older_than_24(self):
        cursor.execute("""SELECT UserInfo.FirstName
        FROM UserInfo 
        JOIN Credentials 
        ON UserInfo.UserId=Credentials.UserId
        AND UserInfo.Age>24""")
        data = cursor.fetchall()
        assert len(data) == 2, f"This is list of users {data} with age more than 24"

    def test_get_users_with_gmail(self):
        cursor.execute("""SELECT UserInfo.FirstName 
        FROM UserInfo 
        JOIN Credentials 
        ON UserInfo.UserId=Credentials.UserId
        WHERE Credentials.UserEmail LIKE '%gmail%'""")
        data = cursor.fetchall()
        assert len(data) == 3, f"People with gmail {data}"

    def test_users_with_id_more_than_1(self):
        cursor.execute("""SELECT UserInfo.FirstName
        FROM UserInfo 
        JOIN Credentials 
        ON UserInfo.UserId=Credentials.UserId
        WHERE Credentials.UserId > 1""")
        data = cursor.fetchall()
        assert len(data) == 2, f"This is list of users {data} with user_id more than 1"

    def test_get_user_with_password_that_ends_2(self):
        cursor.execute("""SELECT UserInfo.FirstName
        FROM UserInfo 
        JOIN Credentials 
        ON UserInfo.UserId=Credentials.UserId
        WHERE Credentials.Password LIKE '%2'""")
        data = cursor.fetchall()
        assert len(data) == 1, f"Person whose password that ends with 2 {data}"