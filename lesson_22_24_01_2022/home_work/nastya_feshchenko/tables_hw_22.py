# Установить MySql
# Создать такие же таблицы(с такими же данными) как и в уроке 20,
# только в своей локальной базею. Либо можете придумать свои таблицы
#
# Переписать запросы из урока 20, чтобы получились тесты
# Либо написать новые запросы и сделать из них тесты
# Минимальное кол-во тестов 5

# !!!Не забудьте закрыть соединение с базой данных

import mysql.connector
connection = mysql.connector.connect(host='localhost',
                                     user='nastya_qap04',
                                     password='Password12345!')
print(connection)
import mysql.connector as mysql


db = mysql.connect(
    host="localhost",
    user="nastya_qap04",
    passwd="Password12345!")

cursor = db.cursor()
cursor.execute("CREATE DATABASE homework_db")

db = mysql.connect(
    host="localhost",
    user="nastya_qap04",
    passwd="Password12345!",
    database="homework_db")


cursor = db.cursor()
cursor.execute("CREATE TABLE users (name VARCHAR(255), user_name VARCHAR(255))")

cursor.execute("CREATE TABLE Credentials (UserId INT NULL,UserEmail Varchar(255),Password Varchar(255))")
cursor.execute("""CREATE TABLE UserInfo(
UserId INT NULL, FirstName Varchar(255), LastName Varchar(255),
Age Int)""")


cursor.execute("""Insert INTO Credentials(UserId, UserEmail, Password)
VALUES (1, 'Dima@gmail.com', 'password1'),
(2, 'Vasya@gmail.com', 'password2'),
(4, 'Petya@gmail.com', 'password3')""")
db.commit()
print(cursor.rowcount, "record inserted")


cursor.execute("""Insert INTO UserInfo(UserId, FirstName, LastName, Age)
VALUES (1, 'Dima', 'Dmitriev', 25),
(2, 'Vasya', 'Vasiliev', 12),
(5, 'Petya', 'Petrov', 37)""")
db.commit()
print(cursor.rowcount, "record inserted")