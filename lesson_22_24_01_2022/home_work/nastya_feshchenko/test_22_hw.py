# Установить MySql
# Создать такие же таблицы(с такими же данными) как и в уроке 20,
# только в своей локальной базею. Либо можете придумать свои таблицы
#
# Переписать запросы из урока 20, чтобы получились тесты
# Либо написать новые запросы и сделать из них тесты
# Минимальное кол-во тестов 5

# !!!Не забудьте закрыть соединение с базой данных


import pytest
import mysql.connector as mysql
from mysql.connector import connect
from sql_service_22 import SqlService


@pytest.fixture()
def setup_connection():
    global connection
    global sql_service
    connection = connect(
        host="localhost",
        user="nastya_qap04",
        passwd="Password12345!",
        database="homework_db")
    sql_service = SqlService(connection)
    yield
    connection.close()


@pytest.mark.usefixtures('setup_connection')
class TestDataBase:
    def test_user_information(self):
        data = sql_service.execute_sql_query('SELECT * FROM UserInfo')
        assert len(data) > 0, "User Information should not be empty."

    def test_user_credentials(self):
        data = sql_service.execute_sql_query('SELECT * FROM Credentials')
        assert len(data) > 0, "Credentials should not be empty."

    def test_if_emails_are_empty(self):
        data = sql_service.execute_sql_query('SELECT Credentials.UserEmail FROM Credentials')
        assert len(data) > 0, "User Email should not be empty"

    def test_if_emails_are_valid(self):
        data = sql_service.execute_sql_query("SELECT Credentials.UserEmail FROM UserInfo "
                                             "JOIN Credentials ON UserInfo.UserId=Credentials.UserId "
                                             "AND Credentials.UserEmail LIKE '%gmail%'")
        assert len(data) > 0, f"All users should have a google email address"

    def test_if_users_are_over_21yo(self):
        data = sql_service.execute_sql_query("SELECT FirstName, LastName FROM UserInfo WHERE Age < 21")
        assert len(data) == 0, f"All users must be older than 21, but we've tracked younger users."


