# Установить MySql
# Создать такие же таблицы(с такими же данными) как и в уроке 20,
# только в своей локальной базею. Либо можете придумать свои таблицы
#
# Переписать запросы из урока 20, чтобы получились тесты
# Либо написать новые запросы и сделать из них тесты
# Минимальное кол-во тестов 5

# !!!Не забудьте закрыть соединение с базой данных

class SqlService:
    def __init__(self, connection):
        self.connection = connection

    def execute_sql_query(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        return data