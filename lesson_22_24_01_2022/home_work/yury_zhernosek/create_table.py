import mysql.connector as mysql


connection = mysql.connect(
    host="localhost",
    user="root",
    passwd="W_j7qH~a,3c9PB6",
    database="test_db"
)

cursor = connection.cursor()

cursor.execute(
    "CREATE TABLE Credentials "
    "(UserId INT NULL,"
    "UserEmail Varchar (255),"
    "Password Varchar (255))"
)
cursor.execute(
    "CREATE TABLE UserInfo "
    "(UserId INT NULL,"
    "FirstName Varchar(255),"
    "LastName Varchar(255),"
    "Age Int)"
)

cursor.execute(
    "INSERT INTO Credentials(UserId, UserEmail, Password)"
    "VALUES (1, 'andrei8281@gmail.com', '1234567'),"
    "(2, 'yurazernosek@mail.ru', '1111'),"
    "(3, 'slava@gmail.com', '1010222888')"
)

cursor.execute(
    "INSERT INTO UserInfo(UserId, FirstName, LastName, Age)"
    "VALUES (1, 'Andrei', 'Petrov', 22),"
    "(2, 'Yura', 'Zhernosek', 18),"
    "(3, 'Slava', 'Sokolow', 20)"
)

connection.commit()
