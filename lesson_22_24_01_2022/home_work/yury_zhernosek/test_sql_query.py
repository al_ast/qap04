import mysql.connector as mysql
import pytest


@pytest.fixture(scope='session')
def create_connection():
    global connection
    global cursor
    connection = mysql.connect(
        host="localhost",
        user="root",
        passwd="W_j7qH~a,3c9PB6",
        database="test_db"
    )
    cursor = connection.cursor()
    yield
    connection.close()


class TestSqlQuery:
    @pytest.mark.usefixtures('create_connection')
    def test_select_all_password(self):
        cursor.execute("SELECT Password FROM Credentials")
        data = cursor.fetchall()
        assert len(data) != 0, "Password list must not be empty"

    def test_select_all_age(self):
        cursor.execute("SELECT Age FROM UserInfo")
        data = cursor.fetchall()
        assert len(data) != 0, "Age list must not be empty"

    def test_select_unique_order_age(self):
        cursor.execute("SELECT DISTINCT Age FROM UserInfo ORDER BY AGE")
        data = cursor.fetchall()
        list_ages = []
        for age in data:
            list_ages.append(age[0])
        sorted_list_ages = list_ages
        sorted_list_ages.sort()
        assert list_ages == sorted_list_ages, "The list of ages must be sorted"

    def test_select_firstname_age_and_email_where_email_contains_gmail(self):
        cursor.execute("SELECT FirstName, Age, Credentials.UserEmail FROM UserInfo "
                       "JOIN Credentials ON UserInfo.UserId=Credentials.UserId "
                       "AND Credentials.UserEmail LIKE '%gmail%'"
                       )
        data = cursor.fetchall()
        for email in data:
            assert "gmail" in email[2], "In email must contain 'gmail'"

    def test_select_lastname_and_age_where_age_over_20(self):
        cursor.execute("SELECT LastName, Age FROM UserInfo WHERE UserInfo.Age>20")
        data = cursor.fetchall()
        for age in data:
            assert age[1] > 20, "Age must be over 20"

    def test_select_email_and_password_where_name_andrei(self):
        cursor.execute("SELECT UserEmail, Password, UserInfo.FirstName FROM Credentials "
                       "JOIN UserInfo ON UserInfo.FirstName='Andrei' "
                       "AND UserInfo.UserID=Credentials.UserID")
        data = cursor.fetchall()
        for firstname in data:
            assert firstname[2] == "Andrei", "Name must be 'Andrei'"
