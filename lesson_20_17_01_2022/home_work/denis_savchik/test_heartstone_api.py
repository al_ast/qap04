from homework_20.heartstone_card_api import ImgurService


class TestHeartStoneCardAPI(ImgurService):

    def test_card_presence_with_id(self):
        endpoint = "EX1_572"
        response_data = self.get_response(endpoint)
        card_id = response_data[0]["cardId"]
        assert card_id == endpoint, "Card have not exist"

    def test_card_name(self):
        endpoint = "Ysera"
        response_data = self.get_response(endpoint)
        card_name = response_data[0]["name"]
        assert card_name == endpoint, "Card name isnt presence"

    def test_presence_card_with_needed_attack(self):
        params = {"attack": 10}
        endpoint = "%7Bclass%7D"
        response_data = self.get_response(endpoint, params)
        assert len(response_data) > 0, "Card with this param is exists"

    def test_search_to_break(self):
        endpoint = "search/novalid"
        assert self.get_status_code(endpoint) != 200, "Expected no 2xx status code"

    def test_presence_card_with_invalid_params(self):
        params = {"health": "20", "durability": "20",
                  "cost": "20", "attack": "20", "callback": "20"}
        endpoint = "Knight"

        assert self.get_status_code(endpoint, params) != 200, "Expected statuscode 404"



