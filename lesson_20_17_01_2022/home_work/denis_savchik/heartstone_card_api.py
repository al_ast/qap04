import requests
import json


class ImgurService:
    HEADERS = {
                'x-rapidapi-host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
                'x-rapidapi-key': '2f961c5d06mshe617a516c24b4bbp14a7aejsn8c3b70f0cbc8'
            }
    BASE_URL = "https://omgvamp-hearthstone-v1.p.rapidapi.com/cards/{}"

    def get_response(self, endpoint, params=None):

        url = self.BASE_URL.format(endpoint)
        response = requests.get(url, headers=self.HEADERS, params=params)
        response_data = response.json()

        return response_data

    def get_status_code(self, endpoint, params=None):

        url = self.BASE_URL.format(endpoint)
        response = requests.get(url, headers=self.HEADERS, params=params)

        return response.status_code



