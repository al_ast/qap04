from imdb_service import ImdbService

class TestImdb(ImdbService):
    def test_nconst_id(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-all-filmography", params)
        nconst_id = params["nconst"]
        response_data_id = response_data["id"]

        assert nconst_id in response_data_id, f"constant {nconst_id} is not found"

    def test_filmography(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-all-filmography", params)

        assert len(response_data["filmography"]) > 0, "Filmography list is not empty"

    def test_name(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-all-filmography", params)
        response_data_name = response_data["base"]["name"]

        assert response_data_name == "Jonathan Rhys Meyers", f"Expected name Jonathan Rhys Meyers but was {response_data_name}"

    def test_get_awards_nconst_id(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-awards", params)
        response_data_id = response_data["resource"]["awards"][2]["nominations"]["names"][0]["id"]
        nconst_id = params["nconst"]

        assert nconst_id in response_data_id, f"constant {nconst_id} is not found"

    def test_get_awards(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-awards", params)

        assert len(response_data) > 0, "Response should be not empty"

    def test_get_all_news(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-all-news", params)

        assert len(response_data["items"]) > 0, "list of news should be not empty"

    def test_get_all_news_id(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-all-news", params)
        nconst_id = params["nconst"]
        response_data_id = response_data["image"]["id"]

        assert nconst_id in response_data_id, f"constant {nconst_id} is not found"

    def test_get_all_news_head(self):
        params = {'nconst': 'nm0001667'}
        response_data = self.get_response_from_server("/get-all-news", params)
        head = "Well Go USA"
        response_data_head = response_data["items"][0]["head"]

        assert head in response_data_head, f"Expected words in list {head}"