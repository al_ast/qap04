from default_method import DefaultMethod


class TestCovid19Data(DefaultMethod):

    def test_get_latest_country_data_by_code(self):
        params = {"code": "IT"}
        data = self.get_response_from_server('country/code', params)
        assert data[0]['code'] == params['code'], f'Code expected: {params["code"]}'

    def test_get_daily_report_by_country_name(self):
        params = {"name": "Italy", "date": "2020-04-01"}
        data = self.get_response_from_server('report/country/name', params)
        assert data[0]['country'] == params['name'], f'Country name expected: {params["name"]}'

    def test_get_daily_report_by_country_code(self):
        params = {"date": "2020-04-01", "code": "it"}
        data = self.get_response_from_server('report/country/code', params)
        assert data[0]['date'] == params['date'], f'Date expected: {params["date"]}'

    def test_get_latest_country_data_by_name(self):
        params = {"name": "Italy"}
        data = self.get_response_from_server('country', params)
        last_change = data[0]['lastChange'][:10]
        last_update = data[0]['lastUpdate'][:10]
        assert data[0]['country'] == params['name'], f'Expected country name: {params["name"]}'
        assert last_update == last_change, 'Same dates expected'

    def test_get_latest_totals(self):
        data = self.get_response_from_server('totals')
        last_change = data[0]['lastChange'][:10]
        last_update = data[0]['lastUpdate'][:10]
        assert last_update == last_change, 'Same dates expected'

    def test_get_list_of_countries(self):
        data = self.get_response_from_server('help/countries')
        assert len(data) > 0, 'List countries is empty'

    def test_open_api_documentation(self):
        data = self.get_response_from_server('docs.json')
        assert data['info']['title'] == 'COVID-19 data API', 'Expected: "COVID-19 data API"'

    def test_get_daily_report_totals(self):
        params = {"date": "2020-07-21"}
        data = self.get_response_from_server('report/totals', params)
        assert data[0]['date'] == params['date'], f'Date expected: {params["date"]}'
