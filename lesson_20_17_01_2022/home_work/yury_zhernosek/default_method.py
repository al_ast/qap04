import json
import requests


class DefaultMethod:

    HEADERS = {
        'x-rapidapi-host': "covid-19-data.p.rapidapi.com",
        'x-rapidapi-key': "9c098981ccmsha849a9714ea5accp1603b5jsnc13da92815f7"
    }
    URL = 'https://covid-19-data.p.rapidapi.com/'

    def get_response_from_server(self, endpoint, params=None):
        url = self.URL + endpoint
        response = requests.get(url, headers=self.HEADERS, params=params)
        data = json.loads(response.text)
        return data
