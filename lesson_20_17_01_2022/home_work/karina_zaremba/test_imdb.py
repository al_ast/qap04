from imdb_requests import ImdbService


params = {"nconst": "nm0001667"}


class TestImdb(ImdbService):
    def test_get_awards(self):
        response_data = self.get_response_from_server("/get-awards", params)
        assert len(response_data) > 0, "Response must be not empty"

    def test_actors_bio(self):
        id = "nm0001667"
        response = self.get_response_from_server("/actors/get-bio", params)
        actor_id = response.get('id')
        assert id in actor_id, f'Incorrect id. Expected {id}.'

    def test_get_actor_name(self):
        bio = self.get_biography('nm0001667')
        actor_name = 'Jonathan Rhys Meyers'
        assert actor_name == bio["name"], f'{actor_name} name not found'

    def test_plots(self):
        plots_in_db = 6
        params = {"tconst": "tt0944947"}
        response = self.get_response_from_server("/title/get-plots", params)
        count_plots_in_response_data = len(response.get('plots'))
        assert count_plots_in_response_data == plots_in_db, \
            f'Expected {plots_in_db} items, but was {count_plots_in_response_data}'

    def test_get_genres(self):
        params = {"tconst": "tt0944947"}
        response_data = self.get_status_code("/title/get-genres", params)
        assert response_data.status_code == 200, f"Expected response code 200 but was:{response_data.status_code}"

    def test_popular_celebs(self):
        list = self.get_response_from_server("/get-awards", params)
        assert len(list) > 0, f"The list of the most popular celebs shouldn't be empty"

    def test_get_all_filmography(self):
        response_data = self.get_status_code("/actors/get-bio", params)
        assert response_data.status_code == 200, f"Expected status code 200, but was:{response_data.status_code}"