import requests
import json


class ImdbService:
    HEADERS = {
        'x-rapidapi-host': 'imdb8.p.rapidapi.com',
        'x-rapidapi-key': '46a60ddcfamsh735cfd849f48005p1f6015jsn9acf1c534c5d'
    }
    URL = "https://imdb8.p.rapidapi.com"

    def get_response_from_server(self, endpoint, params=None):
        url = self.URL + endpoint
        response = requests.get(url, headers=self.HEADERS, params=params)
        data = json.loads(response.text)
        return data

    def get_biography(self, nconst):
        params = {'nconst': nconst}
        return self.get_response_from_server('/actors/get-bio', params)

    def get_status_code(self, endpoint, params=None):
        url = self.URL + endpoint
        response = requests.get(url, headers=self.HEADERS, params=params)
        return response
