# Отправить какой-либо запрос через постмен
#
# Протестировать любой паблик апи отсюда
# ВЫбрать один раздел и написать на него 8 тестов. Должно быть как минимум 3 разных запроса
# https://rapidapi.com/hub
import pytest
from lesson_20_17_01_2022.home_work.pavel_harbachou.google_translate_service import GoogleTranslateService
from lesson_20_17_01_2022.home_work.pavel_harbachou.open_weather_map_service import OpenWeatherMapService


@pytest.fixture
def setup_open_weather_map():
    global open_weather_map_service
    open_weather_map_service = OpenWeatherMapService()


@pytest.fixture
def setup_google_translate():
    global google_translate_service
    google_translate_service = GoogleTranslateService()


@pytest.mark.usefixtures("setup_google_translate")
class TestAPIGoogleTranslate:
    def test_language_correctly_define(self):
        data = google_translate_service.post("detect", "q=English is hard, but detectably so")
        language = data['data']['detections'][0][0]['language']
        assert language == 'en', f'the language is incorrectly defined, expected en, but there is {language}'

    def test_not_empty_list_of_languages(self):
        data = google_translate_service.get("languages")
        number_of_languages = len(data['data']['languages'])
        assert number_of_languages > 0, 'List of languages is empty'

    def test_correctly_translation_into_spanish(self):
        data = google_translate_service.post('', "q=Hello world!&target=es&source=en")
        translated_text = data['data']['translations'][0]['translatedText']
        assert translated_text == '¡Hola Mundo!', f'incorrect translation into Spanish,' \
                                                  f' expected ¡Hola Mundo!, but there is {translated_text}'

    def test_most_popular_language_is_present(self):
        data = google_translate_service.get("languages")
        list_of_available_languages = data['data']['languages']
        most_popular_language = 'en'
        assert most_popular_language in list_of_available_languages.values(),\
            'there is no translation of the most popular language'


@pytest.mark.usefixtures("setup_open_weather_map")
class TestAPIOpenWeatherMap:
    def test_is_weather_correct(self):
        data = open_weather_map_service.get('weather', {"q": "London,uk"})
        weather = data['weather'][0]['main']
        assert weather == 'Clouds', f'incorrect weather is shown, must be Clouds, but there is {weather}'

    def test_is_number_of_days_correct(self):
        data = open_weather_map_service.get('climate/month', {"q": "New York"})
        number_of_days = len(data['list'])
        assert number_of_days == 30, f'incorrect number of days, expected 30, but there is {number_of_days}'

    def test_is_time_zone_correct(self):
        data = open_weather_map_service.get('forecast', {"q": "minsk,blr"})
        time_zone = data['city']['timezone']
        assert time_zone == 10800, f'incorrect time zone, expected 10800, but there is {time_zone}'

    def test_is_sea_level_correct(self):
        data = open_weather_map_service.get('find', {"q": "minsk"})
        sea_level = data['list'][0]['main']['sea_level']
        assert sea_level == 1018, f'incorrect sea level, expected 1018, but there is {sea_level}'
