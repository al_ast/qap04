import json
import requests


class GoogleTranslateService:
    def __init__(self):
        self.headers = {
            'content-type': "application/x-www-form-urlencoded",
            'accept-encoding': "application/gzip",
            'x-rapidapi-host': "google-translate1.p.rapidapi.com",
            'x-rapidapi-key': "5752b2c735msh1ec9afcf7804be9p1b8a0cjsn57b236c302d2"
        }
        self.base_url = "https://google-translate1.p.rapidapi.com/language/translate/v2"

    def get(self, endpoint):
        url = f'{self.base_url}/{endpoint}'
        response = requests.get(url, headers=self.headers)
        data = json.loads(response.text)
        return data

    def post(self, endpoint, data):
        url = f'{self.base_url}/{endpoint}'
        response = requests.post(url, data=data, headers=self.headers)
        data = json.loads(response.text)
        return data
