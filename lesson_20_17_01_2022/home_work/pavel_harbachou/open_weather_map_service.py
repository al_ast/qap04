import json
import requests


class OpenWeatherMapService:
    def __init__(self):
        self.headers = {
            'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com",
            'x-rapidapi-key': "5752b2c735msh1ec9afcf7804be9p1b8a0cjsn57b236c302d2"
        }
        self.base_url = 'https://community-open-weather-map.p.rapidapi.com'

    def get(self, endpoint, data):
        url = f'{self.base_url}/{endpoint}'
        response = requests.get(url, params=data, headers=self.headers)
        data = json.loads(response.text)
        return data
