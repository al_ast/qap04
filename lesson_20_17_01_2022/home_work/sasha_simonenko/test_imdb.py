from imdb_service import ImdbService
import requests
params = {'nconst': 'nm0001667'}
headers = {
    'x-rapidapi-host': "imdb8.p.rapidapi.com",
    'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}


class TestImdb(ImdbService):
    def test_get_awards(self):
        data = self.get_response("/get-awards", params)

        assert len(data) > 0, "The list of awards should not be not empty"

    def test_get_plots(self):
        data = self.get_response("/get-awards", params)

        assert len(data) > 0, "The list of plots should not be not empty"

    def test_list_most_popular_celebs(self):
        data = self.get_response("/get-awards", params)

        assert len(data) > 0, "The list of the most popular celebs should not be not empty"

    def test_get_all_news(self):
        data = self.get_response("/get-awards", params)

        assert len(data) > 0, "The list of news should not be not empty"

    def test_list_born_today(self):
        data = self.get_response("/get-awards", params)

        assert len(data) > 0, "The list of born today should not be not empty"

    def test_get_all_filmography(self):
        url = "https://imdb8.p.rapidapi.com/actors/get-bio"

        response = requests.get(url, headers=headers, params=params)
        assert response.status_code == 200, "Expected status code 200, but was:{response.status_code}"

    def test_get_genres(self):
        url = "https://imdb8.p.rapidapi.com/title/get-genres"

        response = requests.get(url, headers=headers, params=params)
        assert response.status_code == 204, "Expected status code 204, but was:{response.status_code}"

    def test_get_biography(self):
        url = "https://imdb8.p.rapidapi.com/actors/get-bio"

        response = requests.get(url, headers=headers, params=params)
        assert response.status_code == 200, "Expected status code 204, but was:{response.status_code}"


