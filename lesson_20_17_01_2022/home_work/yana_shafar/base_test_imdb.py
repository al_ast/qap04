import requests


class BaseImdb:
    def __init__(self):
        self.api_key = '012e6f82ffmsh81aad0e337b9335p16ea89jsncc113a1382b4'
        self.headers = {
            'x-rapidapi-host': 'imdb8.p.rapidapi.com',
            'x-rapidapi-key': self.api_key
        }
        self.base_url = 'https://imdb8.p.rapidapi.com'

    def get_date(self, url, params):
        response = requests.get(
            url=self.base_url + url, headers=self.headers, params=params
        )
        return response
