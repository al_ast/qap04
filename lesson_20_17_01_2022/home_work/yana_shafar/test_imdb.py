from lesson_20_17_01_2022.home_work.yana_shafar.base_test_imdb import BaseImdb


class TestImdb:
    def test_popular_movies_by_genre(self):
        url = '/title/get-popular-movies-by-genre'
        movies_in_db = 100

        params = {
            'genre': '/chart/popular/genre/adventure'
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert len(response_data) == movies_in_db, f'Expected {movies_in_db} items, but was {len(response_data)}'

    def test_auto_complete(self):
        url = '/auto-complete'

        params = {
            'q': 'game of thr'
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        first_search_result_actors_name = response_data['d'][0]['s']

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert first_search_result_actors_name == 'Emilia Clarke, Peter Dinklage', \
            f"Expected Emilia Clarke, Peter Dinklage, but was {first_search_result_actors_name}"

    def test_bio(self):
        url = '/actors/get-bio'
        id = "nm0001667"

        params = {
            "nconst": id
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        actor_id = response_data.get('id')

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert id in actor_id, f'Incorrect actor id. Expected {id}.'

    def test_awards(self):
        url = '/actors/get-awards'
        id = "nm0001667"

        params = {
            'tconst': 'tt0944947',
            "nconst": id
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        actor_id = response_data.get('resource').get('id')

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert id in actor_id, f'Incorrect actor id. Expected {id}.'

    def test_genres(self):
        url = '/title/get-genres'
        genre_in_db = 4

        params = {
            "tconst": "tt0944947"
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert len(response_data) == genre_in_db, f'Expected {genre_in_db} items, but was {len(response_data)}'

    def test_all_videos(self):
        url = '/actors/get-all-videos'
        videos_in_db = 90

        params = {
            "nconst": "nm0001667", "region": "US"
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        count_videos_in_response_data = len(response_data.get("resource").get("videos"))

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert count_videos_in_response_data == videos_in_db, \
            f'Expected {videos_in_db} items, but was {count_videos_in_response_data}'

    def test_details(self):
        url = '/title/get-details'
        id = "tt0944947"

        params = {
            "tconst": id
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        title_id = response_data.get('id')

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert id in title_id, f'Incorrect title id. Expected {id}.'

    def test_plots(self):
        url = '/title/get-plots'
        plots_in_db = 6

        params = {
            "tconst": "tt0944947"
        }

        response = BaseImdb().get_date(url=url, params=params)
        response_data = response.json()

        count_plots_in_response_data = len(response_data.get('plots'))

        assert response.status_code == 200, f'Expected status code 200, but was {response.status_code}'
        assert count_plots_in_response_data == plots_in_db, \
            f'Expected {plots_in_db} items, but was {count_plots_in_response_data}'
