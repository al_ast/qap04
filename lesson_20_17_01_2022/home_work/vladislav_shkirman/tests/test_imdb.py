from lesson_20_17_01_2022.home_work.vladislav_shkirman.requests.imdb_requests import ImdbService
import pytest


@pytest.fixture()
def imdb_api_setup():
    global imdb

    imdb = ImdbService()


@pytest.mark.usefixtures('imdb_api_setup')
class TestImdb:
    def test_get_actor_name(self):
        bio = imdb.get_biography('nm0001667')
        actor_name = 'Jonathan Rhys Meyers'
        assert actor_name == bio["name"], f'{actor_name} name not found'

    def test_get_all_movies_of_actor(self):
        all_movies = imdb.get_filmography('nm0001661')
        assert len(all_movies) > 0, 'No movies found'

    def test_get_movie_by_title_and_year(self):
        query = 'pulp fiction'
        year = 1994
        all_titles = imdb.get_movie_by_title_and_year(query, year)
        assert len(all_titles) > 0, f'There are no movies with requested title {query}'

    def test_get_movie_rating(self):
        movie_rating = imdb.get_movie_rating('casino royale')
        rating_format_valid = imdb.is_rating_format_valid(movie_rating)
        assert rating_format_valid, 'Rating format is invalid'

    def test_get_top_rated_movies(self):
        top_rated_movies_list = imdb.get_top_rated_movies()
        copied_movie_rating_list = top_rated_movies_list.copy()
        copied_movie_rating_list.sort(reverse=True)
        assert top_rated_movies_list == copied_movie_rating_list, 'Movie ratings are not sorted according to score'

    def test_get_filming_locations(self):
        locations_list = imdb.get_filming_locations('tt0944947')
        location = 'Iceland'
        assert location in locations_list, f'There are no {location} found'

    def test_get_taglines(self):
        taglines_list = imdb.get_taglines('tt0944947')
        assert "" not in taglines_list, f'Tagline for id {taglines_list.index("")} is not found'

    def test_get_top_writers(self):
        writer_info = imdb.get_top_writers('tt0944947')
        writer_id = writer_info[0]
        writer_name = writer_info[1]
        data_bio = imdb.get_biography(writer_id)
        assert data_bio['name'] == writer_name, \
            f'Name {data_bio["name"]} in biography is not equal to the name {writer_name} in top crew'
