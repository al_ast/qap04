import requests
import json
import random
import re


class ImdbService:
    def __init__(self):
        self.imdb_host = 'https://imdb8.p.rapidapi.com{}'
        self.imdb_headers = {
            'x-rapidapi-host': 'imdb8.p.rapidapi.com',
            'x-rapidapi-key': 'fc7481bc65msha6d2e9670b97e1ap101a56jsn9cfbc27afed7'
        }

    def get(self, endpoint, params=None):
        url = self.imdb_host.format(endpoint)
        response = requests.get(url, headers=self.imdb_headers, params=params)
        data = json.loads(response.text)
        return data

    def get_biography(self, nconst):
        params = {'nconst': nconst}
        return self.get('/actors/get-bio', params)

    def get_filmography(self, nconst):
        params = {'nconst': nconst}
        data_get_filmography = self.get('/actors/get-all-filmography', params)
        list_filmography = data_get_filmography['filmography']
        list_movies = [dictionary['title'] for dictionary in list_filmography]
        return list_movies

    def _get_list_title(self, query):
        params = {'q': query}
        data_found_by_title = self.get('/title/find', params)
        return data_found_by_title.get('results')

    def get_movie_by_title_and_year(self, query, year):
        list_results = self._get_list_title(query)
        list_movie_titles = [dictionary_movie['title'] for dictionary_movie in list_results
                             if dictionary_movie.get('titleType') == 'movie' and dictionary_movie.get('year') == year]
        return list_movie_titles

    def get_movie_rating(self, query):
        list_results = self._get_list_title(query)
        title_ids_list = [dictionary_tconst.get('id') for dictionary_tconst in list_results]
        title_id = title_ids_list[0]
        tconst = None
        for element in title_id.split('/'):
            if 'tt' in element:
                tconst = element
        params = {'tconst': tconst}
        data_get_ratings = self.get('/title/get-ratings', params)
        rating = data_get_ratings.get('rating')
        return rating

    def is_rating_format_valid(self, rating):
        match = re.fullmatch("^[0-9][.][0-9]", str(rating))
        if match:
            return True
        return False

    def get_top_rated_movies(self):
        data_rated_movies = self.get('/title/get-top-rated-movies')
        top_movie_rating_list = [movie.get('chartRating') for movie in data_rated_movies]
        return top_movie_rating_list

    def get_filming_locations(self, tconst):
        params = {'tconst': tconst}
        data_locations = self.get('/title/get-filming-locations', params)
        data_locations_list = data_locations.get('locations')
        locations_list = [location['location'] for location in data_locations_list]
        return locations_list

    def get_taglines(self, tconst):
        params = {'tconst': tconst}
        data_taglines = self.get('/title/get-taglines', params)
        taglines_list = data_taglines.get('taglines')
        return taglines_list

    def get_top_writers(self, tconst):
        params = {'tconst': tconst}
        data_top_crew = self.get('/title/get-top-crew', params)
        writers_list = data_top_crew['writers']
        writer_info = random.choice(writers_list)
        writer_id = writer_info.get('id')
        nconst = None
        for element in writer_id.split('/'):
            if 'nm' in element:
                nconst = element
        name = writer_info['name']
        return [nconst, name]
