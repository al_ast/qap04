# Отправить какой-либо запрос через постмен
#
# Протестировать любой паблик апи отсюда
# ВЫбрать один раздел и написать на него 8 тестов. Должно быть как минимум 3 разных запроса
# https://rapidapi.com/hub


import requests
import json

class TestIMBD:
    def test_list_born_today(self):
        url = "https://imdb8.p.rapidapi.com/actors/list-born-today"
        querystring = {"month": "7", "day": "27"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()
        list_born_today = json.loads(response.text)

        assert len(list_born_today) > 0, "The list should not be empty"
        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"

    def test_list_most_popular_celebs(self):
        url = "https://imdb8.p.rapidapi.com/actors/list-most-popular-celebs"
        querystring = {"homeCountry": "US", "currentCountry": "US", "purchaseCountry": "US"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()
        list_of_celebs = json.loads(response.text)

        assert len(list_of_celebs) > 0, "The list should not be empty"
        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"

    def test_get_all_filmography(self):
        url = "https://imdb8.p.rapidapi.com/actors/get-bio"
        querystring = {"nconst": "nm0001667"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()
        filmography = json.loads(response.text)

        assert len(filmography) > 0, "Filmography shouldn't be empty"
        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"

    def test_get_awards(self):
        url = "https://imdb8.p.rapidapi.com/actors/get-awards"
        querystring = {"nconst": "nm0001667"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()

        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"

    def test_get_plots(self):
        url = "https://imdb8.p.rapidapi.com/title/get-plots"
        querystring = {"tconst": "tt0944947"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()

        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"

    def test_get_genres(self):
        url = "https://imdb8.p.rapidapi.com/title/get-genres"
        querystring = {"tconst": "tt0944947"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()

        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"

    def test_get_trivia(self):
        url = "https://imdb8.p.rapidapi.com/title/get-trivia"
        querystring = {"tconst": "tt0944947"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()

        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"

    def test_get_bio(self):
        url = "https://imdb8.p.rapidapi.com/actors/get-bio"
        querystring = {"nconst": "nm0001667"}
        headers = {
            'x-rapidapi-host': "imdb8.p.rapidapi.com",
            'x-rapidapi-key': "7a0bc70d93mshd817fcc3faacd6fp168cddjsn2055c0e50c22"}

        response = requests.get(url, headers=headers, params=querystring)
        data = response.json()

        assert response.status_code == 200, f"Expected response code 200 but was:{response.status_code}"








