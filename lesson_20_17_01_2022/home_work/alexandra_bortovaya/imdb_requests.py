import random
import re

import requests


class ImdbApiRequests:
    URL_ACTORS = "https://imdb8.p.rapidapi.com/actors{}"
    URL_TITLE = "https://imdb8.p.rapidapi.com/title{}"
    HEADERS = {
        'x-rapidapi-host': "imdb8.p.rapidapi.com",
        'x-rapidapi-key': "3967fc1f0emsh5f542ef47487d14p12a5d6jsn070c399610ef"
    }

    def _get_correct_actor_url(self, endpoint):
        return self.URL_ACTORS.format(endpoint)

    def _get_correct_title_url(self, endpoint):
        return self.URL_TITLE.format(endpoint)

    def _send_get_request(self, url, params=None):
        if params:
            return requests.get(url, headers=self.HEADERS, params=params)

        return requests.get(url, headers=self.HEADERS)

    def _get_random_actor_nconst(self):
        response_data = self.list_most_popular_celebs()
        index = random.randint(0, len(response_data))
        random_celeb = response_data[index]

        return random_celeb.split("/")[2]

    def list_most_popular_celebs(self):
        url = self._get_correct_actor_url("/list-most-popular-celebs")
        params = {"homeCountry": "US", "currentCountry": "US", "purchaseCountry": "US"}
        response = self._send_get_request(url, params)

        return response.json()

    def get_bio(self, nconst=None):
        url = self._get_correct_actor_url("/get-bio")
        if not nconst:
            nconst = self._get_random_actor_nconst()
        params = {"nconst": nconst}
        response = self._send_get_request(url, params)

        return nconst, response.json()

    def get_all_filmography(self, nconst=None):
        url = self._get_correct_actor_url("/get-all-filmography")
        if not nconst:
            nconst = self._get_random_actor_nconst()
        params = {"nconst": nconst}
        response = self._send_get_request(url, params)

        return response.json()

    def get_known_for(self, nconst=None):
        url = self._get_correct_actor_url("/get-known-for")
        if not nconst:
            nconst = self._get_random_actor_nconst()
        params = {"nconst": nconst}
        response = self._send_get_request(url, params)

        return response.json()

    def find(self, find_input):
        url = self._get_correct_title_url("/find")
        params = {"q": find_input}
        response = self._send_get_request(url, params)

        return response.json()["results"]

    def get_ratings(self, tconst):
        url = self._get_correct_title_url("/get-ratings")
        params = {"tconst": tconst}
        response = self._send_get_request(url, params)

        return response.json()

    def is_rating_valid(self, rating):
        match = re.fullmatch("^[0-9]\.[0-9]", str(rating))
        if match:
            return True
        return False
