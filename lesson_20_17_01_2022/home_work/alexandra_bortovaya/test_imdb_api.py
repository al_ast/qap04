import pytest

from lesson_20_17_01_2022.home_work.alexandra_bortovaya.imdb_requests import ImdbApiRequests


@pytest.fixture(scope="session")
def setup():
    global imdb_requests

    imdb_requests = ImdbApiRequests()


@pytest.mark.usefixtures("setup")
class TestImdbApi:
    def test_list_most_popular_celebs(self):
        response_data = imdb_requests.list_most_popular_celebs()

        assert len(response_data) > 0, "Most popular celebs list is empty"

    def test_get_bio(self):
        nconst, response_data = imdb_requests.get_bio()
        response_id = response_data.get("id")

        assert nconst in response_id, f"Response does not contain sent ncost ({nconst})"

    @pytest.mark.parametrize("key", ["name", "birthDate", "gender", "realName"])
    def test_get_bio_has_main_keys(self, key):
        nconst, response_data = imdb_requests.get_bio()
        response_keys = response_data.keys()
        assert key in response_keys, f"Response does not contain {key}"

        key_value = response_data.get(key, "")
        assert len(key_value) > 0, f"{key} has empty value"

    def test_get_filmography(self):
        response_data = imdb_requests.get_all_filmography()

        assert len(response_data.get("filmography", "")) > 0, "Filmography list is empty"

    @pytest.mark.parametrize("key", ["title", "imdbRating", "summary", "categories", "whereToWatch"])
    def test_get_known_for(self, key):
        response_data = imdb_requests.get_known_for()
        is_all_films_contain_key = True
        for film in response_data:
            film_keys = film.keys()
            if key not in film_keys:
                is_all_films_contain_key = False

        assert is_all_films_contain_key, f"Some films do not contain {key}"

    def test_find(self):
        response_data = imdb_requests.find("brassic")

        assert len(response_data) > 0, "No results were found for valid input"

    def test_get_ratings(self):
        response_data = imdb_requests.get_ratings("tt9174582")
        rating = response_data.get("rating")
        is_rating_valid = imdb_requests.is_rating_valid(rating)

        assert is_rating_valid, "Rating format is invalid"

    @pytest.mark.parametrize("key", ["Females Aged 30-44", "Females Aged under 18", "Aged 30-44", "Females Aged 18-29",
                                     "Females", "Aged 18-29", "US users", "Top 1000 voters", "Males Aged 30-44",
                                     "Males Aged 18-29", "Females Aged 45+", "Males Aged 45+", "IMDb Users",
                                     "Aged under 18", "Males", "Aged 45+", "Non-US users", "Males Aged under 18"])
    def test_get_ratings_raters(self, key):
        response_data = imdb_requests.get_ratings("tt9174582")
        raters = response_data["ratingsHistograms"]
        raters_keys = raters.keys()

        assert key in raters_keys, f"Response does not contain rating from {key}"
