import requests

from lesson_20_17_01_2022.class_materials.imdb_service import ImdbService


class TestImdb:
    def test_imdb_auto_complete(self):
        imdb_service = ImdbService()
        params = {'q': 'game of thr'}

        response_data = imdb_service.get("auto-complete", params)

        first_search_result_name = response_data['d'][0]['l']
        assert first_search_result_name == 'Game of Thrones', \
            f'First search result name is not correct. Expected :Game of Thrones but was{first_search_result_name}'

    def test_imdb_get_all_images(self):
        imdb_service = ImdbService()
        params = {'nconst': 'nm0001667'}

        response_data = imdb_service.get("auto-complete", params)

        actual_images = response_data["resource"]["images"]
        assert len(actual_images) > 0, "Expected not empty list of images"
