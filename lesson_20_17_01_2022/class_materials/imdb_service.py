import requests


class ImdbService:
    def __init__(self):
        self.headers = {
            'x-rapidapi-host': 'imdb8.p.rapidapi.com',
            'x-rapidapi-key': 'a8db052937msh0270012ac6741ecp19503ejsn8c3272a5383d'
        }
        self.base_url = "https://imdb8.p.rapidapi.com"

    def get(self, endpoint, params):
        response = requests.get(
            f"{self.base_url}/{endpoint}",
            headers=self.headers,
            params=params)

        response_data = response.json()
        return response_data
