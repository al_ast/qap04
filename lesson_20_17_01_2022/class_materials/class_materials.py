import requests
import json

api_key = "a8db052937msh0270012ac6741ecp19503ejsn8c3272a5383d"

headers = {
    'x-rapidapi-host': 'imdb8.p.rapidapi.com',
    'x-rapidapi-key': 'a8db052937msh0270012ac6741ecp19503ejsn8c3272a5383d'
}
params = {'q': 'game of thr'}

response = requests.get(
    "https://imdb8.p.rapidapi.com/auto-complete",
    headers=headers,
    params=params)


response_data = response.json()

assert response.status_code == 200, "Valid message"

assert response_data['d'][0]['l'] == 'Game of Thrones', "Not game of thrones"

response_data_id = response_data["id"]
ncost_request_param = params["nconst"]

assert ncost_request_param in response_data_id, \
    "Requested id not found in the response body"

assert response_data["name"] == "Jonathan Rhys Meyers"

