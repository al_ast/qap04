import pytest

from lesson_16_20_12_2021.class_materials.testing.pages.login_page import LoginPage
from lesson_17_23_12_2021.class_materials.pages.product_page import ProductsPage


class TestLoginPage:
    @pytest.mark.slow
    # @pytest.mark.parametrize("user_name,password",
    #                          [
    #                              ("", ""),
    #                              (" ", " "),
    #                              ("not_valid", "not_valid"),
    #                              ("$!@#$ %^&*()", " !@#$%^&*()_")
    #                          ])
    def test_login_with_not_valid_credentials(self, login_page):
        login_page.login("not valid", "not valid")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    # def test_slow(self):
    #     assert "slow" == "this is slow test"

    #@pytest.mark.fast
    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")

        assert products_page.is_opened(), "User should be redirected to the product page after login with valid creds"

    # def test_fast(self):
    #     assert "fast" == "this is fast test"
