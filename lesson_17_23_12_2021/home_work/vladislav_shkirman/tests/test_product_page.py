import pytest


@pytest.mark.usefixtures("login")
class TestProductPage:
    @pytest.mark.vlad_test
    def test_product_presence(self, products_page):
        backpack = products_page.is_product_presented('Backpack')
        assert backpack, "There is no such product on the page"

    @pytest.mark.vlad_test
    def test_add_to_cart(self, products_page):
        product = products_page.add_to_cart('bike-light')
        assert product, 'Failed add to cart'

    def test_presence_of_prices(self, products_page):
        assert products_page.get_prices_count() == products_page.get_products_count(), \
            f'products number {products_page.get_products_count()} and prices number {products_page.get_prices_count()}'

    @pytest.mark.vlad_test
    def test_price_format_valid(self, products_page):
        products_page.is_price_format_valid()
        assert products_page.is_price_format_valid(), "The price format is invalid for some products"
