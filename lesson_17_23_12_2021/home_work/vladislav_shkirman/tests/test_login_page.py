import pytest


class TestLoginPage:
    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")
        assert products_page.is_opened(), 'Products page should be opened after login with valid credentials'

    @pytest.mark.parametrize("user_name, password",
                             [
                                 ("", ""),
                                 ("standard_user", "invalid_password"),
                                 ("!@#$%^&*", "!@56546"),
                                 ("any_user", "secret_sauce"),
                                 ("standard_user", "secret_sauce")
                             ])
    def test_login_with_different_credentials(self, login_page, products_page, user_name, password):
        login_page.login(user_name, password)
        assert login_page.is_opened(), 'Login page should be opened'
