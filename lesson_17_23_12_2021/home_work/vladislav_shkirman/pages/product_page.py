from lesson_17_23_12_2021.home_work.vladislav_shkirman.pages.base_page import BasePage
import re


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    PRICE_LOCATOR = "//div[@class='inventory_item_price']"
    PRODUCT_NAME_LOCATOR = "//div[contains(text(), '{}')]"
    ADD_TO_CART_LOCATOR = "button[id='add-to-cart-sauce-labs-{}']"
    SHOPPING_CART_BADGE_LOCATOR = "span[class='shopping_cart_badge']"
    PRODUCT_LOCATOR = "//div[@class='inventory_item_name']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def is_product_presented(self, product_name):
        return self.is_element_present(self.PRODUCT_NAME_LOCATOR.format(product_name))

    def _get_price_elements(self):
        return self.find_elements(self.PRICE_LOCATOR)

    def get_prices(self):
        return self.get_element_text(self.PRICE_LOCATOR)

    def get_prices_count(self):
        return self.get_elements_count(self.PRICE_LOCATOR)

    def get_products_count(self):
        return self.get_elements_count(self.PRODUCT_LOCATOR)

    def is_price_format_valid(self):
        self._get_price_elements()
        price_list = []
        for price_element in self._get_price_elements():
            if re.fullmatch(r"[$][0-9]+\.[0-9]{2}", price_element.text) \
                    and price_element.text != '$0.00':
                price_list.append(price_element.text)
            else:
                return False
        return True

    def add_to_cart(self, product_name):
        self.click(self.ADD_TO_CART_LOCATOR.format(product_name))
        return self.is_element_present(self.SHOPPING_CART_BADGE_LOCATOR)
