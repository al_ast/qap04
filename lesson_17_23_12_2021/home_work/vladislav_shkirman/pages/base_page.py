from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def _get_locator_by_type(self, locator):
        if "//" in locator:
            return By.XPATH

        return By.CSS_SELECTOR

    def _element(self, locator, time_out_sec=10):
        locator_by_type = self._get_locator_by_type(locator)

        element = WebDriverWait(self.driver, time_out_sec).until(
            EC.presence_of_element_located((locator_by_type, locator)))

        return element

    def enter_text(self, locator, text):
        element = self._element(locator)
        element.send_keys(text)

    def click(self, locator):
        element = self._element(locator)
        element.click()

    def get_text(self, locator):
        element = self._element(locator)
        return element.text

    def is_element_present(self, locator):
        try:
            element = self._element(locator)
            return True
        except TimeoutException:
            return False

    def open_url(self, url):
        self.driver.get(url)

    def find_elements(self, locator, time_out_sec=10):
        locator_by_type = self._get_locator_by_type(locator)
        elements = WebDriverWait(self.driver, time_out_sec).until(
            EC.presence_of_all_elements_located((locator_by_type, locator)))

        return elements

    def get_elements_count(self, locator):
        all_elements = self.find_elements(locator)
        try:
            if len(all_elements) > 0:
                return len(all_elements)
            return 0
        except TimeoutException:
            return False

    def get_element_text(self, locator):
        elements_text_list = []
        for element in self.find_elements(locator):
            elements_text_list.append(element.text)
        return elements_text_list
