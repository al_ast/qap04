import pytest


class TestLoginPage:
    @pytest.mark.negative_test
    def test_login_with_invalid_credentials(self, login_page):
        login_page.login("invalid_user", "invalid_password")

        assert login_page.is_opened(), "User with invalid credentials must be no logged"

    @pytest.mark.positive_test
    def test_login_with_valid_credentials(self, login_page, product_page):
        login_page.login("standard_user", "secret_sauce")

        assert product_page.is_opened(), "User with valid credentials must be logged"

    @pytest.mark.skip
    def test_should_be_skiped(self, login_page):
        login_page.login("standard_user", "secret_sauce")

        assert login_page.is_opened(), "test should be skiped"



