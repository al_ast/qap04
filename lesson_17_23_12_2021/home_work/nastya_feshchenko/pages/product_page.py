from lesson_17_23_12_2021.home_work.nastya_feshchenko.pages.base_page import BasePage


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    ALL_ITEMS_LOCATOR = "//div[contains(text(), '{}')]"
    PRODUCT_LOCATOR = "//div[@class='inventory_item_name']"
    ITEM_PRICE_LOCATOR = "//div[@class='inventory_item_price']"
    ADD_TO_CART_LOCATOR = "button[id='add-to-cart-sauce-labs-{}']"
    CART_LOCATOR = "span[class='shopping_cart_badge']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def is_item_present(self, item_name):
        return self.is_element_present(self.ALL_ITEMS_LOCATOR.format(item_name))

    def get_prices(self):
        return self.get_elements_list(self.ITEM_PRICE_LOCATOR)

    def get_prices_count(self):
        return self.get_elements_count(self.ITEM_PRICE_LOCATOR)

    def get_products_count(self):
        return self.get_elements_count(self.PRODUCT_LOCATOR)

    def add_to_cart(self, item_name):
        self.click(self.ADD_TO_CART_LOCATOR.format(item_name))
        return self.is_element_present(self.CART_LOCATOR)






