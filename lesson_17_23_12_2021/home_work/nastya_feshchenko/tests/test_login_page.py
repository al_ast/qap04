import pytest


@pytest.mark.usefixtures("setup_navigate_to_login_page")
class TestLoginPage:
    @pytest.mark.negative_test
    def test_login_with_not_valid_credentials(self, login_page):
        login_page.login("not valid", "not valid")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    @pytest.mark.positive_test
    def test_login_with_valid_credentials(self, login_page, products_page):
        login_page.login("standard_user", "secret_sauce")

        assert products_page.is_opened(), "User should be redirected to the product page " \
                                          "after login with valid credentials"
