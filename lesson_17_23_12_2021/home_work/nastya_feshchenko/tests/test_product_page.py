import pytest


@pytest.mark.usefixtures("try_to_login")
class TestProductPage:
    def test_if_a_product_is_present(self, products_page):
        backpack_presence = products_page.is_item_present('Backpack')
        assert backpack_presence, "No backpacks on this page"

    def test_if_prices_are_present(self, products_page):
        prices = products_page.get_prices()
        assert len(prices) > 0, "No prices found"

        not_valid_prices = [price for price in prices if price == " "]
        assert len(not_valid_prices) == 0, f"Some prices are not valid:{not_valid_prices}"

    def test_add_item_to_cart(self, products_page):
        product_is_in_cart = products_page.add_to_cart('backpack')
        assert product_is_in_cart, "The product wasn't added to Cart"
