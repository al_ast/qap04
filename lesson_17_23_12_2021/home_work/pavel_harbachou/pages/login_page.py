from lesson_17_23_12_2021.home_work.pavel_harbachou.pages.base_page import BasePage


class LoginPage(BasePage):
    URL = 'https://www.saucedemo.com/'
    ENTER_USERNAME_LOCATOR = '//input[@id="user-name"]'
    ENTER_PASSWORD_LOCATOR = '//input[@id="password"]'
    LOGIN_BUTTON_LOCATOR = '//input[@id="login-button"]'

    def open_page(self, url):
        self.open_site(url)

    def enter_login(self, user_name):
        self.enter_text(self.ENTER_USERNAME_LOCATOR, user_name)

    def enter_password(self, password):
        self.enter_text(self.ENTER_PASSWORD_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def login_with_valid_data(self):
        self.open_page(self.URL)
        self.enter_login('standard_user')
        self.enter_password('secret_sauce')
        self.click_login_button()

    def login(self, user_name, password):
        self.open_page(self.URL)
        self.enter_login(user_name)
        self.enter_password(password)
        self.click_login_button()
