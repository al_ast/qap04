import pytest


class TestLoginPage:
    @pytest.mark.login
    @pytest.mark.parametrize('user_name , password',
                             [
                                 ("standard_user", "secret_sauce"),
                                 ("problem_user", "secret_sauce"),
                                 ("performance_glitch_user", "secret_sauce")
                             ])
    def test_login_with_valid_data(self, user_name, password, product_page, login_page):
        login_page.login(user_name, password)
        assert product_page.is_opened(), 'Products page should be opened after login with valid data'
