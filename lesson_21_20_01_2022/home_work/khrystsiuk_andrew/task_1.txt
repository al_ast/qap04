1 Выбрать все пароли
    SELECT Password
    FROM Credentials

2 Выбрать все возраста людей
    SELECT UserAge FROM UserInfo

3 Выбрать все уникальные возраста людей по возрастанию
    SELECT UserAge FROM UserInfo ORDER BY UserAge

4 Выбрать Имена и возраста людей  людей, у которых почта заканчивается на 'gmail'
    SELECT UserInfo.FirstName, UserInfo.UserAge
    FROM UserInfo
    JOIN Credentials ON UserInfo.UserID = Credentials.UserID
    WHERE Credentials.UserEmail LIKE '%gmail.com'

5 Придумать несколько своих SQL запросов

    Пользователи в возростном диапазоне
        SELECT UserAge
        FROM UserInfo
        WHERE UserAge BETWEEN 14-25

    Пользователи по ID начиная с 100
        SELECT UserEmail
        FROM UserInfo
        WHERE FirstName LIKE "Andrew%"

    Получить совершеннолетних пользователей
        FROM UserInfo
        JOIN Credentials
        ON Credential.UserID = UserInfo.UserID
        WHERE Credentials.UserAge >= 18