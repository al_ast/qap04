#Выбрать все пароли:
SELECT Credentials.Password FROM UserInfo 
JOIN Credentials ON UserInfo.UserId=Credentials.UserId

#Выбрать все возраста людей:
SELECT UserInfo.Age FROM UserInfo 
JOIN Credentials ON UserInfo.UserId=Credentials.UserId

#Выбрать все уникальные возраста людей по возрастанию
Например [20, 19, 20, 32] => [19, 20, 32]:
SELECT DISTINCT  UserInfo.Age FROM UserInfo 
JOIN Credentials ON UserInfo.UserId=Credentials.UserId
ORDER BY UserInfo.Age

#Выбрать Имена и возраста людей  людей, у которых почта заканчивается на 'gmail':
SELECT UserInfo.FirstName, UserInfo.LastName ,UserInfo.Age FROM UserInfo 
JOIN Credentials ON UserInfo.UserId=Credentials.UserId AND Credentials.UserEmail LIKE '%gmail.com'

#Выбирает Имена и почту людей, у которых фамилия начинается на "V" и повторяется 2 раза:
SELECT UserInfo.FirstName,Credentials.UserEmail FROM UserInfo 
JOIN Credentials ON UserInfo.UserId=Credentials.UserId AND UserInfo.LastName LIKE 'V_%'
