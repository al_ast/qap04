--Выбрать все пароли
SELECT Password FROM Credentials;

--Выбрать все возраста людей
SELECT Age FROM UserInfo;

--Выбрать все уникальные возраста людей по возрастанию
SELECT DISTINCT Age FROM UserInfo ORDER BY Age ASC;

--Выбрать Имена и возраста людей  людей, у которых почта заканчивается на 'gmail'
SELECT
    UserInfo.FirstName,
    UserInfo.LastName,
    UserInfo.Age
FROM UserInfo JOIN Credentials ON Credentials.UserId=UserInfo.UserId
WHERE Credentials.UserEmail LIKE '%gmail';

--Выбрать людей, у которых возраст в диапазоне от 20 до 30
SELECT Age FROM UserInfo WHERE Age BETWEEN 20 AND 30;

--Выбрать пользователя, у которого имя начинается на букву
SELECT FirstName FROM UserInfo WHERE FirstName LIKE 'D%';

--Получить информацию о пользователе по email
SELECT
    UserInfo.*
FROM UserInfo JOIN Credentials ON Credentials.UserId=UserInfo.UserId
WHERE Credentials.UserEmail='Petya@gmail.com';
