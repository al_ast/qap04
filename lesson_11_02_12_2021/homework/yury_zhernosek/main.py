'''
Создать калькулятор с методом сложения:

Сгенерировать случайное число.
Сложить его с 0.

Сгенерировать случайное число,
сложить его  с предыдущей суммой

потоврить сложение 10 раз и вывести на экран

'''
from random import randint
from task_1 import Calculator

calculator = Calculator()
result = 0
for i in range(10):
    result = calculator.sum(result, randint(1, 20))
print(result)