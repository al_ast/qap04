"""
Создать калькулятор с методом сложения: Сгенерировать случайное число. Сложить его с 0.
Сгенерировать случайное число, сложить его  с предыдущей суммой, потоврить сложение 10 раз и вывести на экран.
"""
from random import randint
from task_1_class import Calculator


test_sum = Calculator()
result = test_sum.addition(randint(1, 100), 0)

for i in range(11):
    result = test_sum.addition(randint(1, 100), result)

print(result)
