"""Задание1:
Создать калькулятор с методом сложения:

Сгенерировать случайное число.
Сложить его с 0.

Сгенерировать случайное число,
сложить его  с предыдущей суммой

потоврить сложение 10 раз и вывести на экран

Пример:

0+2(случайное)=2
2(из шага 1)+9(случайное)=11
11(из шага 2) + 25(случайное)=36
...
Вывести результат последний суммы"""


from calculator import Calculator
from random import randint

calculator = Calculator()
first_number = randint(1, 10)
start_sum = calculator.sum(first_number, 0)

for current in range(9):
    rnd_number = randint(1, 10)
    next_sum = calculator.sum(rnd_number, start_sum)
    start_sum = next_sum
print(start_sum)

