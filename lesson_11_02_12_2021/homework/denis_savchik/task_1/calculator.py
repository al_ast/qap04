class Calculator:

    def _check_is_number_format_valid(self, value):
        is_number_valid = isinstance(value, int) \
                          or isinstance(value, float)
        if not is_number_valid:
            raise Exception(f"{value}: is not valid number")

    def sum(self, a, b):

        self._check_is_number_format_valid(a)
        self._check_is_number_format_valid(b)
        return a + b
