from random import randint


class Calculator:
    @staticmethod
    def sum(number1, number2):
        return number1 + number2


calculator_sum = 0

for _ in range(10):
    calculator_sum = Calculator.sum(calculator_sum, randint(0, 20))

print(calculator_sum)