<<<
Выбрать свой сайт
Если совсем никак, то используйте:
https://www.onliner.by/

Найти элемент используя:
1)
=,
contains

и text(), внутренний атрибут
используя все возможные комбинации

итого = +  text(), contains + text()
= + внутренний аттрибут(например id),
contains + внутренний аттрибут

2)с двойным условием (используя and и or)
3) По частичному пути
4) По полному пути
5) по индексу
>>>
1)https://pzz.by/
//strong[text() = '45 минут']	   # текст 45минут в хэдере
//a[contains(text(),'зона ')]    # ссылка зона доставки
//span[@class='filters__open']    # Фильтр
//div[contains(@class,'header-inner')]    # хэдер
2)https://pzz.by/vacancy
//div[@class='pzz-footer-logo' and @style='background-image: url(/assets/img/nhd/bottom-pzz.svg)']    # лого лисицы
//a[text()='Способы оплаты' or @href='/payments']    # ссылка способы оплаты
3)https://pzz.by/contacts
//body//div//div//div//div//div//p//strong[text()='Самовывоз:']    № текст Самовывоз
4)https://pzz.by/desserts
//body//div//div//div//div//ul//img[@src='https://app.pzzby.com/uploads/photos/GkTqBF3F5e.jpg']    # изображение классического чизкейка
5)https://pzz.by/desserts
//*[@id="content"]/div[2]/div[2]/div[1]    
