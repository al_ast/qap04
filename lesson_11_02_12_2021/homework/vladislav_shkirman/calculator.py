from random import randint


class Calculator:

    def summ_random_num(self):
        summ = 0
        count = 0
        while count != 10:
            random_number = randint(0, 10)
            summ += random_number
            count += 1
        return summ
