"""Задание2:
Выбрать свой сайт
Найти элемент используя:
1)
=, contains
и text(), внутренний атрибут, используя все возможные комбинации
итого:
= +  text(), contains + text()
= + внутренний аттрибут(например id), contains + внутренний аттрибут

2)с двойным условием (используя and и or)
3) По частичному пути
4) По полному пути
5) по индексу
"""

1.
url = 'https://store.playstation.com/ru-ru/pages/latest'
//a[text()='Предложения'] # Предложения в хэдере
//div[contains(text(), 'предложения')] # часть текста "Новогодние предложения"
//h2[@id='title-82ced94c-ed3f-4d81-9b50-4d4cf1da170b'] # Предзаказы
//a[contains(@href, 'browse')] # кнопка Просмотр

2.
url = https://store.playstation.com/ru-ru/category/c807cf89-3db1-40cd-8d32-8e181bf9ee18/1
//button[contains(@class, 'store') and @type='button']
//span[@class='shared-nav-search__label' or  text()='Поиск']

3. //body//div//section//div//ul//li//a[contains(@href, 'collections')]
4. //body//div//section//div//div//div//ul//li//a[@href='/ru-ru/pages/subscriptions']
5. //head//link[8]