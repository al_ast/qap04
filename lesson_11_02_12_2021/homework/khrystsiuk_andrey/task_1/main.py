# Создать калькулятор с методом сложения:
#
# Сгенерировать случайное число.
# Сложить его с 0.
#
# Сгенерировать случайное число,
# сложить его  с предыдущей суммой
#
# потоврить сложение 10 раз и вывести на экран


from random import randint
from calculator import Calculator

calculator = Calculator()
first_number = calculator.sum(randint(0, 100), 0)
for i in range(10):
    calc_sum = first_number + randint(0, 100)
    first_number = calc_sum

print(first_number)
