import requests
import pytest


class TestApi:
    def test_google(self):
        response = requests.get("https://google.com")
        assert response.status_code == 200, "Get failed"

    def test_users_regres(self):
        response = requests.get("https://reqres.in/api/users/")
        assert len(response.text) > 0, "No information about users found"

    def test_successful_login(self):
        data = {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
        }
        response = requests.post("https://reqres.in/api/login", data)
        login_data = response.text
        assert len(login_data) > 0, 'Token is not received'

    @pytest.mark.xfail(reason="Expected to fail")
    def test_unsuccessful_login(self):
        data = {
            "email": "peter@klaven"
        }
        response = requests.post("https://reqres.in/api/login", data)
        assert response.status_code == 401, 'Bad request with error code 400 is expected'

