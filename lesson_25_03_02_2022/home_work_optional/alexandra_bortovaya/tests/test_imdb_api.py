import pytest

from imdb_requests import ImdbApiRequests


@pytest.fixture(scope="session")
def setup():
    global imdb_requests

    imdb_requests = ImdbApiRequests()


@pytest.mark.usefixtures("setup")
class TestImdbApi:
    def test_list_most_popular_celebrities(self):
        response_data = imdb_requests.list_most_popular_celebrities()

        assert len(response_data) > 0, "Most popular celebs list is empty"

    @pytest.mark.xfail(reason="This test will fail")
    def test_get_filmography_failed(self):
        response_data = imdb_requests.get_all_filmography()

        assert len(response_data.get("filmography", "")) < 0, "Filmography list is empty"

    def test_find_film(self):
        response_data = imdb_requests.find_film("brassic")

        assert len(response_data) > 0, "No results were found for valid input"
