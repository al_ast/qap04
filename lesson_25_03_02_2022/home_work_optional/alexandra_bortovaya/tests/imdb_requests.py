import random
import re

import requests


class ImdbApiRequests:
    URL_ACTORS = "https://imdb8.p.rapidapi.com/actors{}"
    URL_TITLE = "https://imdb8.p.rapidapi.com/title{}"
    HEADERS = {
        'x-rapidapi-host': "imdb8.p.rapidapi.com",
        'x-rapidapi-key': "3967fc1f0emsh5f542ef47487d14p12a5d6jsn070c399610ef"
    }

    def _get_random_actor_nconst(self):
        response_data = self.list_most_popular_celebrities()
        random_celeb = random.choice(response_data)

        return random_celeb.split("/")[2]

    def list_most_popular_celebrities(self):
        url = self.URL_ACTORS.format("/list-most-popular-celebs")
        params = {"homeCountry": "US", "currentCountry": "US", "purchaseCountry": "US"}
        response = requests.get(url, headers=self.HEADERS, params=params)

        return response.json()

    def get_all_filmography(self, nconst=None):
        url = self.URL_ACTORS.format("/get-all-filmography")
        if not nconst:
            nconst = self._get_random_actor_nconst()
        params = {"nconst": nconst}
        response = requests.get(url, headers=self.HEADERS, params=params)

        return response.json()

    def find_film(self, find_input):
        url = self.URL_TITLE.format("/find")
        params = {"q": find_input}
        response = requests.get(url, headers=self.HEADERS, params=params)

        return response.json()["results"]

