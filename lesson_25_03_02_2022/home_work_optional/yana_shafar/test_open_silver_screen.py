import requests


class TestSilverScreen:
    def test_silver_screen_poster(self):
        response = requests.get("https://silverscreen.by/afisha/")
        assert response.status_code == 200, "Get failed"

    def test_comparing_two_numbers(self):
        assert 8 == 9, "Number 8 is not equal number 9"
