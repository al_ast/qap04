import requests

class Test_YouTube:
    def test_get(self):
        response = requests.get("https://www.youtube.com")
        assert response.status_code == 200, "Get failed"

    def test_open_video(self):
        response = requests.get("https://www.youtube.com/watch?v=duvlWEJJmU0")
        assert response.status_code == 200, "Get video failed"

    def test_failed(self):
        assert 4 == 6, "failed"
