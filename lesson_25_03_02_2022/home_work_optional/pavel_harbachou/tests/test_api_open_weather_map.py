import pytest

from open_weather_map_service import OpenWeatherMapService


@pytest.fixture
def setup_open_weather_map():
    global open_weather_map_service
    open_weather_map_service = OpenWeatherMapService()


@pytest.mark.usefixtures("setup_open_weather_map")
class TestAPIOpenWeatherMap:
    def test_is_weather_correct(self):
        data = open_weather_map_service.get('weather', {"q": "London,uk"})
        weather = data['weather'][0]['main']
        assert weather == 'Clouds', f'incorrect weather is shown, must be Clouds, but there is {weather}'

    def test_is_time_zone_correct(self):
        data = open_weather_map_service.get('forecast', {"q": "minsk,blr"})
        time_zone = data['city']['timezone']
        assert time_zone == 10800, f'incorrect time zone, expected 10800, but there is {time_zone}'

    def test_is_sea_level_correct(self):
        data = open_weather_map_service.get('find', {"q": "minsk"})
        sea_level = data['list'][0]['main']['sea_level']
        assert sea_level == 1030, f'incorrect sea level, expected 1018, but there is {sea_level}'
