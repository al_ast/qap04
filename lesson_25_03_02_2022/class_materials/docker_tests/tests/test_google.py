import requests


class TestGoogle:
    def test_get(self):
        response = requests.get("https://google.com")
        assert response.status_code == 200, "Get failed"

    def test_should_fail(self):
        assert 4 == 5, "Failed"
