from page import Page


class CartPage(Page):
    DELETE_ITEM = "//a[text()='{}']/ancestor::tr//a[contains(@id,'delete')]"

    def delete_item(self, item_name):
        delete_locator = self.DELETE_ITEM.format(item_name)
        self._clicker.click(delete_locator)
