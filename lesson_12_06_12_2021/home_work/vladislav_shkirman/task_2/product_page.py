from page import Page


class ProductPage(Page):
    ADD_TO_WISHLIST = "//span[text()='Добавить в избранное']"

    def add_to_wishlist(self):
        wishlist_locator = self.ADD_TO_WISHLIST
        self._clicker.click(wishlist_locator)
