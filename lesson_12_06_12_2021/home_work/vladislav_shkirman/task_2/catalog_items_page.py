from page import Page


class CatalogItemsPage(Page):
    ITEM_NAME_LOCATOR = "//span[text()='{}']"
    ADD_TO_CART = ITEM_NAME_LOCATOR + "/ancestor::dl//button[@data-ga_action='add_to_cart']"

    def open_product_page(self, item_name):
        item_locator = self.ITEM_NAME_LOCATOR.format(item_name)
        self._clicker.click(item_locator)

    def add_to_cart(self, item_name):
        cart_locator = self.ADD_TO_CART.format(item_name)
        self._clicker.click(cart_locator)
