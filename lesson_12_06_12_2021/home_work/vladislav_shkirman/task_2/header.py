from page import Page


class Header(Page):
    CATEGORY = "//a[text()='{}']"
    CART_BUTTON = "//a[contains(@class,'headerCartBox')]"

    def open_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        self._clicker.click(category_locator)

    def open_cart_page(self):
        cart_button_locator = self.CART_BUTTON
        self._clicker.click(cart_button_locator)
