"""
Задание2:
Выбрать любой сайт
Написать 5-6 действий на разных страницах используя локаторы
см пример в class_materials/pages
Улучшить с использованеим наследования
Использовать готовый класс кликер
"""
# https://www.21vek.by/

from header import Header
from catalog_items_page import CatalogItemsPage
from product_page import ProductPage
from cart_page import CartPage

# Add to cart via category page and delete
main_page1 = Header()
main_page1.open_category('Телевизоры')

catalog_page1 = CatalogItemsPage()
catalog_page1.add_to_cart('Телевизор Horizont 24LE5511D (черный)')

main_page1.open_cart_page()

cart_page = CartPage()
cart_page.delete_item('Телевизор Horizont 24LE5511D (черный)')

# Add to wish list
main_page2 = Header()
main_page2.open_category('Ноутбуки')

catalog_page2 = CatalogItemsPage()
catalog_page2.open_product_page('Ноутбук HP 15s-eq1206ur (24D58EA)')

product_page1 = ProductPage()
product_page1.add_to_wishlist()
