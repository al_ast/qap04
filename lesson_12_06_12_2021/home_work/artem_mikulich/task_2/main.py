from main_page import MainPage
from header import Header
from page_with_product import PageProducts

main_page = MainPage()
main_page.select_category("Шины")
main_page.cart_box("Корзина")

header = Header()
header.select_header("Шины")

page_products = PageProducts()
page_products.select_result_name("Зимняя шина Белшина Artmotion Snow Бел-14")

