from init_clicker import InitClicker


class PageProducts(InitClicker):
    RESULT_NAME = "//span[contains(@class, 'result__name') and contains(text(), '{}')] "

    def select_result_name(self, result_name):
        locator = self.RESULT_NAME.format(result_name)
        self.clicker.click(locator)

