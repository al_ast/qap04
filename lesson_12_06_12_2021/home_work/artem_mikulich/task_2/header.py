from init_clicker import InitClicker


class Header(InitClicker):
    HEADER = "//a[contains(@href,'https://www.21vek.by/') and text() = '{}']"

    def select_header(self, header):
        locator = self.HEADER.format(header)
        self.clicker.click(locator)

