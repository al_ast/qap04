from clicker import Clicker


class MainPage:
    SELECT_CATEGORY_XPATH = "//a[text() = '{}']"
    CART_BOX = "//a[contains(@class,'headerCartBox')]--> {}"

    def __init__(self):
        self.clicker = Clicker()

    def select_category(self,category):
        locator = self.SELECT_CATEGORY_XPATH.format(category)
        self.clicker.click(locator)

    def cart_box(self,cart_box):
        locator_cart_box = self.CART_BOX.format(cart_box)
        self.clicker.click(locator_cart_box)

