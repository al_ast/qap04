from common_methods import CommonMethods


class Cart(CommonMethods):
    QUANTITY = "//span[contains(@class,'j-basket__{}')]"
    CHECKOUT = "//button[contains(@class, 'button__order') and @data-ga_category='Ordering']"

    def change_product_quantity(self, change_action):
        quantity_locator = self.QUANTITY.format(change_action)
        self._clicker.click(quantity_locator)

    def proceed_to_checkout(self):
        self._clicker.click(self.CHECKOUT)
