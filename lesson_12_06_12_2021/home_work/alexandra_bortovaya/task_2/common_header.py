from common_methods import CommonMethods


class CommonHeader(CommonMethods):
    CATEGORY = "//a[text()='{}']"
    CART = "//a[contains(@class,'headerCartBox')]"

    def select_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        self._clicker.click(category_locator)

    def open_cart(self):
        self._clicker.click(self.CART)
