from common_methods import CommonMethods


class Catalog(CommonMethods):
    PRODUCT = "//span[text()='{}']/parent::a"

    def open_product_page_by_name(self, product_name):
        product_locator = self.PRODUCT.format(product_name)
        self._clicker.click(product_locator)
