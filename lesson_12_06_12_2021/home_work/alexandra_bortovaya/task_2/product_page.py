from common_methods import CommonMethods


class ProductPage(CommonMethods):
    ADD_TO_CART = "//div[contains(@class,'item-buyzone')]//button[@data-ga_action='add_to_cart']"

    def add_product_to_cart(self):
        self._clicker.click(self.ADD_TO_CART)
