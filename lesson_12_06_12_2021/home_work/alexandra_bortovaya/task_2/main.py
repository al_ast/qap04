"""
Выбрать любой сайт
Написать 5-6 действий на разных страницах используя локаторы
Улучшить с использованеим наследования
Использовать готовый класс кликер
"""
from common_header import CommonHeader
from catalog import Catalog
from product_page import ProductPage
from cart import Cart


common_header = CommonHeader()
common_header.select_category("Ёлки")

catalog = Catalog()
catalog.open_product_page_by_name("Ель живая HD Nordic Trees Nordmann Датская Премиум (1.2-1.4м, в горшке)")

product_page = ProductPage()
product_page.add_product_to_cart()

common_header.open_cart()

cart = Cart()
cart.change_product_quantity("plus")
cart.proceed_to_checkout()
