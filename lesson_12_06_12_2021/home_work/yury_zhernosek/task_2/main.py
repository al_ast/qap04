'Сайт https://www.21vek.by/'

from main_page import MainPage
from fridge_page import FridgePage
from tv_page import TvPage

main_page1 = MainPage()
fridge_page1 = FridgePage()
tv_page1 = TvPage()

main_page1.select_category('Телевизоры')
fridge_page1.select_fridge('Холодильник с морозильником LG GA-B419SLGL')
fridge_page1.click_button_home()
tv_page1.select_tv('Телевизор Horizont 32LE5511D')
main_page1.select_category('Холодильники')