from base_page import BasePage

class MainPage(BasePage):

    CATEGORY = "//a[text()='{}']"

    def select_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        self.click(category_locator)