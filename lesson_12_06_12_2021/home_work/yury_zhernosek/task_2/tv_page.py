from base_page import BasePage

class TvPage(BasePage):

    TV = "//span[text()='{}']"

    def select_tv(self, name_tv):
        category_locator = self.TV.format(name_tv)
        self.click(category_locator)