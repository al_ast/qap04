from base_page import BasePage

class FridgePage(BasePage):

    FRIDGE = "//span[text()='{}']"

    def select_fridge(self, name_fridge):
        category_locator = self.FRIDGE.format(name_fridge)
        self.click(category_locator)