from clicker_class import Clicker

class HomePage:
    CATALOGUE = "a[href='https://catalog.onliner.by/']"
    CART = "//div[@id='cart-desktop']/a"

    def __init__(self):
        self.clicker = Clicker()

    def go_to_catalogue(self, catalogue):
        catalogue_locator = self.CATALOGUE.format(catalogue)
        clicker = Clicker()
        clicker.click(catalogue_locator)

    def go_to_cart(self, cart):
        cart_locator = self.CART.format(cart)
        clicker = Clicker()
        clicker.click(cart_locator)