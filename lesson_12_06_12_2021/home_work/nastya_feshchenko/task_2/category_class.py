from clicker_class import Clicker
from home_page_class import HomePage

class Category(HomePage):
    ITEM = "//a[@href='https://catalog.onliner.by/christmasdecor/serpantin/serp1850114']"

    def choose_item(self, item):
        item_locator = self.ITEM.format(item)
        clicker = Clicker()
        clicker.click(item_locator)
