from clicker_class import Clicker
from home_page_class import HomePage

class Catalogue(HomePage):
    CATEGORY = "//li[@class='catalog-navigation-classifier__item ']"

    def choose_category(self, category):
        category_locator = self.CATEGORY.format(category)
        clicker = Clicker()
        clicker.click(category_locator)