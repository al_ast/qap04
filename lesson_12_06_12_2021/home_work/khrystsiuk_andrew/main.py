from clicker import Clicker
from main_page import MainPage
from catalogue import Catalogue
from availability import Availability
from shop_page import ShopPage

nvidia_main_page = MainPage()
nvidia_main_page.select_segment('GeForce GPUs')

nvidia_catalogue = Catalogue()
nvidia_catalogue.choose_product('RTX 3090')

nvidia_availability = Availability()
nvidia_availability.check_availability('Gigabyte GV-N3090TURBO-24GD graphics card')
nvidia_availability.check_shop('Regard')

regard_shop_page = ShopPage()
regard_shop_page.add_to_cart()
regard_shop_page.go_to_cart()
