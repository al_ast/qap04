from clicking import Clicking


class ShopPage(Clicking):

    ADD_TO_CART_LOCATOR = "//a[@id='add_cart']"
    GO_TO_CART_LOCATOR = ADD_TO_CART_LOCATOR.replace("]", "and @title='Перейти в корзину']")

    def add_to_cart(self):
        add_to_cart_locator = self.ADD_TO_CART_LOCATOR
        self._clicker.click(add_to_cart_locator)
        print('Adding to cart...')

    def go_to_cart(self):
        go_to_cart_locator = self.GO_TO_CART_LOCATOR
        self._clicker.click(go_to_cart_locator)
        print('Opening cart.')
