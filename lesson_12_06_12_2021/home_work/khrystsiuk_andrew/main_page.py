from clicking import Clicking


class MainPage(Clicking):

    SEGMENT_LOCATOR = "//a[contains(text(),'Видеокарты GeForce') and @class='mm-link']"

    def select_segment(self, segment_name):
        segment_locator = self.SEGMENT_LOCATOR.format(segment_name)
        self._clicker.click(segment_locator)
        print('Choosing segment...')
