from clicking import Clicking


class Catalogue(Clicking):

    CHOOSE_PRODUCT_LOCATOR = "//div[contains(text(),'RTX 3090') and @class='title no-submenu']"

    def choose_product(self, product_name):
        product_locator = self.CHOOSE_PRODUCT_LOCATOR.format(product_name)
        self._clicker.click(product_locator)
        print('Choosing product...')
