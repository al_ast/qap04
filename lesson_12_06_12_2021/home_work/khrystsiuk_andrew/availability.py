from clicking import Clicking


class Availability(Clicking):

    CHECK_AVAILABILITY_LOCATOR = "//button[contains(text(), 'ПРОВЕРИТЬ НАЛИЧИЕ') and @class='extra_style buy-link 289']"
    CHECK_SHOP_LOCATOR = "//a[contains(text(), 'пРОВЕРИТЬ НАлИЧИЕ')]"

    def check_availability(self, check_button):
        check_button_locator = self.CHECK_AVAILABILITY_LOCATOR.format(check_button)
        self._clicker.click(check_button_locator)
        print('Checking availability...')

    def check_shop(self, check_shop):
        check_chop_locator = self.CHECK_SHOP_LOCATOR.format(check_shop)
        self._clicker.click(check_chop_locator)
        print('Opening shop page...')
