from lesson_12_06_12_2021.home_work.yana_shafar.task_2.base import BaseClicker


class ProductPage(BaseClicker):

    def add_to_busket(self):
        busket_locator = "//button[text()='В корзину']"
        self._clicker.click(busket_locator)

    def click_busket(self):
        busket_locator = "//a[@class='headerCartBox']"
        self._clicker.click(busket_locator)