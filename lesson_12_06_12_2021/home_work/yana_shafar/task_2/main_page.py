from lesson_12_06_12_2021.home_work.yana_shafar.task_2.base import BaseClicker


class MainPage(BaseClicker):
    CATEGORY = "//a[@href='https://www.21vek.by/{}/']"

    def select_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        self._clicker.click(category_locator)
