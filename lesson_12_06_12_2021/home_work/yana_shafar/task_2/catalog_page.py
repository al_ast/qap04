from lesson_12_06_12_2021.home_work.yana_shafar.task_2.base import BaseClicker


class CatalogPage(BaseClicker):
    PRODUCT = "//span[text()='{}']"

    def select_product(self, product_name):
        product_locator = self.PRODUCT.format(product_name)
        self._clicker.click(product_locator)
