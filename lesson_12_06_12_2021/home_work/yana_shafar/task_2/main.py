from lesson_12_06_12_2021.home_work.yana_shafar.task_2.busket_page import BusketPage
from lesson_12_06_12_2021.home_work.yana_shafar.task_2.catalog_page import CatalogPage
from lesson_12_06_12_2021.home_work.yana_shafar.task_2.checkout_page import CheckoutPage
from lesson_12_06_12_2021.home_work.yana_shafar.task_2.main_page import MainPage
from lesson_12_06_12_2021.home_work.yana_shafar.task_2.product_page import ProductPage

main_page = MainPage()
main_page.select_category("tv")

catalog_page = CatalogPage()
catalog_page.select_product('Телевизор Horizont 32LE5511D')

product_page = ProductPage()
product_page.add_to_busket()
product_page.click_busket()

busket_page = BusketPage()
busket_page.checkout()

checkout_page = CheckoutPage()
checkout_page.confirm_the_order()

