from lesson_12_06_12_2021.home_work.yana_shafar.task_2.base import BaseClicker


class BusketPage(BaseClicker):
    def checkout (self):
        checkout_locator = "//button[@id='j-basket__ok']"
        self._clicker.click(checkout_locator)