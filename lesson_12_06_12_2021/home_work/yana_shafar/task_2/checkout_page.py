from lesson_12_06_12_2021.home_work.yana_shafar.task_2.base import BaseClicker


class CheckoutPage(BaseClicker):
    def confirm_the_order(self):
        confirm_locator = "//button[@id='j-basket__confirm']"
        self._clicker.click(confirm_locator)
