from clicker import Clicker
class Registration:
    GO_TO_REGISTRATION = "//a[@class='button-style button-style_small cart-form__button button-style_primary']"

    def __init__(self):
        self._clicker = Clicker()

    def go_to_registation(self, registration):
        locator = self.GO_TO_REGISTRATION.format(registration)
        self._clicker.click(locator)