from clicker import Clicker
class MainPage:
    CATEGORY = "//span[contains(text(),'Телевизоры')]"

    def __init__(self):
        self._clicker = Clicker()

    def select_category(self,category_name):
        category_locator = self.CATEGORY.format(category_name)
        self._clicker.click(category_locator)
