from clicker import Clicker
class Basket:
    CHOICE_OF_SUBJECT = "//span[contains(text(),'Телевизор Samsung QE65QN90AAU')]"
    ADD_TO_BASKET = "//a[@class='button-style button-style_base-alter product-aside__item-button button-style_expletive']"

    def __init__(self):
        self._clicker = Clicker()

    def add_to_basket(self,item_name):
        locator = self.ADD_TO_BASKET.format(item_name)
        self._clicker.click(locator)

