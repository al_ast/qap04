from clicker import Clicker
class Payment:
    PAY = "//button[@class='button-style button-style_primary button-style_base cart-form__button cart-form__button_responsive']"


    def __init__(self):
        self._clicker = Clicker()

    def pay(self,payment):
        locator = self.PAY.format(payment)
        self._clicker.click(locator)
