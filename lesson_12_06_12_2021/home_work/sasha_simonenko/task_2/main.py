from clicker import Clicker
from main_page import MainPage
from basket import Basket
from registration import Registration
from payment import Payment

clicker = Clicker()

main_page = MainPage()
main_page.select_category("Телевизоры")

basket = Basket()
basket.add_to_basket("Телевизор Samsung QE43QN90AAU")

registration = Registration()
registration.go_to_registation("Перейти к оформлению")

payment = Payment()
payment.pay("Оплатить")

