from task_2.clicker import Clicker


class HeaderPage(Clicker):
    _CART_HEADER_LOCATOR = "//u[text()='Корзина']"

    def check_cart(self):
        self.click(self._CART_HEADER_LOCATOR)
        
