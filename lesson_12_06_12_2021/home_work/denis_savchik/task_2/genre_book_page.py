from task_2.clicker import Clicker


class GenreBookPage(Clicker):
    _BOOK_LOCATOR = "//img[@alt='{}']"

    def select_book(self, book_name):
        locator = self._BOOK_LOCATOR.format(book_name)
        self.click(locator)
