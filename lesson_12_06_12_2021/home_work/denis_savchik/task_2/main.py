# Задание2:
# Выбрать любой сайт
# Написать 5-6 действий на разных страницах используя локаторы
# см пример в class_materials/pages
# Улучшить с использованеим наследования
# Использовать готовый класс кликер

from task_2.book_page import BookPage
from task_2.books_category_page import BooksCategoryPage
from task_2.cart_page import CartPage
from task_2.genre_book_page import GenreBookPage
from task_2.header_page import HeaderPage
from task_2.main_page import MainPage


main_page = MainPage()
books_category_page = BooksCategoryPage()
genre_book_page = GenreBookPage()
book_page = BookPage()
header_page = HeaderPage()
cart_page = CartPage()


main_page.select_category("Книги")
books_category_page.select_genre("Художественная литература")
genre_book_page.select_book("Гарри Поттер и философский камень")
book_page.put_to_cart()
header_page.check_cart()
cart_page.enter_phone_number("12318743121")
cart_page.checkout_order()




