from task_2.clicker import Clicker


class MainPage(Clicker):
    _CATEGORY_LOCATOR = "//a[contains(@class,'menu-link-action main-nav__list__item') and text()='{}']"

    def select_category(self, category_name):
        locator = self._CATEGORY_LOCATOR.format(category_name)
        self.click(locator)
