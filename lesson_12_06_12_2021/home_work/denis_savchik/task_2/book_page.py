from task_2.clicker import Clicker


class BookPage(Clicker):
    _PUT_TO_CART_LOCATOR = "//span[contains(text(),'Положить в')]"

    def put_to_cart(self):
        self.click(self._PUT_TO_CART_LOCATOR)
