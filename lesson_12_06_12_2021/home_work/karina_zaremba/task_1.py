
URL = "https://kinogo.la/34451-venom___2.html"


description = "//div[@class='lenta']/following-sibling::div/span"
css_description = "a[ href$='KINOGO_BY.jpg'] ~ span"

name_of_the_movie = "//h1/child::span[@class='selection_index']"
css_name_of_the_movie = "div.fullstory h1"

rating_film = "//div[@class='rating']/ul/*[last()]/child::a"
css_rating_film = "a[title='Отлично']"

fullstory = "//div[@class='rating']/ancestor::div[@class='fullstory']"
css_fullstory = ".fullstory"

last_recommendation = "//ul[@class='ul_related']/li[last()]"
css_last_recommendation = "ul.ul_related li ~ li ~ li ~ li ~ li"

