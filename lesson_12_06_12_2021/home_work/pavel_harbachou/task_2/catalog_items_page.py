from method_clicker import MethodClicker

class CatalogItemsPage(MethodClicker):
    PRODUCT = "//img[contains(@alt,'{}')]"


    def select_product(self, product_name):
        product_locator = self.PRODUCT.format(product_name)
        self.clicker.click(product_locator)

