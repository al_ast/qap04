from method_clicker import MethodClicker

class ProductPage(MethodClicker):
    ADD_TO_CART_BUTTON = "//div[@title='В КОРЗИНУ!']"


    def add_to_cart(self):
        locator = self.ADD_TO_CART_BUTTON
        self.clicker.click(locator)
