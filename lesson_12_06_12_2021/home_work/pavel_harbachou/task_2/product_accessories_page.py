from method_clicker import MethodClicker

class ProductAccessoriesPage(MethodClicker):
    PLACE_AN_ORDER_BUTTON = "//div[@title='Заказать товар']"

    def place_an_order(self):
        locator = self.PLACE_AN_ORDER_BUTTON
        self.clicker.click(locator)
