from method_clicker import MethodClicker

class CartPage(MethodClicker):
    CONFIRM_ORDER_BUTTON = "//input[@value='Подтвердить заказ']"

    def confirm_order(self):
        locator = self.CONFIRM_ORDER_BUTTON
        self.clicker.click(locator)
