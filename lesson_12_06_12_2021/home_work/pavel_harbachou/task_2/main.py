# Выбрать любой сайт
# Написать 5-6 действий на разных страницах используя локаторы
# см пример в class_materials/pages
# Улучшить с использованеим наследования
# Использовать готовый класс кликер

# https://sila.by/

from main_page import MainPage
from catalog_items_page import CatalogItemsPage
from product_page import ProductPage
from product_accessories_page import ProductAccessoriesPage
from cart_page import CartPage

main_page = MainPage()
main_page.select_category('Стиральные машины')

catalog_items_page = CatalogItemsPage()
catalog_items_page.select_product('SAMSUNG WW70A6S23AN/LP')

product_page = ProductPage()
product_page.add_to_cart()

product_accessories_page = ProductAccessoriesPage()
product_accessories_page.place_an_order()

cart_page = CartPage()
cart_page.confirm_order()

main_page.go_mobile_version()
