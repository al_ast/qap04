from method_clicker import MethodClicker

class MainPage(MethodClicker):
    CATEGORY = "//div[@class='header_menu__item']//a[text()='{}']"
    MOBILE_VERSION_BUTTON = "//a[@class='mob_ver']"

    def select_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        self.clicker.click(category_locator)

    def go_mobile_version(self):
        locator = self.MOBILE_VERSION_BUTTON
        self.clicker.click(locator)
