#https://www.21vek.by/
from lesson_12_06_12_2021.home_work.clicker import Clicker


class MainPage:
    CATEGORY = "//a[text()='{}']"

    def __init__(self):
        self._clicker = Clicker()

    def select_category(self, category_name):
        category_locator = self.CATEGORY.format(category_name)
        self._clicker.click(category_locator)
