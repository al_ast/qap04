from lesson_12_06_12_2021.home_work.clicker import Clicker


class CatalogItemsPage:
    ITEM_SECTION_LOCATOR = "//span[text()='{}']/ancestor::dl"
    ADD_TO_CART_BUTTON = ITEM_SECTION_LOCATOR + "//button[@data-ga_action='add_to_cart']"

    def __init__(self):
        self._clicker = Clicker()

    def add_to_cart(self, item_name):
        locator = self.ADD_TO_CART_BUTTON.format(item_name)
        self._clicker.click(locator)
