from lesson_12_06_12_2021.class_materials.pages.catalog_items_page import CatalogItemsPage
from lesson_12_06_12_2021.class_materials.pages.main_page import MainPage

main_page = MainPage()
main_page.select_category("Телевизоры")

fridges_page = CatalogItemsPage()
fridges_page.add_to_cart("Холодильник с морозильником Indesit TIA 140")