# Перевести строку в массив по пробеллу
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "lists", "they", "are", "my", "favorite"]

sentence = "I love arrays they are my favorite"
new = sentence.split(" ")
print(f"Текст после выполнения метода split: {new}")

sentence1 = "Кто ничего не делает, тот лентяй"
new1 = sentence1.split(" ")
print(f"Текст после выполнения метода split: {new1}")