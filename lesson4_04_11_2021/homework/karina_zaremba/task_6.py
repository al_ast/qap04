# Дана строка. Получите из нее новую строку используя элементы с нечетным индексом и уберите из нее все пробелы. Использовать срезы
# "Моя крутая строка" => "о ртясрл" => убираем пробеллы "ортясрл"
main_string = "Моя крутая строка"
new_string = main_string[1::2]
print(new_string)
split_string = new_string.split(" ")
print(split_string)
print("".join(split_string))


