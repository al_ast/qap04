# С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
# Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение
expression = 3 + 4 - 7
attempt = 0
while expression == 0:
    user_answer = int(input('решите задачу: 3+4-7=? '))
    if user_answer != expression:
        print('Неверное решение')
        attempt += 1
        if attempt == 3:
            print('У вас закончились попытки')
            break
    else:
        print('Правильно')
        break




