#1. Пользователь вводит имя файла с расширением
# например my_file.txt
# Программа должна вывести какой это файл:
# txt -> Вы передали текстовый файл
# xlsx -> Вы передали эксел файл
# exe - > Вы передали исполняемый файл
#
# если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"
file_name = input('enter file name with extension : ')
file_name_split = file_name.split('.')
file_extension = file_name_split[-1]
if file_extension == 'txt':
    print('Вы передали текстовый файл')
elif file_extension == 'xlsx':
    print('Вы передали эксел файл')
elif file_extension == 'exe':
    print('Вы передали исполняемый файл')
else:
    print(f'Мы не поддерживаем .{file_extension} формат')
