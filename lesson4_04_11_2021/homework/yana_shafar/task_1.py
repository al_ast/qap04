def check_file(file_name):
    file_extension = file_name.split('.')[-1]
    if file_extension == 'txt':
        print(f'Вы передали текстовый файл')
    elif file_extension == 'xlsx':
        print(f'Вы передали эксел файл')
    elif file_extension == 'exe':
        print(f'Вы передали исполняемый файл')
    else:
        print(f'Мы не поддерживаем .{file_extension} формат')


input_file = input('Input file name: ')
check_file(input_file)
