try_count = 4
true_answer = 2
loop = True

print(f'Решите задачу: 7-5')
print(f'Количество попыток: {try_count}')

while loop:
    answer = input('Ваш ответ: ')
    if int(answer) != true_answer:
        print('Неверное решение')
        try_count -= 1
        if try_count == 0:
            print('У вас закончились попытки')
            loop = False
    else:
        print('Правильно')
        loop = False

    


