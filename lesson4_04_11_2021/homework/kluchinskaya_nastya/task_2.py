# 2.С помощью цикла for заполнить список нечетными числами от 1 до 100. Вывести список на экран

for number in range(1, 100):
    if number % 2 == 1:
        print(number, end=" ")

print()

# second variation using list method
odd_numbers = []
for number in range(1, 101):
    if number % 2 != 0:
        odd_numbers.append(number)
        print(number,  end=" ")