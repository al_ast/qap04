# 1. Пользователь вводит имя файла с расширением
# например my_file.txt
# Программа должна вывести какой это файл:
# txt -> Вы передали текстовый файл
# xlsx -> Вы передали эксел файл
# exe - > Вы передали исполняемый файл
#если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"

file = input("Enter file name with extension: ")
extension = file.split(".")
if extension[1] == "txt":
    print("You enter text file.")
elif extension[1] == "xlsx":
    print("You enter excel file.")
elif extension[1] == "exe":
    print("You enter executable file.")
else:
    print(f"Don't support {extension[1]} files.")