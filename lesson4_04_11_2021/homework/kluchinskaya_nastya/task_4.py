# 4. Перевести строку в массив по пробеллу
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "lists", "they", "are", "my", "favorite"]


string = "Robin Singh"
list1 = string.split(" ")
print(list1)

string2 = "I love arrays they are my favorite"
print(string2.split(" "))
