'''7*.Вам передан массив чисел.
Известно, что каждое число в этом массиве имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5'''
nums = [1, 5, 2, 9, 2, 9, 1]
s_nums = sorted(nums)

for i, num in enumerate(s_nums[::2]):
    if num != s_nums[i+1]:
        print(num)
        break