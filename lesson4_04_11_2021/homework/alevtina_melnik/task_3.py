'''3.С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение'''
import random

n = 3
Solve_example = f"{random.randrange(0, 100, 1)}{random.choice(['-', '+'])}{random.randrange(0, 100, 1)}{random.choice(['-', '+'])}{random.randrange(0, 100, 1)}"

result = eval(Solve_example)

user_value = input(f"Решите пример: {Solve_example} = ?: ")

while n > 1:
    try:
        if int(user_value) == result:
            print("Верно. Поздравлеяем! :)")
            break
        else:
            n = n - 1
            user_value = input(f"Неверно, у Вас осталось {n} попыток: ")
    except:
        user_value = input(f"Ошибка. Введено не число, повторите попытку: ")
else:
    print("К сожалению у Вас закончились попытки ввода :(")