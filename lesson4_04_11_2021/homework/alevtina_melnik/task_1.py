'''1. Пользователь вводит имя файла с расширением
 например my_file.txt
 Программа должна вывести какой это файл:
 txt -> Вы передали текстовый файл
 xlsx -> Вы передали эксел файл
 exe - > Вы передали исполняемый файл
 если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"'''

input_text = input("Enter value: ")
Split_Format = input_text.split('.')
if Split_Format[1]=='txt':
    print('Вы передали текстовый файл')
elif Split_Format[1]=='xlsx':
    print('Вы передали эксел файл')
elif Split_Format[1]=='exe':
    print('Вы передали исполняемый файл')
else:
    print(f'Мы не поддерживаем {Split_Format[1]} формат')

