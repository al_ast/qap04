# 6. Дана строка. Получите из нее новую строку используя элементы
# с нечетным индексом и уберите из нее все пробелы. Использовать срезы
# "Моя крутая строка" => "о ртясрл" => убираем пробеллы "ортясрл"

input_string = input("Enter any string: ")
input_string_sl = input_string[1::2]
print(input_string_sl)
input_string_slice_rep = input_string_sl.replace(" ", "")
print(input_string_slice_rep)
