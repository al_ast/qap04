# 4. Перевести строку в массив по пробеллу
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "lists", "they", "are", "my", "favorite"]

given_list_1 = "Robin Singh"
given_list_2 = "I love arrays they are my favorite"
split_list_1 = given_list_1.split(" ")
split_list_2 = given_list_2.split(" ")
print(split_list_1)
print(split_list_2)
