# 3.С помощью цикла while просите пользователя решить пример,
# пока он не введет правильный ответ.

print("Решите пример: 3+4-7:")
input_number = int(input())
correct_answer = 3 + 4 - 7
while input_number != correct_answer:
    print("Неверное решение")
    input_number = int(input())
print("Верно!")

# задача с количеством попыток
number_of_try = 3                                                  #количество попыток
print(f"У вас {number_of_try} попытки \nРешите пример: 3+4-7:")
input_number = int(input())
correct_answer = 3 + 4 - 7
while input_number != correct_answer and number_of_try != 1:
    print("Неверное решение")
    input_number = int(input())
    number_of_try -= 1
if input_number != correct_answer:
    print("У вас закончились попытки. Выходим из приложения")
else:
    print("Верно!")
