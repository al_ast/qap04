# 1. Пользователь вводит имя файла с расширением
# например my_file.txt
# Программа должна вывести какой это файл:
# txt -> Вы передали текстовый файл
# xlsx -> Вы передали эксел файл
# exe - > Вы передали исполняемый файл
# если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"

file_name = input("Enter name of file ")
file_extension = file_name.split(".")
if file_extension[-1] == "txt":
    print("Вы передали текстовый файл")
elif file_extension[-1] == "xlsx":
    print("Вы передали эксел файл")
elif file_extension[-1] == "exe":
    print("Вы передали исполняемый файл")
else:
    print(f"Мы не поддерживаем .{file_extension[-1]} формат")