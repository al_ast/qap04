name_file = 'Homework.exe'
splitting = name_file.split('.')
if splitting[1] == 'txt':
    print('This is a text file')
elif splitting[1] == 'xlsx':
    print('This is an excel file')
elif splitting[1] == 'exe':
    print('This is an executable file')
else:
    print(f'We do not support "{splitting[1]}"')