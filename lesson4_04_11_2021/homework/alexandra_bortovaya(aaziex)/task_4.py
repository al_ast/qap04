"""
Перевести строку в массив по пробеллу
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" => ["I", "love", "lists", "they", "are", "my", "favorite"]
"""

user_string = "Lorem ipsum dolor sit amet"
print(user_string.split())
