"""
Дан список ["I", "love", "lists", "they", "are", "my", "favorite"] сделайте из него строку =>
"I love arrays they are my favorite"
"""

user_list = ["I", "love", "lists", "they", "are", "my", "favorite"]
new_string = " ".join(user_list)
new_string = new_string.replace("lists", "arrays")

print(new_string)
