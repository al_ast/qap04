"""
Пользователь вводит имя файла с расширением
например my_file.txt
Программа должна вывести какой это файл:
txt -> Вы передали текстовый файл
xlsx -> Вы передали эксел файл
exe - > Вы передали исаолняемый файл
если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"
"""

user_input = "my_file.name.csv"

split_input = user_input.split(".")
extension = split_input[-1]

if extension == "txt":
    print("Вы передали текстовый файл")
elif extension == "xlsx":
    print("Вы передали эксел файл")
elif extension == "exe":
    print("Вы передали исполняемый файл")
else:
    print(f"Мы не поддерживаем .{extension} формат")
