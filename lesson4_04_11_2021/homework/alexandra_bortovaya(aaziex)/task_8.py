"""
Дан текст, который содержит различные английские буквы и знаки препинания. Вам необходимо найти самую частую букву
в тексте. Результатом должна быть буква в нижнем регистре.
При поиске самой частой буквы, регистр не имеет значения, так что при подсчете считайте, что "A" == "a".
Убедитесь, что вы не считайте знаки препинания, цифры и пробелы, а только буквы.
Если в тексте две и больше буквы с одинаковой частотой, тогда результатом будет буква, которая идет первой в алфавите.
Для примера, "one" содержит "o", "n", "e" по одному разу, так что мы выбираем "e".

"a-z" == "a"
"Hello World!" == "l"
"How do you do?" == "o"
"One" == "e"
"Oops!" == "o"
"AAaooo!!!!" == "a"
"a" * 9000 + "b" * 1000 == "a"

Подсказка: можно почитать про словари
"""
text = "!@$# jwhdg dh zzz aaa 2222222222545"

# Переводим текст в нижний регистр и удаляем все, кроме букв
text = text.lower()
for sign in text:
    if not sign.isalpha():
        text = text.replace(sign, "")

# Составляем словарь из всех букв и количества их упоминаний
letter_count = {}
for letter in text:
    count = text.count(letter)
    letter_count[letter] = count

# Находим максимально кол-во упоминаний букв/буквы
max_count = 0
for letter in letter_count:
    if letter_count[letter] > max_count:
        max_count = letter_count[letter]

# Составляем список из букв/буквы с максимальным количеством упоминаний
letters_with_max_mention = []
for letter in letter_count:
    if letter_count[letter] == max_count:
        letters_with_max_mention.append(letter)

# Сортируем финальный список в алфавитном порядке
final_list = sorted(letters_with_max_mention)

print(f"'{final_list[0]}' mentioned {max_count} times")
