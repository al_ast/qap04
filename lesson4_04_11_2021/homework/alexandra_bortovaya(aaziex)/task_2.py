"""
С помощью цикла for заполнить список нечетными числами от 1 до 100. Вывести список на экран
"""
number_list = []
for number in range(1, 101):
    if number % 2:
        number_list.append(number)

print(number_list)
