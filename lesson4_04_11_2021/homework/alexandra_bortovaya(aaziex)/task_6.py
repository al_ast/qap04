"""
Дана строка. Получите из нее новую строку используя элементы с нечетным индексом и уберите из нее все пробелы.
Использовать срезы
"Моя крутая строка" => "о ртясрл" => убираем пробеллы "ортясрл"
"""

user_string = "I'm not a fan of puppeteers, but I've a nagging fear someone else is pulling at the strings"
new_string = user_string[::2]
new_string = "".join(new_string.split(" "))

print(new_string)
