"""
С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение
"""
task = "3+4-7"
expected_answer = str(eval(task))
max_tries = 5
current_try = 0
is_correct_answer_is_given = False

print(f"Решите задачу: {task}=?")

while current_try < max_tries and not is_correct_answer_is_given:
    user_answer = input("Введите ответ:")
    is_correct_answer_is_given = user_answer == expected_answer
    current_try += 1

    if not is_correct_answer_is_given:
        print("Ответ неверный.")

if is_correct_answer_is_given:
    print("Вы ввели правильный ответ")
else:
    print("У вас закончились попытки")
