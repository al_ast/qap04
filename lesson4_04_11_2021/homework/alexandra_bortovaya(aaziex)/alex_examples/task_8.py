"""
Дан текст, который содержит различные английские буквы и знаки препинания. Вам необходимо найти самую частую букву
в тексте. Результатом должна быть буква в нижнем регистре.
При поиске самой частой буквы, регистр не имеет значения, так что при подсчете считайте, что "A" == "a".
Убедитесь, что вы не считайте знаки препинания, цифры и пробелы, а только буквы.
Если в тексте две и больше буквы с одинаковой частотой, тогда результатом будет буква, которая идет первой в алфавите.
Для примера, "one" содержит "o", "n", "e" по одному разу, так что мы выбираем "e".

"a-z" == "a"
"Hello World!" == "l"
"How do you do?" == "o"
"One" == "e"
"Oops!" == "o"
"AAaooo!!!!" == "a"
"a" * 9000 + "b" * 1000 == "a"

Подсказка: можно почитать про словари
"""
result_dict = dict()

text = "!@$# jwhdg dh zzz aaa 2222222222545"
unique_characters = ''.join(set(text))
unique_characters = unique_characters.lower()

for character in unique_characters:
    if character.isalpha():
        result_dict[character] = text.count(character)

max_count = max(result_dict.values())
result_letters = []

for character, count in result_dict.items():
    if count == max_count:
        result_letters.append(character)

sorted_letters = sorted(result_letters)
print(f"'{sorted_letters[0]}' mentioned {max_count} times")
