"""
С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение
"""
task = "3+4-7"
expected_answer = str(eval(task))
max_tries = 5
current_try = 0

print(f"Решите задачу: {task}=?")

while True:
    user_answer = input("Введите ответ:")

    if user_answer == expected_answer:
        print("Вы ввели правильный ответ")
        break
    else:
        print("Вы ввели неправильный ответ")

    current_try += 1
    if current_try == max_tries:
        print("У вас закончились попытки")
        break
