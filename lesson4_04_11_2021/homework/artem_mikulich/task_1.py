#Пользователь вводит имя файла с расширением
#например my_file.txt
#Программа должна вывести какой это файл:
#txt -> Вы передали текстовый файл
#xlsx -> Вы передали эксел файл
#exe - > Вы передали исполняемый файл
#если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"

file_name = input("Type file name")
file_name_split = file_name.split(".")
file_name1 = file_name_split[1]
if file_name1 == "txt":
    print(f"You have sent a text file: {file_name}")
elif file_name1 == "xlsx":
    print(f"You have sent a excel file: {file_name}")
elif file_name1 == "exe":
    print(f"You have sent a executable file: {file_name}")
else:
    print(f"We do not support {file_name1} format")
