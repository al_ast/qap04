#Перевести строку в массив по пробеллу
string1 = ("Robin Singh")
string2 = ("I love arrays they are my favorite")
string1_split = string1.split(" ")
string2_split = string2.replace("arrays", "lists").split(" ")

print(f"string1 after split: {string1_split}")
print(f"string2 after split: {string2_split}")