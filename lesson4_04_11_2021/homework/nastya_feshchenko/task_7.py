#7*.Вам передан массив чисел.
#Известно, что каждое число в этом массиве имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5

numbers = [1, 5, 2, 9, 2, 9, 1]

a = 0
for number in numbers:
    a = numbers.count(number)
    if a == 1:
        print(f"{number} has no pair")