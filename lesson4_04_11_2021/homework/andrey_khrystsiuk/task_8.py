# Дан текст, который содержит различные английские буквы и знаки препинания. Вам необходимо найти самую частую букву
# в тексте. Результатом должна быть буква в нижнем регистре.
# При поиске самой частой буквы, регистр не имеет значения, так что при подсчете считайте, что "A" == "a".
# Убедитесь, что вы не считайте знаки препинания, цифры и пробелы, а только буквы.
# Если в тексте две и больше буквы с одинаковой частотой, тогда результатом будет буква, которая идет первой в алфавите.
# Для примера, "one" содержит "o", "n", "e" по одному разу, так что мы выбираем "e".

text = 'This standard basis makes the complex numbers a Cartesian plane, called the complex plane.' \
    'This allows a geometric interpretation of the complex numbers and their operations.' \
    'some numbers and symbols for example:135047512890510267310847!!@#!#%*!!!$@!@#!@#&*&*%@$^'

text = text.lower()
for extra in text:
    if not extra.isalpha():
        text = text.replace(extra, '')

letter_list = {}
for letters in text:
    count = text.count(letters)
    letter_list[letters] = count

max_count = 0
for letters in letter_list:
    if letter_list[letters] > max_count:
        max_count = letter_list[letters]

max_mention = []
for letters in letter_list:
    if letter_list[letters] == max_count:
        max_mention.append(letters)

print(f'Letter {max_mention} is a most mentioned letter with {max_count} mentions')
