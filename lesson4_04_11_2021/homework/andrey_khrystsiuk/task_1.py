# 1. Пользователь вводит имя файла с расширением
# например my_file.txt
# Программа должна вывести какой это файл:
# txt -> Вы передали текстовый файл
# xlsx -> Вы передали эксел файл
# exe - > Вы передали исполняемый файл
#
# если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"

file = input()
split_file = file.split(".")
extension = split_file[-1]

if extension == "txt":
    print("You gave text file")
elif extension == "xlsx":
    print("You gave excel file")
elif extension == "exe":
    print("You gave executable file")
else:
    print(f"We do not support .{extension} files")