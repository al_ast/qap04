# 3.С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
# Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение

n = 0
number = 0
answer = 732
while number != answer:
    number = int(input('28 / 2 * 8 + 620 = '))
    n += 1
    if number == answer:
        print('Right answer')
    elif number != answer:
        print('Try again')
        if n == 5:
            print('You lose')
            break
