# 2.С помощью цикла for заполнить список нечетными числами от 1 до 100. Вывести список на экран

odds_list = []
for num in range(100):
    if num % 2 != 0:
        odds_list.append(num)
print(odds_list)

# Also can be:

# odds_list = []
# for num in range(1, 100, 2):
#     odds_list.append(num)
# print(odds_list)
