# 7*.Вам передан массив чисел.
# Известно, что каждое число в этом массиве имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5

massive = [7, 2, 7, 2, 3, 3, 1, 5, 2, 9, 2, 1, 5]
for no_pair in massive:
    if massive.count(no_pair) == 1:
        print(f'{no_pair} has no pair')
