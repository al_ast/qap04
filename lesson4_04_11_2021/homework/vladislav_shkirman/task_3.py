# 3.С помощью цикла while просите пользователя решить пример, пока он не введет правильный ответ.
# Так же у пользователя есть заданное количество попыток. Если он их использовал, то вывести об этом сообщение

expression = 3 + 4 - 5
attempt = 0
print('Solve example: 3 + 4 - 5 = ?\nNumber of attempt is 4\n')
while expression == 2:
    result = int(input('Your result: '))
    if result != expression:
        attempt += 1
        print('Wrong answer')
        print(f'Attempt is number {attempt}\n')
        if attempt == 4:
            print('You are out of attempts')
            break
    else:
        print('Correct!\nExit from the application')
        break
