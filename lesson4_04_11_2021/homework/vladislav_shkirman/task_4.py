# 4. Перевести строку в массив по пробеллу
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "lists", "they", "are", "my", "favorite"]

string1 = 'Vlad Shkirman'
print((string1.split()))

string2 = 'This is a nice string'
print((string2.replace('string', 'list').split()))
