# 8**.
# Дан текст, который содержит различные английские буквы и знаки препинания. Вам необходимо найти самую частую букву
# в тексте. Результатом должна быть буква в нижнем регистре.
# При поиске самой частой буквы, регистр не имеет значения, так что при подсчете считайте, что "A" == "a".
# Убедитесь, что вы не считайте знаки препинания, цифры и пробелы, а только буквы.
# Если в тексте две и больше буквы с одинаковой частотой, тогда результатом будет буква, которая идет первой в алфавите.
# Для примера, "one" содержит "o", "n", "e" по одному разу, так что мы выбираем "e".
#
# "a-z" == "a"
# "Hello World!" == "l"
# "How do you do?" == "o"
# "One" == "e"
# "Oops!" == "o"
# "AAaooo!!!!" == "a"
# "a" * 9000 + "b" * 1000 == "a"
#
# Подсказка: можно почитать про словари
import re

text = 'This is some random text. Obviously, it has letters within.aaaaAXXXXXX.999'
text_letters_and_numbers = text.replace(' ', '').replace(',', '').replace('.', '').lower()
pattern = r'[0-9]'
text_letters = re.sub(pattern, '', text_letters_and_numbers)    # getting rid of numbers in string
dictionary = {}
for i in text_letters:
    dictionary[i] = text_letters.count(i)
print(f'All letters and their quantity in the text:\n {dictionary}')
dict_sorted = dict(sorted(dictionary.items()))              # sorted dictionary from A to Z without any other symbols
print(f''
      f'\nThe most frequent letter in text is "{max(dict_sorted, key=dict_sorted.get)}" '
      f'with the highest value {max(dict_sorted.values())}')
