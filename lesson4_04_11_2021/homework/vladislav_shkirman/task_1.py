# 1. Пользователь вводит имя файла с расширением
# например my_file.txt
# Программа должна вывести какой это файл:
# txt -> Вы передали текстовый файл
# xlsx -> Вы передали эксел файл
# exe - > Вы передали исполняемый файл
#
# если передан какой-то другой формат например: my_file.csv -> "Мы не поддерживаем .csv формат"

file = input('Enter file name with extension: ')
file_list = file.split('.')
if file_list[1] == 'txt':
    print('You entered text file')
elif file_list[1] == 'xlsx':
    print('You entered excel file')
elif file_list[1] == 'exe':
    print('You entered executable file')
else:
    print(f'We do not support {file_list[1]} format')
