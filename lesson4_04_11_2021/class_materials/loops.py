students = ["Student1", "Student2", "Student3"]

for current_student in students:
    student_name = current_student + "_good"
    print(student_name)
    # кода нет

numbers = [1, 2, 3, 4, 5]
output = []
for current_number in numbers:
    output.append(current_number)

print(output)
print("While:")
number = 1
while number < 6:
    print(number)
    number += 1

# идет сюда

# a = 5
#
# # while a < 10:
# #     print(a)
# #     a += 1
#
# # a = 5
# # while a < 10:
# #     print(f"Current {a=}")
# #
# #     if a == 8:
# #         print(f"Leave(break) while with {a=}")
# #         break
# #
# #     print(f"Code after break. {a=}")
# #
# #     a += 1
# #
# #     if a == 7:
# #         print(f"Continue while. Next iteration {a=}")
# #         continue
# #
# numbers_range = range(10)
# #
# # for number in numbers_range:
# #     print(f"For {number=}")
#
#
# for number in numbers_range:
#     for number2 in numbers_range:
#         print(f"For {number=}. {number2=}")
#         if number2 == 4:
#             break
#
# for number in numbers_range:
#     for number2 in numbers_range:
#         print(f"For {number=}. {number2=}")
#         if number2 == 4:
#             continue
