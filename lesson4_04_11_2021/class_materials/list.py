students_group1 = ["Vasya", "Lena"]
students_group2 = ["Masha", "Petya"]
students_group3 = ["Ivan"]
print(f"Students group1: {students_group1}")



# Access by index
print(f"Access by index:{students_group1[0]}")

# Append
students_group1.append("Kolya")
print(f"Students group1 after append(): {students_group1}")

# +
new_group = students_group1 + students_group2
print(f"new group after using + for list: {new_group}")

# Extend
students_group1.extend(students_group2)
print(f"Students group1 after extend() group1: {students_group1}")
print(f"Students group2 after extend() group2: {students_group2}")


# # Remove

# students_group1.remove("Elena")
# print(f"Students group1 after remove() Elena: {students_group1}")

students_group1.remove("Lena")
print(f"Students group1 after remove() Lena: {students_group1}")

# Pop
students_group1.pop(-1)
print(f"Students group1 after pop(1): {students_group1}")

# Count
students_group1.extend(["Igor", "Igor", "Igor"])
print(f"Igors count in the group. count(Igor): {students_group1.count('Igor')}")
