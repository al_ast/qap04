# Replace
wrong_string = "My name wrongWord is Alex wrongWord"
correct_string = wrong_string.replace("wrongWord", "correctWord")
print(f"wrong_string after replace wrongWord:{wrong_string}")
print(f"correct string after replace wring word: {correct_string}")

# Split
sentence = "Hello. My name is Alex. I'm from Belarus"
words_split_by_space = sentence.split(" ")
words_split_by_default = sentence.split()
print(f"Sentence after split:{sentence}")
print(f"words after split sentence by spaces:{words_split_by_space}")
print(f"words after split  sentence be default:{words_split_by_default}")

url = "https://domen//domen1/domen2//domen3"
urls_parts = url.split("//")
print(f"Split urls by //: {urls_parts}")

# Join
words = ["My", "Name", "is", "Alex"]
join_by_some_word_separator = " SEPARATOR ".join(words)
join_by_space = " ".join(words)
join_by_empty_string = "".join(words)

print(f"List after joins:{words}")
print(f"Join by some text separator: {join_by_some_word_separator}")
print(f"Join by space: {join_by_space}")
print(f"Join by empty string: {join_by_empty_string}")
