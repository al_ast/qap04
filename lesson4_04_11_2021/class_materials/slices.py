# Нам нужен любой массив, или строка(строка является массивом внутри питона)
# Синтаксис item_to_slice[start_index:stop_index:step]
# start включительно, stop_index
my_string = "Hello world"

slice_with_step_1 = my_string[0:-1:1]
print(f"Slice from start till end with step=1,last is not included:{slice_with_step_1}")

slice_with_step_2 = my_string[0:-1:2]
print(f"Slice from start till end with step=2,last is not included:{slice_with_step_1}")

all_items_slice_with_step_1 = my_string[::1]
print(f"Slice from start till end with step=1,all included:{all_items_slice_with_step_1}")

revert_string = my_string[::-1]
print(f"Slice from start till end with step=-1,last is not included:{revert_string}")

slice_2_letters = my_string[:2:]
print(f"Slice from start to select 2 characters:{slice_2_letters}")

letters_list = ["A", "B", "C"]
slice_letters_list = letters_list[::2]
print(f"Slice list{slice_letters_list}")
