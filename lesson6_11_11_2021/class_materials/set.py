set_example = {1, 2, 3}
set_example_2 = {4, 5, 6}
set_example_3 = {1, 2, 3}

words = ["Hello", "Abc", "hello", "Hello"]
set_words = set(words)

print(set_words)

# Нельзя set = {} будет словарь

print(f"Коk-во элементов: {len(set_words)}")
print(f"Hello in set: {'Hello' in set_words}")

print(f"Сет1 и Сет2 не имеют общих элементов:"
      f"{set_example.isdisjoint(set_example_2)}")

print(f"Сет1 и Сет 3 полностью одинаковые: {set_example == set_example_3}")

# set_example.remove(21) error
set_example.remove(2)
set_example.discard(103)
set_example.add(4)

# Сет неупорядочен. Удаляем первый элемент но неизвестно какой
print(set_words.pop())

print(f"Удалили 2 и добавили 4: {set_example}")

set_example.discard(103)
