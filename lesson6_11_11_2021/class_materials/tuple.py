my_tuple = tuple()

my_tuple = ('s')
print(f"Not correct one element tuple:{my_tuple}")

my_tuple = ('s',)
print(f"Correct one element tuple:{my_tuple}")

new_tuple = tuple("hello world")
print(new_tuple)

print(new_tuple[1])

print(new_tuple[::2])

new_tuple2 = my_tuple + new_tuple

print(new_tuple2)

print(my_tuple * 3)

for item in new_tuple2:
    print(item)

print("h" in new_tuple2)

numbers = (1,2,3)
new_numbers = tuple((n for n in numbers if n >=2))
a = 0