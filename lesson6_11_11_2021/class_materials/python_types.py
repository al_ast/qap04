# immutable: int, float, bool, tuple, str, frozen_set

# mutable: list, set, dict

dict = {
    1: ["A", "B", "B"],
    2: ["C", "D", "C"]
}

dict = {
    1: ["A", "B"],
    2: ["C", "D"]
}

int1 = 5
int2 = int1
int1 = int1 + 1

print(f"{int2=}")
print(f"{int1=}")

float1 = 5.1
float2 = float1
float1 = 4.1 + 0.2

print(f"{float2=}")
print(f"{float1=}")

bool1 = True
bool2 = bool1
bool1 = False

print(f"{bool1=}")
print(f"{bool2=}")

str1 = "abc"
str2 = str1
str1 = str1 + "d"

print(f"{str1=}")
print(f"{str2=}")

tuple1 = (1, 2, 3)
tuple2 = tuple1
tuple1 = (4, 5, 6)
print(f"{tuple1=}")
print(f"{tuple2=}")

# Mutable
list1 = [1, 2, 3]
list2 = list1
list1[0] = 2

print(f"{list1=}")
print(f"{list2=}")

dict1 = {"student1": "name1", "student2": "name"}
dict2 = dict1
dict1["student1"] = "name1111"

print(f"{dict1=}")
print(f"{dict2=}")

set1 = {1, 2, 3}
set2 = set1
set1.add(4)

print(f"{set1}")
print(f"{set2}")

