# Создайте 2 множества:
# - Если они одинаковые: вывести что они равны
# - Если 1 множество полностью состоит из второго: вывести сообщение множество 1
# состоит из множества2
#
# - Если 2 множество полностью состоит из 1: вывести сообщение множество 2
#   состоит из множества 1
#
# - Если они пересекаются: вывести элементы в которых они пересекаются
# - Если не пересекаются: вывести сообщение об этом
# Я тут подумал, о почему бы не сделать рандомными наши множества
from random import randint
set1_start = []
set2_start = []
for i in list(range(10)):
    set1_start.append(randint(1, 10,))
    set2_start.append(randint(1, 10))
    set_1 = set(set1_start)
    set_2 = set(set2_start)
print(set_1)
print(set_2)

# set_1 = {1, 2, 3, 4, 5}
# set_2 = {1, 2, 3, 4, 6}
if set_1 == set_2:
    print("Set1 and set2 equal")
elif set_1.issubset(set_2):
    print("Set1 consists of set2")
elif set_2.issubset(set_1):
    print("Set2 consists of set1")
elif not set_1.isdisjoint(set_2):
    print(f"Set1 and set2 have intersection: {set_1.intersection(set_2)}")
else:
    print("Set1 and set2 haven't intersaction")