# Николай знает, что кортежи являются неизменяемыми,
# но он с этим не готов соглашаться.
# Ученик решил создать функцию del_from_tuple(),
# которая будет удалять первое появление определенного элемента из кортежа
# по значению и возвращать кортеж без оного.
# Попробуйте повторить шедевр не признающего авторитеты начинающего программиста.
# К слову, Николай не всегда уверен в наличии элемента в кортеже
# (в этом случае кортеж вернется функцией в исходном виде).

def del_from_tuple(start_tuple, elements):
    list_tuple = list(start_tuple)
    if elements in list_tuple:
        list_tuple.remove(elements)
    final_tuple = tuple(list_tuple)
    print(final_tuple)
enter_element = int(input("Enter number"))
user_tuple = (1, 2, 3, 4, 5)
del_from_tuple(user_tuple, enter_element)