# Задача5**:
# Цветочница.
# Определить иерархию и создать несколько цветов.
# Собрать букет (можно использовать аксессуары) с определением его стоимости.
# Определить время его увядания по среднему времени жизни всех цветов в букете.
# Позволить сортировку цветов в букете на основе различных параметров (свежесть/цвет/длина стебля/стоимость...)
# Реализовать поиск цветов в букете по определенным параметрам.
# Узнать, есть ли цветок в букете.

class Flower:

    def __init__(self, name, cost, fresh, length, color):
        self.name = str(name)
        self.cost = int(cost)
        self.fresh = int(fresh)
        self.length = int(length)
        self.color = str(color)

    def __repr__(self):
        return f'{self.name},price is {self.cost} RUB, it will leave for {self.fresh} hours,' \
               f' length is {self.length} cm, color is {self.color}'


bouquet = []
bouquet_price = []
live_time = []
exit_shop = 0
while exit_shop == 0:
    one_more_flower = Flower(name=str(input('What is the name of a flower you want to add: ')),
                             cost=int(input('What is preferable price for flower?: ')),
                             fresh=int(input('How many hours you want it to live?: ')),
                             length=int(input('What is preferable length for flower?: ')),
                             color=str(input('What color should the flower be?: ')))

    # bouquet should have more than one flower to be bouquet, so:

    if bouquet:
        is_bouquet_done = int(input('Do you want to add more flowers\n1.Yes\n2.No\n'))
        if is_bouquet_done == 1:
            bouquet.append(one_more_flower)
            bouquet_price.append(one_more_flower.cost)
            live_time.append(one_more_flower.fresh)
            continue
        elif is_bouquet_done == 2:
            exit_shop += 1
        else:
            print('You should choose from variants given')
    bouquet.append(one_more_flower)
    bouquet_price.append(one_more_flower.cost)
    live_time.append(one_more_flower.fresh)

print('This bouquet consists of:', bouquet)
print('bouquet price:', sum(bouquet_price), 'RUB')
print('On average, this bouquet should be fresh for', int(sum(live_time)/len(live_time)), 'hours')
