# Задача2:
# Создайте 2 множества:
# - Если они одинаковые: вывести что они равны
# - Если 1 множество полностью состоит из второго: вывести сообщение множество 1
# состоит из множества2
#
# - Если 2 множество полностью состоит из 1: вывести сообщение множество 2
#   состоит из множества 1
#
# - Если они пересекаются: вывести элементы в которых они пересекаются
# - Если не пересекаются: вывести сообщение об этом

from random import choices as chs

list0 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

list1 = chs(list0, k=5)
list2 = chs(list0, k=5)

set1 = set(list1)
set2 = set(list2)

# ↑ have made a randomizer for sets elements for easier check runs

if set1 == set2:
    print("Sets are equal")
elif set1.issubset(set2):
    print("Set1 is subset of set2")
elif set1.issuperset(set2):
    print("Set2 is subset of set1")
elif not set1.isdisjoint(set2):
    print(f"Sets have intersection: {set1.intersection(set2)}")
else:
    print("Sets have no intersections")
