class Investment:
    def __init__(self, amount, percentage_per_year, term_years, capitalization_type=None):
        self.amount = amount
        self.percentage_per_year = percentage_per_year
        self.float_percentage_per_year = percentage_per_year / 100
        self.term_years = term_years
        self.capitalization_type = capitalization_type

    def __repr__(self):
        return f"Amount: {self.amount}. Percentage per year: {self.percentage_per_year}. " \
               f"Term years:{self.term_years}. Capitalization: {self.capitalization_type}"


class Bank:
    def invest(self, investment):
        if investment.capitalization_type is None:
            return investment.amount * (1 + investment.term_years * investment.float_percentage_per_year)

        if investment.capitalization_type == "month":
            months = investment.term_years * 12
            float_percentage_per_month = investment.float_percentage_per_year / 12

            amount = investment.amount * ((1 + float_percentage_per_month) ** months)
            amount = round(amount, 2)

            return amount

        raise Exception(
            f"Not supported capitalization type:{investment.capitalization_type}. {investment=}")


investment1 = Investment(amount=100, percentage_per_year=10, term_years=2, capitalization_type="month")
# Тоже самое Investment(100, 10, 2, "month")

investment2 = Investment(amount=100, percentage_per_year=10, term_years=3)
investment3 = Investment(amount=100, percentage_per_year=10, term_years=3, capitalization_type="year")


bank = Bank()
final_money1 = bank.invest(investment1)
print(f"{final_money1=}")

final_money2 = bank.invest(investment2)
print(f"{final_money2=}")

final_money3 = bank.invest(investment3)
print(f"{final_money3=}")
