class User:
    def __init__(self, name):
        self.name = name
        self.deposits = []


class Bank:
    def __init__(self, users):
        self.users = users

    def deposit(self, user, amount, term_years, percentage=10, capitalization=None):
        float_percentage_per_year = percentage / 100

        if capitalization is None:
            amount = amount * (1 + term_years * float_percentage_per_year)

        if capitalization == "month":
            months = term_years * 12
            float_percentage_per_month = float_percentage_per_year / 12

            amount = amount * ((1 + float_percentage_per_month) ** months)
            amount = round(amount, 2)

        # Добавляем клиенту депозит. Тут тоже желательно использовать объект депозита
        user.deposits.append(amount)
        return amount


# Есть человек(user)
# Есть банк, в котором обслуживается этот человек
user1 = User("Alex")
bank = Bank(users=[user1])

amount_from_bank = bank.deposit(user1, 100, 2, capitalization="month")
amount_from_bank2 = bank.deposit(user1, 100, 3, capitalization=None)

print(amount_from_bank)
print(f"user deposit:{user1.deposits}")
