"""
Николай знает, что кортежи являются неизменяемыми, но он с этим не готов соглашаться.
Ученик решил создать функцию del_from_tuple(), которая будет удалять первое появление определенного элемента из кортежа
по значению и возвращать кортеж без оного.
Попробуйте повторить шедевр не признающего авторитеты начинающего программиста.
К слову, Николай не всегда уверен в наличии элемента в кортеже (в этом случае кортеж вернется функцией в исходном виде).
"""


def del_from_tuple(original_tuple, element):
    list_from_tuple = list(original_tuple)

    if element in list_from_tuple:
        list_from_tuple.remove(element)
    final_tuple = tuple(list_from_tuple)

    print(final_tuple)


user_tuple = ('a', 'b', 'a', 'd', 'b')
del_from_tuple(user_tuple, 'b')
