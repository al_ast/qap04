"""
Создайте 2 множества:
- Если они одинаковые: вывести что они равны
- Если 1 множество полностью состоит из второго: вывести сообщение множество 1
состоит из множества2
- Если 2 множество полностью состоит из 1: вывести сообщение множество 2
  состоит из множества 1
- Если они пересекаются: вывести элементы в которых они пересекаются
- Если не пересекаются: вывести сообщение об этом
"""

set1 = {'f', 'g', 'h', 'a'}
set2 = {'a', 'b', 'c', 'd', 'f'}

if set1 == set2:
    print("Sets are equal")
elif set1.issubset(set2):
    print("Set1 is subset of set2")
elif set1.issuperset(set2):
    print("Set2 is subset of set1")
elif not set1.isdisjoint(set2):
    print(f"Sets have intersection ({set1.intersection(set2)})")
else:
    print("Sets have no intersection")
