"""
Создать кортеж с числами от 1 до 50 используя генератор списков
"""
new_list = []

for number in range(1, 51):
    new_list.append(number)

final_tuple = tuple(new_list)
print(final_tuple)
