"""Задача2:
Создайте 2 множества:
- Если они одинаковые: вывести что они равны
- Если 1 множество полностью состоит из второго: вывести сообщение множество 1
состоит из множества2

- Если 2 множество полностью состоит из 1: вывести сообщение множество 2
  состоит из множества 1

- Если они пересекаются: вывести элементы в которых они пересекаются
- Если не пересекаются: вывести сообщение об этом
"""
# {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'k', 'l', 'm', 'n'}
# {'d', 'e', 'f', 'g', 'h', 'k'}
# {'e', 'd', 'g', 'f', 'k', 'h', 'x'}
# {'x', 'y', 'z'}
array1 = {'d', 'e', 'f', 'g', 'h', 'k', 'x'}
array2 = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'k', 'l', 'm', 'n'}
if array1 == array2:
    print('The arrays are equal')
if array1.issubset(array2):
    print(f'The array1 fully included in array2')
elif array2.issubset(array1):
    print(f'The array2 fully included in array1')
elif array1.intersection(array2):
    print(f'Intersection is {array1.intersection(array2)}')
else:
    print('Arrays do not intersect')

