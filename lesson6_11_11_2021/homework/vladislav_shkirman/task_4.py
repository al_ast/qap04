"""Задача4**:
Создайте класс инвестиция. Который содержит необходимые поля и методы,
 например сумма инвестиция и его срок.
Пользователь делает инвестиция в размере N рублей сроком на R лет под 10%
годовых (инвестиция с возможностью ежемесячной капитализации - это означает,
что проценты прибавляются к сумме инвестиции ежемесячно).
Написать класс Bank, метод deposit принимает аргументы N и R,
и возвращает сумму, которая будет на счету пользователя.
"""
from ClassInvestments import Bank

inv = Bank(150, 7)
inv.invest()
print("Total sum on user's count:", inv.deposit())
