class Bank:

    def __init__(self, n, r):
        self.n = int(n)
        self.r = int(r)

    def invest(self):
        print(f'Amount of investments: {self.n}\nTerm: {self.r}')

    def deposit(self):
        return round(self.n * (1 + 0.1/12)**(12*self.r), 2)
