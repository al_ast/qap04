set_number_1 = {1, 2, 3, 4}
set_number_2 = {1, 2}
set_intersections = set_number_1.intersection(set_number_2)
if set_number_1 == set_number_2:
    print("the sets are equal")
elif set_number_1 >= set_number_2:
    print("second set consist of first set")
elif set_number_2 >= set_number_1:
    print("first set consist of second set")
if not set_number_1.isdisjoint(set_number_2):
    print(set_intersections)
else:
    print("the sets do not intersect")
