# Задача2:
# Создайте 2 множества:
# - Если они одинаковые: вывести что они равны
# - Если 1 множество полностью состоит из второго: вывести сообщение множество 1
# состоит из множества2
#
# - Если 2 множество полностью состоит из 1: вывести сообщение множество 2
#   состоит из множества 1
#
# - Если они пересекаются: вывести элементы в которых они пересекаются
# - Если не пересекаются: вывести сообщение об этом

set_1 = {1, 2, 3, 4}
set_2 = {5, 6, 7, 8}
if set_1 == set_2:
    print("Sets are equal")
elif set_1.issubset(set_2):  # сет1 является подмножеством сет2
    print(f"Set 1 is subset set 2" "\n", (set_1.intersection(set_2)))
elif set_1.issuperset(set_2):  # 1 явл надмножеством 2
    print(f"Set 1 is superset set 2" "\n", (set_1.intersection(set_2)))
elif not set_1.isdisjoint(set_2):  # если сет1 не имеет не пересечений с сет2
    print(f"Sets are joint" "\n", (set_1.intersection(set_2)))
else:
    set_1.isdisjoint(set_2)  # сет1 не пересекается с сет2
    print("Sets are disjoint")