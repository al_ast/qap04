set_1 = {1,2,3,4,5,6,7}
set_2 = {5,6,7,8,9,10}

if set_1 == set_2:
    print('Множества равны')
elif set_1.issubset(set_2):
    print('Множество 1 состоит из множества 2')
elif set_2.issubset(set_1):
    print('Множество 2 состоит из множества 1')
elif set_1.intersection(set_2):
    print(f'Повторяющиеся элементы: {set_1.intersection(set_2)}')
else:
    print('Элементы не пересекаются')

