any_tuple = (0,1,2,4,5,6)
def del_from_tuple(number):
    new_list = list(any_tuple)

    if number in any_tuple:
        new_list.remove(number)
        print(tuple(new_list))
    else:
        print(any_tuple)

del_from_tuple(3)
# del_from_tuple(6)
# del_from_tuple(1)
