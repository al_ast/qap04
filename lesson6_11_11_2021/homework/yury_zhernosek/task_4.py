def deposit(money, years):
    term = years * 12
    for month in range(term):
        money = money + money * 0.1 / 12
    print(f'Your profit: {round(money)}')
deposit(20000,2)