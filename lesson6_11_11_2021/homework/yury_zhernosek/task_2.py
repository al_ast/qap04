first_set = {1,2,3,4,5}
second_set = {1,2,3,4}
if first_set == second_set:
    print('Identical')
elif first_set <= second_set:
    print('The first set consists of the second')
elif second_set <= first_set:
    print('The second set consists of the first')
if not first_set.isdisjoint(second_set):
    print(f'Intersections: {first_set.intersection(second_set)}')