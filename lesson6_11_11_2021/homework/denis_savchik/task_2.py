# Задача2:
# Создайте 2 множества:
# - Если они одинаковые: вывести что они равны
# - Если 1 множество полностью состоит из второго: вывести сообщение множество 1
# состоит из множества2
# - Если 2 множество полностью состоит из 1: вывести сообщение множество 2
#   состоит из множества 1
# - Если они пересекаются: вывести элементы в которых они пересекаются
# - Если не пересекаются: вывести сообщение об этом


list_of_number1 = []
print("Entered first set")
for element in range(5):
    list_of_number1.append(input())
new_set1 = set(list_of_number1)

list_of_number2 = []
print("Entered second set")
for element in range(5):
    list_of_number2.append(input())
new_set2 = set(list_of_number2)

print(new_set1)
print(new_set2)
if new_set1 == new_set2:
    print("this sets are equal")
elif new_set1.issubset(new_set2) is True:
    print("First set contains the second")
elif new_set2.issubset(new_set1) is True:
    print("Second set contains the first")
elif new_set1.isdisjoint(new_set2) is True:
    print("This sets arent crossing")
elif new_set1.isdisjoint(new_set2) is False:
    print("This sets are crossing")
