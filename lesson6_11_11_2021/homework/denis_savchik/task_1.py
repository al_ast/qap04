# Задача1:
# Николай знает, что кортежи являются неизменяемыми,
# но он с этим не готов соглашаться.
# Ученик решил создать функцию del_from_tuple(),
# которая будет удалять первое появление определенного элемента из кортежа
# по значению и возвращать кортеж без оного.
# Попробуйте повторить шедевр не признающего авторитеты начинающего программиста.
# К слову, Николай не всегда уверен в наличии элемента в кортеже
# (в этом случае кортеж вернется функцией в исходном виде).

def del_from_tuple(some_tuple, deleted_element):
    tuple_in_list = list(some_tuple)
    if deleted_element in tuple_in_list:
        tuple_in_list.remove(deleted_element)
        print(tuple(tuple_in_list))
    else:
        print(some_tuple)                     # либо же return some tuple,не уверен как нужно по условию


rnd_tuple = (1, 2, 3, 4, 5, 5)
del_from_tuple(rnd_tuple, 5)
