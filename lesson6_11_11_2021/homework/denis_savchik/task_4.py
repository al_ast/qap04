# Задача4**:
# Создайте класс инвестиция. Который содержит необходимые поля и методы,
#  например сумма инвестиция и его срок.
# Пользователь делает инвестиция в размере N рублей сроком на R лет под 10%
# годовых (инвестиция с возможностью ежемесячной капитализации - это означает,
# что проценты прибавляются к сумме инвестиции ежемесячно).
# Написать класс Bank, метод deposit принимает аргументы N и R,
# и возвращает сумму, которая будет на счету пользователя.

class Investment:
    def __init__(self, sum_of_deposit, deposit_delay):
        self.sum = sum_of_deposit
        self.delay = deposit_delay
# процент пока не учел

    def get_sum(self):
        print(f"Sum of investment: {self.sum}")
        return self.sum

    def get_delay(self):
        print(f"Time of deposit: {self.delay}")
        return self.delay


class Bank:
    def __init__(self, sum_of_deposit, delay):
        self.sum = sum_of_deposit
        self.delay = delay * 12

    def deposit(self):
        for current in range(self.delay):
            self.sum = self.sum + self.sum * (0.1/12)
        print(f"Ur money is {self.sum}")


my_investment = Investment(100000, 1)
sum_of_deposit = my_investment.get_sum()
time_deposit = my_investment.get_delay()
deposit = Bank(sum_of_deposit, time_deposit)
deposit.deposit()
