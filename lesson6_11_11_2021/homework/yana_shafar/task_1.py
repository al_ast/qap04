def del_from_tuple(tuple1, elem):
    if elem in tuple1:
        list1 = list(tuple1)
        list1.remove(elem)
        tuple1 = tuple(list1)
    return tuple1


print(del_from_tuple((1, 2, 3), 2))
print(del_from_tuple((1, 2, 3, 4, 5), 6))