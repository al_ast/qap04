set_1 = {1, 2, 3, 4}
set_2 = {1, 2, 3, 4, 5}

if set_1 == set_2:
    print('Set 1 is equal to set 2')
elif set_2.issubset(set_1):
    print('Set 1 consists of set 2')
elif set_1.issubset(set_2):
    print('Set 2 consists of set 1')
if set_1.intersection(set_2):
    print(f'Intersection of set 1 and set 2: {set_1.intersection(set_2)}')
else:
    print('Set 1 and set 2 do not intersect')

