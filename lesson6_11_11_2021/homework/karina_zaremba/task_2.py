first_set = {1, 2, 3, 3}
second_set = {1, 2, 3, 1, 2}

if first_set == second_set:
    print("Множества равны")
else:
    print("Нет")

# - Если 1 множество полностью состоит из второго: вывести сообщение множество 1
# состоит из множества2
if first_set.issubset(second_set):
    print("1 множество полностью состоит из 2")
else:
    print("Не состоит")

# Если 2 множество полностью состоит из 1: вывести сообщение множество 2
if second_set.issubset(first_set):
    print("2 множество полностью состоит из 1")
else:
    print("Не состоит")

# - Если они пересекаются: вывести элементы в которых они пересекаются
print(first_set.intersection(second_set))
if first_set.intersection(second_set):
    general = first_set.intersection(second_set)
    print("Множества пересекаются:", general)
else:
    print("Множества не пересекаются")


# if first_set == second_set:
#     print("Равны")
# elif first_set.issubset(second_set):
#     print("1 множество полностью состоит из 2")
# elif second_set.issubset(first_set):
#     print("2 множество полностью состоит из 1")
# elif first_set.intersection(second_set):
#     general = first_set.intersection(second_set)
#     print("Множества пересекаются:", general)
# else:
#     print("Множества не пересекаются:")