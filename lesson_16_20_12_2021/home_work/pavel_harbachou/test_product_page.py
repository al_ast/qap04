import pytest
from product_page import ProductPage


@pytest.fixture(scope='session')
def setup_product_page():
    global product_page
    product_page = ProductPage()
    product_page.open_product_page()
    yield
    product_page.quit()


@pytest.mark.usefixtures('setup_product_page')
class TestProductPage:
    def test_presence_backpack(self):
        assert product_page.check_number_of_backpacks(), 'Error: No one backpack on the product page'

    def test_valid_prices(self):
        assert product_page.check_not_empty_price(), 'Error: One or more prices are empty'
        assert product_page.check_valid_price(), 'Error: Invalid price'
