import re
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(ChromeDriverManager().install())


class ProductPage:
    def open_product_page(self):
        ENTER_USERNAME_LOCATOR = '//input[@id="user-name"]'
        ENTER_PASSWORD_LOCATOR = '//input[@id="password"]'
        LOGIN_BUTTON_LOCATOR = '//input[@id="login-button"]'
        driver.get('https://www.saucedemo.com/')
        driver.maximize_window()
        enter_user_name = driver.find_element(By.XPATH, ENTER_USERNAME_LOCATOR)
        enter_password_locator = driver.find_element(By.XPATH, ENTER_PASSWORD_LOCATOR)
        login_button = driver.find_element(By.XPATH, LOGIN_BUTTON_LOCATOR)
        enter_user_name.send_keys('standard_user')
        enter_password_locator.send_keys('secret_sauce')
        login_button.click()

    def check_number_of_backpacks(self):
        INVENTORY_ITEM_NAMES_LOCATOR = '//div[@class="inventory_item_name"]'
        inventory_items = driver.find_elements(By.XPATH, INVENTORY_ITEM_NAMES_LOCATOR)
        number_of_backpacks = 0
        for inventory_item in inventory_items:
            if 'Backpack' in inventory_item.text:
                number_of_backpacks += 1
        if number_of_backpacks >= 1:
            return True

    def check_not_empty_price(self):
        INVENTORY_ITEM_PRICES_LOCATOR = '//div[@class="inventory_item_price"]'
        inventory_prices = driver.find_elements(By.XPATH, INVENTORY_ITEM_PRICES_LOCATOR)
        if len(inventory_prices) == 6:
            return True
        return False

    def check_valid_price(self):
        INVENTORY_ITEM_PRICES_LOCATOR = '//div[@class="inventory_item_price"]'
        inventory_prices = driver.find_elements(By.XPATH, INVENTORY_ITEM_PRICES_LOCATOR)
        for inventory_price in inventory_prices:
            if not (re.match("^\$[0-9]\.[0-9][0-9]$", inventory_price.text)
                or re.match("^\$[0-9][0-9]\.[0-9][0-9]$", inventory_price.text)) \
                    and (inventory_price.text != '$0.00' and inventory_price != '$00.00'):
                return False
            return True

    def quit(self):
        return driver.quit()
