"""
Задание1:
https://www.saucedemo.com/

Написать 2 теста на логин страницу
Написать 2 теста на страницу с рюкзаками
(првоерить, что рюкзаков больше чем 0)
(проверить, что цена не пустая)

Задание 2**:
Проверить что цена валидная, а не пустая

$29.99 - Правильная

29.99 - не правильная
$0.00 неправильная
-$29.99
...
все кроме $n.nn неверно
"""
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from lesson_16_20_12_2021.class_materials.testing.pages.login_page import LoginPage
from lesson_16_20_12_2021.class_materials.testing.pages.product_page import ProductsPage


@pytest.fixture
def setup_test_login_page():
    global login_page
    global products_page
    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_page = LoginPage(driver)
    products_page = ProductsPage(driver)
    login_page.navigate()
    yield
    driver.quit()


@pytest.mark.usefixtures('setup_test_login_page')
class TestLoginPage:
    def test_login_page_with_valid_data(self):
        login_page.login('standard_user', 'secret_sauce')
        assert products_page.is_opened(), 'Error: User must be redirected in products page when' \
                                          'valid username and password entered'

    def test_login_page_with_invalid_data(self):
        login_page.login('wrong username', 'wrong password')
        assert login_page.is_opened(), 'Error: User must stay in login page when invalid username or password entered'
