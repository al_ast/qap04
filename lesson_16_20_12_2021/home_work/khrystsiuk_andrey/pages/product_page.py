from selenium.common.exceptions import TimeoutException
from base_page import BasePage


class ProductPage(BasePage):
    ITEMS_LOCATOR = "//*[@class='inventory_item_description']"
    PRODUCTS_PRICES_LOCATOR = "//div[@class='inventory_item_price']"
    PRODUCT_PAGE_LOCATOR = "//span[contains(text(),'Products')]"

    def is_opened(self):
        return self.is_element_present(self.PRODUCT_PAGE_LOCATOR)

    def count_elements(self, item_name):
        try:
            count_products = self.find_elements(self.ITEMS_LOCATOR.format(item_name))
            return len(count_products)
        except TimeoutException:
            return 'No items detected'

    def get_prices(self):
        try:
            prices_elements_list = self.find_elements(self.PRODUCTS_PRICES_LOCATOR)
            prices_list = []
            for element in prices_elements_list:
                prices_list.append(element.text)
                if len(prices_list) > 0:
                    return prices_list
        except TimeoutException:
            return False
