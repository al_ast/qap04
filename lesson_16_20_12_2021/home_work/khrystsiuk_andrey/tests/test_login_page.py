from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from login_page import LoginPage
from product_page import ProductPage
import pytest


@pytest.fixture()
def setup_login_page():
    global product_page
    global login_page
    global driver
    driver = webdriver.Chrome(ChromeDriverManager().install())
    product_page = ProductPage(driver)
    login_page = LoginPage(driver)
    login_page.navigate()

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_login_page")
class TestLoginPage:
    def test_login_with_valid_credentials(self):
        login_page.login("standard_user", "secret_sauce")
        assert product_page.is_opened(), "product page is not opened with valid credentials"

    def test_login_with_invalid_credentials(self):
        login_page.login("standard_userr", "seret_sauce")
        assert login_page.is_opened(), "other page opened with invalid credentials"
