import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from product_page import ProductsPage
from login_page import LoginPage


@pytest.fixture(scope="session")
def setup_test_to_product_page():
    global driver
    global product_page

    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    product_page = ProductsPage(driver)
    login_page.navigate()
    login_page.login_correct_data("standard_user", "secret_sauce")

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_test_to_product_page")
class TestProductPage:
    def test_product_presence(self):
        presented_product = product_page.is_product_presented("Backpack")
        assert presented_product, "No such products on the page"
