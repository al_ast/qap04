import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from login_page import LoginPage
from product_page import ProductsPage


@pytest.fixture()
def setup_login_to_login_page():
    global login_page
    global driver
    global product_page

    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_page = LoginPage(driver)
    product_page = ProductsPage(driver)
    login_page.navigate()

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_login_to_login_page")
class TestLoginPage:
    def test_login_page_invalid_credentials(self):
        login_page.login_correct_data("not_valid_user_name", "not_valid_user_password")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    def test_login_page_with_valid_credentials(self):
        login_page.login_correct_data("standard_user", "secret_sauce")

        assert product_page.is_opened(), "User should be redirected to the product with valid credentials"
