import re

from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.base_page import BasePage


class ProductPage(BasePage):
    PRODUCTS_PAGE_HEADER_LOCATOR = "//span[text()='Products']"
    INVENTORY_ITEM_LOCATOR = "//div[@class='inventory_item']"
    ITEM_NAME_LOCATOR = "//div[@class='inventory_item_name']"
    ITEM_DESCRIPTION_LOCATOR = "//div[@class='inventory_item_desc']"
    ITEM_PRICE_LOCATOR = "//div[@class='inventory_item_price']"
    ADD_TO_CART_BUTTON_LOCATOR = "//button[contains(@id,'add-to-cart')]"

    def is_page_opened(self):
        return self.is_element_present(self.PRODUCTS_PAGE_HEADER_LOCATOR)

    def get_products_count(self):
        return self.get_elements_count(self.INVENTORY_ITEM_LOCATOR)

    def get_product_names_count(self):
        return self.get_elements_count(self.ITEM_PRICE_LOCATOR, must_have_text=True)

    def get_product_descriptions_count(self):
        return self.get_elements_count(self.ITEM_DESCRIPTION_LOCATOR, must_have_text=True)

    def get_add_to_cart_buttons_count(self):
        return self.get_elements_count(self.ADD_TO_CART_BUTTON_LOCATOR)

    def get_product_prices_count(self):
        return self.get_elements_count(self.ITEM_PRICE_LOCATOR, must_have_text=True)

    def get_products_prices(self):
        return self.get_elements_list(self.ITEM_PRICE_LOCATOR)

    def is_price_format_valid(self, price):
        match = re.fullmatch("^\$[0-9]+\.[0-9]{2}", price)
        if match and price != "$0.00":
            return True
        return False

    def is_all_product_prices_valid(self):
        products_prices_list = self.get_products_prices()
        invalid_prices = ''
        is_prices_valid = True
        for price in products_prices_list:
            if not self.is_price_format_valid(price):
                is_prices_valid = False
                invalid_prices += f'{price} '
        return is_prices_valid, invalid_prices
