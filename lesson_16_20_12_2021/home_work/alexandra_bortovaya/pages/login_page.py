from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.base_page import BasePage


class LoginPage(BasePage):
    URL = "https://www.saucedemo.com/"

    USERNAME_INPUT_LOCATOR = "//input[@id='user-name']"
    PASSWORD_INPUT_LOCATOR = "//input[@id='password']"
    LOGIN_BUTTON_LOCATOR = "//input[@id='login-button']"
    LOGIN_PAGE_IMAGE_LOCATOR = "//div[@class='bot_column']"

    STANDARD_USER_LOGIN = "standard_user"
    LOCKED_OUT_USER_LOGIN = "locked_out_user"
    INVALID_USER_LOGIN = "invalid_user"
    VALID_USER_PASSWORD = "secret_sauce"
    INVALID_USER_PASSWORD = "invalid_password"

    def navigate(self):
        self.open_url(self.URL)

    def enter_username(self, username):
        self.enter_text(self.USERNAME_INPUT_LOCATOR, username)

    def enter_password(self, password):
        self.enter_text(self.PASSWORD_INPUT_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def login(self, username, password):
        self.enter_username(username)
        self.enter_password(password)
        self.click_login_button()

    def is_page_opened(self):
        return self.is_element_present(self.LOGIN_PAGE_IMAGE_LOCATOR)
