from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def _define_by_type(self, locator):
        if "/" in locator:
            return By.XPATH

        return By.CSS_SELECTOR

    def _find_element_by_locator(self, locator, timeout=10):
        by_type = self._define_by_type(locator)
        return WebDriverWait(self.driver, timeout).until(
            expected_conditions.presence_of_element_located((by_type, locator))
        )

    def _find_elements_by_locator(self, locator, timeout=10):
        by_type = self._define_by_type(locator)
        return WebDriverWait(self.driver, timeout).until(
            expected_conditions.presence_of_all_elements_located((by_type, locator))
        )

    def click(self, locator):
        element = self._find_element_by_locator(locator)
        element.click()

    def enter_text(self, locator, text):
        element = self._find_element_by_locator(locator)
        element.send_keys(text)

    def is_element_present(self, locator):
        try:
            self._find_element_by_locator(locator)
            return True
        except TimeoutException:
            return False

    def open_url(self, url):
        self.driver.get(url)

    def get_elements_count(self, locator, must_have_text=False):
        try:
            web_elements_list = self._find_elements_by_locator(locator)
            if must_have_text:
                elements_with_text = []
                for web_element in web_elements_list:
                    if len(web_element.text):
                        elements_with_text.append(web_element.text)
                return len(elements_with_text)
            return len(web_elements_list)
        except TimeoutException:
            return 0

    def get_elements_list(self, locator):
        try:
            web_elements_list = self._find_elements_by_locator(locator)
            elements_list = []
            for web_element in web_elements_list:
                elements_list.append(web_element.text)
            return elements_list
        except TimeoutException:
            return []
