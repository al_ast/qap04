import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.login_page import LoginPage
from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.product_page import ProductPage


@pytest.fixture(scope="session")
def setup():
    global product_page
    global driver
    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    product_page = ProductPage(driver)

    login_page.navigate()
    login_page.login("standard_user", "secret_sauce")

    yield
    driver.quit()


@pytest.mark.usefixtures("setup")
class TestLoginPage:
    def test_products_are_present(self):
        assert product_page.get_products_count() > 0, "There are no products on page"

    def test_products_prices_are_valid(self):
        products_prices_list = product_page.get_products_prices()
        is_prices_valid = True
        for price in products_prices_list:
            if not product_page.is_price_format_valid(price):
                is_prices_valid = False
                break

        assert is_prices_valid, "One or more products prices have invalid format"
