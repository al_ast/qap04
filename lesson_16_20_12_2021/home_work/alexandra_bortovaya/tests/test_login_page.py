import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.login_page import LoginPage
from lesson_16_20_12_2021.home_work.alexandra_bortovaya.pages.product_page import ProductPage


@pytest.fixture()
def setup():
    global login_page
    global product_page
    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    product_page = ProductPage(driver)

    login_page.navigate()

    yield
    driver.quit()


@pytest.mark.usefixtures("setup")
class TestLoginPage:
    def test_login_with_invalid_credentials(self):
        login_page.login("invalid_name", "invalid_password")

        assert login_page.is_page_opened(), "User with invalid credentials must not be logged"

    def test_login_with_valid_credentials(self):
        login_page.login("standard_user", "secret_sauce")

        assert product_page.is_page_opened(), "User with valid credentials must be logged"
