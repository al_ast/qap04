from base_page import BasePage


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    PRODUCTS_NAME_LOCATOR = '//div[contains(text(),"{}")]'
    PRODUCTS_LOCATOR = ".inventory_item"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def checking_the_number_of_products(self, name_product):
        locator = self.PRODUCTS_NAME_LOCATOR.format(name_product)
        assert self.is_element_present(locator), "The product is out of stock"