import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from login_page import LoginPage
from product_page import ProductsPage


@pytest.fixture()
def setup_login():
    global login_page
    global product_page

    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_page = LoginPage(driver)
    product_page = ProductsPage(driver)
    login_page.navigate()

    yield
    print("Good job")
    driver.quit()


@pytest.mark.usefixtures("setup_login")
class TestLoginPage:
    def test_login_with_valid_credentials(self):
        login_page.login("standard_user", "secret_sauce")

        assert product_page.is_opened(), "You should be redirected to the product page after login"

    def test_login_with_not_valid_credentials(self):
        login_page.login("abc", "abc")

        assert login_page.is_opened(), "Error"
