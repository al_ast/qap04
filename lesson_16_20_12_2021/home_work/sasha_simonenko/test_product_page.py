import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from login_page import LoginPage
from product_page import ProductsPage

driver = webdriver.Chrome(ChromeDriverManager().install())
login_page = LoginPage(driver)
product_page = ProductsPage(driver)
login_page.navigate()


@pytest.fixture()
def setup_products():
    global login_page
    global product_page


    yield
    print("Good job")
    driver.quit()


@pytest.mark.usefixtures("setup_products")
class TestProductPage:
    def test_checking_the_number_og_backpacks(self):
        login_page.login('standard_user', 'secret_sauce')
        product_page.checking_the_number_of_products('Backpack')