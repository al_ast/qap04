# Написать 2 теста на страницу с рюкзаками
# (проверить, что рюкзаков больше чем 0)
# (проверить, что цена не пустая)
from selenium.common.exceptions import NoSuchElementException

from base_page import BasePage
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    ITEM_LOCATOR = "//div[@class='inventory_item']"
    ITEM_PRICE_LOCATOR = "//div[@class='inventory_item_price']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def get_items_count(self):
        return self.get_elements_count(self.ITEM_LOCATOR)

    def get_prices(self):
        try:
            prices = self._get_elements(self.ITEM_PRICE_LOCATOR)
            prices_list = []
            for price in prices:
                prices_list.append(price.text)
            return prices_list
        except NoSuchElementException:
            return []


