# Задание1:
# https://www.saucedemo.com/
#
# Написать 2 теста на логин страницу
# Написать 2 теста на страницу с рюкзаками
# (проверить, что рюкзаков больше чем 0)
# (проверить, что цена не пустая)

import pytest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from login_page import LoginPage
from product_page import ProductsPage


@pytest.fixture()
def setup_user_try_to_login():
    global login_page
    global product_page
    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_page = LoginPage(driver)
    login_page.navigate()
    product_page = ProductsPage(driver)

    yield
    print("Test is finished.")
    driver.quit()


@pytest.mark.usefixtures("setup_user_try_to_login")
class TestProductPage:
    def test_product_quantity(self):
        login_page.login('standard_user', 'secret_sauce')
        items_count = product_page.get_items_count()

        assert items_count > 0, "No items found"

    def test_valid_prices(self):
        login_page.login('standard_user', 'secret_sauce')
        prices = product_page.get_prices()

        assert len(prices) > 0, "No prices found"

        not_valid_prices = [price for price in prices if price == " "]

        assert len(not_valid_prices) == 0, f"Some prices are not valid:{not_valid_prices}"
