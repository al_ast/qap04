# Задание1:
# https://www.saucedemo.com/
#
# Написать 2 теста на логин страницу
# Написать 2 теста на страницу с рюкзаками
# (проверить, что рюкзаков больше чем 0)
# (проверить, что цена не пустая)

import pytest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from login_page import LoginPage
from product_page import ProductsPage

@pytest.fixture()
def setup_user_try_to_login():
    global login_page
    global product_page
    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_page = LoginPage(driver)
    product_page = ProductsPage(driver)

    yield
    print("Test is finished.")
    driver.quit()


@pytest.mark.usefixtures("setup_user_try_to_login")
class TestLoginPage:
    def test_login_with_invalid_credentials(self):
        login_page.login("invalid_username", "invalid_password")

        assert login_page.is_opened(), "error message"

    def test_login_with_valid_credentials(self):
        login_page.login("standard_user", "secret_sauce")

        assert product_page.is_opened(), "User is redirected to the product page"



