from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
import pytest
from login_page import LoginPage
from product_page import ProductsPage

@pytest.fixture(autouse=True)
def navigate_to_login_page(chromedriver):
    login_page = LoginPage(chromedriver)
    login_page.navigate()
    return

@pytest.fixture()
def chromedriver():
    return webdriver.Chrome(ChromeDriverManager().install())

@pytest.fixture()
def login_page(chromedriver):
    return LoginPage(chromedriver)

@pytest.fixture()
def product_page(chromedriver):
    return ProductsPage(chromedriver)
