"""
Задание1:
https://www.saucedemo.com/
Написать 2 теста на страницу с рюкзаками
(проверить, что рюкзаков больше чем 0)
(проверить, что цена не пустая)
Задание 2**:
Проверить что цена валидная, а не пустая
$29.99 - Правильная
29.99 - не правильная
$0.00 неправильная
-$29.99
...
все кроме $n.nn неверно
"""

import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from lesson_16_20_12_2021.class_materials.testing.pages.login_page import LoginPage
from product_page import ProductsPage


@pytest.fixture
def setup_data_for_product_page():
    global login_page
    global product_page

    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_page = LoginPage(driver)
    login_page.navigate()
    login_page.login('standard_user', 'secret_sauce')

    product_page = ProductsPage(driver)
    product_page.is_opened()

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_data_for_product_page")
class TestProductPage:
    def test_product_presence(self):
        presented_product = product_page.is_product_presented("Backpack")
        assert presented_product, "No such products on the page"

    def test_prices_presented(self):
        product_page.is_price_presented()
        assert product_page.is_price_presented(), "Some products has empty price"

    def test_price_format_valid(self):
        product_page.is_price_format_valid()
        assert product_page.is_price_format_valid(), "The price format is invalid for some products"
