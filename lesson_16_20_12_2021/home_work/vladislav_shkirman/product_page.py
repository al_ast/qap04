from base_page import BasePage
import re


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    PRODUCT_NAME_LOCATOR = "//div[contains(text(), '{}')]"
    PRICE_LOCATOR = "//div[@class='inventory_item_price']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def is_product_presented(self, product_name):
        return self.is_element_present(self.PRODUCT_NAME_LOCATOR.format(product_name))

    def _get_price_element(self):
        return self.find_elements(self.PRICE_LOCATOR)

    def is_price_presented(self):
        self._get_price_element()
        price_list = []
        for price_element in self._get_price_element():
            if len(price_element.text) > 0:
                price_list.append(price_element.text)
            else:
                return False
        return price_list

    def is_price_format_valid(self):
        self._get_price_element()
        price_list = []
        for price_element in self._get_price_element():
            if re.fullmatch(r"[$][0-9]+\.[0-9]{2}", price_element.text) \
                    and price_element.text != '$0.00':
                price_list.append(price_element.text)
            else:
                return False
        return price_list
