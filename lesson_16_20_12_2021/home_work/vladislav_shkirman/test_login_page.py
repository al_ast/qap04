"""
Задание1:
https://www.saucedemo.com/
Написать 2 теста на логин страницу
"""

import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from lesson_16_20_12_2021.class_materials.testing.pages.login_page import LoginPage
from lesson_16_20_12_2021.class_materials.testing.pages.product_page import ProductsPage


@pytest.fixture
def setup_data_for_login():
    global login_page
    global product_page

    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_page = LoginPage(driver)
    login_page.navigate()
    product_page = ProductsPage(driver)

    yield
    print('Test finished')
    driver.quit()


@pytest.mark.usefixtures("setup_data_for_login")
class TestLoginPage:
    def test_login_with_invalid_credentials(self):
        login_page.login('invalid_user', 'invalid_password')
        assert login_page.is_opened(), 'User should not be loged in'

    def test_login_with_valid_credentials(self):
        login_page.login('standard_user', 'secret_sauce')
        assert product_page.is_opened(), 'User should be loged in'
