import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from login_page import LoginPage


@pytest.fixture()
def setup_login_page():
    global login_page
    global driver

    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    login_page.navigate()

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_login_page")
class TestLoginPage:
    def test_login_with_invalid_credentials(self):
        login_page.login("nonexistent_username", "nonexistent_password")

        assert login_page.is_opened(), "product page opened with invalid credentials"


    def test_login_with_valid_credentials(self):
        login_page.login("standard_user", "secret_sauce")

        assert not login_page.is_opened(), "product page not opened with valid credentials"
