from base_page import BasePage


class LoginPage(BasePage):
    URL = "https://www.saucedemo.com"

    USER_NAME_INPUT_LOCATOR = "//input[@id='user-name']"
    PASSWORD_INPUT_LOCATOR = "//input[@id='password']"
    LOGIN_BUTTON_LOCATOR = "//input[@type='submit']"
    LOGIN_PAGE_LOGO_LOCATOR = "//div[@class='login_logo']"

    def enter_username(self, username):
        self.enter_text(self.USER_NAME_INPUT_LOCATOR, username)

    def navigate(self):
        self.open_url(self.URL)

    def enter_password(self, password):
        self.enter_text(self.PASSWORD_INPUT_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def login(self, username, password):
        self.enter_username(username)
        self.enter_password(password)
        self.click_login_button()

    def is_opened(self):
        return self.is_element_present(self.LOGIN_PAGE_LOGO_LOCATOR)