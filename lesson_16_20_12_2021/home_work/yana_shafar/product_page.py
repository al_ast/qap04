from selenium.common.exceptions import TimeoutException

from lesson_16_20_12_2021.home_work.yana_shafar.base_page import BasePage


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    PRODUCT_LOCATOR = "//div[contains(text(), '{}')]"
    PRICE_OF_PRODUCT_LOCATOR = "//div[@class='inventory_item_price']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def check_number_of_products(self, product_name):
        try:
            number_of_products = self.find_elements_by_locator(self.PRODUCT_LOCATOR.format(product_name))
            return len(number_of_products)
        except TimeoutException:
            return 0

    def check_prices_of_products(self):
        try:
            prices_of_products_list = self.find_elements_by_locator(self.PRICE_OF_PRODUCT_LOCATOR)
            prices = []
            for element in prices_of_products_list:
                prices.append(element.text)
                return prices
        except TimeoutException:
            return 0


