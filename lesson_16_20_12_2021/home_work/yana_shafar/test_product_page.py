import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_16_20_12_2021.home_work.yana_shafar.login_page import LoginPage
from lesson_16_20_12_2021.home_work.yana_shafar.product_page import ProductsPage


@pytest.fixture
def setup_test_product_page():
    global login_page
    global products_page
    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    products_page = ProductsPage(driver)

    login_page.navigate()
    login_page.login("standard_user", "secret_sauce")

    yield
    driver.quit()


@pytest.mark.usefixtures("setup_test_product_page")
class TestProductPage:
    def test_number_of_products(self):
        assert products_page.check_number_of_products("Backpack") > 0, "There are no backpacks on the page"

    def test_prices_of_products(self):
        assert products_page.check_prices_of_products(), "The price of some product is empty"

