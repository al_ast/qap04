from selenium.webdriver.common.by import By

from lesson_16_20_12_2021.class_materials.testing.pages.base_page import BasePage


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    PRODUCTS_NAME_LOCATOR = '//div[contains(text(),"{}")]'

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def checking_the_number_product(self, name_product):
        locator = self.PRODUCTS_NAME_LOCATOR.format(name_product)
        assert self.is_element_present(locator), "The product is out of stock"

    def take_all_price(self):
        index = 0
        elements = self.driver.find_elements(By.XPATH, '//div[text()="$"]')
        for element in elements:
            elements[index] = element.text
            index += 1
        return elements

    def _check_price_for_valid_symbol(self, price):
        valid_symbol = '1234567890.$'
        for symbol in price:
            if not symbol in valid_symbol:
                raise Exception('Not valid symbol')

    def _сhecking_the_validity_of_the_price(self, price):
        try:
            if float(price) <= 0:
                raise Exception('Not valid price')
        except ValueError:
            print('Not valid data')

    def check_price(self):
        index = 0
        price_list = self.take_all_price()
        replace_price_list = self._replace_price()
        for price in price_list:
            self._check_price_for_valid_symbol(price)
            self._сhecking_the_validity_of_the_price(replace_price_list[index])
            index += 1


    def _replace_price(self):
        index = 0
        price_list = self.take_all_price()
        for price in price_list:
            price_list[index] = price.replace('$', '')
            index += 1
        return price_list