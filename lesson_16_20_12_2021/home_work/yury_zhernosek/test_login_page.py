'''
Задание 1:

https://www.saucedemo.com/
Написать 2 теста на логин страницу
'''

import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_16_20_12_2021.class_materials.testing.pages.login_page import LoginPage
from lesson_16_20_12_2021.class_materials.testing.pages.product_page import ProductsPage


@pytest.fixture()
def setup_base_actions():
    global login_page
    global products_page
    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    products_page = ProductsPage(driver)

    login_page.navigate()

    yield
    driver.quit()


@pytest.mark.usefixtures('setup_base_actions')
class TestLoginPage:

    def test_login_with_valid_data(self):
        login_page.login('standard_user', 'secret_sauce')
        assert products_page.is_opened(), "Error"

    def test_login_with_not_valid_data(self):
        login_page.login('12312', '3fgds')
        assert not products_page.is_opened(), "Error"
