'''
Задание 1:
https://www.saucedemo.com/
Написать 2 теста на страницу с рюкзаками
(првоерить, что рюкзаков больше чем 0)
(проверить, что цена не пустая)


Задание 2**:
Проверить что цена валидная, а не пустая

$29.99 - Правильная

29.99 - неправильная
$0.00 неправильная
-$29.99
все кроме $n.nn неверно
'''
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from lesson_16_20_12_2021.class_materials.testing.pages.login_page import LoginPage
from product_page import ProductsPage

@pytest.fixture()
def setup_base_actions():
    global login_page
    global products_page
    driver = webdriver.Chrome(ChromeDriverManager().install())

    login_page = LoginPage(driver)
    products_page = ProductsPage(driver)

    login_page.navigate()

    yield
    driver.quit()


@pytest.mark.usefixtures('setup_base_actions')
class TestProductPage:

    def test_checking_the_number_of_backpacks(self):
        login_page.login('standard_user', 'secret_sauce')
        products_page.checking_the_number_product('Backpack')

    def test_checking_valid_price(self):
        login_page.login('standard_user', 'secret_sauce')
        products_page.check_price()