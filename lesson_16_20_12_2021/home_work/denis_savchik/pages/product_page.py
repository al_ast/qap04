from pages.base_page import BasePage


class ProductsPage(BasePage):
    PRODUCTS_LOGO_LOCATOR = "//span[text()='Products']"
    ITEMS_LOCATOR = "//*[@class='inventory_item_description']"

    def is_opened(self):
        return self.is_element_present(self.PRODUCTS_LOGO_LOCATOR)

    def count_elements(self, item_name):

        try:
            count_products = self.get_text(self.ITEMS_LOCATOR)
            return len(count_products)

        except TimeoutError:
            return 'No items detected'
