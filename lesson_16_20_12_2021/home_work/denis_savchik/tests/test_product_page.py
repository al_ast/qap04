import pytest


@pytest.mark.usefixtures("open_product_page")
class TestProductPage:

    def test_count_of_backpack(self, product_page):
        assert product_page.count_elements("Items") > 0, "No items on page"
