class TestLoginPage:

    def test_login_with_invalid_credentials(self, login_page):
        login_page.login("invalid_user", "invalid_password")

        assert login_page.is_opened(), "User with invalid credentials must be no logged"

    def test_login_with_valid_credentials(self, login_page, product_page):
        login_page.login("standard_user", "secret_sauce")

        assert product_page.is_opened(), "User with valid credentials must be logged"
