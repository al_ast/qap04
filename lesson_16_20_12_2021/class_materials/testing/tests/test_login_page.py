import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


from lesson_16_20_12_2021.class_materials.testing.pages.login_page import LoginPage
from lesson_16_20_12_2021.class_materials.testing.pages.product_page import ProductsPage


# @pytest.fixture
# def setup_test_login():
#     global login_page
#     global products_page
#     driver = webdriver.Chrome(ChromeDriverManager().install())
#
#     login_page = LoginPage(driver)
#     products_page = ProductsPage(driver)
#
#     login_page.navigate()
#
#     yield
#     driver.quit()


@pytest.mark.usefixtures("setup_test_login")
class TestLoginPage:
    def test_login_with_not_valid_credentials(self):
        login_page.login("not_valid", "not_valid")

        if login_page.is_opened():
            pass
        else:
            raise Exception("User should not be logged in with not valid credentials")

        assert login_page.is_opened(), "User should not be logged in with not valid credentials"

    def test_login_with_valid_credentials(self):
        login_page.login("standard_user", "secret_sauce")

        assert products_page.is_opened(), "User should be redirected to the product page after login with valid creds"
